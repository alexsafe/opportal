package com.workers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.workers.activities.MainActivity;
import com.workers.util.Log;

public class ResidentNotes extends Activity
{
	Button button;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resident_notes);
		// new HideSysUi(getWindow());
		Log.d("Enter ResidentNotes");
		button = (Button) findViewById(R.id.btnResident);
		button.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Toast.makeText(getApplicationContext(), "Sent", Toast.LENGTH_LONG).show();
				moveTaskToBack(true);
				Log.d("cod dupa move back");
				Intent i = new Intent(getApplicationContext(), MainActivity.class);
				startActivity(i);
				finish();
			}
		});
	}
}
