package com.workers.fcm;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.workers.util.Log;
import com.workers.util.ServerUtilities;


public class MyInstanceIDListenerService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {

        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.d("Firbase id login Refreshed token: " + refreshedToken);


            //To displaying token on logcat
            Log.d("TOKEN: " + refreshedToken);

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            preferences.edit().putString("FIREBASE_TOKEN", refreshedToken).apply();


            ServerUtilities.registerDevice(getApplicationContext(), refreshedToken);
        } catch (Exception e) {
            Log.e("onTokenRefresh exception:" + e);
            e.printStackTrace();
        }

    }

    class RegisterTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            // Register on our server // On server creates a new
            // user
            Log.d("registerind device");
            ServerUtilities.registerDevice(getApplicationContext(), params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }

    }

    ;
}