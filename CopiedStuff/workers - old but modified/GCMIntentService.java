package com.workers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.workers.util.ServerUtilities;

import static com.workers.util.CommonUtilities.SENDER_ID;
import static com.workers.util.CommonUtilities.TAG;
import static com.workers.util.CommonUtilities.displayMessage;


public class GCMIntentService {//extends GCMBaseIntentService {
/*
//    public GCMIntentService() {
//        super(SENDER_ID);
//    }


    private static void generateNotification(Context context, String message) {
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);

        String title = context.getString(R.string.app_name);

        Intent notificationIntent = new Intent(context, MainActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
//        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

        // notification.sound = Uri.parse("android.resource://" +
        // context.getPackageName() + "your_sound_file_name.mp3");

        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);

    }

    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, " GCMIntentService Device registered: regId = " + registrationId);
        displayMessage(context, "Your device registred with GCM");
        Log.d("NAME", MainActivity.name);
        ServerUtilities.register(context, MainActivity.name, MainActivity.email, registrationId);
    }

    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "GCMIntentService Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        ServerUtilities.unregister(context, registrationId);
    }


    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, " GCMIntentService Received message");
        Log.i(TAG, "intent" + intent);
        Log.i(TAG, "intent extras" + intent.getExtras());
        String message = intent.getExtras().getString("price");
        Log.i(TAG, "message:" + message);
        if (message.equals("internet")) {
            Log.i(TAG, "in if staring intent");
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
            browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(browserIntent);
        }
        if (message.equals("tasks")) {
            Log.i(TAG, "in if staring intent");
            Intent tasksIntent = new Intent(this, TaskList.class);
            tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(tasksIntent);
        }
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }

    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "GCMIntentService Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }


    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "GCMIntentService Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "GCMIntentService Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error, errorId));
        return super.onRecoverableError(context, errorId);
    }
*/
}
