package com.workers.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.R;
import com.workers.activities.TaskDetail;
import com.workers.types.Tasks;
import com.workers.util.Log;
import com.workers.util.Util;

public class TaskAdapter extends ArrayAdapter<Tasks>
{
	TaskAdapter dataAdapter = null;
	private final Context context;

	public ArrayList<Tasks> TasksList;

	public TaskAdapter(Context context, int textViewResourceId, ArrayList<Tasks> TasksList)
	{
		super(context, textViewResourceId, TasksList);
		this.context = context;
		this.TasksList = new ArrayList<Tasks>();
		this.TasksList.addAll(TasksList);
	}

	private class ViewHolder
	{
		TextView code;
		CheckBox name;
		TextView quantity;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{

		ViewHolder holder = null;
		Log.v("ConvertView" + String.valueOf(position));

		if (convertView == null)
		{
			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(R.layout.list_items, null);

			holder = new ViewHolder();
			holder.code = (TextView) convertView.findViewById(R.id.code);
			holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
			holder.quantity = (TextView) convertView.findViewById(R.id.quantity);

			convertView.setTag(holder);

			final Tasks Tasks = TasksList.get(position);
			holder.code.setText(" " + Tasks.getName() + "");
			// holder.name.setText(Tasks.getName());
			if (!(Tasks.getQuantity().equals("0")))
				holder.quantity.setText(Tasks.getQuantity());
			holder.name.setChecked(Util.stringToBool(Tasks.getSelected()));
			holder.name.setTag(Tasks);
			Log.d("holder.quantity:" + holder.quantity);
			Log.d("holder.quantity getText:" + holder.quantity.getText());
			String q = holder.quantity.getText().toString();

			if (q.equals(""))
			{
				Log.d("holder.quantity: nimic" + holder.code.getText());
				// holder.quantity.setVisibility(View.GONE);
			}
			else
			{
				Log.d("holder.quantity: ceva |" + q + "|");
			}

			holder.code.setOnClickListener(new View.OnClickListener()
			{
				public void onClick(View v)
				{
					TextView tv = (TextView) v;
					// ArrayList<Tasks> pointsExtra = new ArrayList<Tasks>();

					Tasks tasks = (Tasks) tv.getTag();
					// Log.d("tasks:"+tasks);
					// Log.d("this:"+this.toString());
					// Log.d("Tasks:"+Tasks.getName());
					// Toast.makeText(context.getApplicationContext(),
					// "Clicked on text: " + tv.getText(),
					// Toast.LENGTH_LONG).show();
					// Tasks.setSelected(cb.isChecked());
					Intent i = new Intent(context, TaskDetail.class);
					i.putExtra("Task", Tasks);
					i.putExtra("position", "" + position);
					i.putExtra("poz", "0");
					context.startActivity(i);
				}
			});
			holder.name.setOnClickListener(new View.OnClickListener()
			{
				public void onClick(View v)
				{

					CheckBox cb = (CheckBox) v;
					Tasks Tasks = (Tasks) cb.getTag();
					// Toast.makeText(context.getApplicationContext(),
					// "Clicked on ckbox: " + cb.getText() + " is " +
					// cb.isChecked(), Toast.LENGTH_LONG).show();
					Tasks.setSelected(Util.boolToString(cb.isChecked()));
					Log.d("Tasks.getName():" + Tasks.getName());
					RequestParams params = new RequestParams();
					String poz = Integer.toString(position);
					Log.d("poz:" + poz);
					params.put("checked", "one");
					AsyncHttpClient client = new AsyncHttpClient();
					client.post("http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/saveTaskDemo.php", params, new AsyncHttpResponseHandler()
					{
						@Override
						public void onSuccess(String response)
						{
							Log.d("success  !!!!");
						}
					});

					if (!(Tasks.getQuantity().equals("")))
					{
						Intent i = new Intent(context, TaskDetail.class);
						i.putExtra("Task", Tasks);
						i.putExtra("position", "" + position);
						i.putExtra("poz", "0");
						context.startActivity(i);
					}
				}
			});
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		return convertView;

	}

}
