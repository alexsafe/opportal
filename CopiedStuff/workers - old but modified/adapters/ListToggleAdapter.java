package com.workers.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.activities.CameraActivity;
import com.workers.R;
import com.workers.types.Concern;
import com.workers.util.Log;

public class ListToggleAdapter extends ArrayAdapter<Concern>
{
	ListToggleAdapter dataAdapter = null;
	private final Context context;

	public ArrayList<Concern> TasksList;

	public ListToggleAdapter(Context context, int textViewResourceId, ArrayList<Concern> TasksList)
	{
		super(context, textViewResourceId, TasksList);
		this.context = context;
		this.TasksList = new ArrayList<Concern>();
		this.TasksList.addAll(TasksList);
	}

	private class ViewHolder
	{
		TextView item;
		TextView marked;
		Button ok;
		Button notOk;
		Button notA;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{

		ViewHolder holder = null;

		if (convertView == null)
		{

			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(R.layout.list_hs, null);
			holder = new ViewHolder();

			holder.item = (TextView) convertView.findViewById(R.id.itemQ);
			// holder.marked = (TextView)
			// convertView.findViewById(R.id.itemMarked);
			holder.ok = (Button) convertView.findViewById(R.id.buttonYes);
			holder.notOk = (Button) convertView.findViewById(R.id.buttonNo);
			holder.notA = (Button) convertView.findViewById(R.id.buttonNA);
			String className = getContext().getClass().getSimpleName();
			if (className.equals("QualityWb"))
				holder.notA.setText("Non existent");
			convertView.setTag(holder);
			holder.ok.setTag(holder);
			final RelativeLayout listConcerns = (RelativeLayout) convertView.findViewById(R.id.listConcerns);

			Log.d("TasksList:" + TasksList);
			for (Concern taskItem : TasksList)
			{
				Log.d("item:" + taskItem.getName() + ": " + taskItem.getMarked());
			}
			Concern Tasks = TasksList.get(position);
			holder.item.setText(" " + Tasks.getName() + " ");
			if (Tasks.getMarked().toLowerCase().equals("OK".toLowerCase()))
			{
				listConcerns.setBackgroundColor(Color.GREEN);
				listConcerns.getBackground().setAlpha(91);
			}
			if (Tasks.getMarked().toLowerCase().equals("not ok".toLowerCase()))
			{
				listConcerns.setBackgroundColor(Color.RED);
				listConcerns.getBackground().setAlpha(151);
			}

			holder.ok.setOnClickListener(new View.OnClickListener()
			{
				public void onClick(View v)
				{
					Log.d("ok");
					ViewHolder holder1 = (ViewHolder) v.getTag();
					Log.d("holder1:" + holder1);
					Log.d("context:" + context.getClass().getName());
					// holder1.marked.setText("Marked OK");
					// holder1.marked.setBackgroundColor(Color.GREEN);
					// holder1.item.setBackgroundColor(Color.GREEN);
					// holder1.item.getBackground().setAlpha(91);
					Log.d("position licked:" + position);
					listConcerns.setBackgroundColor(Color.GREEN);
					listConcerns.getBackground().setAlpha(91);
					Concern Tasks = TasksList.get(position);
					Tasks.setMarked("Marked OK");
					Log.d("get:" + Tasks.getMarked());
					sendPost(position, "ok");
					// Intent intent = new Intent(getContext(),
					// CameraActivity.class);
					// getContext().startActivity(intent);
				}
			});
			holder.notOk.setTag(holder);
			holder.notOk.setOnClickListener(new View.OnClickListener()
			{
				public void onClick(View v)
				{
					Log.d("not ok");
					ViewHolder holder1 = (ViewHolder) v.getTag();
					Log.d("holder1:" + holder1);
					// holder1.marked.setText("Marked Not OK");
					// holder1.marked.setBackgroundColor(Color.RED);
					listConcerns.setBackgroundColor(Color.RED);
					listConcerns.getBackground().setAlpha(151);
					Concern Tasks = TasksList.get(position);
					Tasks.setMarked("Marked Not OK");
					String className = getContext().getClass().getSimpleName();
					String itemName = holder1.item.getText().toString();
					sendPost(position, "not ok");
					Intent intent = new Intent(getContext(), CameraActivity.class);
					intent.putExtra("photoPath", className + "_" + itemName);
					getContext().startActivity(intent);
				}
			});
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		// holder.name.setText(Tasks.getName());
		// holder.name.setChecked(Tasks.isSelected());
		// holder.name.setTag(Tasks);

		return convertView;

	}

	public void sendPost(int position, String type)
	{
		Log.d("in send post ba");
		Log.d("position:" + position);
		Log.d("type:" + type);
		String className1 = getContext().getClass().getSimpleName();
		if (className1.equals("HS"))
		{
			RequestParams params = new RequestParams();
			String poz = Integer.toString(position);
			Log.d("poz:" + poz);
			params.put("index", poz);
			params.put("type", type);
			AsyncHttpClient client = new AsyncHttpClient();
			client.post("http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/saveHsXml.php", params, new AsyncHttpResponseHandler()
			{
				@Override
				public void onSuccess(String response)
				{
					Log.d("success  !!!!");
				}
			});
		}

	}

}
