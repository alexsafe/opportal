package com.workers.adapters;

import java.util.ArrayList;

import com.workers.R;
import com.workers.types.Concern;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class TaskListAdapter extends ArrayAdapter<Concern> {
	TaskListAdapter dataAdapter = null;
	private final Context context;

	public ArrayList<Concern> TasksList;

	public TaskListAdapter(Context context, int textViewResourceId,
			ArrayList<Concern> TasksList) {
		super(context, textViewResourceId, TasksList);
		this.context = context;
		this.TasksList = new ArrayList<Concern>();
		this.TasksList.addAll(TasksList);
	}

	private class ViewHolder {
		TextView code;
		CheckBox name;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder = null;
		Log.v("ConvertView", String.valueOf(position));

		if (convertView == null) {
			LayoutInflater vi = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = vi.inflate(R.layout.list_items, null);

			holder = new ViewHolder();
			holder.code = (TextView) convertView.findViewById(R.id.code);
			holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
			convertView.setTag(holder);

			holder.name.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					CheckBox cb = (CheckBox) v;
					Concern Tasks = (Concern) cb.getTag();
					Toast.makeText(
							context.getApplicationContext(),
							"Clicked on Checkbox: " + cb.getText() + " is "
									+ cb.isChecked(), Toast.LENGTH_LONG).show();
					Tasks.setSelected(cb.isChecked());
				}
			});
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Concern Tasks = TasksList.get(position);
		holder.code.setText(" (" + Tasks.getCode() + ")");
		holder.name.setText(Tasks.getName());
		holder.name.setChecked(Tasks.isSelected());
		holder.name.setTag(Tasks);

		return convertView;

	}

}
