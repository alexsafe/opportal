/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.workers.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

import com.workers.types.Concern;
import com.workers.types.Tasks;

/**
 * This class parses XML feeds from stackoverflow.com. Given an InputStream
 * representation of a feed, it returns a List of entries, where each list
 * element represents a single entry (post) in the XML feed.
 */
public class XmlParser
{
	private static final String ns = null;

	// We don't use namespaces

	public List<Concern> parse(InputStream in, String parseObject) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			if (parseObject.equals("hs"))
				return readHs(parser);
			else
				return readProperties(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}

	}

	public List<Tasks> parseTasks(InputStream in, String parseObject) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readTasks(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}

	}

	private List<Concern> readHs(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<Concern> ConcernList = new ArrayList<Concern>();
		int eventType = parser.getEventType();
		Log.d("in readHs:" + parser.toString());
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<Concern>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);
				if (name == "rows")
				{
					// currentConcern = new Concern();
				}
				// else if (currentConcern != null)
				{

					if (name.equals("row"))
					{
						Log.d("next");
						parser.nextTag();
						String idConcern = parser.getAttributeValue(null, "id");
						String nameConcern = parser.getAttributeValue(null, "name");
						String markedConcern = parser.getAttributeValue(null, "marked");
						Log.d("id:" + idConcern);
						Log.d("name:" + nameConcern);
						Log.d("marked:" + markedConcern);
						// currentConcern.setId(idConcern);
						// currentConcern.setName(nameConcern);
						Concern currentConcern = new Concern(idConcern, nameConcern, markedConcern);
						ConcernList.add(currentConcern);
					}
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return ConcernList;
	}

	private List<Tasks> readTasks(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<Tasks> ConcernList = new ArrayList<Tasks>();
		int eventType = parser.getEventType();
		Log.d("in readHs:" + parser.toString());
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<Tasks>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);
				if (name == "rows")
				{
					// currentConcern = new Concern();
				}
				// else if (currentConcern != null)
				{

					if (name.equals("row"))
					{
						Log.d("next");
						parser.nextTag();
						String idConcern = parser.getAttributeValue(null, "id");
						String nameConcern = parser.getAttributeValue(null, "name");
						String quantityConcern = parser.getAttributeValue(null, "quantity");
						String selectedConcern = parser.getAttributeValue(null, "checked");
						Log.d("id:" + idConcern);
						Log.d("name:" + nameConcern);
						Log.d("marked:" + quantityConcern);
						// currentConcern.setId(idConcern);
						// currentConcern.setName(nameConcern);
						Tasks currentConcern = new Tasks(idConcern, nameConcern, selectedConcern, quantityConcern );
						ConcernList.add(currentConcern);
					}
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return ConcernList;
	}

	private List<Concern> readProperties(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<Concern> ConcernList = new ArrayList<Concern>();
		int eventType = parser.getEventType();
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<Concern>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);
				if (name == "rows")
				{
					// currentConcern = new Concern();
				}
				// else if (currentConcern != null)
				{

					if (name.equals("row"))
					{
						Log.d("next");
						parser.nextTag();
						String idConcern = parser.getAttributeValue(null, "id");
						String nameConcern = parser.getAttributeValue(null, "name");
						String markedConcern = parser.getAttributeValue(null, "marked");
						Log.d("id:" + idConcern);
						;
						Log.d("name:" + nameConcern);
						// currentConcern.setId(idConcern);
						// currentConcern.setName(nameConcern);
						Concern currentConcern = new Concern(idConcern, nameConcern, markedConcern);
						ConcernList.add(currentConcern);
					}
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return ConcernList;
	}
}
