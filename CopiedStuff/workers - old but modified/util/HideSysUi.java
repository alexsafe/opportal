package com.workers.util;

import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.View.OnSystemUiVisibilityChangeListener;

public class HideSysUi
{
	public Window window;
	final Handler mHideHandler = new Handler();
	
	public HideSysUi(final Window window)
	{
		hideSystemUi(window);
		final Runnable mHideRunnable = new Runnable()
		{
			@Override
			public void run()
			{
				hideSystemUi(window);
			}
		};	
		window.getDecorView().setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener()
		{
			@Override
			public void onSystemUiVisibilityChange(int visibility)
			{
				if (visibility == 0)
				{
					mHideHandler.postDelayed(mHideRunnable, 2000);
				}
			}
		});
	}
	public void hideSystemUi(Window window)
	{
		window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
	}
}
