package com.workers.util;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.os.AsyncTask;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.adapters.ListToggleAdapter;
import com.workers.types.Concern;
import com.workers.types.Tasks;

public class Util
{
	public static Boolean stringToBool(String string)
	{
		if (string.equals("true"))
			return true;
		return false;
	}

	public static String boolToString(Boolean boolVar)
	{
		if (boolVar)
			return "true";
		return "false";
	}

	public static List<Concern> loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException
	{
		Log.d("inloadXmlFromNetwork");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		List<Concern> entries = null;
		String title = null;
		String url = null;
		String summary = null;
		Calendar rightNow = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

		StringBuilder htmlString = new StringBuilder();
		try
		{
			stream = downloadUrl(urlString);
			entries = xmlParser.parse(stream, "hs");
			Log.d("entries:" + entries);

		}
		finally
		{
			if (stream != null)
			{
				Log.d(":stream: close" + stream);
				stream.close();
				Log.d(":stream: closed" + stream);
			}
		}
		Log.d("in return gen");
		for (Concern entry : entries)
		{
			Log.d("id:" + entry.getCode());
			Log.d("name:" + entry.getName());
			Log.d("marked:" + entry.getMarked());
			Log.d("chestie:" + entry);
		}
		return entries;
	}

	private List<Tasks> loadXmlFromNetworkTasks(String urlString) throws XmlPullParserException, IOException
	{
		Log.d("inloadXmlFromNetwork");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		List<Tasks> entries = null;
		String title = null;
		String url = null;
		String summary = null;
		Calendar rightNow = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

		StringBuilder htmlString = new StringBuilder();
		try
		{
			stream = downloadUrl(urlString);
			entries = xmlParser.parseTasks(stream, "tasks");
			Log.d("entries:" + entries);

		}
		finally
		{
			if (stream != null)
			{
				Log.d(":stream: close" + stream);
				stream.close();
				Log.d(":stream: closed" + stream);
			}
		}
		Log.d("in return gen");
		for (Tasks entry : entries)
		{
			Log.d("id:" + entry.getCode());
			Log.d("name:" + entry.getName());
			Log.d("sel:" + entry.getSelected());
			Log.d("quantity:" + entry.getQuantity());
			Log.d("chestie:" + entry);
		}
		return entries;
	}

	// Given a string representation of a URL, sets up a connection and gets
	// an input stream.
	private static InputStream downloadUrl(String urlString) throws IOException
	{
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(10000 /* milliseconds */);
		conn.setConnectTimeout(15000 /* milliseconds */);
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.addRequestProperty("Cache-Control", "no-cache");

		// Starts the query
		conn.connect();
		InputStream stream = conn.getInputStream();
		return stream;
	}

	public class DownloadXmlTask extends AsyncTask<String, Void, List<Concern>>
	{
		
		@Override
		protected void onPostExecute(List<Concern> result)
		{
//			setContentView(R.layout.list_hs);
			// Displays the HTML string in the UI via a WebView
			ListToggleAdapter dataAdapter = null;
//			dataAdapter = new ListToggleAdapter(this, R.layout.list_hs, listItems);
//			setListAdapter(dataAdapter);
			//myWebView.loadData(result, "text/html", null);
		}
		
		@Override
		protected List<Concern> doInBackground(String... urls)
		{
			try
			{
				Log.d(" in doInBackground");
				return loadXmlFromNetwork(urls[0]);
			}
			catch (IOException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			catch (XmlPullParserException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			return null;
		}
	}

	public class DownloadXmlTasks extends AsyncTask<String, Void, List<Tasks>>
	{

		@Override
		protected List<Tasks> doInBackground(String... urls)
		{
			try
			{
				Log.d(" in doInBackground");
				return loadXmlFromNetworkTasks(urls[0]);
			}
			catch (IOException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			catch (XmlPullParserException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			return null;
		}
	}

	public static void initHsXml()
	{
		Log.d("in init xml");
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		client.post("http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/initHsXml.php", params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("success  !!!!");

			}
		});
		try
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
			Log.d("InterruptedException:" + e);
		}
		Log.d("do rest  !!!!");
	}

	public static void initTasksXml()
	{
		Log.d("in init tasks xml");
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		client.post("http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/initTasksXml.php", params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("success  !!!!");

			}
		});
		try
		{
			Thread.sleep(500);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
			Log.d("InterruptedException:" + e);
		}
		Log.d("do rest  !!!!");
	}

}
