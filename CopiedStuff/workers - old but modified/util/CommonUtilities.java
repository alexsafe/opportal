package com.workers.util;

import android.content.Context;
import android.content.Intent;

public final class CommonUtilities
{

	// give your server registration url here
	//static final String SERVER_URL = "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/register.php"; 
	// give your server registration url here
	static final String SERVER_URL = "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/register_regId.php"; 
	public static final String REGISTER_URL = "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/register_regId.php";
	public static final String SERVER_XML = "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/xml_string.xml";
	public static final String hs_xml = "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/xmls/hs2.xml";
	public static final String tasks_xml = "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/xmls/tasks.xml";

	// Google project id
//	public static final String SENDER_ID = "961999046108"; //apiphpserver project id
	public static final String SENDER_ID = "1060667327177"; //tasks proj id

	public static final String TAG = "Workers";
	public static final String DISPLAY_MESSAGE_ACTION = "com.workers.DISPLAY_MESSAGE";
	public static final String EXTRA_MESSAGE = "message";
	public static final String PICTURES_DIR = "/Pictures/CameraSpike/";

	public static void displayMessage(Context context, String message)
	{
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}
}
