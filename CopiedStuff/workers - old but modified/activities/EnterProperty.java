package com.workers.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.workers.R;
import com.workers.util.HideSysUi;
import com.workers.util.Log;

public class EnterProperty extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enter_property);
		final HideSysUi hideUi = new HideSysUi(getWindow());
		sendCheckIn();
	}
	private void sendCheckIn()
	{

		Button yes = (Button) findViewById(R.id.buttonYes);
		Button no = (Button) findViewById(R.id.buttonNO);
		yes.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.d("click yes");
//				Toast.makeText(getApplicationContext(), "yes", Toast.LENGTH_LONG).show();
				Intent i = new Intent(getApplicationContext(), HS.class);
				startActivity(i);
			}
		});
		no.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.d("click no");
//				Toast.makeText(getApplicationContext(), "no", Toast.LENGTH_LONG).show();
			}
		});

	}
}
