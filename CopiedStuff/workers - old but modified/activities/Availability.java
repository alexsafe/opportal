package com.workers.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.R;
import com.workers.util.HideSysUi;
import com.workers.util.Log;

public class Availability<postData> extends Activity
{

//	private RadioGroup radioAvailabilityGroup;
//	private RadioButton radioAvailabilityButton;
	private Button btnDisplay, yes, no;
	private EditText editOther;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.availability);
		final HideSysUi hideUi = new HideSysUi(getWindow());
//		radioAvailabilityGroup = (RadioGroup) findViewById(R.id.radioAvailability);
		btnDisplay = (Button) findViewById(R.id.btnDisplay);
		yes = (Button) findViewById(R.id.radioAvailable);
		no = (Button) findViewById(R.id.radioNotAvailable);

		btnDisplay.setVisibility(View.INVISIBLE);
		editOther = (EditText) findViewById(R.id.otherText);
		editOther.setVisibility(View.INVISIBLE);
		editOther.setRawInputType(InputType.TYPE_NULL);
		editOther.setFocusable(true);
		yes.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Intent i = new Intent(getApplicationContext(), EnterProperty.class);
				startActivity(i);
			}
		});
		no.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				btnDisplay.setVisibility(View.VISIBLE);
				enableSoftInputFromAppearing(editOther);
//				editOther.setVisibility(View.VISIBLE);
			}
		});

		// radioAvailabilityGroup.setOnCheckedChangeListener(new
		// OnCheckedChangeListener()
		// {
		// public void onCheckedChanged(RadioGroup group, int checkedId)
		// {
		// Log.d("checkedId:" + checkedId);
		// switch (checkedId)
		// {
		// case R.id.radioAvailable:
		// disableSoftInputFromAppearing(editOther);
		// break;
		// case R.id.radioNotAvailable:
		// enableSoftInputFromAppearing(editOther);
		// break;
		// }
		// }
		// });
		btnDisplay.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{

				// get selected radio button from radioGroup
				// int selectedId =
				// radioAvailabilityGroup.getCheckedRadioButtonId();

				// find the radiobutton by returned id
				// radioAvailabilityButton = (RadioButton)
				// findViewById(selectedId);

				// Toast.makeText(Availability.this,
				// radioAvailabilityButton.getText(),
				// Toast.LENGTH_SHORT).show();

				// if (radioAvailabilityButton.getText().equals("Available"))
				// {
				// Intent i = new Intent(getApplicationContext(),
				// EnterProperty.class);
				// startActivity(i);
				// }
				// else
				// {
				Log.d("send email");
				Log.d("editOther:" + editOther.getText());
				// Toast.makeText(Availability.this, "sending message...",
				// Toast.LENGTH_SHORT).show();
				send_message(editOther.getText().toString());
				Intent i = new Intent(getApplicationContext(), EnterProperty.class);
				startActivity(i);
				// sendEmail();
				// }
			}

		});

	}

	public void send_message(String messageBody)
	{
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("myHttpData", "MyPictureName");
		params.put("email", "true");
		params.put("title", "Availability");
		params.put("message", messageBody);

		client.post("http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/send_mail.php", params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("success!!!!");
				Toast.makeText(Availability.this, "Mesage sent!", Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void sendEmail()
	{
		// String to = textTo.getText().toString();
		// String subject = textSubject.getText().toString();
		// String message = textMessage.getText().toString();
		String to = "a_capitaneanu@hotmail.com";
		String subject = "muie";
		String message = "vezi mai sus";

		// String mail_server =
		// "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/upload_example.php";
		// Ur url;
		// url = new URL(mail_server);
		// String boundary = "*****";
		// HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		// conn.setDoInput(true); // Allow Inputs
		// conn.setDoOutput(true); // Allow Outputs
		// conn.setUseCaches(false); // Don't use a Cached Copy
		// conn.setRequestMethod("POST");
		// conn.setRequestProperty("Connection", "Keep-Alive");
		// conn.setRequestProperty("ENCTYPE", "multipart/form-data");
		// conn.setRequestProperty("Content-Type",
		// "multipart/form-data;boundary=" + boundary);

		Intent email = new Intent(Intent.ACTION_SEND);
		email.putExtra(Intent.EXTRA_EMAIL, new String[]
		{
			to
		});
		// email.putExtra(Intent.EXTRA_CC, new String[]{ to});
		// email.putExtra(Intent.EXTRA_BCC, new String[]{to});
		email.putExtra(Intent.EXTRA_SUBJECT, subject);
		email.putExtra(Intent.EXTRA_TEXT, message);

		// need this to prompts email client only
		email.setType("message/rfc822");

		startActivity(Intent.createChooser(email, "Choose an Email client :"));

	}

	public static void disableSoftInputFromAppearing(EditText editText)
	{
		Log.d("other disableSoftInputFromAppearing");
		editText.setEnabled(false);
		editText.setRawInputType(InputType.TYPE_NULL);
		editText.setFocusable(true);
		editText.setVisibility(View.INVISIBLE);
	}

	public static void enableSoftInputFromAppearing(EditText editText)
	{
		Log.d("other enableSoftInputFromAppearing");
		editText.setVisibility(View.VISIBLE);
		editText.setEnabled(true);
		editText.setInputType(InputType.TYPE_CLASS_TEXT);
		editText.setFocusable(true);

	}

	// @Override
	// public void onBackPressed()
	// {
	// // moveTaskToBack(true);
	//
	//
	// }
}
