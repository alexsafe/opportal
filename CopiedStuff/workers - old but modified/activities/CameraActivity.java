package com.workers.activities;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.os.Environment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.workers.GalleryView;
import com.workers.GalleryView.PicAdapter;
import com.workers.R;
import com.workers.util.Log;
import com.workers.widget.CameraPreview;
import static com.workers.util.CommonUtilities.PICTURES_DIR;

public class CameraActivity extends Activity implements SurfaceHolder.Callback, PictureCallback
{
	private Preview mPreview;
	protected static final String EXTRA_IMAGE_PATH = "com.workers.activities.CameraActivity.EXTRA_IMAGE_PATH";
	private static final String SDCARD = Environment.getExternalStorageDirectory().getPath();
	private static final String DIRECTORY = SDCARD + PICTURES_DIR;
	Camera mCamera;
	private CameraPreview cameraPreview;
	private static final int REQ_CAMERA_IMAGE = 123;
	int numberOfCameras;
	final private int CAPTURE_IMAGE = 2;
	int cameraCurrentlyLocked;
	private String lastImage = "";
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	boolean previewing = false;
	LayoutInflater controlInflater = null;
	int defaultCameraId;
	private PicAdapter imgAdapt;
	String upLoadServerUri = null;
	private static final Random random = new Random();

	/********** File Path *************/
	final String uploadFilePath = Environment.getExternalStorageDirectory().getPath() + "/Pictures/Spike/";
	final String uploadFileName = "IMG_20140205_180913.jpg";
	int serverResponseCode = 0;

	// public static void postImage(String path)
	// {
	// // InputStream myInputStream = "blah";
	// RequestParams params = new RequestParams();
	// // params.put("secret_passwords", myInputStream, "passwords.txt");
	// File myFile = new File(path);
	// try
	// {
	// params.put("last_photo_taken", myFile);
	// params.put("picture[name]", "MyPictureName");
	// params.put("picture[image]", myFile);
	// params.put("myHttpData", "myFile");
	// }
	// catch (FileNotFoundException e)
	// {
	// Log.d("exceptie ba la fisier:"+e);
	// }
	// AsyncHttpClient client = new AsyncHttpClient();
	// client.post("http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/upload.php",
	// params, new AsyncHttpResponseHandler()
	// {
	// @Override
	// public void onSuccess(String response)
	// {
	// Log.d("success!!!!:" + response);
	// }
	// });
	// }

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		// getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		Intent intent = getIntent();
		String photoId = intent.getStringExtra("photoPath");
		Log.d("photoId:"+photoId);
//		Toast.makeText(getApplicationContext(), "photoId:"+photoId, Toast.LENGTH_LONG).show();
		mPreview = new Preview(this);
		setContentView(mPreview);
		// setContentView(R.layout.main);
		surfaceView = (SurfaceView) findViewById(R.id.camerapreview);
		if (surfaceView != null)
		{
			surfaceHolder = surfaceView.getHolder();
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		controlInflater = LayoutInflater.from(getBaseContext());
		View viewControl = controlInflater.inflate(R.layout.control, null);
		LayoutParams layoutParamsControl = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		this.addContentView(viewControl, layoutParamsControl);

		CameraInfo cameraInfo = new CameraInfo();
		for (int i = 0; i < numberOfCameras; i++)
		{
			Camera.getCameraInfo(i, cameraInfo);
			if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK)
			{
				defaultCameraId = i;
			}
		}
		GalleryView gv = new GalleryView();
		PicAdapter imgAdapt = gv.new PicAdapter(this);
		imgAdapt.getFilePaths();
		ArrayList<String> filePaths = imgAdapt.getFilePaths();
		if (filePaths.size() > 0)
		{
			String lastPath = filePaths.get(filePaths.size() - 1);
			displayImage(lastPath);
		}

		mPreview.removeView(mPreview.mSurfaceView);
		mPreview.addView(mPreview.mSurfaceView);
		mCamera = Camera.open();
		cameraCurrentlyLocked = defaultCameraId;
		mPreview.setCamera(mCamera);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.d("on activityresult!!!!!!!!!!!!!!!!!!");
		if (requestCode == REQ_CAMERA_IMAGE && resultCode == RESULT_OK)
		{
			Log.d("on activityresult in if");
			String imgPath = data.getStringExtra(CameraActivity.EXTRA_IMAGE_PATH);
			Log.i("Got image path: " + imgPath);
			displayImage(imgPath);
			// postImage(imgPath);
			// uploadImage(imgPath);
		}
		else if (requestCode == REQ_CAMERA_IMAGE && resultCode == RESULT_CANCELED)
		{
			Log.i("User didn't take an image");
		}
		else
		{
			Log.i("no req_camera");
		}
	}

	protected void clickResult(final String imgPath)
	{
		long backoff = 2000 + random.nextInt(1000);
		Log.d("on clickResult!!!!!!!!!!!!!!!!!!");
		// String imgPath =
		// data.getStringExtra(CameraActivity.EXTRA_IMAGE_PATH);
		Log.i("Got image path: " + imgPath);
		setLastImage(imgPath);
		displayImage(imgPath);

		upLoadServerUri = "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/upload_example.php";

		// for (int i = 0; i < 5; i++)
		// {
		// Log.d("Attempt #" + i + " to save");
		try
		{
			new Thread(new Runnable()
			{
				public void run()
				{
					runOnUiThread(new Runnable()
					{
						public void run()
						{
							Log.d("uploading started.....");
						}
					});

					uploadFile(imgPath);

				}
			}).start();
			return;
		}
		catch (Exception e)
		{
			Log.e("Failed to upload :" + e);
		}
	}

	public int uploadFile(String sourceFileUri)
	{

		String fileName = sourceFileUri;

		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		File sourceFile = new File(sourceFileUri);

		if (!sourceFile.isFile())
		{

			// dialog.dismiss();

			Log.d("Source File not exist :" + uploadFilePath + "" + uploadFileName);

			runOnUiThread(new Runnable()
			{
				public void run()
				{
					Log.d("Source File not exist :" + uploadFilePath + "" + uploadFileName);
				}
			});

			return 0;

		}
		else
		{
			try
			{
				FileInputStream fileInputStream = null;
				URL url;
				fileInputStream = new FileInputStream(sourceFile);
				url = new URL(upLoadServerUri);
				// if (serverResponseCode != 200)
				// {
				// open a URL connection to the Servlet

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0)
				{

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.d("HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200)
				{

					runOnUiThread(new Runnable()
					{
						public void run()
						{

							String msg = "File Upload Completed.";
							Log.d(msg);
						}
					});
				}

				fileInputStream.close();
				dos.flush();
				dos.close();
				// }
			}
			catch (MalformedURLException ex)
			{

				// dialog.dismiss();
				ex.printStackTrace();

				runOnUiThread(new Runnable()
				{
					public void run()
					{
						// messageText.setText("MalformedURLException Exception : check script url.");
						// Toast.makeText(UploadToServer.this,
						// "MalformedURLException", Toast.LENGTH_SHORT).show();
						Log.d("MalformedURLException Exception : check script url.");
					}
				});

				Log.d("Upload file to server error: " + ex);
			}
			catch (Exception e)
			{

				// dialog.dismiss();
				// e.printStackTrace();

				runOnUiThread(new Runnable()
				{
					public void run()
					{
						// messageText.setText("Got Exception : see logcat ");
						// Toast.makeText(UploadToServer.this,
						// "Got Exception : see logcat ",
						// Toast.LENGTH_SHORT).show();
						Log.d("Got Exception : see logcat: ");
					}
				});
				// return 2;
				Log.d("Upload file to server Exception : " + e);
			}
			finally
			{
				if (conn != null)
				{
					Log.d("finaly: ");
					conn.disconnect();
				}

			}
			// dialog.dismiss();
			return serverResponseCode;

		} // End else block
	}

	private void displayImage(String path)
	{
		Log.d("in display image");
		ImageView imageView = (ImageView) findViewById(R.id.image_view_captured_image);
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inSampleSize = 2;
		o.outHeight = 250;
		o.outWidth = 300;
		Bitmap b = BitmapFactory.decodeFile(path, o);
		imageView.setImageBitmap(b);
		imageView.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				Log.d("image clicked");
				Toast.makeText(getApplicationContext(), "Photo click", Toast.LENGTH_LONG).show();
				Intent intent = new Intent(CameraActivity.this, GalleryView.class);
				startActivity(intent);
			}
		});

	}

	public static File getOutputMediaFile()
	{
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraSpike");
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists())
		{
			if (!mediaStorageDir.mkdirs())
			{
				Log.d("failed to create directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

		return mediaFile;
	}

	public static boolean saveToFile(byte[] bytes, File file)
	{
		boolean saved = false;
		try
		{
			FileOutputStream fos = new FileOutputStream(file);
			fos.write(bytes);
			fos.close();
			saved = true;
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException", e);
		}
		catch (IOException e)
		{
			Log.e("IOException", e);
		}
		return saved;
	}

	public void onCaptureClick(View button)
	{
		// Take a picture with a callback when the photo has been created
		// Here you can add callbacks if you want to give feedback when the
		// picture is being taken

		mCamera.takePicture(null, null, this);
		// Log.d(getIntent());
		//
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera)
	{
		Log.d("Picture taken");
		String path = savePictureToFileSystem(data);
		Log.d(path);
		setResult(path);
		clickResult(path);
		// finish();
		Log.d("before  startPreview");
		mCamera.startPreview();
		Log.d("after  startPreview");
		// onPause();
		// onResume();
	}

	private static String savePictureToFileSystem(byte[] data)
	{
		File file = getOutputMediaFile();
		saveToFile(data, file);
		return file.getAbsolutePath();
	}

	private void setResult(String path)
	{
		Intent intent = new Intent();

		// Intent intent = new Intent(this, Preview.class);
		intent.putExtra(EXTRA_IMAGE_PATH, path);
		// startActivityForResult(intent, REQ_CAMERA_IMAGE);
		setResult(RESULT_OK, intent);
	}

	public static Camera getCameraInstance()
	{
		Camera c = null;
		try
		{
			c = Camera.open(); // attempt to get a Camera instance
		}
		catch (Exception e)
		{
			Log.d("Exceptie:"+e);
		}
		return c; 
	}

	protected void releaseCamera()
	{
		if (mCamera != null)
		{
			mPreview.setCamera(null);
			mCamera.release();
			mCamera = null;
		}
	}

//	@Override
//	protected void onResume()
//	{
//		
//		super.onResume();
//		Log.d("on resume");
//		mPreview.removeView(mPreview.mSurfaceView);
//		mPreview.addView(mPreview.mSurfaceView);
//		mCamera = Camera.open();
//		cameraCurrentlyLocked = defaultCameraId;
//		mPreview.setCamera(mCamera);
//	}

	@Override
	protected void onPause()
	{
		Log.d("on pAUSE");
		super.onPause();
		// mPreview.removeView(mPreview.mSurfaceView);
		if (mCamera != null)
		{
			mPreview.setCamera(null);
			mCamera.release();
			mCamera = null;
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
		Log.d(" surfaceChanged maine ");
		if (previewing)
		{
			mCamera.stopPreview();
			previewing = false;
		}

		if (mCamera != null)
		{
			try
			{
				mCamera.setPreviewDisplay(surfaceHolder);
				mCamera.startPreview();
				previewing = true;
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		Log.d(" surfaceCreated maine ");
		mCamera = Camera.open();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		Log.d(" surfaceDestroyed maine ");
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
		previewing = false;
	}

	public String getLastImage()
	{
		return lastImage;
	}

	public void setLastImage(String lastImage)
	{
		this.lastImage = lastImage;
	}

	class Preview extends ViewGroup implements SurfaceHolder.Callback
	{
		private final String TAG = "Preview";

		SurfaceView mSurfaceView;
		SurfaceHolder mHolder;
		Size mPreviewSize;
		List<Size> mSupportedPreviewSizes;
		Camera mCamera;
		public Boolean isPreviewRunning;

		Preview(Context context)
		{
			super(context);
			isPreviewRunning = false;
			mSurfaceView = new SurfaceView(context);
			addView(mSurfaceView);
			// mSurfaceView.setBackgroundColor(Color.RED);
			mHolder = mSurfaceView.getHolder();
			mHolder.addCallback(this);
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		public void setCamera(Camera camera)
		{
			Log.d("setCamera");
			mCamera = camera;
			if (mCamera != null)
			{
				mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
				// requestLayout();

				// cameraPreview = (CameraPreview)
				// findViewById(R.id.camera_preview);
				// cameraPreview.init(camera);
			}
		}

		// public void switchCamera(Camera camera)
		// {
		// Log.d("switch");
		// setCamera(camera);
		// try
		// {
		// camera.setPreviewDisplay(mHolder);
		// }
		// catch (IOException exception)
		// {
		// Log.e("IOException caused by setPreviewDisplay()", exception);
		// }
		// Camera.Parameters parameters = camera.getParameters();
		// parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
		// requestLayout();
		//
		// camera.setParameters(parameters);
		// }

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
		{

			int desiredWidth = 100;
			int desiredHeight = 100;

			int widthMode = MeasureSpec.getMode(widthMeasureSpec);
			int widthSize = MeasureSpec.getSize(widthMeasureSpec);
			int heightMode = MeasureSpec.getMode(heightMeasureSpec);
			int heightSize = MeasureSpec.getSize(heightMeasureSpec);

			int width;
			int height;

			// Measure Width
			if (widthMode == MeasureSpec.EXACTLY)
			{
				// Must be this size
				width = widthSize;
			}
			else if (widthMode == MeasureSpec.AT_MOST)
			{
				// Can't be bigger than...
				width = Math.min(desiredWidth, widthSize);
			}
			else
			{
				// Be whatever you want
				width = desiredWidth;
			}

			// Measure Height
			if (heightMode == MeasureSpec.EXACTLY)
			{
				// Must be this size
				height = heightSize;
			}
			else if (heightMode == MeasureSpec.AT_MOST)
			{
				// Can't be bigger than...
				height = Math.min(desiredHeight, heightSize);
			}
			else
			{
				// Be whatever you want
				height = desiredHeight;
			}
			if (mSupportedPreviewSizes != null)
			{
				mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
			}
			// MUST CALL THIS
			setMeasuredDimension(width, height);
		}

		// @Override
		// protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
		// {
		// Log.d("onMeasure:");
		// // Log.d("|-widthMeasureSpec:" + widthMeasureSpec);
		// // Log.d("heightMeasureSpec:" + heightMeasureSpec);
		// final int width = resolveSize(getSuggestedMinimumWidth(),
		// widthMeasureSpec);
		// final int height = resolveSize(getSuggestedMinimumHeight(),
		// heightMeasureSpec);
		// // final int height = 830;
		// // Log.d("|-width:" + width);
		// // Log.d("|-height:" + height);
		//
		//
		// if (mSupportedPreviewSizes != null)
		// {
		// mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width,
		// height);
		// }
		//
		// // Log.d("|-mPreviewSize:" + mPreviewSize);
		// // Log.d("|-mPreviewSize width:" + mPreviewSize.width);
		// // Log.d("|-mPreviewSize height:" + mPreviewSize.height);
		// setMeasuredDimension(width, height);
		// }

		@Override
		protected void onLayout(boolean changed, int l, int t, int r, int b)
		{
			if (changed && getChildCount() > 0)
			{
				Log.d("onLayout preview in if");
				// Log.d("|-mPreviewSize:" + mPreviewSize);
				// Log.d("|-mPreviewSize:" + mPreviewSize.width);
				// Log.d("|-mPreviewSize:" + mPreviewSize.height);
				final View child = getChildAt(0);
				Log.d("child:" + child);

				final int width = r - l;
				final int height = b - t;

				int previewWidth = width;
				int previewHeight = height;
				if (mPreviewSize != null)
				{
					Log.d("in if != null");
					previewWidth = mPreviewSize.width;
					previewHeight = mPreviewSize.height;
				}

				if (width * previewHeight > height * previewWidth)
				{
					final int scaledChildWidth = previewWidth * height / previewHeight;
					child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
				}
				else
				{
					final int scaledChildHeight = previewHeight * width / previewWidth;
					child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
				}
			}
		}

		public void surfaceCreated(SurfaceHolder holder)
		{
			Log.d("surfaceCreated");
			try
			{
				if (mCamera != null)
				{

					mCamera.setPreviewDisplay(holder);
				}
			}
			catch (IOException exception)
			{
				Log.e("IOException caused by setPreviewDisplay()", exception);
			}
		}

		// public void surfaceCreated(SurfaceHolder holder)
		// {
		// mCamera = Camera.open();
		// mCamera.setDisplayOrientation(90);
		// try
		// {
		// if (mCamera != null)
		// {
		// mCamera.setPreviewDisplay(holder);
		// mCamera.setPreviewCallback(new PreviewCallback()
		// {
		//
		// @Override
		// public void onPreviewFrame(byte[] data, Camera camera)
		// {
		// }
		// });
		// }
		//
		// }
		// catch (Exception e)
		// {
		// e.printStackTrace();
		// }
		// }
		public void previewCamera()
		{
			try
			{
				mCamera.setPreviewDisplay(mHolder);
				mCamera.startPreview();
				isPreviewRunning = true;
			}
			catch (Exception e)
			{
				Log.d("Cannot start preview", e);
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder)
		{
			Log.d("surfaceDestroyed");
			if (mCamera != null)
			{
				mCamera.stopPreview();
			}
		}

		private Size getOptimalPreviewSize(List<Size> sizes, int w, int h)
		{
			Log.d("getOptimalPreviewSize");
			// Log.d("w:" + w);
			// Log.d("h:" + h);

			final double ASPECT_TOLERANCE = 0.1;
			double targetRatio = (double) w / h;
			if (sizes == null)
				return null;

			Size optimalSize = null;
			double minDiff = Double.MAX_VALUE;

			int targetHeight = h;

			for (Size size : sizes)
			{
				Log.d("size:" + size);
				double ratio = (double) size.width / size.height;
				if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
					continue;
				if (Math.abs(size.height - targetHeight) < minDiff)
				{
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}

			if (optimalSize == null)
			{
				minDiff = Double.MAX_VALUE;
				for (Size size : sizes)
				{
					if (Math.abs(size.height - targetHeight) < minDiff)
					{
						optimalSize = size;
						minDiff = Math.abs(size.height - targetHeight);
					}
				}
			}
			return optimalSize;
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
		{
			Log.d("in surface changed");
			// Log.d("in surface changed width "+width);
			// Log.d("in surface changed height "+height);
			// Log.d("in surface changed mPreviewSize widtht "+mPreviewSize.width);
			// Log.d("in surface changed mPreviewSize height "+mPreviewSize.height);

			// isPreviewRunning=true;
			// if (isPreviewRunning)
			// {
			// mCamera.stopPreview();
			// }

			Parameters parameters = mCamera.getParameters();

			parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
			Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();

//			if (display.getRotation() == Surface.ROTATION_0)
//			{
//				Log.d("surfaceChanged		1");
//				parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
//				// mCamera.setDisplayOrientation(90);
//			}
//
//			if (display.getRotation() == Surface.ROTATION_90)
//			{
//				Log.d("surfaceChanged		2");
//				parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
//				mCamera.setDisplayOrientation(270);
//			}
//
//			if (display.getRotation() == Surface.ROTATION_180)
//			{
//				Log.d("surfaceChanged		3");
//				parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
//				mCamera.setDisplayOrientation(180);
//			}
//
//			if (display.getRotation() == Surface.ROTATION_270)
//			{
//				Log.d("surfaceChanged		4");
//				parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
//				mCamera.setDisplayOrientation(90);
//			}

			mCamera.setParameters(parameters);
			previewCamera();
		}
		// public void surfaceChanged(SurfaceHolder holder, int format, int w,
		// int h)
		// {
		// Log.d("surfacechanged");
		// Camera.Parameters parameters = mCamera.getParameters();
		// parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
		// requestLayout();
		//
		// mCamera.setParameters(parameters);
		// mCamera.startPreview();
		// }
	}
}
