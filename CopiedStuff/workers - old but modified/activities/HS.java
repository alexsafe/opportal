package com.workers.activities;

import static com.workers.util.CommonUtilities.hs_xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.xmlpull.v1.XmlPullParserException;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.workers.R;
import com.workers.adapters.ListToggleAdapter;
import com.workers.types.Concern;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class HS extends ListActivity
{
	ListView listView;
	ListToggleAdapter dataAdapter = null;
	ToggleButton tgbutton;

	private void handleSysUi()
	{
		Log.d("in handle sys ui");
		final HideSysUi hideUi = new HideSysUi(getWindow());
		final Handler mHideHandler = new Handler();
		final Runnable mHideRunnable = new Runnable()
		{
			@Override
			public void run()
			{
				hideUi.hideSystemUi(getWindow());
			}
		};
		getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener()
		{
			@Override
			public void onSystemUiVisibilityChange(int visibility)
			{
				if (visibility == 0)
				{
					mHideHandler.postDelayed(mHideRunnable, 2000);
				}
			}
		});
	}

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hs);
		new HideSysUi(getWindow());
		Util.initHsXml();
//		displayListView();
		new DownloadXmlTask().execute(hs_xml);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow());
	}


	public class DownloadXmlTask extends AsyncTask<String, Void, List<Concern>>
	{
		
		@Override
		protected void onPostExecute(List<Concern> result)
		{
			displayListView(result);
//			setContentView(R.layout.list_hs);
			// Displays the HTML string in the UI via a WebView
			ListToggleAdapter dataAdapter = null;
//			dataAdapter = new ListToggleAdapter(this, R.layout.list_hs, listItems);
//			setListAdapter(dataAdapter);
			//myWebView.loadData(result, "text/html", null);
		}
		
		@Override
		protected List<Concern> doInBackground(String... urls)
		{
			try
			{
				Log.d(" in doInBackground");
				return Util.loadXmlFromNetwork(urls[0]);
			}
			catch (IOException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			catch (XmlPullParserException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			return null;
		}
	}

	
	private void displayListView(List<Concern> list)
	{
		Log.d("list:"+list);
		Util util = new Util();
		// List<Concern> listItems = null;
		ArrayList<Concern> listItems = new ArrayList<Concern>();
		AsyncTask<String, Void, List<Concern>> listItemsConcern = util.new DownloadXmlTask().execute(hs_xml);
		Log.d("listItemsConcern:" + listItemsConcern);
		try
		{
			listItems = (ArrayList<Concern>) listItemsConcern.get();
			Log.d("listItemsConcern size:" + listItemsConcern.get().size());
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("InterruptedException:" + e);
		}
		catch (ExecutionException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("ExecutionException:" + e);
		}

		String[] vals = new String[list.size()];
		// String all = "#";
		int i = 0;
		for (Concern property : list)
		{
			vals[i] = property.getName();
			// all.concat(property.getName());
			i++;
		}
		// Log.d("all:" + all);
		Log.d("vals:" + vals);
		for (String val : vals)
		{
			Log.d("valoare:" + val);
		}
		// Array list of countries
		ArrayList<Concern> TasksList = new ArrayList<Concern>();
		Concern Tasks = new Concern("0", "HS0", true, "yyy");
		TasksList.add(Tasks);
		Tasks = new Concern("1", "HS2", false, "");
		TasksList.add(Tasks);
		Tasks = new Concern("2", "HS2", false, "");
		TasksList.add(Tasks);
		Tasks = new Concern("3", "HS3", false, "");
		TasksList.add(Tasks);
		Tasks = new Concern("4", "HS4", false, "");
		TasksList.add(Tasks);

		// create an ArrayAdaptar from the String Array
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, vals);
		dataAdapter = new ListToggleAdapter(this, R.layout.list_hs, listItems);
		// ListView listView = (ListView) findViewById(R.id.list_hs);
		TextView next = (TextView) findViewById(R.id.nextText);
		setListAdapter(dataAdapter);
		// listView.setAdapter(dataAdapter);
		// tgbutton = (ToggleButton) findViewById(R.id.toggleButton1);
		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click:");
				Boolean allAnswered = checkAnswered();
				allAnswered=true;
				if (allAnswered)
				{
					Intent i = new Intent(getApplicationContext(), Quality.class);
					startActivity(i);
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		super.onRestoreInstanceState(savedInstanceState);
		// Read values from the "savedInstanceState"-object and put them in your
		// textview
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		// Save the values you need from your textview into "outState"-object
		super.onSaveInstanceState(outState);
	}

	public Boolean checkAnswered()
	{
		ListView lv = getListView();
		Log.d("lv:" + lv);
		int size = getListAdapter().getCount();
		Log.d("click pe next");
		Log.d("size:" + size);
		Boolean allAnswered = true;
		for (int i = 0; i < size; i++)
		{
			Concern task = (Concern) lv.getItemAtPosition(i);
			Log.d("name:" + task.getName());
			Log.d("marked:" + task.getMarked());
			Log.d("item at " + i + " : " + lv.getItemAtPosition(i).getClass().getName());
			if (task.getMarked().equals(""))
			{
				Log.d("egal ");
				allAnswered = false;
				break;
			}
		}
		Log.d("allAnswered:" + allAnswered);
		return allAnswered;
	}

	public void goToQuality(View v)
	{
		Log.d("click pe goto");
		Boolean allAnswered = checkAnswered();
		if (allAnswered)
		{
			Intent i = new Intent(getApplicationContext(), Quality.class);
			startActivity(i);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
		}
	}
}
