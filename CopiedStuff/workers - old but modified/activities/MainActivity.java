package com.workers.activities;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.workers.Language;
import com.workers.R;
import com.workers.WakeLocker;
import com.workers.util.AlertDialogManager;
import com.workers.util.ConnectionDetector;
import com.workers.util.HideSysUi;
import com.workers.util.Log;

import static com.workers.util.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.workers.util.CommonUtilities.EXTRA_MESSAGE;

public class MainActivity extends Activity {


    private final static int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 345;
    public static String name = "name";
    public static String email = "email";
    /**
     * Receiving push messages
     */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("receiver");
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            /**
             * Take appropriate action on this message depending upon your app
             * requirement For now i am just displaying it on the screen
             * */

            // Showing received message
            Log.d("New Message: " + newMessage);
            Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();

            // Releasing wake lock
            WakeLocker.release();
        }
    };
    Button buttonLogIn;
    ConnectionDetector cd;
    AsyncTask<Void, Void, Void> mRegisterTask;
    AlertDialogManager alert = new AlertDialogManager();

    // private void hideSystemUi()
    // {
    // getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
    // | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
    // View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
    // View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN |
    // View.SYSTEM_UI_FLAG_IMMERSIVE);
    // }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final HideSysUi hideUi = new HideSysUi(getWindow());
        Log.d("oncreate token:");

//
//        // Here, thisActivity is the current activity
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//
//            // Permission is not granted
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
//
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
//
//                // No explanation needed; request the permission
//                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, MY_PERMISSIONS_REQUEST_READ_PHONE_STATE);
//
//                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                // app-defined int constant. The callback method gets the
//                // result of the request.
//            }
//        } else {
//            // Permission has already been granted
//        }

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Log.d("oncreate imei:" + telephonyManager.getDeviceId());


        // hide android menu
        // hideUi.hideSystemUi(getWindow());
        // final Handler mHideHandler = new Handler();
        // final Runnable mHideRunnable = new Runnable()
        // {
        // @Override
        // public void run()
        // {
        // hideUi.hideSystemUi(getWindow());
        // }
        // };
        // getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new
        // OnSystemUiVisibilityChangeListener()
        // {
        // @Override
        // public void onSystemUiVisibilityChange(int visibility)
        // {
        // if (visibility == 0)
        // {
        // mHideHandler.postDelayed(mHideRunnable, 2000);
        // }
        // }
        // });
        Log.d("Enter pwd");
        buttonLogIn = findViewById(R.id.butLogin);
        buttonLogIn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d("login button");
                Intent i = new Intent(getApplicationContext(), Language.class);
                startActivity(i);
            }

        });

        // gcm register
        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(MainActivity.this, "Internet Connection Error", "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
        try {
            // Make sure the device has the proper dependencies.
//			GCMRegistrar.checkDevice(this);
        } catch (Exception e) {
            Log.d("exceptie baaa:" + e);
        }
//		GCMRegistrar.checkManifest(this);
        // GCMRegistrar.unregister(this);
        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));

        final String regId = FirebaseInstanceId.getInstance().getToken();
        Log.d("oncreate token:" + regId);
//		regId = GCMRegistrar.getRegistrationId(this);
        // regId:APA91bELOwjQ1lhcXpBJz-bgsKMtiHNkiJeXI98qZd-KS1GpfF8V7uLeYsVe3BkYlDY5lgeUDtGCH_HGvKXAxWK2gel5Pij5sH2z5lbCWZxaTkSHe-xL5-qHmCeAcKW8ztrH-NkHSi04tLXMVik_RRCkL9oSgG_mpA

        Log.d("regId:" + regId);
        // Check if regid already presents
        if (regId == null) {
            // Registration is not present, register now with GCM
//			GCMRegistrar.register(this, SENDER_ID);

            Log.d("register:");
        } else {
            Log.d("else");
            final Context context = this;
//             ServerUtilities.registerDevice(context, regId);
//			if (GCMRegistrar.isRegisteredOnServer(this))
            {
                // Skips registration.
                // GCMRegistrar.unregister(this);
//				Log.d("Already registered with GCM");
                // Toast.makeText(getApplicationContext(),
                // "Already registered with GCM", Toast.LENGTH_LONG).show();
            }
//			else
            {
                Log.d("GCMRegistrar not trying again...");
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.

                mRegisterTask = new AsyncTask<Void, Void, Void>() {

                    @Override
                    protected Void doInBackground(Void... params) {
                        // Register on our server // On server creates a new
                        // user
                        Log.d("registerind device");
//                        ServerUtilities.registerDevice(context, regId);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }

                };
                mRegisterTask.execute(null, null, null);

            }
        }
    }

    public void logIn(View v){
        Intent i = new Intent(getApplicationContext(), Language.class);
        startActivity(i);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {

            case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
//			GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.d("UnRegister Receiver Error" + e);
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
