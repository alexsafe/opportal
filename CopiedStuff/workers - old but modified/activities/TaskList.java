package com.workers.activities;

import static com.workers.util.CommonUtilities.tasks_xml;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.R;
import com.workers.ResidentNotes;
import com.workers.activities.QualityWb;
import com.workers.adapters.TaskAdapter;
import com.workers.types.Tasks;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class TaskList extends Activity
{

	/** Called when the activity is first created. */
	ListView listView;
	TaskAdapter dataAdapter = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.task_list);
		new HideSysUi(getWindow());
		displayListView();
		// checkButtonClick();
	}

	private void displayListView()
	{
		ArrayList<Tasks> TasksList = new ArrayList<Tasks>();
		Tasks Tasks = new Tasks("0", "Skim walls / Kitchen", "false", "");
		TasksList.add(Tasks);
		Tasks = new Tasks("1", "Bonding walls / Kitchen", "false", "");
		TasksList.add(Tasks);
		Tasks = new Tasks("2", "Latex / Kitchen", "false", "");
		TasksList.add(Tasks);
		Tasks = new Tasks("3", "Skim walls / Bathroom", "false", "");
		TasksList.add(Tasks);
		Tasks = new Tasks("4", "Bonding walls / Bathroom", "false", "");
		TasksList.add(Tasks);

		Util util = new Util();
		ArrayList<Tasks> listItems = new ArrayList<Tasks>();
		AsyncTask<String, Void, List<Tasks>> listItemsConcern = util.new DownloadXmlTasks().execute(tasks_xml);
		Log.d("listItemsConcern:" + listItemsConcern);
		try
		{
			listItems = (ArrayList<Tasks>) listItemsConcern.get();
			Log.d("listItemsConcern size:" + listItemsConcern.get().size());
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("InterruptedException:" + e);
		}
		catch (ExecutionException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.d("ExecutionException:" + e);
		}

		String[] vals = new String[listItems.size()];
		int o = 0;
		for (Tasks property : listItems)
		{
			vals[o] = property.getName();
			o++;
		}

		Log.d("tasks:" + Tasks);
		// create an ArrayAdaptar from the String Array
		// dataAdapter = new TaskAdapter(this, R.layout.list_items, TasksList);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, vals);
		dataAdapter = new TaskAdapter(this, R.layout.list_items, listItems);
		ListView listView = (ListView) findViewById(R.id.listViewTask);
		// Assign adapter to ListView
		listView.setAdapter(dataAdapter);
		listView.setOnItemClickListener(new OnItemClickListener()
		{
			public void onItemClick(AdapterView<?> parent, View view, int position, long id)
			{
				// When clicked, show a toast with the TextView text
				Tasks Tasks = (Tasks) parent.getItemAtPosition(position);
				Toast.makeText(getApplicationContext(), "Clicked on Row: " + Tasks.getName(), Toast.LENGTH_LONG).show();
			}
		});
		TextView next = (TextView) findViewById(R.id.nextText);
		TextView back = (TextView) findViewById(R.id.prevText);

		back.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe back");
				Intent i = new Intent(getApplicationContext(), QualityWb.class);
				startActivity(i);
			}
		});
		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe next");
				ArrayList<Tasks> TasksList = dataAdapter.TasksList;
				Boolean checkedAll = true;
				for (int i = 0; i < TasksList.size(); i++)
				{
					Tasks Tasks = TasksList.get(i);
					// Log.d("get sel:" + Tasks.getSelected());
					// Log.d("selecred :" + Tasks.isSelected());
					if (Tasks.getSelected().equals("false"))
					{
						checkedAll = false;
					}
				}
				Log.d("ch all:" + checkedAll);
				if (checkedAll)
				{
					RequestParams params = new RequestParams();
					params.put("checked", "all");
					AsyncHttpClient client = new AsyncHttpClient();
					client.post("http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/saveTaskDemo.php", params, new AsyncHttpResponseHandler()
					{
						@Override
						public void onSuccess(String response)
						{
							Log.d("success  !!!!");
						}
					});
				}
				Intent i = new Intent(getApplicationContext(), ResidentNotes.class);
				startActivity(i);
			}
		});

	}

	// private void checkButtonClick()
	// {
	//
	// Button myButton = (Button) findViewById(R.id.findSelected);
	// myButton.setOnClickListener(new OnClickListener()
	// {
	//
	// @Override
	// public void onClick(View v)
	// {
	//
	// StringBuffer responseText = new StringBuffer();
	// responseText.append("The following were selected...\n");
	//
	// ArrayList<Tasks> TasksList = dataAdapter.TasksList;
	// for (int i = 0; i < TasksList.size(); i++)
	// {
	// Tasks Tasks = TasksList.get(i);
	// if (Tasks.isSelected())
	// {
	// responseText.append("\n" + Tasks.getName());
	// }
	// }
	//
	// Toast.makeText(getApplicationContext(), responseText,
	// Toast.LENGTH_LONG).show();
	//
	// }
	// });

	// }

	public void goToQualityWb(View v)
	{
		Log.d("click pe prev ");
		Intent i = new Intent(getApplicationContext(), QualityWb.class);
		startActivity(i);
	}

	public void goToResidentSatisfaction(View v)
	{
		Log.d("click pe next goToResidentSatisfaction ");
		Intent i = new Intent(getApplicationContext(), ResidentNotes.class);
		startActivity(i);
	}
}
