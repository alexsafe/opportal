package com.workers.activities;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.workers.R;
import com.workers.activities.Availability;
import com.workers.util.HideSysUi;

public class PropertyDetails extends Activity
{

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.property_details);
		new HideSysUi(getWindow());
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		TextView curDate=(TextView) findViewById(R.id.currentDate);
		curDate.setText(": "+dateFormat.format(date));
	}
	
	public void GoToAvailability(View v)
	{
		Intent i = new Intent(getApplicationContext(), Availability.class);
		startActivity(i);
	}
}
