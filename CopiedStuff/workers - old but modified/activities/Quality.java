package com.workers.activities;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.workers.R;
import com.workers.adapters.ListToggleAdapter;
import com.workers.types.Concern;
import com.workers.util.HideSysUi;
import com.workers.util.Log;

public class Quality extends ListActivity
{
	ListView listView;
	ListToggleAdapter dataAdapter = null;
	ToggleButton tgbutton;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quality);
		new HideSysUi(getWindow());
		displayListView();
	}

	// @Override
	// protected void onRestoreInstanceState(Bundle savedinstancestate)
	// {
	// super.onRestoreInstanceState(savedinstancestate);
	// Log.d("onrestoreinstancestate:" + savedinstancestate);
	// // read values from the "savedinstancestate"-object and put them in your
	// // textview
	// if (savedinstancestate instanceof Bundle)
	// {
	// Bundle bundle = (Bundle) savedinstancestate;
	// savedinstancestate = bundle.getParcelable("instanceState");
	// }
	// }
	//
	// @Override
	// protected void onSaveInstanceState(Bundle outstate)
	// {
	// // save the values you need from your textview into "outstate"-object
	// super.onSaveInstanceState(outstate);
	// Log.d("onsaveinstancestate:" + outstate);
	// Bundle bundle = new Bundle();
	// bundle.putParcelable("instanceState", outstate);
	// }

	private void displayListView()
	{
		// Array list of countries
		ArrayList<Concern> TasksList = new ArrayList<Concern>();
		Concern Tasks = new Concern("0", "Labouring(strip out) works quality", true, "");
		TasksList.add(Tasks);
		Tasks = new Concern("1", "Electrical works quality", false, "");
		 TasksList.add(Tasks);
		// Tasks = new Concern("2", "preceding task 2", false, "");
		// TasksList.add(Tasks);
		// Tasks = new Concern("3", "preceding task 3", false, "");
		// TasksList.add(Tasks);
		// Tasks = new Concern("4", "preceding task 4", false, "");
		// TasksList.add(Tasks);
		// Tasks = new Concern("5", "preceding task 5", false, "");
		// TasksList.add(Tasks);
		// Tasks = new Concern("6", "preceding task 6", false, "");
		// TasksList.add(Tasks);

		// create an ArrayAdaptar from the String Array
		dataAdapter = new ListToggleAdapter(this, R.layout.list_quality, TasksList);
		// ListView listView = (ListView) findViewById(R.id.list_quality);
		// listView.setAdapter(dataAdapter);
		TextView next = (TextView) findViewById(R.id.nextText);
		TextView back = (TextView) findViewById(R.id.prevText);
		setListAdapter(dataAdapter);
		back.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe next");
				Intent i = new Intent(getApplicationContext(), HS.class);
				startActivity(i);
			}
		});
		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{

				Log.d("click:");
				Boolean allAnswered = checkAnswered();
				allAnswered=true;
				if (allAnswered)
				{
					Intent i = new Intent(getApplicationContext(), QualityWb.class);
					startActivity(i);
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
				}
			}
		});
		//
		// listView.setOnItemClickListener(new OnItemClickListener()
		// {
		// public void onItemClick(AdapterView<?> parent, View view, int
		// position, long id)
		// {
		//
		// // When clicked, show a toast with the TextView text
		// Tasks Tasks = (Tasks) parent.getItemAtPosition(position);
		// Log.d("code:" + Tasks.getCode());
		// Toast.makeText(getApplicationContext(), "Clicked on Row: " +
		// Tasks.getName(), Toast.LENGTH_LONG).show();
		//
		// }
		// });
		// // Assign adapter to ListView
		// listView.setAdapter(dataAdapter);
		//
		// listView.setOnItemClickListener(new OnItemClickListener()
		// {
		// public void onItemClick(AdapterView<?> parent, View view, int
		// position, long id)
		// {
		// // When clicked, show a toast with the TextView text
		// Tasks Tasks = (Tasks) parent.getItemAtPosition(position);
		// Log.d("code:" + Tasks.getCode());
		// Toast.makeText(getApplicationContext(), "Clicked on Row: " +
		// Tasks.getName(), Toast.LENGTH_LONG).show();
		//
		// }
		// });

	}

	public Boolean checkAnswered()
	{
		ListView lv = getListView();
		Log.d("lv:" + lv);
		int size = getListAdapter().getCount();
		Log.d("click pe next");
		Log.d("size:" + size);
		Boolean allAnswered = true;
		for (int i = 0; i < size; i++)
		{
			Concern task = (Concern) lv.getItemAtPosition(i);
			Log.d("name:" + task.getName());
			Log.d("marked:" + task.getMarked());
			Log.d("item at " + i + " : " + lv.getItemAtPosition(i).getClass().getName());
			if (task.getMarked() == "")
			{
				allAnswered = false;
				break;
			}
		}
		Log.d("allAnswered:" + allAnswered);
		return allAnswered;
	}

	public void goToHs(View v)
	{
		Log.d("click pe prev ");
		Intent i = new Intent(getApplicationContext(), HS.class);
		startActivity(i);
	}

	public void goToQualityWb(View v)
	{
		Log.d("click pe next ");
		Boolean allAnswered = checkAnswered();
		if (allAnswered)
		{
			Intent i = new Intent(getApplicationContext(), QualityWb.class);
			startActivity(i);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
		}
	}
}
