package com.workers.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.CameraConfirm;
import com.workers.R;
import com.workers.adapters.TaskAdapter;
import com.workers.types.Tasks;
import com.workers.util.HideSysUi;
import com.workers.util.Log;

public class TaskDetail extends Activity
{

	/** Called when the activity is first created. */
	TaskAdapter dataAdapter = null;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Log.d("tab3");
		new HideSysUi(getWindow());
		String id = (String) getIntent().getExtras().get("id");
		Log.i("idul task" + id);
		Bundle data = getIntent().getExtras();
		Bitmap bmp;
		Intent intent = getIntent();
		Log.d("data get:" + data);
		Log.d("poz:" + intent.getStringExtra("poz"));
		Log.d("position:!!!!" + intent.getStringExtra("position"));
		final Tasks tasks = (Tasks) data.getParcelable("Task");
		final String position = intent.getStringExtra("position");

		Log.d("tasks:" + tasks.getName());
		setContentView(R.layout.task_detail);
		TextView name = (TextView) findViewById(R.id.name);
		final TextView selected = (TextView) findViewById(R.id.selected);
		final EditText quantity = (EditText) findViewById(R.id.quantity);
		Button saveValue = (Button) findViewById(R.id.saveValue);
		name.setText(tasks.getName());
		// if (!(tasks.getQuantity().equals("0")))
		// quantity.setText(tasks.getQuantity());
		// selected.setText(tasks.getSelected());
		if (!(tasks.getQuantity().equals("")))
		{
			if (tasks.getQuantity().equals("0"))
				quantity.setText("");
			else
				quantity.setText(tasks.getQuantity());
		}
		else
		{
			quantity.setVisibility(View.INVISIBLE);
		}
		TextView next = (TextView) findViewById(R.id.nextText);
		TextView back = (TextView) findViewById(R.id.prevText);

		saveValue.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("save value");
				RequestParams params = new RequestParams();
				String poz = tasks.getCode();
				Log.d("tasks:" + tasks);
				Log.d("tasks q:" + tasks.getQuantity());
				Log.d("tasks s:" + tasks.getSelected());
				Log.d("q:" + quantity.getText());
				Log.d("q:" + quantity.getText().toString());
				Log.d("s:" + selected.getText());
				Log.d("position:" + position);
				Log.d("poz:" + poz);
				params.put("index", position);
				params.put("quantity", quantity.getText().toString());
				params.put("checked", selected.getText());
				AsyncHttpClient client = new AsyncHttpClient();
				client.post("http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/saveTasksXml.php", params, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("success  !!!!");
						new CameraConfirm();
						int cameraData = 0;
						Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						startActivityForResult(i, cameraData);
						// Intent i = new Intent(getApplicationContext(),
						// CameraConfirm.class);
						// startActivity(i);

					}
				});

			}
		});

		back.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe back");
				Intent i = new Intent(getApplicationContext(), TaskList.class);
				startActivity(i);
			}
		});
		// next.setOnClickListener(new OnClickListener()
		// {
		// @Override
		// public void onClick(View arg0)
		// {
		// Log.d("click pe next");
		// Intent i = new Intent(getApplicationContext(),
		// ResidentSatisfaction.class);
		// startActivity(i);
		// }
		// });
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK)
		{
			Bundle extras = data.getExtras();
			Bitmap bmp = (Bitmap) extras.get("data");
		}
	}

	public void saveValue(View button)
	{

	}

	// @Override
	// public void onBackPressed()
	// {
	// // moveTaskToBack(true);
	// Intent i = new Intent(getApplicationContext(), MainActivity.class);
	// startActivity(i);
	// }

	public void goToTasksList(View v)
	{
		Log.d("click pe prev ");
		Intent i = new Intent(getApplicationContext(), TaskList.class);
		startActivity(i);
	}

	// public void goToResidentSatisfaction(View v)
	// {
	// Log.d("click pe next goToResidentSatisfaction ");
	// Intent i = new Intent(getApplicationContext(),
	// ResidentSatisfaction.class);
	// startActivity(i);
	// }
}
