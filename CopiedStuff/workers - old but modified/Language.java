package com.workers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.workers.activities.PropertyDetails;
import com.workers.util.HideSysUi;

public class Language extends Activity
{

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lang_select);
		new HideSysUi(getWindow());
	}

	public void GoToDetails(View v)
	{
		Intent i = new Intent(getApplicationContext(), PropertyDetails.class);
		startActivity(i);
	}
}
