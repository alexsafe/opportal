package com.workers;

import static com.workers.util.CommonUtilities.currDWritten;
import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.propertyGlobalAddress;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.types.AppParameters;
import com.workers.types.Property;
import com.workers.util.CommonUtilities;
import com.workers.util.ConnectionDetector;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class EnterProperty extends Activity
{
	TextView textAddress, textView1;
	RelativeLayout layout;
	Button yes, no;
	String employee, registerId, empName, propId;
	private NotificationReceiver nReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.enter_property);

		new HideSysUi(getWindow(), getApplicationContext());

		// setVolumeControlStream(AudioManager.STREAM_MUSIC);
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "enterProperty");
		editor1.commit();
		Log.d("uniqId enterProperty :" + getIntent().getStringExtra("uniqId"));
		layout = (RelativeLayout) findViewById(R.id.enter_property);
		// textView1=(TextView) findViewById(R.id.textView1);
		yes = new Button(getApplicationContext());
		no = new Button(getApplicationContext());
		textView1 = new TextView(getApplicationContext());
		nReceiver = new NotificationReceiver();
		IntentFilter filter = new IntentFilter();
		filter.addAction("com.workers.NOTIFICATION_LISTENER_EXAMPLE");
		registerReceiver(nReceiver, filter);
		if (!new File(xmlFolderPath + "propertiesCurrDetails.xml").exists())
		{
			Log.d("wwee nu exista!!!");
			new myAsyncTask().execute();
		}
		else
		{
			Log.d("wwee exista");
			doEnterProp();
		}

		RequestParams paramsRegister = new RequestParams();
		paramsRegister.put("activity", "enter_enterProperty");
		paramsRegister.put("empId", employee);
		paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
		paramsRegister.put("propId", propId);

		AsyncHttpClient client = new AsyncHttpClient();
		client.post(op, paramsRegister, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("AsyncHttpClient enteredProperty op success!!!!:" + response);
				// processResponse(response);
			}

			@Override
			public void onFailure(Throwable e, String response)
			{
				Log.d("AsyncHttpClient enteredProperty op onFailure e !!!!:" + e);
				Log.d("AsyncHttpClient enteredProperty op onFailure response!!!!:" + response);
			}
		});
	}

	private class myAsyncTask extends AsyncTask<Void, Void, Void>
	{
		@Override
		protected Void doInBackground(Void... params)
		{
			AsyncHttpClient client1 = new AsyncHttpClient();
			RequestParams paramsCurrProperty = new RequestParams();
			Log.d("hhh emp id bef:"+empId);
			if (Util.checkParametersExists(getApplicationContext()))
			{
				AppParameters appParameters;
				try
				{
					appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
					empId = appParameters.getEmpId();
					Log.d("hhh emp id after:"+empId);
				}
				catch (XmlPullParserException e1)
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				catch (IOException e1)
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				paramsCurrProperty.put("activity", "enterProperty");
				paramsCurrProperty.put("empId", empId);
				client1.setTimeout(20000);
				client1.setMaxRetriesAndTimeout(7, 30000);
				client1.setMaxConnections(20);
				client1.post(op, paramsCurrProperty, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("AsyncHttpClient PropertyCurrDetails op success!!!!:" + response);
						Log.d("xmlFolderPath propertiesCurrDetails:" + xmlFolderPath + "propertiesCurrDetails.xml");
						File details = new File(xmlFolderPath + "propertiesCurrDetails.xml");
						if (details.exists())
						{
							Log.d("delete curr details before write:");
							// details.delete();
							// DeleteRecursive(details);
						}
						Log.d("currDWritten utils:" + currDWritten);
						Util.writeFileOnSDCard(response, getBaseContext(), xmlFolderPath, "propertiesCurrDetails.xml", getIntent().getStringExtra("uniqId"));

						Log.d("currDWritten utils:" + currDWritten);
						List<Property> resultSdCard;
						RequestParams paramsRegister = new RequestParams();
						paramsRegister.put("activity", "ack_propertiesCurrDetails");
						paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
						paramsRegister.put("empId", empId);
						// paramsRegister.put("updated", "yes");
						AsyncHttpClient client = new AsyncHttpClient();
						client.post(op, paramsRegister, new AsyncHttpResponseHandler()
						{
							@Override
							public void onSuccess(String response)
							{
								Log.d("AsyncHttpClient ack_propertiesCurrDetails op success!!!!:" + response);
							}

							@Override
							public void onFailure(Throwable e, String response)
							{
								Log.d("AsyncHttpClient ack_propertiesCurrDetails op onFailure e !!!!:" + e);
								Log.d("AsyncHttpClient ack_propertiesCurrDetails op onFailure response!!!!:" + response);
							}
						});
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("AsyncHttpClient propertiesCurrDetails op onFailure e !!!!:" + e);
						Log.d("AsyncHttpClient propertiesCurrDetails op onFailure response!!!!:" + response);
					}
				});
				Util.getXmls(getApplicationContext(), "0", getIntent().getStringExtra("uniqId"));
				
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			doEnterProp();
		}
	}

	protected void doEnterProp()
	{
		try
		{
			// if (!CommonUtilities.currDWritten.equals("1") )
			if (!new File(xmlFolderPath + "propertiesCurrDetails.xml").exists())
			{
				Log.d("currDWritten in nu exista:" + CommonUtilities.currDWritten);
				showEmpty();
			}
			else
			{
				if (Util.checkParametersExists(getApplicationContext()))
				{
					List<Property> resultSdCard = Util.loadPropertiesXmlFromSdCard(getApplicationContext(), xmlFolderPath, "propertiesCurrDetails");
					AppParameters appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
					employee = appParameters.getEmpId();
					registerId = appParameters.getRegId();
					empName = appParameters.getEmpName();
					String title = (String) getTitle();
					String name = appParameters.getEmpName();
					final Spannable spanYou = new SpannableString(title + " " + name);
					spanYou.setSpan(new ForegroundColorSpan(Color.GREEN), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new RelativeSizeSpan(0.7f), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new StyleSpan(Typeface.ITALIC), title.length(), spanYou.length(), 0);
					setTitle(spanYou);
					// createRadioButton(size);
					int rgId = resultSdCard.size() + 1;
					final RadioGroup rg = new RadioGroup(this);
					rg.setId(rgId);
					// create the RadioGroup
					rg.setOrientation(RadioGroup.HORIZONTAL);// or
																// RadioGroup.VERTICAL
					int index = 0;
					// for (int i = 0; i < size; i++)
					if (resultSdCard.size() == 0)
					{
						Log.d("in zero");
						showEmpty();
					}
					else
					{
						for (final Property property : resultSdCard)
						{
							Log.d("property:" + property);
							RelativeLayout.LayoutParams radioLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
							RadioButton rb = new RadioButton(this);
							rg.setOrientation(RadioGroup.VERTICAL);
							rb.setText(property.getAddres());
							rb.setId(index);
							if (index == 0)
							{
								rb.setChecked(true);
								propId = property.getId();
								Log.d("propId:" + propId);
								SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
								Editor editor1 = sharedPreferences.edit();
								editor1.putString("propId", propId);
								editor1.commit();
								propertyGlobalAddress = property.getAddres();
								ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
								Util.createParametersXml(employee, propId, registerId, empName);
								// if (cd.isConnectingToInternet())
								{
									// Util.getXmls(getApplicationContext(),
									// property.getId());
								}
							}
							radioLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
							if (index > 0)
								radioLayoutParams.addRule(RelativeLayout.BELOW, index - 1);
							rg.addView(rb, radioLayoutParams);
							rb.setOnCheckedChangeListener(new OnCheckedChangeListener()
							{
								@Override
								public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
								{
									if (isChecked)
									{
										Log.d("button view:" + buttonView);
										RadioButton r = (RadioButton) buttonView;
										Log.d("button view r:" + r.getText());
										Log.d("button view address:" + property.getAddres());

										propId = property.getId();
										propertyGlobalAddress = property.getAddres();
										SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
										Editor editor1 = sharedPreferences.edit();
										editor1.putString("propId", propId);
										editor1.commit();
										File parametersFile = new File(xmlFolderPath + "parameters.xml");
										// if (!(parametersFile.exists()))// ||
										// !(parameters1File.exists()))
										{
											Util.createParametersXml(employee, propId, registerId, empName);
										}
										ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
										// if (cd.isConnectingToInternet())
										{
											// Util.getXmls(getApplicationContext(),
											// property.getId());
										}
									}
								}
							});
							index++;
							Log.d("index:" + index);
						}
						int id = 11;
						RelativeLayout.LayoutParams textview1Params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						textview1Params.addRule(RelativeLayout.BELOW, rg.getId());
						textview1Params.addRule(RelativeLayout.CENTER_HORIZONTAL);
						textview1Params.topMargin = 20;
						textView1.setId(id);
						textView1.setText(R.string.txtEnterProperty);
						textView1.setTextSize(25);
						layout.addView(textView1, textview1Params);
						RelativeLayout.LayoutParams yesParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						yesParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						yesParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						yesParams.addRule(RelativeLayout.BELOW, textView1.getId());
						yesParams.height = 150;
						yes.setTextAppearance(getApplicationContext(), R.style.btnStyle1);
						yes.setBackgroundResource(R.drawable.custom_button);
						id++;
						yes.setId(id);
						yes.setText("Yes");
						yes.setTextSize(25);
						layout.addView(yes, yesParams);
						RelativeLayout.LayoutParams noParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						noParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						noParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						noParams.addRule(RelativeLayout.BELOW, yes.getId());
						noParams.topMargin = 10;
						noParams.height = 150;
						id++;
						no.setTextAppearance(getApplicationContext(), R.style.btnStyle1);
						no.setBackgroundResource(R.drawable.custom_button);
						no.setId(id);
						no.setText("No");
						no.setTextSize(25);
						layout.addView(no, noParams);

						sendCheckIn(employee);

						layout.addView(rg);
						layout.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(View arg0)
							{
								Log.d("click pe layout in enter");
								new HideSysUi(getWindow(), getApplicationContext());
								NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
								mNotificationManager.cancelAll();
							}
						});
					}
				}
			}

		}
		catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showEmpty()
	{
		TextView message = new TextView(getApplicationContext());
		RelativeLayout.LayoutParams propAddressLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		// RelativeLayout.LayoutParams propAddressLayoutParams = new
		// RelativeLayout.LayoutParams(200,300, weight);
		propAddressLayoutParams.leftMargin = 20;
		propAddressLayoutParams.topMargin = 60;
		propAddressLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		propAddressLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		propAddressLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
		propAddressLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
		message.setLines(2);
		message.setId(1000);
		// message.setLayoutParams(new
		// TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
		// LayoutParams.WRAP_CONTENT, 0f));
		message.setTextSize(27);
		message.setTextColor(Color.WHITE);
		message.setText("Sorry, this page has no data at the moment!");
		layout.addView(message, propAddressLayoutParams);
		Button okButton = new Button(getApplicationContext());
		RelativeLayout.LayoutParams propOkButtonLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		propOkButtonLayoutParams.topMargin = 300;
		propOkButtonLayoutParams.height = 200;
		propOkButtonLayoutParams.addRule(RelativeLayout.BELOW, message.getId());
		propOkButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		propOkButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		propOkButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		okButton.setId(2000);
		okButton.setTextSize(25);
		okButton.setText("Ok");

		okButton.setTextAppearance(getApplicationContext(), R.style.btnStyle1);
		okButton.setBackgroundResource(R.drawable.custom_button);
		okButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click:");
				RequestParams paramsRegister = new RequestParams();
				paramsRegister.put("activity", "property_details_ok");
				paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
				paramsRegister.put("empId", empId);
				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("AsyncHttpClient ack_acknowledge op success!!!!:" + response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("AsyncHttpClient ack_acknowledge  onFailure e !!!!:" + e);
						Log.d("AsyncHttpClient ack_acknowledge  onFailure response!!!!:" + response);
					}
				});
				// Intent i = new Intent(getApplicationContext(),
				// Availability.class);
				// startActivity(i);
				moveTaskToBack(true);
			}
		});
		layout.addView(okButton, propOkButtonLayoutParams);

	}

	@Override
	public void onBackPressed()
	{
		// moveTaskToBack(true);

	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		unregisterReceiver(nReceiver);
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		// Save UI state changes to the savedInstanceState.
		// This bundle will be passed to onCreate if the process is
		// killed and restarted.
		savedInstanceState.putBoolean("myBoolean", true);
		savedInstanceState.putDouble("myDouble", 1.9);
		savedInstanceState.putInt("myInt", 1);
		savedInstanceState.putString("myString", "enterProp");
		// etc.
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d("on pause enter prop");
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "enterProperty");
		editor1.commit();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow(), getApplicationContext());
	}

	private void sendCheckIn(final String employee)
	{
		// textAddress = (TextView) findViewById(R.id.textAddress);
		// textAddress.setText(propertyGlobalAddress);
		yes.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.d("click yes");
				Log.d(" empid" + employee);
				Log.d(" propId" + propId);
				RequestParams paramsRegister = new RequestParams();
				paramsRegister.put("activity", "enteredProperty");
				paramsRegister.put("entered", "yes");
				paramsRegister.put("empId", employee);
				paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
				paramsRegister.put("propId", propId);

				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("AsyncHttpClient enteredProperty op success!!!!:" + response);
						// processResponse(response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("AsyncHttpClient enteredProperty op onFailure e !!!!:" + e);
						Log.d("AsyncHttpClient enteredProperty op onFailure response!!!!:" + response);
					}
				});
				Intent i = new Intent(getApplicationContext(), CameraActivity.class);
				i.putExtra("cameraSource", "enterProperty");
				i.putExtra("takenFrom", "enterproperty");
				i.putExtra("quantity", "-1");
				i.putExtra("marked", "-1");
				i.putExtra("name", "enterproperty");
				i.putExtra("clsName", "enterproperty");
				i.putExtra("ew", "-1");
				i.putExtra("takenFor", "0");
				startActivity(i);
			}
		});
		no.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.d("click no");
				moveTaskToBack(true);
				// Toast.makeText(getApplicationContext(), "no",
				// Toast.LENGTH_LONG).show();
			}
		});

	}

	class NotificationReceiver extends BroadcastReceiver
	{

		@Override
		public void onReceive(Context context, Intent intent)
		{
			Log.d("RECEIVE!!!!");
			// String temp = intent.getStringExtra("notification_event") + "n" +
			// txtView.getText();
			// txtView.setText(temp);
		}
	}

	private void createRadioButton(int size)
	{
		// final RadioButton[] rb = new RadioButton[5];
		RadioGroup rg = new RadioGroup(this); // create the RadioGroup
		rg.setOrientation(RadioGroup.HORIZONTAL);// or RadioGroup.VERTICAL
		int index = 0;
		for (int i = 0; i < size; i++)
		{
			RelativeLayout.LayoutParams radioLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
			RadioButton rb = new RadioButton(this);
			rb.setText("txt");
			rb.setId(i);
			if (i > 0)
				radioLayoutParams.addRule(RelativeLayout.BELOW, i - 1);
			rg.addView(rb, radioLayoutParams);
			index = i;
			Log.d("index:" + index);
		}
		layout.addView(rg);// you add the whole RadioGroup to the layout
		Log.d("index la final:" + index);
	}
}
