package com.workers;

import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.xmlFolderPath;
import static com.workers.util.Util.loadXmlFromSdCard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.workers.types.AppParameters;
import com.workers.types.Concern;
import com.workers.types.Tasks;
import com.workers.util.Log;
import com.workers.util.Util;

public class TaskAdapter extends ArrayAdapter<Tasks>
{
	TaskAdapter dataAdapter = null;
	private final Context context;
	Tasks tasks;
	String q;
	public ArrayList<Integer> selected = new ArrayList<Integer>();

	public ArrayList<Tasks> TasksList;

	public TaskAdapter(Context context, int textViewResourceId, ArrayList<Tasks> TasksList)
	{
		super(context, textViewResourceId, TasksList);
		this.context = context;
		this.TasksList = new ArrayList<Tasks>();
		this.TasksList.addAll(TasksList);
	}

	private class ViewHolder
	{
		TextView name;
		CheckBox checkbox;
		TextView quantity;
	}

	public OnClickListener myClickListener = new OnClickListener()
	{
		public void onClick(View v)
		{
			Log.d("click pe click listener");
		}
	};

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{

		ViewHolder holder = null;
		RelativeLayout lists;
		Log.v("ConvertView" + String.valueOf(position));
		if (convertView == null)
		{

			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			lists = (RelativeLayout) vi.inflate(R.layout.list_items, null);
			lists.setClickable(true);
			lists.setOnClickListener(myClickListener);
			// convertView = vi.inflate(R.layout.list_hs, null);

		}
		else
		{
			// holder = (ViewHolder) convertView.getTag();
			lists = (RelativeLayout) convertView;
			lists.setClickable(true);
			lists.setOnClickListener(myClickListener);
		}

		holder = new ViewHolder();
		holder.name = (TextView) lists.findViewById(R.id.code);
		holder.checkbox = (CheckBox) lists.findViewById(R.id.checkBox1);
		holder.quantity = (TextView) lists.findViewById(R.id.q1);

		final CheckBox checkbox = (CheckBox) lists.findViewById(R.id.checkBox1);
		final TextView name = (TextView) lists.findViewById(R.id.code);
		final TextView quantity = (TextView) lists.findViewById(R.id.q1);

		lists.setTag(holder);

		tasks = TasksList.get(position);
		Log.d("tasks pe pozitia:" + position);
		Log.d("tasks pe pozitia name:" + tasks.getName());
		Log.d("tasks pe pozitia id:" + tasks.getCode());
		Log.d("tasks pe pozitia quantity:" + tasks.getQuantity());
		Log.d("tasks pe pozitia selected:" + tasks.getSelected());
		Log.d("tasks pe pozitia ew:" + tasks.getEw());
		for (int i = 0; i < TasksList.size(); i++)
		{
			Log.d("item:" + TasksList.get(i).getName() + ": " + TasksList.get(i).getSelected());
			Log.d("|---:" + i + ": " + position);
			if (TasksList.get(i).getSelected().toLowerCase().equals("1".toLowerCase()))
			{
				Log.d("selected");
				if (position == i)
				{
					Log.d("position ok:" + position);
					selected.add(position);
				}
			}
		}
		String taskName = tasks.getName().replace("/", "/ \n");
		name.setText(taskName);
		// quantity.setText(tasks.getQuantity());
		if (tasks.getQuantity().equals("0"))
		{
			quantity.setBackgroundColor(Color.TRANSPARENT);
			quantity.setText("  ");
		}
		else if (tasks.getQuantity().equals("1"))
		{
			quantity.setBackgroundColor(Color.WHITE);
			quantity.setText("  ");
		}
		else
		{
			quantity.setBackgroundColor(Color.TRANSPARENT);
			quantity.setText(tasks.getQuantity());
		}
		holder.checkbox.setChecked(Util.stringToBool(tasks.getSelected()));
		holder.checkbox.setTag(tasks);
		holder.name.setTag(tasks);

		Log.d("holder.quantity:" + holder.quantity);
		Log.d("holder.quantity getText:" + holder.quantity.getText());
		q = holder.quantity.getText().toString();

		if (q.equals("0"))
		{
			Log.d("holder.quantity: nimic" + holder.name.getText());
			// holder.quantity.setVisibility(View.GONE);
		}
		else
		{
			Log.d("holder.quantity: ceva |" + q + "|");
			Log.d("holder.quantity: ceva |" + holder.name.getText().toString() + "|");
			// holder.quantity.setText("?");
			// holder.quantity.setBackgroundColor(Color.WHITE);
		}

		holder.name.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				TextView tv = (TextView) v;
				// ArrayList<Tasks> pointsExtra = new ArrayList<Tasks>();
				Log.d("click pe layout name:");
				NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancelAll();

				Tasks tasks = (Tasks) tv.getTag();
				Log.d("tasks code in :" + tasks);
				// Log.d("tasks code getQuantity in :" + tasks.getQuantity());
				// Log.d("requires q:" + tasks.getQuantity());
				// if (!(q.equals("0")))
				// {
				Intent i = new Intent(context, TaskDetail.class);
				i.putExtra("TaskList", TasksList);
				i.putExtra("Task", tasks);
				i.putExtra("position", "" + position);
				i.putExtra("from", "tasklist");
				context.startActivity(i);

				// }
				// else
				// {
				// Tasks Tasks = TasksList.get(position);
				// Tasks.setSelected("1");
				// TasksList.set(position, Tasks);
				// Util.saveTaskListToXml(TasksList, "tasks");
				// }
			}
		});
		holder.checkbox.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{

				TextView tv = (TextView) v;

				Tasks tasks = (Tasks) tv.getTag();
				Log.d("tasks name in :" + tasks);
				Log.d("tasks name getQuantity in :" + tasks.getName());
				Log.d("tasks name getQuantity in :" + tasks.getQuantity());
				Log.d("tasks name getQuantity in :" + tasks.getSelected());
				Log.d("requires q:" + tasks.getQuantity());
				AppParameters appParameters;
				try
				{
					if (Util.checkParametersExists(context))
					{
						appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
						empId = appParameters.getEmpId();
						propId = appParameters.getPropId();
					}
				}
				catch (XmlPullParserException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (!(tasks.getQuantity().equals("0")))
				{
					if (!(tasks.getSelected().equals("0")))
					{

						Tasks Tasks = TasksList.get(position);
						Tasks.setSelected("0");
						TasksList.set(position, Tasks);
						Util.saveTaskListToXml(TasksList, "task", propId);
						Util.saveToUploadXml(empId, propId, "tasks", tasks.getCode(), tasks.getName(), tasks.getSelected(), tasks.getQuantity(), "", "-1", tasks.getEw());
						Log.d("tasks name uncheck :" + tasks);
						Log.d("tasks name uncheck getQuantity in :" + tasks.getName());
						Log.d("tasks name uncheck getQuantity in :" + tasks.getQuantity());
						Log.d("tasks name uncheck getQuantity in :" + tasks.getSelected());
					}
					else
					{
						Intent i = new Intent(context, TaskDetail.class);
						i.putExtra("TaskList", TasksList);
						i.putExtra("Task", tasks);
						i.putExtra("position", "" + position);
						i.putExtra("from", "tasklist");
						context.startActivity(i);
					}
				}
				else
				{
					Tasks Tasks = TasksList.get(position);
					Tasks.setSelected("1");
					TasksList.set(position, Tasks);
					Util.saveTaskListToXml(TasksList, "tasks", propId);
					Util.saveToUploadXml(empId, propId, "tasks", tasks.getCode(), tasks.getName(), tasks.getSelected(), "0", "", "-1", tasks.getEw());
				}
			}
		});

		checkbox.setChecked(selected.contains(position) ? true : false);
		// quantity.setText(selected.contains(position) ? true : false);
		return lists;

	}
}
