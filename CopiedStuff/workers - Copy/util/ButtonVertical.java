package com.workers.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.TextView.BufferType;

public class ButtonVertical extends Button
{
	String s = "";

	public ButtonVertical(Context context)
	{
		super(context);
	}

	public ButtonVertical(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
	}

	public ButtonVertical(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void setText(CharSequence text, BufferType type)
	{
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub

		for (int i = 0; i < text.length(); i++)
		{
			if (s == null)
				s = "";

			s = s + String.valueOf(text.charAt(i)) + "\n";
		}

		super.setText(s, type);
	}
}