package com.workers.util;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import com.workers.types.Concern;

public final class CommonUtilities
{

	// give your server registration url here
	// static final String SERVER_URL =
	// "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/register.php";
	// give your server registration url here
	static final String SERVER_URL = "http://pill1.pilon.co.uk/Example/xmpphp/gcm_server_php/scripts/register_regId.php";
//	public static final String hs = "http://pill.pilon.co.uk/Example/xmpphp/gcm_server_php/xmls/hs2.xml";
//	public static final String tasks_xml = "http://pill.pilon.co.uk/Example/xmpphp/gcm_server_php/xmls/tasks.xml";
//	public static final String upload_photos_server = "http://pill.pilon.co.uk/Example/xmpphp/gcm_server_php/scripts/upload_photo.php";
//	public static final String upload_test = "http://pill.pilon.co.uk/Example/xmpphp/gcm_server_php/upload_test.php";
//	public static final String initHs = "http://pill.pilon.co.uk/Example/xmpphp/gcm_server_php/scripts/initHsXml.php";
//	public static final String saveHs = "http://pill.pilon.co.uk/Example/xmpphp/gcm_server_php/scripts/saveHs.php";
//	public static final String initTasks = "http://pill.pilon.co.uk/Example/xmpphp/gcm_server_php/scripts/initTasksXml.php";
//	public static final String saveTask = "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/scripts/saveTask.php";

	public static final String op = "http://pill1.pilon.co.uk/OP/op.php";
	public static final String uploadFilesServer = "http://pill1.pilon.co.uk/OP/opUpload.php";
	public static final String imgPillServer = "http://pill1.pilon.co.uk/img/SMPimg/";
	public static final String sdCardPath = Environment.getExternalStorageDirectory() + "/xmls/";
	public static final String xmlFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "XmlFolder" + File.separator;
	public static final String ImgFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "XmlFolder" + File.separator + "Images" + File.separator;
	public static final File photosLocation = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
	public static final String photosFolder = "CameraSpike";
	public static boolean phGot = false;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static String currDWritten="0";
	// Google project id
	// public static final String SENDER_ID = "961999046108"; //apiphpserver
	// project id
	public static final String SENDER_ID = "1060667327177"; // tasks proj id

	public static ArrayList<Concern> hs_xml_get_got = null;
	public static String empId = "0";
	public static String propId = "0";
	public static String propertyGlobalAddress = "-";
	public static String regId;

	public static final String TAG = "Workers";
	public static final String DISPLAY_MESSAGE_ACTION = "com.workers.DISPLAY_MESSAGE";
	public static final String EXTRA_MESSAGE = "message";
	public static final String PICTURES_DIR = "/Pictures/CameraSpike/";

	public static void displayMessage(Context context, String message)
	{
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}
}
