package com.workers.util;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.os.AsyncTask;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.phGot;

public class DownloadTask extends AsyncTask<String, Void, Boolean>
{
	String photo;

	@Override
	protected Boolean doInBackground(String... urls)
	{
		Log.d("urls:"+urls);
		photo=urls[0];
		Log.d("urls[0]:"+urls[0]);
		Log.d("urls[1]:"+urls[1]);
		Log.d("urls[2]:"+urls[2]);
		Boolean ret=Util.savePhotos(urls[0],urls[1],urls[2]);
		return ret;
	}

	@Override
	protected void onPostExecute(Boolean result)
	{
		Log.d("show rez:" + result);
		Util.photosGot=true;
		phGot=true;
		Util.setPhotosGot(true);
//		RequestParams paramsRegister = new RequestParams();
//		paramsRegister.put("activity", "ack_photos_saved");
//		paramsRegister.put("photo", photo);
//		AsyncHttpClient client = new AsyncHttpClient();
//		client.post(op, paramsRegister, new AsyncHttpResponseHandler()
//		{
//			@Override
//			public void onSuccess(String response)
//			{
//				Log.d("AsyncHttpClient ack_photos_saved op success!!!!:" + response);
//			}
//			@Override
//			public void onFailure(Throwable e, String response)
//			{
//				Log.d("AsyncHttpClient ack_photos_saved op onFailure e !!!!:" + e);
//				Log.d("AsyncHttpClient ack_photos_saved op onFailure response!!!!:" + response);
//			}
//		});		
	}
}
