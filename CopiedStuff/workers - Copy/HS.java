package com.workers;

import static com.workers.util.CommonUtilities.xmlFolderPath;
import static com.workers.util.CommonUtilities.hs_xml_get_got;
import static com.workers.util.CommonUtilities.sdCardPath;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.Util.loadXmlFromSdCard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.xmlpull.v1.XmlPullParserException;

import android.app.ListActivity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemClickListener;

import com.workers.types.AppParameters;
import com.workers.types.Concern;
import com.workers.types.Tasks;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class HS extends ListActivity
{
	ListView listView;
	ListToggleAdapter dataAdapter = null;
	ToggleButton tgbutton;
	RelativeLayout hsLayout;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hs);
		hsLayout = (RelativeLayout) findViewById(R.id.hsLayout);
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "hs");
		editor1.commit();
		new HideSysUi(getWindow());
		hsLayout.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe layout hs");
				new HideSysUi(getWindow());
				NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancelAll();
			}
		});

		try
		{
			if (Util.checkParametersExists(getApplicationContext()))
			{
				AppParameters appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				propId = appParameters.getPropId();
				Log.d("propId in HS:" + propId);
				List<Concern> resultSdCard = loadXmlFromSdCard(xmlFolderPath, "hs_" + propId);
				if (resultSdCard != null)
				{
					// Log.d("HS result size:" + resultSdCard.size());

					String title = (String) getTitle();
					String name = appParameters.getEmpName();
					final Spannable spanYou = new SpannableString(title + " " + name);
					spanYou.setSpan(new ForegroundColorSpan(Color.GREEN), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new RelativeSizeSpan(0.7f), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new StyleSpan(Typeface.ITALIC), title.length(), spanYou.length(), 0);
					setTitle(spanYou);
					// List<Concern> resultSdCard =
					// loadXmlFromSdCard(sdCardPath,"hs");
					displayListView(resultSdCard);
				}
			}
		}
		catch (XmlPullParserException e)
		{
			Log.d("XmlPullParserException:" + e);
			e.printStackTrace();
		}
		catch (IOException e)
		{
			Log.d("IOException:" + e);
			e.printStackTrace();
		}
		// displayListView();
		// new DownloadXmlTask().execute(hs_xml);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d("on pause hs");
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "hs");
		editor1.commit();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow());
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{

		super.onConfigurationChanged(newConfig);
		Log.d("onConfigurationChanged");
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
		{
			Log.d("ORIENTATION_PORTRAIT");
		}
		else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			Log.d("ORIENTATION_LANDSCAPE");

		}

	};

	@Override
	public void onBackPressed()
	{
		// moveTaskToBack(true);

	}

	public class DownloadXmlTask extends AsyncTask<String, Void, List<Concern>>
	{

		@Override
		protected void onPostExecute(List<Concern> result)
		{
			Log.d("result:" + result);
			hs_xml_get_got = (ArrayList<Concern>) result;
			displayListView(result);
			// setContentView(R.layout.list_hs);
			// Displays the HTML string in the UI via a WebView
			ListToggleAdapter dataAdapter = null;
			// dataAdapter = new ListToggleAdapter(this, R.layout.list_hs,
			// listItems);
			// setListAdapter(dataAdapter);
			// myWebView.loadData(result, "text/html", null);
		}

		@Override
		protected List<Concern> doInBackground(String... urls)
		{
			try
			{
				Log.d(" in doInBackground");
				return Util.loadXmlFromNetwork(urls[0]);
			}
			catch (IOException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			catch (XmlPullParserException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			return null;
		}
	}

	private void displayListView(List<Concern> list)
	{
		Log.d("list:" + list);
		Util util = new Util();
		ArrayList<Concern> listItems = (ArrayList<Concern>) list;
		Log.d("listItems:" + listItems);

		dataAdapter = new ListToggleAdapter(this, R.layout.list_hs, listItems);

		// ListView listView = (ListView) findViewById(R.id.list_hs);
		TextView next = (TextView) findViewById(R.id.nextText);
		setListAdapter(dataAdapter);

		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click:");
				Boolean allAnswered = checkAnswered();
				// allAnswered=true;
				if (allAnswered)
				{
					Intent i = new Intent(getApplicationContext(), Quality.class);
					startActivity(i);
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		super.onRestoreInstanceState(savedInstanceState);
		// Read values from the "savedInstanceState"-object and put them in your
		// textview
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		// Save the values you need from your textview into "outState"-object
		super.onSaveInstanceState(outState);
	}

	public Boolean checkAnswered()
	{
		ListView lv = getListView();
		Log.d("lv:" + lv);
		int size = 0;
		if (getListAdapter() != null)
			size = getListAdapter().getCount();
		Log.d("click pe next");
		Log.d("size:" + size);
		Boolean allAnswered = true;
		for (int i = 0; i < size; i++)
		{
			Concern task = (Concern) lv.getItemAtPosition(i);
			Log.d("name:" + task.getName());
			Log.d("marked:" + task.getMarked());
			Log.d("item at " + i + " : " + lv.getItemAtPosition(i).getClass().getName());
			if (task.getMarked().equals("0"))
			{
				Log.d("egal ");
				allAnswered = false;
				// break;
				return false;
			}
		}
		Log.d("allAnswered:" + allAnswered);
		return allAnswered;
	}

	public void goToQuality(View v)
	{
		Log.d("click pe goto");
		Boolean allAnswered = checkAnswered();
		if (allAnswered)
		{
			Intent i = new Intent(getApplicationContext(), Quality.class);
			startActivity(i);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
		}
	}
}
