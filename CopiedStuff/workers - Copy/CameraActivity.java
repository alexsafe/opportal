package com.workers;

import static com.workers.util.CommonUtilities.PICTURES_DIR;
import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.propId;

import static com.workers.util.CommonUtilities.photosLocation;
import static com.workers.util.CommonUtilities.photosFolder;
import static com.workers.util.CommonUtilities.uploadFilesServer;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.workers.GalleryView.PicAdapter;
import com.workers.types.AppParameters;
import com.workers.util.Log;
import com.workers.util.Util;
import com.workers.widget.CameraPreview;

public class CameraActivity extends Activity implements SurfaceHolder.Callback, PictureCallback, SensorEventListener
{
	private Preview mPreview;
	protected static final String EXTRA_IMAGE_PATH = "com.workers.CameraActivity.EXTRA_IMAGE_PATH";
	private static final String SDCARD = Environment.getExternalStorageDirectory().getPath();
	private static final String DIRECTORY = SDCARD + PICTURES_DIR;
	Camera mCamera;
	private CameraPreview cameraPreview;
	private static final int REQ_CAMERA_IMAGE = 123;
	int numberOfCameras;
	final private int CAPTURE_IMAGE = 2;
	int cameraCurrentlyLocked;
	private String lastImage, detailInception, cameraSource, takenFrom, takenFor, last, ew;
	private static String imageName;
	private String pozition = "portrait";
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	boolean previewing = false;
	LayoutInflater controlInflater = null;
	int defaultCameraId;
	private PicAdapter imgAdapt;
	String upLoadServerUri = null;
	private static final Random random = new Random();
	Button takepic, dialogButtonSave, dialogButtonRetake, dialogConfirmYes, dialogConfirmNo;
	public static String empId;
	final Context context = this;
	private OrientationEventListener mOrientationEventListener;
	private int mOrientation = -1;
	RelativeLayout dPhotoLayout, dConfirmLayout;
	ImageView imageLayout;
	private static final int ORIENTATION_PORTRAIT_NORMAL = 1;
	private static final int ORIENTATION_PORTRAIT_INVERTED = 2;
	private static final int ORIENTATION_LANDSCAPE_NORMAL = 3;
	private static final int ORIENTATION_LANDSCAPE_INVERTED = 4;
	SharedPreferences sharedPreferences;
	// private Camera.PictureCallback mJpegCallback = new
	// Camera.PictureCallback();
	/********** File Path *************/
	final String uploadFilePath = Environment.getExternalStorageDirectory().getPath() + "/Pictures/Spike/";
	final String uploadFileName = "IMG_20140205_180913.jpg";
	int serverResponseCode = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Log.d("on create");
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		// setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR |
		// ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Log.d("sharedPreferences.getString empId:" + sharedPreferences.getString("empId", "empId"));
		Log.d("sharedPreferences.getString propId:" + sharedPreferences.getString("propId", "propId"));
		AppParameters appParameters;
		try
		{
			if (Util.checkParametersExists(context))
			{
				appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				empId = appParameters.getEmpId();
				propId = appParameters.getPropId();
				if (empId.equals("") || empId.equals("1") || empId.equals("-1") || empId.equals("0"))
				{
					empId = sharedPreferences.getString("empId", "empId");
					Log.d("in incidenta empId" + empId);
				}
				if (propId.equals("") || propId.equals("1") || propId.equals("-1") || propId.equals("0"))
				{

					propId = sharedPreferences.getString("propId", "propId");
					Log.d("in incidenta propId" + propId);
				}
			}
		}
		catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Intent intent = getIntent();
		String photoId = intent.getStringExtra("photoPath");
		detailInception = intent.getStringExtra("classInception");
		cameraSource = intent.getStringExtra("cameraSource");
		takenFrom = intent.getStringExtra("takenFrom");
		takenFor = intent.getStringExtra("takenFor");
		last = intent.getStringExtra("last");
		ew = intent.getStringExtra("ew");
		Log.d("photostuff photoId:" + photoId);
		Log.d("photostuff detailInception:" + detailInception);
		Log.d("photostuff cameraSource:" + cameraSource);
		Log.d("photostuff takenFrom:" + takenFrom);
		Log.d("photostuff takenFor:" + takenFor);
		Log.d("photostuff ew:" + ew);
		// Toast.makeText(getApplicationContext(), "photoId:"+photoId,
		// Toast.LENGTH_LONG).show();
		mPreview = new Preview(this);
		setContentView(mPreview);
		// setContentView(R.layout.main);
		surfaceView = (SurfaceView) findViewById(R.id.camerapreview);
		if (surfaceView != null)
		{
			surfaceHolder = surfaceView.getHolder();
			surfaceHolder.addCallback(this);
			surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}
		controlInflater = LayoutInflater.from(getBaseContext());

		// Intent buttonIntent = new Intent(getApplicationContext(),
		// ButtonActivity.class);
		// startActivityForResult(buttonIntent,1);

		View viewControl = controlInflater.inflate(R.layout.control, null);
		Log.d("or:" + getResources().getConfiguration().orientation);

		dPhotoLayout = (RelativeLayout) viewControl.findViewById(R.id.photo_dialog_layout);
		dConfirmLayout = (RelativeLayout) viewControl.findViewById(R.id.photo_dialog_confirm_layout);
		dialogButtonSave = (Button) dPhotoLayout.findViewById(R.id.dialogButtonOK);
		dialogButtonRetake = (Button) dPhotoLayout.findViewById(R.id.dialogButtonNotOK);
		dialogConfirmYes = (Button) dConfirmLayout.findViewById(R.id.confirmButtonOK);
		dialogConfirmNo = (Button) dConfirmLayout.findViewById(R.id.confirmButtonNotOK);

		imageLayout = (ImageView) dPhotoLayout.findViewById(R.id.image);

		dPhotoLayout.setVisibility(View.INVISIBLE);
		dConfirmLayout.setVisibility(View.INVISIBLE);
		takepic = (Button) viewControl.findViewById(R.id.takepicture);
		Log.d("takephoto button:" + takepic);

		takepic.setBackgroundResource(R.drawable.take_photo_2);
		takepic.setWidth(300);
		takepic.setHeight(500);
		// takepic.getBackground().setAlpha(100);
		LayoutParams layoutParamsControl = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
		this.addContentView(viewControl, layoutParamsControl);

		CameraInfo cameraInfo = new CameraInfo();
		for (int i = 0; i < numberOfCameras; i++)
		{
			Camera.getCameraInfo(i, cameraInfo);
			if (cameraInfo.facing == CameraInfo.CAMERA_FACING_BACK)
			{
				defaultCameraId = i;
			}
		}

		// ------------picture preview--------------------
		// GalleryView gv = new GalleryView();
		// PicAdapter imgAdapt = gv.new PicAdapter(this);
		// imgAdapt.getFilePaths();
		// ArrayList<String> filePaths = imgAdapt.getFilePaths();
		// if (filePaths.size() > 0)
		// {
		// String lastPath = filePaths.get(filePaths.size() - 1);
		// displayImage(lastPath);
		// }

		mPreview.removeView(mPreview.mSurfaceView);
		mPreview.addView(mPreview.mSurfaceView);
		mCamera = Camera.open();
		cameraCurrentlyLocked = defaultCameraId;
		mPreview.setCamera(mCamera);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("camera onResume");
		Log.d("mCamera:" + mCamera);
		if (mCamera == null)
		{
			Log.d("mcamera e null");
			mPreview.removeView(mPreview.mSurfaceView);
			mPreview.addView(mPreview.mSurfaceView);
			mCamera = Camera.open();

			cameraCurrentlyLocked = defaultCameraId;
			mPreview.setCamera(mCamera);
			mCamera.startPreview();
			Log.d("after all");
		}
		if (mOrientationEventListener == null)
		{
			mOrientationEventListener = new OrientationEventListener(this, SensorManager.SENSOR_DELAY_NORMAL)
			{

				@Override
				public void onOrientationChanged(int orientation)
				{

					// determine our orientation based on sensor response
					int lastOrientation = mOrientation;
					Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
					if (display.getOrientation() == Surface.ROTATION_0)
					{ // landscape oriented devices
						if (orientation >= 315 || orientation < 45)
						{
							if (mOrientation != ORIENTATION_LANDSCAPE_NORMAL)
							{
								mOrientation = ORIENTATION_LANDSCAPE_NORMAL;
							}
						}
						else if (orientation < 315 && orientation >= 225)
						{
							if (mOrientation != ORIENTATION_PORTRAIT_INVERTED)
							{
								mOrientation = ORIENTATION_PORTRAIT_INVERTED;
							}
						}
						else if (orientation < 225 && orientation >= 135)
						{
							if (mOrientation != ORIENTATION_LANDSCAPE_INVERTED)
							{
								mOrientation = ORIENTATION_LANDSCAPE_INVERTED;
							}
						}
						else if (orientation < 135 && orientation > 45)
						{
							if (mOrientation != ORIENTATION_PORTRAIT_NORMAL)
							{
								mOrientation = ORIENTATION_PORTRAIT_NORMAL;
							}
						}
					}
					else
					{ // portrait oriented devices
						if (orientation >= 315 || orientation < 45)
						{
							if (mOrientation != ORIENTATION_PORTRAIT_NORMAL)
							{
								mOrientation = ORIENTATION_PORTRAIT_NORMAL;
							}
						}
						else if (orientation < 315 && orientation >= 225)
						{
							if (mOrientation != ORIENTATION_LANDSCAPE_NORMAL)
							{
								mOrientation = ORIENTATION_LANDSCAPE_NORMAL;
							}
						}
						else if (orientation < 225 && orientation >= 135)
						{
							if (mOrientation != ORIENTATION_PORTRAIT_INVERTED)
							{
								mOrientation = ORIENTATION_PORTRAIT_INVERTED;
							}
						}
						else if (orientation < 135 && orientation > 45)
						{
							if (mOrientation != ORIENTATION_LANDSCAPE_INVERTED)
							{
								mOrientation = ORIENTATION_LANDSCAPE_INVERTED;
							}
						}
					}

					if (lastOrientation != mOrientation)
					{
						changeRotation(mOrientation, lastOrientation);
					}
				}
			};
		}
		if (mOrientationEventListener.canDetectOrientation())
		{
			mOrientationEventListener.enable();
		}
	}

	@Override
	protected void onPause()
	{
		Log.d("on pAUSE");
		super.onPause();
		mOrientationEventListener.disable();
		// mPreview.removeView(mPreview.mSurfaceView);
		if (mCamera != null)
		{
			Log.d("release mcamera");
			mCamera.release();
			// mCamera.stopPreview();
			mPreview.setCamera(null);
			//
			mCamera = null;
		}
	}

	/**
	 * Performs required action to accommodate new orientation
	 * 
	 * @param orientation
	 * @param lastOrientation
	 */
	private void changeRotation(int orientation, int lastOrientation)
	{
		switch (orientation)
		{
		case ORIENTATION_PORTRAIT_NORMAL:
			// mSnapButton.setImageDrawable(getRotatedImage(android.R.drawable.ic_menu_camera,
			// 270));
			// mBackButton.setImageDrawable(getRotatedImage(android.R.drawable.ic_menu_revert,
			// 270));
			takepic.setWidth(300);
			takepic.setHeight(500);
			pozition = "portrait";
			takepic.setBackgroundResource(R.drawable.take_photo_2);
			dialogButtonSave.setRotation(270);
			dialogButtonRetake.setRotation(270);
			dialogConfirmYes.setRotation(270);
			dialogConfirmNo.setRotation(270);
			Log.d("CameraActivity Orientation = 90");
			break;
		case ORIENTATION_LANDSCAPE_NORMAL:
			// mSnapButton.setImageResource(android.R.drawable.ic_menu_camera);
			// mBackButton.setImageResource(android.R.drawable.ic_menu_revert);
			takepic.setWidth(500);
			takepic.setHeight(300);
			takepic.setBackgroundResource(R.drawable.take_photo_1);

			dialogButtonSave.setRotation(360);
			dialogButtonRetake.setRotation(360);
			dialogConfirmYes.setRotation(360);
			dialogConfirmNo.setRotation(360);
			pozition = "landscape";
			Log.d("CameraActivity Orientation = 0");
			break;
		case ORIENTATION_PORTRAIT_INVERTED:
			// mSnapButton.setImageDrawable(getRotatedImage(android.R.drawable.ic_menu_camera,
			// 90));
			// mBackButton.setImageDrawable(getRotatedImage(android.R.drawable.ic_menu_revert,
			// 90));
			pozition = "portrait";
			Log.d("CameraActivity Orientation = 270");
			break;
		case ORIENTATION_LANDSCAPE_INVERTED:
			// mSnapButton.setImageDrawable(getRotatedImage(android.R.drawable.ic_menu_camera,
			// 180));
			// mBackButton.setImageDrawable(getRotatedImage(android.R.drawable.ic_menu_revert,
			// 180));
			pozition = "landscape";
			Log.d("CameraActivity Orientation = 180");
			break;
		}
	}

	/**
	 * Rotates given Drawable
	 * 
	 * @param drawableId
	 *            Drawable Id to rotate
	 * @param degrees
	 *            Rotate drawable by Degrees
	 * @return Rotated Drawable
	 */
	private Drawable getRotatedImage(int drawableId, int degrees)
	{
		Bitmap original = BitmapFactory.decodeResource(getResources(), drawableId);
		Matrix matrix = new Matrix();
		matrix.postRotate(degrees);

		Bitmap rotated = Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight(), matrix, true);
		return new BitmapDrawable(rotated);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{

		super.onConfigurationChanged(newConfig);
		Log.d("onConfigurationChanged");
		if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
		{
			Log.d("ORIENTATION_PORTRAIT");
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
		else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
		{
			Log.d("ORIENTATION_LANDSCAPE");
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		}

	};

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		Log.d("on activityresult!!!!!!!!!!!!!!!!!!");
		if (requestCode == REQ_CAMERA_IMAGE && resultCode == RESULT_OK)
		{
			Log.d("on activityresult in if");
			String imgPath = data.getStringExtra(CameraActivity.EXTRA_IMAGE_PATH);
			Log.i("Got image path: " + imgPath);
			// displayImage(imgPath);
			// postImage(imgPath);
			// uploadImage(imgPath);
		}
		else if (requestCode == REQ_CAMERA_IMAGE && resultCode == RESULT_CANCELED)
		{
			Log.i("User didn't take an image");
		}
		else
		{
			Log.i("no req_camera");
		}
	}

	protected void clickResult(final String imgPath)
	{
		long backoff = 2000 + random.nextInt(1000);

		if (pozition == null)
		{
			Log.d("pozition = null ");
		}
		if (pozition == "null")
		{
			Log.d("pozition = 'null'");
		}
		Log.d("on clickResult!!!!!!!!!!!!!!!!!!" + pozition);
		// String imgPath =
		// data.getStringExtra(CameraActivity.EXTRA_IMAGE_PATH);
		Log.i("Got image path: " + imgPath);
		setLastImage(imgPath);
		// displayImage(imgPath);

		upLoadServerUri = uploadFilesServer;
		// custom dialog
		final Dialog dialog = new Dialog(context);
		final Dialog dialogConfirm = new Dialog(context);
		dialogConfirm.setContentView(R.layout.photo_dialog_confirm);
		dialog.setCanceledOnTouchOutside(false);
		dialogConfirm.setCanceledOnTouchOutside(false);
		dialog.setContentView(R.layout.photo_dialog);

		// dialog.setTitle("Title...");

		// set the custom dialog components - text, image and button
		RelativeLayout dialog1Layout = (RelativeLayout) dialog.findViewById(R.id.photo_dialog_layout);
		// dialog1Layout.setRotation(270.0f);
		ImageView image = (ImageView) dialog.findViewById(R.id.image);

		BitmapFactory.Options o = new BitmapFactory.Options();

		o.inSampleSize = 2;
		// o.outHeight = 900;
		// o.outWidth = 500;
		Bitmap b = BitmapFactory.decodeFile(imgPath, o);

		image.setImageBitmap(b);

		// image.setImageResource(R.drawable.ic_launcher);

		imageLayout.setImageBitmap(b);

		dPhotoLayout.setFocusable(true);
		dPhotoLayout.setVisibility(View.VISIBLE);
		if (pozition == "landscape")
		{
			dialogButtonSave.setRotation(360);
			dialogButtonRetake.setRotation(360);
			dialogConfirmYes.setRotation(360);
			dialogConfirmNo.setRotation(360);
		}

		dialogButtonSave.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				try
				{
					new Thread(new Runnable()
					{
						public void run()
						{
							runOnUiThread(new Runnable()
							{
								public void run()
								{
									Log.d("uploading started.....");
								}
							});

							uploadFile(imgPath);

							Util.savePhotoToXml(imageName, empId, propId, takenFrom, takenFor);
							Intent i = getIntent();
							String clsName = i.getStringExtra("clsName");
							String code = i.getStringExtra("code");
							if (code == null)
								code = i.getStringExtra("takenFor");

							String name = i.getStringExtra("name");
							String marked = i.getStringExtra("marked");
							ew = i.getStringExtra("ew");

							Log.d("normal:" + clsName + code + name + marked);
							Log.d("task stuff:" + clsName + code + name + marked);
							// if ()
							// {
							String quantity = i.getStringExtra("quantity");

							// }
							// if (clsName != null && code != null && name !=
							// null && marked != null)
							// {
							Log.d("in if:" + clsName + code + name + marked);
							Log.d(clsName + code + name + marked);
							String cale = imgPath.substring(imgPath.lastIndexOf("/") + 1, imgPath.length());
							Log.d("imag last index:" + imgPath.lastIndexOf("/"));
							Log.d("imag length:" + imgPath.length());
							Log.d("task stuff imgname:" + imageName);
							Log.d("task stuff cale:" + cale);
							Log.d("task stuff path:" + imgPath);
							Log.d("task stuff params:" + empId + "," + propId + "," + clsName + "," + code + "," + name + "," + marked + "," + quantity + "" + "," + imageName + "");
							Util.saveToUploadXml(empId, propId, clsName, code, name, marked, quantity, "", imageName, ew);
							// Util.saveToUploadXml(empId, propId, "tasks",
							// Tasks.getCode(), Tasks.getName(),
							// Tasks.getSelected(), qXml, "", "-1",
							// Tasks.getEw());
							// }
						}
					}).start();
					// return;
				}
				catch (Exception e)
				{
					Log.e("Failed to upload :" + e);
				}
				takepic.setVisibility(View.INVISIBLE);
				dPhotoLayout.setVisibility(View.INVISIBLE);
				dConfirmLayout.setFocusable(true);
				dConfirmLayout.setVisibility(View.VISIBLE);

				dialogConfirmYes.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						Log.d("dialog confirm dismiss");
						dConfirmLayout.setVisibility(View.INVISIBLE);
						takepic.setVisibility(View.VISIBLE);
					}
				});
				dialogConfirmNo.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						Log.d("dialog confirm back");
						Log.d("detailInception:" + detailInception);
						Log.d("last:" + last);
						if (detailInception != null)
						{
							if (detailInception.equals("TaskDetail"))
							{
								Intent i = new Intent(getApplicationContext(), TaskList.class);
								startActivity(i);
							}
						}
						if (cameraSource != null)
						{
							if (cameraSource.equals("enterProperty"))
							{
								// moveTaskToBack(true);147300
								Intent i = new Intent(getApplicationContext(), HS.class);
								startActivity(i);
							}
							if (cameraSource.equals("residentNotes"))
							{
								// Toast.makeText(getApplicationContext(),
								// "Thank you for today!",
								// Toast.LENGTH_LONG).show();
								Intent intent = new Intent(getApplicationContext(), Goodbye.class);
								startActivity(intent);
								Log.d("Goodbye nit activ");
								// try
								// {
								// Thread.sleep(5000);
								// }
								// catch (InterruptedException e)
								// {
								// // TODO Auto-generated catch block
								// e.printStackTrace();
								// }
								// moveTaskToBack(true);
								// Log.d("cod dupa move back init activ");
								// Intent i = new
								// Intent(getApplicationContext(),
								// InitActivity.class);
								// startActivity(i);
							}
						}
						if (last != null)
						{
							if (last.equals("true"))
							{
								Util.goNext(getApplicationContext(), takenFrom);
							}
						}
						finish();
						dConfirmLayout.setVisibility(View.INVISIBLE);
					}
				});

				// dConfirmLayout.setVisibility(View.INVISIBLE);
			}
		});
		dialogButtonRetake.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dPhotoLayout.setVisibility(View.INVISIBLE);
				takepic.setVisibility(View.VISIBLE);
			}
		});

		/*
		 * dialogButtonSave.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { dialogConfirm.show(); try {
		 * new Thread(new Runnable() { public void run() { runOnUiThread(new
		 * Runnable() { public void run() { Log.d("uploading started....."); }
		 * });
		 * 
		 * uploadFile(imgPath);
		 * 
		 * } }).start(); // return; } catch (Exception e) {
		 * Log.e("Failed to upload :" + e); } Button dialogConfirmYes = (Button)
		 * dialogConfirm.findViewById(R.id.dialogButtonOK); Button
		 * dialogConfirmNo = (Button)
		 * dialogConfirm.findViewById(R.id.dialogButtonNotOK);
		 * 
		 * dialogConfirmYes.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * Log.d("dialog confirm dismiss"); dialogConfirm.dismiss(); } });
		 * dialogConfirmNo.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { Log.d("dialog confirm back");
		 * Log.d("detailInception:" + detailInception); if (detailInception !=
		 * null) { if (detailInception.equals("TaskDetail")) { Intent i = new
		 * Intent(getApplicationContext(), TaskList.class); startActivity(i); }
		 * } if (cameraSource != null) { if
		 * (cameraSource.equals("enterProperty")) { Intent i = new
		 * Intent(getApplicationContext(), HS.class); startActivity(i); } if
		 * (cameraSource.equals("residentNotes")) {
		 * Toast.makeText(getApplicationContext(), "Goodbye",
		 * Toast.LENGTH_LONG).show(); moveTaskToBack(true);
		 * Log.d("cod dupa move back"); Intent i = new
		 * Intent(getApplicationContext(), MainActivity.class);
		 * startActivity(i); } } finish(); dialogConfirm.dismiss(); } });
		 * 
		 * dialog.dismiss(); } }); dialogButtonRetake.setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { dialog.dismiss(); } });
		 * dialog.show(); // for (int i = 0; i < 5; i++) // { //
		 * Log.d("Attempt #" + i + " to save");
		 */
	}

	public int uploadFile(String sourceFileUri)
	{

		String fileName = sourceFileUri;
		// idpropr.empId.
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inSampleSize = 2;
		Log.d("1" + new Date());
		Bitmap b = BitmapFactory.decodeFile(sourceFileUri, o);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		int w = b.getWidth();
		int h = b.getHeight();
		if (pozition.equals("portrait"))
		{
			// Setting post rotate to 90
			Matrix mtx = new Matrix();
			mtx.postRotate(90);
			// Rotating Bitmap
			Log.d("2" + new Date());
			Bitmap rotatedBMP = Bitmap.createBitmap(b, 0, 0, w, h, mtx, true);
			Log.d("3" + new Date());

			Log.d("4" + new Date());
			rotatedBMP.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			Log.d("5" + new Date());
		}
		else
		{
			b.compress(Bitmap.CompressFormat.JPEG, 100, stream);
		}
		InputStream is = new ByteArrayInputStream(stream.toByteArray());
		Log.d("is:" + is);
		File sourceFile = new File(sourceFileUri);
		Log.d("" + new Date());
		if (!sourceFile.isFile())
		{

			// dialog.dismiss();

			Log.d("Source File not exist :" + uploadFilePath + "" + uploadFileName);

			runOnUiThread(new Runnable()
			{
				public void run()
				{
					Log.d("Source File not exist :" + uploadFilePath + "" + uploadFileName);
				}
			});

			return 0;

		}
		else
		{
			try
			{
				InputStream fileInputStream = null;
				URL url;
				fileInputStream = is;// new FileInputStream(sourceFile);

				Log.d("fileInputStream:" + fileInputStream);
				Log.d("6" + new Date());
				url = new URL(upLoadServerUri);
				// if (serverResponseCode != 200)
				// {
				// open a URL connection to the Servlet

				// Open a HTTP connection to the URL
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true); // Allow Inputs
				conn.setDoOutput(true); // Allow Outputs
				conn.setUseCaches(false); // Don't use a Cached Copy
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Connection", "Keep-Alive");
				conn.setRequestProperty("ENCTYPE", "multipart/form-data");
				conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
				conn.setRequestProperty("uploaded_file", fileName);

				dos = new DataOutputStream(conn.getOutputStream());

				dos.writeBytes(twoHyphens + boundary + lineEnd);
				dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);

				dos.writeBytes(lineEnd);

				// create a buffer of maximum size
				bytesAvailable = fileInputStream.available();

				bufferSize = Math.min(bytesAvailable, maxBufferSize);
				buffer = new byte[bufferSize];

				// read file and write it into form...
				bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				while (bytesRead > 0)
				{

					dos.write(buffer, 0, bufferSize);
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);

				}

				// send multipart form data necesssary after file data...
				dos.writeBytes(lineEnd);
				dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

				// Responses from the server (code and message)
				serverResponseCode = conn.getResponseCode();
				String serverResponseMessage = conn.getResponseMessage();

				Log.d("HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

				if (serverResponseCode == 200)
				{

					runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(context, "File Upload Completed", Toast.LENGTH_SHORT).show();
							String msg = "File Upload Completed.";
							Log.d("7" + new Date());
							Log.d(msg);
						}
					});
				}

				fileInputStream.close();
				dos.flush();
				dos.close();
				// }
			}
			catch (MalformedURLException ex)
			{

				// dialog.dismiss();
				ex.printStackTrace();

				runOnUiThread(new Runnable()
				{
					public void run()
					{
						// messageText.setText("MalformedURLException Exception : check script url.");
						// Toast.makeText(UploadToServer.this,
						// "MalformedURLException", Toast.LENGTH_SHORT).show();
						Log.d("MalformedURLException Exception : check script url.");
					}
				});

				Log.d("Upload file to server error: " + ex);
			}
			catch (Exception e)
			{

				// dialog.dismiss();
				// e.printStackTrace();

				runOnUiThread(new Runnable()
				{
					public void run()
					{
						// messageText.setText("Got Exception : see logcat ");
						// Toast.makeText(UploadToServer.this,
						// "Got Exception : see logcat ",
						// Toast.LENGTH_SHORT).show();
						Log.d("Got Exception : see logcat: ");
					}
				});
				// return 2;
				Log.d("Upload file to server Exception : " + e);
			}
			finally
			{
				if (conn != null)
				{
					Log.d("finaly: ");
					conn.disconnect();
				}

			}
			// dialog.dismiss();
			return serverResponseCode;

		} // End else block
	}

	public Context getContext()
	{
		return getApplicationContext();
	}

	private void displayImage(String path)
	{
		Log.d("in display image");

		ImageView imageView = (ImageView) findViewById(R.id.image_view_captured_image);
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inSampleSize = 2;
		o.outHeight = 250;
		o.outWidth = 300;
		Bitmap b = BitmapFactory.decodeFile(path, o);
		imageView.setImageBitmap(b);
		imageView.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				Log.d("image clicked");
				// Toast.makeText(getApplicationContext(), "Photo click",
				// Toast.LENGTH_LONG).show();
				Intent intent = new Intent(CameraActivity.this, GalleryView.class);
				startActivity(intent);
			}
		});

	}

	public static File getOutputMediaFile()
	{
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		// File mediaStorageDir = new
		// File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
		// "CameraSpike");
		File mediaStorageDir = new File(photosLocation, photosFolder);
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists())
		{
			if (!mediaStorageDir.mkdirs())
			{
				Log.d("failed to create directory");
				return null;
			}
		}
		try
		{
			// CameraActivity ca=new CameraActivity();
			// ca.getContext();
			// if (Util.checkParametersExists(ca.getContext()))
			{
				AppParameters appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				empId = appParameters.getEmpId();
				propId = appParameters.getPropId();
			}
		}
		catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		imageName = empId + "_" + propId + "_IMG_" + timeStamp + ".jpg";
		File mediaFile = new File(mediaStorageDir.getPath() + File.separator + imageName);

		return mediaFile;
	}

	public static String getPhotoName()
	{
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		// File mediaStorageDir = new
		// File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
		// "CameraSpike");
		File mediaStorageDir = new File(photosLocation, photosFolder);
		// This location works best if you want the created images to be shared
		// between applications and persist after your app has been uninstalled.

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists())
		{
			if (!mediaStorageDir.mkdirs())
			{
				Log.d("failed to create directory");
				return null;
			}
		}

		// Create a media file name
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		File mediaFile = new File(mediaStorageDir.getPath() + File.separator + empId + "_" + propId + "_IMG_" + timeStamp + ".jpg");

		return empId + "_" + propId + "_IMG_" + timeStamp + ".jpg";
	}

	public static boolean saveToFile(byte[] bytes, File file)
	{
		boolean saved = false;
		try
		{

			FileOutputStream fos = new FileOutputStream(file);

			// fos = new FileOutputStream
			// (String.format(imageFilePath,System.currentTimeMillis()));

			fos.write(bytes);
			fos.close();
			saved = true;
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException", e);
		}
		catch (IOException e)
		{
			Log.e("IOException", e);
		}
		Log.d("saved");
		return saved;
	}

	public void onCaptureClick(View button)
	{
		// Take a picture with a callback when the photo has been created
		// Here you can add callbacks if you want to give feedback when the
		// picture is being taken

		mCamera.takePicture(null, null, this);
		// Log.d(getIntent());
		//
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera)
	{
		Log.d("Picture taken");
		String path = savePictureToFileSystem(data);
		File mediaStorageDir = new File(photosLocation, photosFolder);
		Log.d(path);
		setResult(path);
		clickResult(path);
		// finish();
		Log.d("before  startPreview");
		mCamera.startPreview();
		Log.d("after  startPreview");
		// onPause();
		// onResume();
	}

	private static String savePictureToFileSystem(byte[] data)
	{
		File file = getOutputMediaFile();

		saveToFile(data, file);
		return file.getAbsolutePath();
	}

	private void setResult(String path)
	{
		Intent intent = new Intent();

		// Intent intent = new Intent(this, Preview.class);
		intent.putExtra(EXTRA_IMAGE_PATH, path);
		// startActivityForResult(intent, REQ_CAMERA_IMAGE);
		setResult(RESULT_OK, intent);
	}

	public static Camera getCameraInstance()
	{
		Camera c = null;
		try
		{
			c = Camera.open(); // attempt to get a Camera instance
		}
		catch (Exception e)
		{
			Log.d("Exceptie:" + e);
		}
		return c;
	}

	protected void releaseCamera()
	{
		if (mCamera != null)
		{
			mPreview.setCamera(null);
			mCamera.release();
			mCamera = null;
		}
	}

	// @Override
	// protected void onResume()
	// {
	//
	// super.onResume();
	// Log.d("on resume");
	// mPreview.removeView(mPreview.mSurfaceView);
	// mPreview.addView(mPreview.mSurfaceView);
	// mCamera = Camera.open();
	// cameraCurrentlyLocked = defaultCameraId;
	// mPreview.setCamera(mCamera);
	// }

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
		Log.d(" surfaceChanged maine ");
		if (previewing)
		{
			mCamera.stopPreview();
			previewing = false;
		}

		if (mCamera != null)
		{
			try
			{
				mCamera.setPreviewDisplay(surfaceHolder);
				mCamera.startPreview();
				previewing = true;
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		Log.d(" surfaceCreated maine ");
		if (mCamera == null)
		{
			mCamera = Camera.open();
			try
			{
				mCamera.setPreviewDisplay(holder);

				// TODO test how much setPreviewCallbackWithBuffer is faster
				// mCamera.setPreviewCallback(this);
			}
			catch (IOException e)
			{
				mCamera.release();
				mCamera = null;
			}
		}
		// Log.d(" surfaceCreated maine ");
		// mCamera = Camera.open();
		// Camera.Parameters p = mCamera.getParameters();
		// p.set("jpeg-quality", 100);
		// p.set("orientation", "landscape");
		// p.set("rotation", 90);
		// p.setPictureFormat(PixelFormat.JPEG);
		// p.setPreviewSize(h, w);// here w h are reversed
		// mCamera.setParameters(p);
		// mCamera.setDisplayOrientation(90);

		// try
		// {
		// Log.d("surfaceCreated(), holder" + holder.toString());
		// mCamera = null;
		// mCamera = Camera.open();
		// Log.d("surfaceCreated(), mCamera=" + mCamera);
		// mCamera.setDisplayOrientation(90);
		// mCamera.setPreviewDisplay(holder);
		// Camera.Parameters params = mCamera.getParameters();
		// params.set("jpeg-quality", 72);
		// params.set("rotation", 90);
		// params.setPreviewSize(mPreview.getHeight(), mPreview.getWidth());
		// params.set("orientation", "portrait");
		// params.setPictureFormat(PixelFormat.JPEG);
		// mCamera.setParameters(params);
		// // createZoomlayout();
		//
		// }
		// catch (Exception e)
		// {
		// Toast.makeText(CameraActivity.this, " surfaceCreated " +
		// e.getMessage(), Toast.LENGTH_LONG).show();
		// e.printStackTrace();
		// }
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		Log.d(" surfaceDestroyed maine ");
		mCamera.stopPreview();
		mCamera.release();
		mCamera = null;
		previewing = false;
	}

	public String getLastImage()
	{
		return lastImage;
	}

	public void setLastImage(String lastImage)
	{
		this.lastImage = lastImage;
	}

	class Preview extends ViewGroup implements SurfaceHolder.Callback
	{
		private final String TAG = "Preview";

		SurfaceView mSurfaceView;
		SurfaceHolder mHolder;
		Size mPreviewSize;
		List<Size> mSupportedPreviewSizes;
		Camera mCamera;
		public Boolean isPreviewRunning;

		Preview(Context context)
		{
			super(context);
			isPreviewRunning = false;
			mSurfaceView = new SurfaceView(context);
			addView(mSurfaceView);
			// mSurfaceView.setBackgroundColor(Color.RED);
			mHolder = mSurfaceView.getHolder();
			mHolder.addCallback(this);
			mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		}

		public void setCamera(Camera camera)
		{
			Log.d("setCamera:" + camera);
			mCamera = camera;
			if (mCamera != null)
			{
				mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
				// requestLayout();

				// cameraPreview = (CameraPreview)
				// findViewById(R.id.camera_preview);
				// cameraPreview.init(camera);
			}
			// else
			// {
			// releaseCamera();
			// }
		}

		// public void switchCamera(Camera camera)
		// {
		// Log.d("switch");
		// setCamera(camera);
		// try
		// {
		// camera.setPreviewDisplay(mHolder);
		// }
		// catch (IOException exception)
		// {
		// Log.e("IOException caused by setPreviewDisplay()", exception);
		// }
		// Camera.Parameters parameters = camera.getParameters();
		// parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
		// requestLayout();
		//
		// camera.setParameters(parameters);
		// }

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
		{

			Log.d("on measure");
			int desiredWidth = 100;
			int desiredHeight = 100;

			int widthMode = MeasureSpec.getMode(widthMeasureSpec);
			int widthSize = MeasureSpec.getSize(widthMeasureSpec);
			int heightMode = MeasureSpec.getMode(heightMeasureSpec);
			int heightSize = MeasureSpec.getSize(heightMeasureSpec);

			int width;
			int height;

			// Measure Width
			if (widthMode == MeasureSpec.EXACTLY)
			{
				// Must be this size
				width = widthSize;
			}
			else if (widthMode == MeasureSpec.AT_MOST)
			{
				// Can't be bigger than...
				width = Math.min(desiredWidth, widthSize);
			}
			else
			{
				// Be whatever you want
				width = desiredWidth;
			}

			// Measure Height
			if (heightMode == MeasureSpec.EXACTLY)
			{
				// Must be this size
				height = heightSize;
			}
			else if (heightMode == MeasureSpec.AT_MOST)
			{
				// Can't be bigger than...
				height = Math.min(desiredHeight, heightSize);
			}
			else
			{
				// Be whatever you want
				height = desiredHeight;
			}
			if (mSupportedPreviewSizes != null)
			{
				mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
			}
			Log.d("width:" + width + " height:" + height);
			// MUST CALL THIS
			setMeasuredDimension(width, height);
		}

		// @Override
		// protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
		// {
		// Log.d("onMeasure:");
		// // Log.d("|-widthMeasureSpec:" + widthMeasureSpec);
		// // Log.d("heightMeasureSpec:" + heightMeasureSpec);
		// final int width = resolveSize(getSuggestedMinimumWidth(),
		// widthMeasureSpec);
		// final int height = resolveSize(getSuggestedMinimumHeight(),
		// heightMeasureSpec);
		// // final int height = 830;
		// // Log.d("|-width:" + width);
		// // Log.d("|-height:" + height);
		//
		//
		// if (mSupportedPreviewSizes != null)
		// {
		// mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width,
		// height);
		// }
		//
		// // Log.d("|-mPreviewSize:" + mPreviewSize);
		// // Log.d("|-mPreviewSize width:" + mPreviewSize.width);
		// // Log.d("|-mPreviewSize height:" + mPreviewSize.height);
		// setMeasuredDimension(width, height);
		// }

		@Override
		protected void onLayout(boolean changed, int l, int t, int r, int b)
		{
			if (changed && getChildCount() > 0)
			{
				Log.d("onLayout preview in if");
				// Log.d("|-mPreviewSize:" + mPreviewSize);
				// Log.d("|-mPreviewSize:" + mPreviewSize.width);
				// Log.d("|-mPreviewSize:" + mPreviewSize.height);
				final View child = getChildAt(0);
				Log.d("child:" + child);

				final int width = r - l;
				final int height = b - t;

				int previewWidth = width;
				int previewHeight = height;
				if (mPreviewSize != null)
				{
					Log.d("in if != null");
					previewWidth = mPreviewSize.width;
					previewHeight = mPreviewSize.height;
				}

				if (width * previewHeight > height * previewWidth)
				{
					final int scaledChildWidth = previewWidth * height / previewHeight;
					child.layout((width - scaledChildWidth) / 2, 0, (width + scaledChildWidth) / 2, height);
				}
				else
				{
					final int scaledChildHeight = previewHeight * width / previewWidth;
					child.layout(0, (height - scaledChildHeight) / 2, width, (height + scaledChildHeight) / 2);
				}
			}
		}

		public void surfaceCreated(SurfaceHolder holder)
		{
			Log.d("surfaceCreated");
			try
			{
				if (mCamera == null)
				{
					mCamera.startPreview();

				}
				mCamera.setPreviewDisplay(holder);
			}
			catch (IOException exception)
			{
				Log.e("IOException caused by setPreviewDisplay()", exception);
			}
		}

		// public void surfaceCreated(SurfaceHolder holder)
		// {
		// mCamera = Camera.open();
		// mCamera.setDisplayOrientation(90);
		// try
		// {
		// if (mCamera != null)
		// {
		// mCamera.setPreviewDisplay(holder);
		// mCamera.setPreviewCallback(new PreviewCallback()
		// {
		//
		// @Override
		// public void onPreviewFrame(byte[] data, Camera camera)
		// {
		// }
		// });
		// }
		//
		// }
		// catch (Exception e)
		// {
		// e.printStackTrace();
		// }
		// }
		public void previewCamera()
		{
			try
			{
				mCamera.setPreviewDisplay(mHolder);
				mCamera.startPreview();
				isPreviewRunning = true;
			}
			catch (Exception e)
			{
				Log.d("Cannot start preview", e);
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder)
		{
			Log.d("surfaceDestroyed");
			if (mCamera != null)
			{
				mCamera.stopPreview();
				mCamera.release();
				mCamera = null;
				previewing = false;
			}
		}

		private Size getOptimalPreviewSize(List<Size> sizes, int w, int h)
		{
			Log.d("getOptimalPreviewSize");
			// Log.d("w:" + w);
			// Log.d("h:" + h);

			final double ASPECT_TOLERANCE = 0.1;
			double targetRatio = (double) w / h;
			if (sizes == null)
				return null;

			Size optimalSize = null;
			double minDiff = Double.MAX_VALUE;

			int targetHeight = h;

			for (Size size : sizes)
			{
				Log.d("size:" + size);
				double ratio = (double) size.width / size.height;
				if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
					continue;
				if (Math.abs(size.height - targetHeight) < minDiff)
				{
					optimalSize = size;
					minDiff = Math.abs(size.height - targetHeight);
				}
			}

			if (optimalSize == null)
			{
				minDiff = Double.MAX_VALUE;
				for (Size size : sizes)
				{
					if (Math.abs(size.height - targetHeight) < minDiff)
					{
						optimalSize = size;
						minDiff = Math.abs(size.height - targetHeight);
					}
				}
			}
			return optimalSize;
		}

		@Override
		public void onConfigurationChanged(Configuration newConfig)
		{

			super.onConfigurationChanged(newConfig);
			Log.d("onConfigurationChanged1");
			// Toast.makeText(getApplicationContext(),
			// "onConfigurationChanged1 changed:", Toast.LENGTH_LONG).show();
			if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
			{
				Log.d("1ORIENTATION_PORTRAIT");
			}
			else if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			{
				Log.d("1ORIENTATION_LANDSCAPE");

			}

		};

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
		{
			Log.d("in surface changed");
			Log.d("button:" + takepic);
			// Toast.makeText(getApplicationContext(), "surface changed:",
			// Toast.LENGTH_LONG).show();
			// Log.d("in surface changed width "+width);
			// Log.d("in surface changed height "+height);
			// Log.d("in surface changed mPreviewSize widtht "+mPreviewSize.width);
			// Log.d("in surface changed mPreviewSize height "+mPreviewSize.height);

			// isPreviewRunning=true;
			if (isPreviewRunning)
			{
				if (mCamera != null)
					mCamera.stopPreview();
				else
				{
					Log.d("mCamera e null");
					mCamera = Camera.open();
				}
			}

			Parameters parameters = mCamera.getParameters();

			parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
			Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();

			if (display.getRotation() == Surface.ROTATION_0)
			{
				Log.d("surfaceChanged		1");

				// Log.d(mPreviewSize.width+","+mPreviewSize.height);
				// parameters.setPreviewSize(mPreviewSize.width,mPreviewSize.height);
				// mCamera.setDisplayOrientation(90);
			}

			if (display.getRotation() == Surface.ROTATION_90)
			{
				Log.d("surfaceChanged		2");

				// parameters.setPreviewSize(mPreviewSize.width,
				// mPreviewSize.height);
				// mCamera.setDisplayOrientation(270);
			}

			if (display.getRotation() == Surface.ROTATION_180)
			{
				Log.d("surfaceChanged		3");
				// parameters.setPreviewSize(mPreviewSize.width,
				// mPreviewSize.height);
				// mCamera.setDisplayOrientation(180);
			}

			if (display.getRotation() == Surface.ROTATION_270)
			{
				Log.d("surfaceChanged		4");
				// parameters.setPreviewSize(mPreviewSize.width,
				// mPreviewSize.height);
				// mCamera.setDisplayOrientation(90);
			}

			mCamera.setParameters(parameters);
			previewCamera();
		}

		// public void surfaceChanged(SurfaceHolder holder, int format, int w,
		// int h)
		// {
		// Log.d("surfacechanged");
		// Camera.Parameters parameters = mCamera.getParameters();
		// parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
		// requestLayout();
		//
		// mCamera.setParameters(parameters);
		// mCamera.startPreview();
		// }
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1)
	{
		Log.d("onAccuracyChanged");
		Log.d("arg0:" + arg0);
		Log.d("arg1:" + arg1);

	}

	@Override
	public void onSensorChanged(SensorEvent event)
	{
		Log.d("onSensorChanged");
		Log.d("event:" + event);

	}
}
