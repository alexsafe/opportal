package com.workers;

import static com.workers.util.CommonUtilities.SENDER_ID;
import static com.workers.util.CommonUtilities.displayMessage;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.regId;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.types.AppParameters;
import com.workers.util.CommonUtilities;
import com.workers.util.ServerUtilities;
import com.workers.util.Util;

public class GCMIntentService extends GCMBaseIntentService
{
	static String TAG = "GCM intent";
	String uniqId, empId = "0";

	public GCMIntentService()
	{
		super(SENDER_ID);
	}

	/**
	 * Method called on device registered
	 **/
	@Override
	protected void onRegistered(Context context, String registrationId)
	{
		Log.d("GCM intent", " GCMIntentService Device registered: regId = " + registrationId);
		displayMessage(context, "Your device registred with GCM");
		regId = registrationId;
		// Log.d("NAME", MainActivity.name);
		ServerUtilities.register(context, MainActivity.name, MainActivity.email, registrationId);
	}

	/**
	 * Method called on device un registred
	 * */
	@Override
	protected void onUnregistered(Context context, String registrationId)
	{
		Log.d(TAG, "GCMIntentService Device unregistered");
		displayMessage(context, getString(R.string.gcm_unregistered));
		ServerUtilities.unregister(context, registrationId);
	}

	/**
	 * Method called on Receiving a new message
	 * */

	@Override
	protected void onMessage(Context context, Intent intent)
	{
		RequestParams paramsRegister = new RequestParams();

		AppParameters appParameters;
		try
		{
			File parametersFile = new File(xmlFolderPath + "parameters.xml");
			if (parametersFile.exists())
			{
				appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				empId = appParameters.getEmpId();
				paramsRegister.put("activity", "message_received");
				paramsRegister.put("empId", appParameters.getEmpId());
				paramsRegister.put("propId", appParameters.getPropId());
				paramsRegister.put("get_extras", intent.getExtras().toString());

				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d(TAG, "AsyncHttpClient message_received op success!!!!:" + response);
						// processResponse(response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d(TAG, "AsyncHttpClient message_received op onFailure e !!!!:" + e);
						Log.d(TAG, "AsyncHttpClient message_received op onFailure response!!!!:" + response);
					}
				});
			}
			else
			{
				paramsRegister.put("activity", "message_received");
				paramsRegister.put("empId", "parametersNA");
				paramsRegister.put("propId", "parametersNA");
				paramsRegister.put("get_extras", intent.getExtras().toString());

				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d(TAG, "AsyncHttpClient message_received  else op success!!!!:" + response);
						// processResponse(response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d(TAG, "AsyncHttpClient message_received else op onFailure e !!!!:" + e);
						Log.d(TAG, "AsyncHttpClient message_received else op onFailure response!!!!:" + response);
					}
				});
			}
				
		}
		catch (XmlPullParserException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Log.i(TAG, " GCMIntentService Received message");
		Log.i(TAG, "intent extras" + intent.getExtras());
		String message = intent.getExtras().getString("price");
		uniqId = intent.getExtras().getString("uniqId");
		String data = intent.getExtras().getString("data");
		String photo = intent.getExtras().getString("photo");
		String update = intent.getExtras().getString("update");
		String delete_photos = intent.getExtras().getString("delete_photos");
		String delete_photo_id = intent.getExtras().getString("delete_photo_id");
		String delete_xml = intent.getExtras().getString("delete_xml");
		String delete_xml_less_params = intent.getExtras().getString("delete_xml_less_params");
		String delete_xml_id = intent.getExtras().getString("delete_xml_id");
		String delete = intent.getExtras().getString("delete");
		String delete_all = intent.getExtras().getString("delete_all");
		String message_type = intent.getExtras().getString("message_type");
		String upload_photos = intent.getExtras().getString("upload_photos");
		String upload_photo = intent.getExtras().getString("upload_photo");
		String delete_pictures = intent.getExtras().getString("delete_pictures");
		String delete_picture = intent.getExtras().getString("delete_picture");
		Log.d(TAG, "message_type:" + message_type);
		Log.d(TAG, "uniqId:" + uniqId);
		Log.d(TAG, "message:" + message);
		Log.d(TAG, "photo:" + photo);
		Log.d(TAG, "delete:" + delete);
		Log.d(TAG, "delete_photos:" + delete_photos);
		Log.d(TAG, "delete_all:" + delete_all);
		Log.d(TAG, "delete_xml:" + delete_xml);
		Log.d(TAG, "delete_xml_id:" + delete_xml_id);
		Log.d(TAG, "delete_photo_id:" + delete_photo_id);
		Log.d(TAG, "delete_xml_less_params:" + delete_xml_less_params);
		Log.d(TAG, "upload_photos:" + upload_photos);
		Log.d(TAG, "upload_photo:" + upload_photo);
		Log.d(TAG, "delete_pictures:" + delete_pictures);
		Log.d(TAG, "delete_picture:" + delete_picture);
		Log.d(TAG, "update:" + update);
		if (upload_photos != null)
		{
			File fileOrDirectory = new File(CommonUtilities.photosLocation + File.separator + CommonUtilities.photosFolder);
			for (File child : fileOrDirectory.listFiles())
			{
				// if (i<2)
				Util.uploadFile(CommonUtilities.photosLocation + File.separator + CommonUtilities.photosFolder + File.separator + child.getName(), context, uniqId);
			}
		}
		if (upload_photo != null)
		{
			Util.uploadFile(CommonUtilities.photosLocation + File.separator + CommonUtilities.photosFolder + File.separator + upload_photo, context, uniqId);
		}
		if (delete_pictures != null)
		{
			Util.DeleteRecursive(new File(CommonUtilities.photosLocation + File.separator + CommonUtilities.photosFolder + File.separator), uniqId);
		}
		if (delete_picture != null)
		{
			Util.DeleteRecursive(new File(CommonUtilities.photosLocation + File.separator + CommonUtilities.photosFolder + File.separator + delete_picture), uniqId);
		}
		if (delete_all != null)
		{
			Util.DeleteRecursive(new File(CommonUtilities.xmlFolderPath), uniqId);
		}
		if (delete != null)
		{
			Util.DeleteRecursive(new File(CommonUtilities.xmlFolderPath + delete), uniqId);
		}
		if (delete_xml != null)
		{
			Util.DeleteXmls(new File(CommonUtilities.xmlFolderPath), uniqId);
		}
		if (delete_xml_less_params != null)
		{
			Log.d(TAG, "delete_xml_less_params!!");
			Util.DeleteXmlsLessParams(new File(CommonUtilities.xmlFolderPath), uniqId);
		}
		if (delete_photos != null)
		{
			Log.d(TAG, "path:" + CommonUtilities.xmlFolderPath + "Images" + File.separator);
			Util.DeleteRecursive(new File(CommonUtilities.xmlFolderPath + "Images" + File.separator), uniqId);
		}
		if (delete_xml_id != null)
		{
			Log.d(TAG, "path:" + CommonUtilities.xmlFolderPath);
			Util.DeleteXmlsId(new File(CommonUtilities.xmlFolderPath), delete_xml_id, uniqId);
		}
		if (delete_photo_id != null)
		{
			Log.d(TAG, "path:" + CommonUtilities.xmlFolderPath + "Images" + File.separator);
			Util.DeletePhotoId(delete_photo_id, uniqId);
		}
		if (photo != null)
		{
			Intent serviceIntent = new Intent();
			serviceIntent.setClass(this, Download.class);
			// startService(new Intent(this, Download.class));
			serviceIntent.setAction("dld_photo");
			serviceIntent.putExtra("photo", photo);
			serviceIntent.putExtra("uniqId", uniqId);
			startService(serviceIntent);
		}
		if (update != null)
		{
			Log.d(TAG, " update: iii " + update);
			Intent serviceIntent = new Intent();
			serviceIntent.setClass(this, Download.class);
			// startService(new Intent(this, Download.class));
			serviceIntent.setAction("update");
			serviceIntent.putExtra("update", update);
			startService(serviceIntent);
		}
		if (message != null)
		{
			if (message.equals("create_shortcut"))
			{
				Log.i(TAG, "in if create_shortcut intent");

				Intent shortcutIntent = new Intent(getApplicationContext(), InitActivity.class);
				shortcutIntent.setAction(Intent.ACTION_MAIN);
				shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
				Intent addIntent = new Intent();
				addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);

				addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));

				addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
				getApplicationContext().sendBroadcast(addIntent, null);

				Intent shortcutIntent1 = new Intent(getApplicationContext(), InitActivity.class);

				shortcutIntent1.setAction(Intent.ACTION_MAIN);
				Intent addIntent1 = new Intent();
				addIntent1.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent1);
				addIntent1.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
				addIntent1.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.ic_launcher));

				addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
				getApplicationContext().sendBroadcast(addIntent);
			}
			if (message.equals("internet"))
			{
				Log.i(TAG, "in if staring intent");
				Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
				browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(browserIntent);
			}
			if (message.equals("end_program"))
			{
				Log.i(TAG, "in if end_program staring intent");
				Intent tasksIntent = new Intent(this, EndProgram.class);
				tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				tasksIntent.putExtra("uniqId", uniqId);
				tasksIntent.putExtra("end_program", "end_program");
				startActivity(tasksIntent);
			}
			if (message.equals("delete_cache"))
			{
				Log.i(TAG, "in if delete_cache staring intent");
				Util.DeleteRecursive(context.getCacheDir(), uniqId);
			}
			if (message.equals("delete_app_data"))
			{
				Log.i(TAG, "in if delete_cache staring intent");
				Util.DeleteRecursive(context.getFilesDir(), uniqId);
			}
			if (message.equals("clear_app_data"))
			{
				Log.i(TAG, "in if delete_cache staring intent");
				Util.DeleteRecursive(context.getCacheDir(), uniqId);
				Util.DeleteRecursive(context.getFilesDir(), uniqId);
			}
			if (message.equals("init"))
			{
				Log.i(TAG, "in if init intent");
				Intent tasksIntent = new Intent(this, InitActivity.class);
				tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				tasksIntent.putExtra("uniqId", uniqId);
				startActivity(tasksIntent);
			}
			if (message.equals("check_upload_date"))
			{
				File file = new File(CommonUtilities.xmlFolderPath + "upload.xml");

				paramsRegister.put("activity", "check_upload_date");
				paramsRegister.put("uniqId", uniqId);
				if (file.exists())
				{
					Date lastModified = new Date(file.lastModified());
					Log.d("SPIKE:", "lastModified:" + lastModified);
					String date = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss", lastModified).toString();
					// SimpleDateFormat sdf = new
					// SimpleDateFormat("yyyy-MM-dd HH:mm:ss", lastModified);
					Log.d("SPIKE:", "lastModified 1:" + date);
					paramsRegister.put("upload_date", date);
				}
				else
				{
					paramsRegister.put("upload_date", "0");
				}

				paramsRegister.put("empId", empId);
				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("Spike", "AsyncHttpClient isAvailable op success!!!!:" + response);
						// processResponse(response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("Spike", "AsyncHttpClient isAvailable op onFailure e !!!!:" + e);
						Log.d("Spike", "AsyncHttpClient isAvailable op onFailure response!!!!:" + response);
					}
				});
			}
			if (message.equals("tasks"))
			{
				Log.i(TAG, "in if staring intent");
				Intent tasksIntent = new Intent(this, TaskList.class);
				tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				tasksIntent.putExtra("uniqId", uniqId);
				startActivity(tasksIntent);
			}
			if (message.equals("download"))
			{
				Log.i(TAG, "in download staring intent");
				// Intent downloadIntent = new Intent(this, Download.class);
				// startActivity(downloadIntent);
				Intent serviceIntent = new Intent();
				serviceIntent.setClass(this, Download.class);
				serviceIntent.putExtra("uniqId", uniqId);
				startService(serviceIntent);
			}
			if (message.equals("upload"))
			{
				Log.i(TAG, "in upload staring intent");
				// Intent downloadIntent = new Intent(this, Download.class);
				// startActivity(downloadIntent);
				Intent serviceIntent = new Intent();
				serviceIntent.setClass(this, Upload.class);
				serviceIntent.putExtra("uniqId", uniqId);
				startService(serviceIntent);
			}
			if (message.equals("availability"))
			{
				Log.i(TAG, "in if staring intent");
				Intent avIntent = new Intent(this, Availability.class);
				avIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				avIntent.putExtra("uniqId", uniqId);
				startActivity(avIntent);
			}
			if (message.equals("get_version"))
			{
				PackageInfo pInfo1;
				String version = "1";
				try
				{
					pInfo1 = getPackageManager().getPackageInfo(getPackageName(), 0);
					version = pInfo1.versionName;
				}
				catch (NameNotFoundException e1)
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
				String imei = mngr.getDeviceId();

				paramsRegister.put("activity", "get_version");
				paramsRegister.put("uniqId", uniqId);
				paramsRegister.put("empId", empId);
				paramsRegister.put("version", version);
				paramsRegister.put("imei", imei);
				Log.d("Spike", "sent:" + uniqId + " " + empId + " " + version + " " + imei);
				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("Spike", "AsyncHttpClient get_version op success!!!!:" + response);
						// processResponse(response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("Spike", "AsyncHttpClient get_version op onFailure e !!!!:" + e);
						Log.d("Spike", "AsyncHttpClient get_version op onFailure response!!!!:" + response);
					}
				});
			}
			if (message.equals("property"))
			{
				Log.i(TAG, "in if staring intent");
				Intent propIntent = new Intent(this, PropertyDetails.class);
				propIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				propIntent.putExtra("uniqId", uniqId);
				startActivity(propIntent);
			}
			if (message.equals("enter"))
			{
				Log.i(TAG, "in if enter staring intent");
				Intent entIntent = new Intent(this, EnterProperty.class);
				entIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				entIntent.putExtra("uniqId", uniqId);
				startActivity(entIntent);
			}

			if (message.equals("hs"))
			{
				Log.i(TAG, "in if staring intent");
				Intent entIntent = new Intent(this, HS.class);
				entIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(entIntent);
			}
			if (message.equals("uninstall"))
			{
				Intent i = new Intent(Intent.ACTION_DELETE);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.setData(Uri.parse("package:com.workers"));
				startActivity(i);
			}
			if (message.equals("uninstall_delete"))
			{
				Log.d(TAG, "unsintall delete");
				Util.DeleteRecursive(new File(CommonUtilities.xmlFolderPath), uniqId);
				Intent i = new Intent(Intent.ACTION_DELETE);
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				i.setData(Uri.parse("package:com.workers"));
				startActivity(i);
			}
			displayMessage(context, message);
			// notifies user
			generateNotification(context, message);
		}
	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total)
	{
		Log.i(TAG, "GCMIntentService Received deleted messages notification");
		Log.d(TAG, "GCMIntentService Received deleted messages notification");

		String message = getString(R.string.gcm_deleted, total);
		displayMessage(context, message);
		// notifies user
		generateNotification(context, message);
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId)
	{
		Log.d(TAG, "GCMIntentService Received error: " + errorId);
		displayMessage(context, getString(R.string.gcm_error, errorId));
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId)
	{
		// log message
		Log.d(TAG, "GCMIntentService Received recoverable error: " + errorId);
		displayMessage(context, getString(R.string.gcm_recoverable_error, errorId));
		return super.onRecoverableError(context, errorId);
	}

	/**
	 * Issues a notification to inform the user that server has sent a message.
	 */
	private static void generateNotification(final Context context, String message)
	{
		Log.d(TAG, "generateNotification: " + message);
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);
		final AudioManager audiomanager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		Log.d(TAG, "ringer mode:" + audiomanager.getRingerMode());
		// if (audiomanager.getRingerMode()!=2)
		// {

		// }
		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent();// , MainActivity.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		NotificationCompat.Builder builder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_launcher).setContentTitle(title).setContentText(message);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		stackBuilder.addParentStack(MainActivity.class);
		stackBuilder.addNextIntent(notificationIntent);
		PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		builder.setContentIntent(resultPendingIntent);
		// builder.setOngoing(true);
		builder.setAutoCancel(true);

		// Vibration
		builder.setVibrate(new long[]
		{
				1000, 1000, 600, 1000, 600, 1000, 600, 1000, 600, 1000, 400, 1000, 500, 700, 300, 1000, 300, 1000, 300, 1000, 300, 2000, 300
		});
		builder.setVibrate(new long[]
		{
				1000, 1000, 600, 1000, 600, 2000
		});
		// builder.setVibrate(2000);

		// LED
		// builder.setLights(Color.RED, 200, 200);

		// Vibrator v = (Vibrator)
		// context.getSystemService(Context.VIBRATOR_SERVICE);
		// // Vibrate for 500 milliseconds
		// v.vibrate(500);

		builder.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.bell_message));
		Notification note = builder.build();
		// note.defaults |= Notification.DEFAULT_VIBRATE;
		// note.defaults |= Notification.DEFAULT_SOUND;

		NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		// if ((!(message.toLowerCase().equalsIgnoreCase("upload"))) &&
		// (!(message.toLowerCase().equalsIgnoreCase("download")))
		// && (!(message.toLowerCase().equalsIgnoreCase("hs"))) &&
		// (!(message.toLowerCase().equalsIgnoreCase("uninstall")))
		// && (!(message.toLowerCase().equalsIgnoreCase("uninstall_delete"))) &&
		// (!(message.toLowerCase().equalsIgnoreCase("check_upload_date")))
		// && (!(message.toLowerCase().equalsIgnoreCase("init")))
		// )
		Log.d(TAG, "message e:" + message);
		if (!message.equals(null) || !message.equals(""))
		{
			if (message.toLowerCase().equals("availability") || message.toLowerCase().equals("property") || message.toLowerCase().equals("enter") || message.toLowerCase().equals("tasks"))
			{
				Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_MUSIC):" + audiomanager.getStreamVolume(AudioManager.STREAM_MUSIC));
				Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM):" + audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM));
				Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_ALARM):" + audiomanager.getStreamVolume(AudioManager.STREAM_ALARM));
				Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION):" + audiomanager.getStreamVolume(AudioManager.STREAM_VOICE_CALL));

				audiomanager.setRingerMode(2);
				audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 3, 0);

				SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
				String mode = sharedPreferences.getString("initRingerMode", "mode");
				String volume = sharedPreferences.getString("initRingerVolume", "volume");
				Log.d(TAG, "mode:" + mode);
				Log.d(TAG, "volume:" + volume);
				if (!mode.equals("mode"))
				{
					audiomanager.setRingerMode(Integer.parseInt(mode));
					audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, Integer.parseInt(volume), 0);
				}
				Handler handler = new Handler();
				Log.d(TAG, "init after set getRingerMode():" + audiomanager.getRingerMode());
				Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);:" + audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION));
				handler.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						Log.d(TAG, "delayed 2");
						audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 1, 0);
					}

				}, 2000);
				handler.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						Log.d(TAG, "delayed 2");
						// audiomanager.setRingerMode(2);
						audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 2, 0);
					}

				}, 4000);
				handler.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						Log.d(TAG, "delayed 3");// audiomanager.setRingerMode(2);
						audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 3, 0);
					}

				}, 9000);
				handler.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						Log.d(TAG, "delayed 4");// audiomanager.setRingerMode(2);
						audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 4, 0);
					}

				}, 14000);
				handler.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						Log.d(TAG, "delayed sfarsit");
						SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
						String mode = sharedPreferences.getString("initRingerMode", "mode");
						String volume = sharedPreferences.getString("initRingerVolume", "volume");
						Log.d(TAG, "mode:" + mode);
						Log.d(TAG, "volume:" + volume);
						if (!mode.equals("mode"))
						{
							audiomanager.setRingerMode(Integer.parseInt(mode));
							audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, Integer.parseInt(volume), 0);
						}
						// audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,
						// initVolume, 0);
					}

				}, 17000);

				// Log.d(TAG,"audiomanager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)::"+audiomanager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
				// Play default notification sound
				// notification.defaults |= Notification.DEFAULT_SOUND;

				// notification.sound = Uri.parse("android.resource://" +
				// context.getPackageName() + "your_sound_file_name.mp3");

				// Vibrate if vibrate is enabled
				// notification.defaults |= Notification.DEFAULT_VIBRATE;

				notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.bell_message);
				// notificationManager.notify(0, notification);
				mNotificationManager.notify(0, note);
			}
		}

	}
}
