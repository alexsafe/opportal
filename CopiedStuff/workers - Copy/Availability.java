package com.workers;

import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.types.AppParameters;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class Availability<postData> extends Activity
{
	private Button btnDisplay, yes, no;
	private LinearLayout availLayout;
	private EditText editOther;
	String employee = "0";

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.availability);
		final HideSysUi hideUi = new HideSysUi(getWindow(), getApplicationContext());
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "availability");
		editor1.commit();

		AudioManager audiomanager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor11 = sharedPrefs.edit();
		editor11.putString("initRingerMode", audiomanager.getRingerMode() + "");
		editor11.putString("initRingerVolume", audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM) + "");
		Log.d("availability audiomanager.getRingerMode():" + audiomanager.getRingerMode());
		Log.d("availability audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM):" + audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM));
		editor11.commit();

		availLayout = (LinearLayout) findViewById(R.id.availLayout);
		btnDisplay = (Button) findViewById(R.id.btnDisplay);
		yes = (Button) findViewById(R.id.radioAvailable);
		no = (Button) findViewById(R.id.radioNotAvailable);

		btnDisplay.setVisibility(View.INVISIBLE);
		editOther = (EditText) findViewById(R.id.otherText);
		editOther.setVisibility(View.INVISIBLE);
		editOther.setRawInputType(InputType.TYPE_NULL);
		editOther.setFocusable(true);
		AppParameters appParameters;
		Log.d("empId din details: " + empId);
		try
		{
			if (Util.checkParametersExists(getApplicationContext()))
			{
				appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				empId = appParameters.getEmpId();
				employee = empId;
				String title = (String) getTitle();
				String name = appParameters.getEmpName();
				final Spannable spanYou = new SpannableString(title + " " + name);
				spanYou.setSpan(new ForegroundColorSpan(Color.GREEN), title.length(), spanYou.length(), 0);
				spanYou.setSpan(new RelativeSizeSpan(0.7f), title.length(), spanYou.length(), 0);
				spanYou.setSpan(new StyleSpan(Typeface.ITALIC), title.length(), spanYou.length(), 0);
				setTitle(spanYou);
				Log.d("empId din avability: " + empId);
			}
		}
		catch (XmlPullParserException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		RequestParams paramsRegister = new RequestParams();
		paramsRegister.put("activity", "enter_availability");
		paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
		paramsRegister.put("empId", employee);

		AsyncHttpClient client = new AsyncHttpClient();
		client.post(op, paramsRegister, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("AsyncHttpClient enter_availability op success!!!!:" + response);
				// processResponse(response);
			}

			@Override
			public void onFailure(Throwable e, String response)
			{
				Log.d("AsyncHttpClient enter_availability op onFailure e !!!!:" + e);
				Log.d("AsyncHttpClient enter_availability op onFailure response!!!!:" + response);
			}
		});
		yes.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Log.d("on click available");
				Log.d("empId:" + employee);
				RequestParams paramsRegister = new RequestParams();
				paramsRegister.put("activity", "isAvailable");
				paramsRegister.put("available", "yes");
				paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
				paramsRegister.put("empId", employee);

				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("AsyncHttpClient isAvailable op success!!!!:" + response);
						// processResponse(response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("AsyncHttpClient isAvailable op onFailure e !!!!:" + e);
						Log.d("AsyncHttpClient isAvailable op onFailure response!!!!:" + response);
					}
				});
				// Intent i = new Intent(getApplicationContext(),
				// EnterProperty.class);
				// startActivity(i);
				moveTaskToBack(true);
			}
		});
		no.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				btnDisplay.setVisibility(View.VISIBLE);
				enableSoftInputFromAppearing(editOther);
				// editOther.setVisibility(View.VISIBLE);
			}
		});

		btnDisplay.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Log.d("send email");
				Log.d("editOther:" + editOther.getText());
				Util.saveToUploadXml(empId, propId, "availability", "", "", "", "-1", editOther.getText().toString(), "-1", "-1");
				send_message(editOther.getText().toString());
				// Intent i = new Intent(getApplicationContext(),
				// EnterProperty.class);
				// startActivity(i);
				moveTaskToBack(true);
			}

		});
		availLayout.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe layout availability:");
				new HideSysUi(getWindow(), getApplicationContext());
				NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancelAll();
			}
		});

	}

	public void send_message(String messageBody)
	{
		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("activity", "isAvailable");
		params.put("available", "no");
		params.put("empId", employee);
		params.put("message", messageBody);

		client.post(op, params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("success!!!!");
				Toast.makeText(Availability.this, "Mesage sent!", Toast.LENGTH_SHORT).show();
			}

			@Override
			public void onFailure(Throwable e, String response)
			{
				Log.d("AsyncHttpClient message op onFailure e !!!!:" + e);
				Log.d("AsyncHttpClient message op onFailure response!!!!:" + response);
			}
		});
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow(), getApplicationContext());
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d("on pause enter prop");
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "availability");
		editor1.commit();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		// Save UI state changes to the savedInstanceState.
		// This bundle will be passed to onCreate if the process is
		// killed and restarted.
		savedInstanceState.putBoolean("myBoolean", true);
		savedInstanceState.putDouble("myDouble", 1.9);
		savedInstanceState.putInt("myInt", 1);
		savedInstanceState.putString("myString", "availability");
		// etc.
		super.onSaveInstanceState(savedInstanceState);
	}

	public void sendEmail()
	{
		// String to = textTo.getText().toString();
		// String subject = textSubject.getText().toString();
		// String message = textMessage.getText().toString();
		String to = "a_capitaneanu@hotmail.com";
		String subject = "adada";
		String message = "vezi mai sus";

		// String mail_server =
		// "http://pillar.pilon.co.uk/Example/xmpphp/gcm_server_php/upload_example.php";
		// Ur url;
		// url = new URL(mail_server);
		// String boundary = "*****";
		// HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		// conn.setDoInput(true); // Allow Inputs
		// conn.setDoOutput(true); // Allow Outputs
		// conn.setUseCaches(false); // Don't use a Cached Copy
		// conn.setRequestMethod("POST");
		// conn.setRequestProperty("Connection", "Keep-Alive");
		// conn.setRequestProperty("ENCTYPE", "multipart/form-data");
		// conn.setRequestProperty("Content-Type",
		// "multipart/form-data;boundary=" + boundary);

		Intent email = new Intent(Intent.ACTION_SEND);
		email.putExtra(Intent.EXTRA_EMAIL, new String[]
		{
			to
		});
		// email.putExtra(Intent.EXTRA_CC, new String[]{ to});
		// email.putExtra(Intent.EXTRA_BCC, new String[]{to});
		email.putExtra(Intent.EXTRA_SUBJECT, subject);
		email.putExtra(Intent.EXTRA_TEXT, message);

		// need this to prompts email client only
		email.setType("message/rfc822");

		startActivity(Intent.createChooser(email, "Choose an Email client :"));

	}

	public static void disableSoftInputFromAppearing(EditText editText)
	{
		Log.d("other disableSoftInputFromAppearing");
		editText.setEnabled(false);
		editText.setRawInputType(InputType.TYPE_NULL);
		editText.setFocusable(true);
		editText.setVisibility(View.INVISIBLE);
	}

	public static void enableSoftInputFromAppearing(EditText editText)
	{
		Log.d("other enableSoftInputFromAppearing");
		editText.setVisibility(View.VISIBLE);
		editText.setEnabled(true);
		editText.setInputType(InputType.TYPE_CLASS_TEXT);
		editText.setFocusable(true);

	}

	@Override
	public void onBackPressed()
	{
		// moveTaskToBack(true);

	}
}
