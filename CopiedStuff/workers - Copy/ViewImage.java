package com.workers;

import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.util.ConnectionDetector;
import com.workers.util.GifView;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.TouchImageView;
import com.workers.util.Util;

public class ViewImage extends Activity
{
	ImageView thinking;
	TouchImageView img;
	TextView loading;
	RelativeLayout zoomLayout;

	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.zoom_image);
		new HideSysUi(getWindow());
				
//		gifView=new GifView(this);
		zoomLayout = (RelativeLayout) findViewById(R.id.zoom_image_layout);
		loading= (TextView) findViewById(R.id.loading);
		Intent i = getIntent();
		String res = i.getStringExtra("image");
		String uniqId = i.getStringExtra("uniqId");
		Log.d("parameter sent:" + res);
		// int resID = getResources().getIdentifier(res, "drawable",
		// getPackageName());
		img = (TouchImageView) findViewById(R.id.img);
		loading.setVisibility(View.VISIBLE);
		img.setVisibility(View.INVISIBLE);
		File parametersFile = new File(xmlFolderPath + "Images/FullSize/" + res);
		Log.d("res file:"+xmlFolderPath + "Images/FullSize/" + res);
		String path = xmlFolderPath + "Images/FullSize/" + res;
		if (!(parametersFile.exists()))
		{
			// Util.getPhotos(path,"FullSize");
			ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
			if (!cd.isConnectingToInternet())
			{
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
				// set title
				alertDialogBuilder.setTitle("Internet Connection Error");
				// set dialog message
				alertDialogBuilder.setMessage("Please connect to working Internet connection").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int id)
					{
						// if this button is clicked, close
						// current activity
						ViewImage.this.finish();
					}
				});
				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();
				// show it
				alertDialog.show();
				return;
			}
			new DownloadTask().execute(res, "FullSize",uniqId);
		}
		else
		{
			showPhoto("full",path);

		}
	}

	public void showPhoto(String typePath,String path)
	{
		String realPath=path;
		if (typePath.equals("file"))
			realPath=xmlFolderPath + "Images/FullSize/"+path;
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inSampleSize = 3;
		o.outHeight = 300;
		o.outWidth = 300;
		Bitmap b = BitmapFactory.decodeFile(realPath, o);
		img.setImageBitmap(b);
		img.setVisibility(View.VISIBLE);
		loading.setVisibility(View.INVISIBLE);
		// img.setImageResource(resID);
		img.setMaxZoom(4);		
	}

	public void close(View v)
	{
		Log.d("close");
		finish();
		Intent i = new Intent(getApplicationContext(), PropertyDetails.class);
		startActivity(i);
		// ImageView img = (ImageView) v;
		// Log.d("img:"+img.getDrawable());
		// fullPreview.setImageDrawable(img.getDrawable());
		// fullPreview.setVisibility(View.INVISIBLE);
		// close.setVisibility(View.INVISIBLE);
		// fullPreview.setI
	}

	public class DownloadTask extends AsyncTask<String, Void, Boolean>
	{

		String pas;

		@Override
		protected Boolean doInBackground(String... urls)
		{
			Log.d("dld");
			Log.d("urls:" + urls);
			Log.d("urls[0]:" + urls[0]);
			Log.d("urls[1]:" + urls[1]);
			Log.d("urls[2]:" + urls[2]);
			pas = urls[0];
			Boolean ret = Util.savePhotos(urls[0], urls[1], urls[2]);
			//loading.setVisibility(View.VISIBLE);
			return ret;
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			Log.d("show rez:" + result);
			if (result)
			{
				showPhoto("file",pas);
			}
//			RequestParams paramsRegister = new RequestParams();
//			paramsRegister.put("activity", "ack_photos_saved");
//			paramsRegister.put("photo", pas);
//			AsyncHttpClient client = new AsyncHttpClient();
//			client.post(op, paramsRegister, new AsyncHttpResponseHandler()
//			{
//				@Override
//				public void onSuccess(String response)
//				{
//					Log.d("AsyncHttpClient ack_photos_saved view image op success!!!!:" + response);
//				}
//				@Override
//				public void onFailure(Throwable e, String response)
//				{
//					Log.d("AsyncHttpClient ack_photos_saved view image op onFailure e !!!!:" + e);
//					Log.d("AsyncHttpClient ack_photos_saved view image op onFailure response!!!!:" + response);
//				}
//			});	
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow());
	}
}
