package com.workers;


import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.nio.charset.UnmappableCharacterException;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.types.AppParameters;
import com.workers.types.Concern;
import com.workers.types.Tasks;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class ListToggleAdapter extends ArrayAdapter<Concern>
{
	ListToggleAdapter dataAdapter = null;
	private final Context context;

	public ArrayList<Concern> TasksList;
	public List<Concern> ListToSave = new ArrayList<Concern>();
	public ArrayList<Integer> okIds = new ArrayList<Integer>();
	public ArrayList<Integer> notOkIds = new ArrayList<Integer>();
	public ArrayList<Integer> nAIds = new ArrayList<Integer>();
	public ArrayList<Integer> okEdIds = new ArrayList<Integer>();
	public ArrayList<Integer> notOkEdIds = new ArrayList<Integer>();
	public ArrayList<Integer> naEdIds = new ArrayList<Integer>();
	int marked, unmarked = 0, listSize = 0;
	public String clsName, className = "";

	public ListToggleAdapter(Context context, int textViewResourceId, ArrayList<Concern> TasksList)
	{

		super(context, textViewResourceId, TasksList);
		this.context = context;
		this.ListToSave = new ArrayList<Concern>();
		this.TasksList = new ArrayList<Concern>();

		this.TasksList.addAll(TasksList);
		this.ListToSave.addAll(TasksList);
	}

	private class ViewHolder
	{
		TextView item;
		TextView marked;
		Button ok;
		Button notOk;
		Button nA;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent)
	{

		ViewHolder holder = null;
		RelativeLayout lists;

		className = context.getClass().getSimpleName();
		Log.d("class:" + context.getClass().getSimpleName());
		if (convertView == null)
		{

			LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			lists = (RelativeLayout) vi.inflate(R.layout.list_hs, null);
			// convertView = vi.inflate(R.layout.list_hs, null);

		}
		else
		{
			// holder = (ViewHolder) convertView.getTag();
			lists = (RelativeLayout) convertView;
		}

		// holder.name.setText(Tasks.getName());
		// holder.name.setChecked(Tasks.isSelected());
		// holder.name.setTag(Tasks);
		holder = new ViewHolder();

		holder.item = (TextView) lists.findViewById(R.id.itemQ);
		// holder.marked = (TextView)
		// lists.findViewById(R.id.itemMarked);
		holder.ok = (Button) lists.findViewById(R.id.buttonYes);
		holder.notOk = (Button) lists.findViewById(R.id.buttonNo);
		holder.nA = (Button) lists.findViewById(R.id.buttonNA);
		className = getContext().getClass().getSimpleName();
		if (className.equals("QualityWG"))
			holder.nA.setText("Non existent");
		lists.setTag(holder);
		holder.ok.setTag(holder);
		final RelativeLayout listConcerns = (RelativeLayout) lists.findViewById(R.id.listConcerns);
		final TextView text = (TextView) lists.findViewById(R.id.itemQ);
		final Button butOk = (Button) lists.findViewById(R.id.buttonYes);
		final Button butNotOk = (Button) lists.findViewById(R.id.buttonNo);
		final Button butNA = (Button) lists.findViewById(R.id.buttonNA);
		listSize = TasksList.size();
		Log.d("TasksList:" + TasksList);
		Log.d("TasksList size:" + TasksList.size());
		Log.d("position list adapter:" + position);
		unmarked = TasksList.size();
		if (Util.checkParametersExists(context))
		{
			try
			{
				AppParameters appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				empId=appParameters.getEmpId();
				propId=appParameters.getPropId();
			}
			catch (Exception e)
			{
				Log.d("ListToggleAdapter appparams exception : " + e);
			}
		}
		// for (Concern taskItem : TasksList)
		for (int i = 0; i < TasksList.size(); i++)
		{
			Log.d("item:" + TasksList.get(i).getName() + ": " + TasksList.get(i).getMarked());
			Log.d("|---:" + i + ": " + position);
			// listConcerns.setBackgroundColor(Color.YELLOW);
			// listConcerns.getBackground().setAlpha(151);
			if (TasksList.get(i).getMarked().toLowerCase().equals("1".toLowerCase()))
			{

				Log.d("ok");
				// if (okEdIds.contains(position))
				// {
				// okEdIds.remove(position);
				// }
				// else
				// {

				if (position == i)
				{
					Log.d("position ok:" + position);
					okEdIds.add(position);
				}
				// holder.item.setBackgroundColor(Color.GREEN);
				// parent.getChildAt(position).setBackgroundColor(Color.GREEN);
				// butOk.setBackgroundResource(R.drawable.custom_ok_button_clicked);
				// listConcerns.setBackgroundColor(Color.GREEN);
				// listConcerns.getBackground().setAlpha(91);
			}
			if (TasksList.get(i).getMarked().toLowerCase().equals("3".toLowerCase()))
			{
				Log.d("not ok");

				if (position == i)
				{
					Log.d("position nok:" + position);
					notOkEdIds.add(position);
				}

				// holder.item.setBackgroundColor(Color.RED);
				// parent.getChildAt(position).setBackgroundColor(Color.RED);
				// butNotOk.setBackgroundResource(R.drawable.custom_not_ok_button_clicked);
				// listConcerns.setBackgroundColor(Color.RED);
				// listConcerns.getBackground().setAlpha(151);
			}
			if (TasksList.get(i).getMarked().toLowerCase().equals("2".toLowerCase()))
			{
				Log.d("na");
				if (position == i)
				{
					Log.d("position na:" + position);
					naEdIds.add(position);
				}
			}
		}
		Concern Tasks = TasksList.get(position);

		clsName = getContext().getClass().getSimpleName().toLowerCase();
		Log.d("class name:" + clsName);
		holder.item.setText(" " + Tasks.getName() + " ");
		holder.ok.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				Log.d("ok");
				Log.d("pe pozitia:" + position);
				new HideSysUi(((Activity) context).getWindow());
				Log.d("okIds:" + okIds);
				Log.d("naIds:" + nAIds);
				if (okIds.contains(position))
				{
					int poz = okIds.indexOf(position);
					okIds.remove(poz);
				}
				else
				{
					okIds.add(position);
				}

				ViewHolder holder1 = (ViewHolder) v.getTag();

				butOk.setBackgroundResource(R.drawable.custom_ok_button_clicked);
				butNotOk.setBackgroundResource(R.drawable.custom_not_ok_button);
				holder1.item.setBackgroundColor(Color.parseColor("#208208"));
				holder1.item.setTextColor(Color.parseColor("#ffffff"));

				Concern Tasks = TasksList.get(position);

				if (Tasks.getMarked().equals("0"))
				{
					Log.d("marked new");
					marked++;
				}
				Tasks.setMarked("1");
				TasksList.set(position, Tasks);

				Log.d("position:" + position);
				Log.d("global: empId" + empId + " propId:" + propId);
				String className = getContext().getClass().getSimpleName();
				Util.saveListToXml(TasksList, clsName);
				Util.saveToUploadXml(empId, propId, clsName, Tasks.getCode(), Tasks.getName(), Tasks.getMarked(), "-1", "", "-1", "-1");
				Log.d("marked:" + marked + " listSize:" + listSize + " unmarked:" + unmarked);

				int marcate = 0;
				for (Concern taskToVerify : TasksList)
				{

					if (!(taskToVerify.getMarked().equals("0")))
					{
						unmarked--;
						marcate++;
						// goNext(className);
					}

					Log.d("macate taskmarked:" + taskToVerify.getMarked() + taskToVerify.getName());
					Log.d("macate:" + marcate);
					Log.d("macate TasksList.size():" + TasksList.size());

				}

				if (marcate == TasksList.size())
				{
					goNext(className);
				}

				if (marked == listSize)
				{
					Log.d("buckleup din adapter ok");
					Log.d("buckleup for class:" + className);
					goNext(className);
				}

			}
		});
		holder.notOk.setTag(holder);
		holder.notOk.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				Log.d("not ok");
				Log.d("pe pozitia:" + position);
				new HideSysUi(((Activity) context).getWindow());
				if (notOkIds.contains(position))
				{
					int poz = notOkIds.indexOf(position);
					notOkIds.remove(poz);
				}
				else
				{
					notOkIds.add(position);
				}
				ViewHolder holder1 = (ViewHolder) v.getTag();

				butOk.setBackgroundResource(R.drawable.custom_ok_button);
				butNotOk.setBackgroundResource(R.drawable.custom_not_ok_button_clicked);
				holder1.item.setBackgroundColor(Color.parseColor("#a11005"));
				holder1.item.setTextColor(Color.parseColor("#ffffff"));

				Concern Tasks = TasksList.get(position);

				if (Tasks.getMarked().equals("0"))
				{
					Log.d("marked new");
					marked++;
				}
				Tasks.setMarked("3");
				String className = getContext().getClass().getSimpleName();
				// String itemName = holder1.item.getText().toString();
				String itemName = Tasks.getCode().toString();
				// sendPost(position, "not ok");
				TasksList.set(position, Tasks);

				Util.saveListToXml(TasksList, clsName);
				//Util.saveToUploadXml(empId, propId, clsName, Tasks.getCode(), Tasks.getName(), Tasks.getMarked(), "-1", "", "-1", "-1");
				Log.d("position:" + position);

				Log.d("marked:" + marked + " listSize:" + listSize + " unmarked:" + unmarked);
				int marcate = 0;
				for (Concern taskToVerify : TasksList)
				{
					if (!(taskToVerify.getMarked().equals("0")))
					{
						unmarked--;
						marcate++;
						// goNext(className);
					}
				}
				if (marcate == TasksList.size())
				{
					goNext(className);
				}
				Util.saveToUploadXml(empId, propId, clsName, Tasks.getCode(), Tasks.getName(), Tasks.getMarked(), "-1", "", "-1", "-1");
				Intent i = new Intent(getContext(), CameraActivity.class);
				i.putExtra("clsName", clsName);
				i.putExtra("code", Tasks.getCode());
				i.putExtra("name", Tasks.getName());
				i.putExtra("quantity", "-1");
				i.putExtra("ew", "-1");
				i.putExtra("marked", Tasks.getMarked());
				if (marked == listSize)
				{
					Log.d("buckleup din adapter not ok");
					Log.d("buckleup for class:" + className);

					i.putExtra("photoPath", className + "_" + itemName);
					i.putExtra("takenFrom", className);
					i.putExtra("takenFor", itemName);
					i.putExtra("last", "true");

					// goNext(className);
				}
				else
				{
					i.putExtra("photoPath", className + "_" + itemName);
					i.putExtra("takenFrom", className);
					i.putExtra("takenFor", itemName);

				}
				getContext().startActivity(i);
			}
		});
		holder.nA.setTag(holder);
		holder.nA.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				Log.d("na");
				Log.d("pe pozitia:" + position);
				Log.d("nAIds:" + nAIds);
				Log.d("okIds:" + okIds);
				new HideSysUi(((Activity) context).getWindow());
				if (nAIds.contains(position))
				{
					int poz = nAIds.indexOf(position);
					nAIds.remove(poz);
				}
				else
				{
					nAIds.add(position);
				}
				ViewHolder holder1 = (ViewHolder) v.getTag();

				butOk.setBackgroundResource(R.drawable.custom_ok_button);
				butNotOk.setBackgroundResource(R.drawable.custom_not_ok_button);
				butNA.setBackgroundResource(R.drawable.custom_na_button_clicked);
				holder1.item.setBackgroundColor(Color.parseColor("#a3a890"));
				holder1.item.setTextColor(Color.parseColor("#000000"));

				String className = getContext().getClass().getSimpleName();
				String itemName = holder1.item.getText().toString();
				Concern Tasks = TasksList.get(position);
				if (Tasks.getMarked().equals("0"))
				{
					Log.d("marked new");
					marked++;
				}
				Tasks.setMarked("2");
				

				// sendPost(position, "na");
				// saveXml(position, "2");
				Log.d("position:" + position);
				TasksList.set(position, Tasks);

				Log.d("marked:" + marked + " listSize:" + listSize + " unmarked:" + unmarked);
				Util.saveListToXml(TasksList, clsName);
				Util.saveToUploadXml(empId, propId, clsName, Tasks.getCode(), Tasks.getName(), Tasks.getMarked(), "-1", "", "-1", "-1");
				Log.d("marked:" + marked + " listSize:" + listSize);
				int marcate=0;
				for (Concern taskToVerify : TasksList)
				{
					if (!(taskToVerify.getMarked().equals("0")))
					{
						unmarked--;
						marcate++;
						// goNext(className);
					}
				}
				if (marcate==TasksList.size())
				{
					goNext(className);	
				}
				
				if (marked == listSize)
				{
					Log.d("buckleup din adapter na");
					Log.d("buckleup for class:" + className);
					goNext(className);
				}
			}
		});

		butNotOk.setBackgroundResource(notOkIds.contains(position) ? R.drawable.custom_not_ok_button_clicked : R.drawable.custom_not_ok_button);
		butOk.setBackgroundResource(okIds.contains(position) ? R.drawable.custom_ok_button_clicked : R.drawable.custom_ok_button);
		butNA.setBackgroundResource(nAIds.contains(position) ? R.drawable.custom_na_button_clicked : R.drawable.custom_na_button);

		if (okEdIds.contains(position))
		{
			butOk.setBackgroundResource(R.drawable.custom_ok_button_clicked);
			butNotOk.setBackgroundResource(R.drawable.custom_not_ok_button);
			butNA.setBackgroundResource(R.drawable.custom_na_button);
			holder.item.setBackgroundColor(Color.parseColor("#208208"));
			holder.item.setTextColor(Color.parseColor("#ffffff"));
			// holder.item.setBackgroundResource(Color.parseColor("#208208"));
			// holder.item.setTextColor(Color.parseColor("#2DBA09"));
		}
		else if (notOkEdIds.contains(position))
		{
			butOk.setBackgroundResource(R.drawable.custom_ok_button);
			butNotOk.setBackgroundResource(R.drawable.custom_not_ok_button_clicked);
			butNA.setBackgroundResource(R.drawable.custom_na_button);
			holder.item.setBackgroundColor(Color.parseColor("#a11005"));
			holder.item.setTextColor(Color.parseColor("#ffffff"));
			// holder.item.setBackgroundResource(Color.parseColor("#a11005"));
			// holder.item.setTextColor(Color.parseColor("#a11005"));
		}
		else if (naEdIds.contains(position))
		{
			butOk.setBackgroundResource(R.drawable.custom_ok_button);
			butNotOk.setBackgroundResource(R.drawable.custom_not_ok_button);
			butNA.setBackgroundResource(R.drawable.custom_na_button_clicked);
			holder.item.setBackgroundColor(Color.parseColor("#a3a890"));
			holder.item.setTextColor(Color.parseColor("#000000"));
			// holder.item.setBackgroundResource(Color.parseColor("#a11005"));
			// holder.item.setTextColor(Color.parseColor("#a11005"));
		}
		else
		{
			butOk.setBackgroundResource(R.drawable.custom_ok_button);
			butNotOk.setBackgroundResource(R.drawable.custom_not_ok_button);
			butNA.setBackgroundResource(R.drawable.custom_na_button);
			holder.item.setBackgroundColor(Color.TRANSPARENT);

		}
		return lists;

	}

	private void goNext(String currentClass)
	{
		if (currentClass.toLowerCase().equalsIgnoreCase("HS"))
		{
			Log.d("launching from HS");
			Intent i = new Intent(context, Quality.class);
			context.startActivity(i);
		}
		if (currentClass.toLowerCase().equalsIgnoreCase("quality"))
		{
			Log.d("launching from  quality");
			Intent i = new Intent(context, QualityWG.class);
			context.startActivity(i);

		}
		if (currentClass.toLowerCase().equalsIgnoreCase("qualitywg"))
		{
			Log.d("launching from qualitywb");
			// Util.initTasksXml();
			Intent i = new Intent(context, TaskList.class);
			context.startActivity(i);
		}
	}

	// public void sendPost(int position, String type)
	// {
	// // Log.d("in send post ba");
	// // Log.d("position:" + position);
	// // Log.d("type:" + type);
	// String className1 = getContext().getClass().getSimpleName();
	// if (className1.equals("HS"))
	// {
	// RequestParams params = new RequestParams();
	// String poz = Integer.toString(position);
	// Log.d("poz:" + poz);
	// params.put("index", poz);
	// params.put("type", type);
	// AsyncHttpClient client = new AsyncHttpClient();
	// client.post(saveHs, params, new AsyncHttpResponseHandler()
	// {
	// @Override
	// public void onSuccess(String response)
	// {
	// Log.d("success  !!!!");
	// }
	// });
	// }
	//
	// }

}
