package com.workers;

import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.types.AppParameters;
import com.workers.types.PhotoType;
import com.workers.types.Property;
import com.workers.types.Tasks;
import com.workers.util.CommonUtilities;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.TouchImageView;
import com.workers.util.Util;

public class PropertyDetails extends Activity
{
	TouchImageView fullPreview;
	RelativeLayout fullImageLayout, detailsLayout;
	TextView close;
	TextView loading;
	ScrollView scrollView;
	Context context;
	String fileNamePath, uniqId;
	String employee = "0";
	public static List<Property> resultSdCard;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		// setContentView(R.layout.property_details);
		context = this.getApplicationContext();
		String title = (String) getTitle();
		// setTitleColor(Color.BLUE);
		String uniqId = getIntent().getStringExtra("uniqId");
		setContentView(R.layout.property_details_card);
		new HideSysUi(getWindow(), getApplicationContext());
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "propertyDetails");
		editor1.commit();
		detailsLayout = (RelativeLayout) findViewById(R.id.propertyLayout);
		scrollView = (ScrollView) findViewById(R.id.scrollView);
		scrollView.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe layout details");
				NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancelAll();
			}
		});
		detailsLayout.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe layout details");
				NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancelAll();
			}
		});
		// loading = new TextView(getApplicationContext());
		// RelativeLayout.LayoutParams loadingLayoutParams = new
		// RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
		// RelativeLayout.LayoutParams.WRAP_CONTENT);
		// loading.setGravity(Gravity.CENTER);
		// loading.setTextSize(27);
		// loading.setTextColor(Color.GREEN);
		// loading.setText("Loading...");
		// detailsLayout.addView(loading, loadingLayoutParams);
		loading = (TextView) findViewById(R.id.loading);
		try
		{
			Log.d("prop details ggg");
			if (Util.checkParametersExists(getApplicationContext()))
			{
				Log.d("prop details ggg if");
				File propertiesDetails = new File(xmlFolderPath + "propertiesDetails.xml");
				if (propertiesDetails.exists())
				{
					Log.d("prop details ggg if exists");
					resultSdCard = Util.loadPropertiesXmlFromSdCard(getApplicationContext(), xmlFolderPath, "propertiesDetails");
					displayListView(resultSdCard);
					AppParameters appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
					if (appParameters.getRegId().equals(""))
					{

					}
					empId = appParameters.getEmpId();
					employee = empId;
					String name = appParameters.getEmpName();
					final Spannable spanYou = new SpannableString(title + " " + name);
					spanYou.setSpan(new ForegroundColorSpan(Color.GREEN), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new RelativeSizeSpan(0.7f), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new StyleSpan(Typeface.ITALIC), title.length(), spanYou.length(), 0);
					setTitle(spanYou);
				}
				else
				{
					Log.d("prop details ggg else");
					Util.getXmls(context, "0", uniqId);

					File propertiesDetails1 = new File(xmlFolderPath + "propertiesDetails.xml");
					new CheckFileTask().execute(propertiesDetails1);
					// while (!propertiesDetails1.exists()) // Thread.sleep(1);
					//
					// {
					// Thread.sleep(1);
					// }
					// if (propertiesDetails1.exists())
					// {
					// resultSdCard =
					// Util.loadPropertiesXmlFromSdCard(getApplicationContext(),
					// xmlFolderPath, "propertiesDetails");
					// displayListView(resultSdCard);
					// }
				}
			}
			else
			{
				Log.d("prop details ggg nada");
			}
		}
		catch (XmlPullParserException e)
		{
			Log.d("XmlPullParserException:" + e);
			e.printStackTrace();
		}
		catch (IOException e)
		{
			Log.d("IOException:" + e);
			e.printStackTrace();
		}
		catch (Exception e)
		{
			Log.d("Exception loadPropertiesXmlFromSdCard :" + e);
			e.printStackTrace();
			Intent tasksIntent = new Intent(this, EndProgram.class);
			// tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			tasksIntent.putExtra("uniqId", uniqId);
			tasksIntent.putExtra("moveback", "moveback");
			tasksIntent.putExtra("end_program", "end_program");
			startActivity(tasksIntent);
		}
		RequestParams paramsRegister = new RequestParams();
		paramsRegister.put("activity", "enter_propertyDetails");
		paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
		paramsRegister.put("empId", employee);

		AsyncHttpClient client = new AsyncHttpClient();
		client.post(op, paramsRegister, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("AsyncHttpClient enter_propertyDetails op success!!!!:" + response);
				// processResponse(response);
			}

			@Override
			public void onFailure(Throwable e, String response)
			{
				Log.d("AsyncHttpClient enter_propertyDetails op onFailure e !!!!:" + e);
				Log.d("AsyncHttpClient enter_propertyDetails op onFailure response!!!!:" + response);
			}
		});
		fullImageLayout = (RelativeLayout) findViewById(R.id.fullImageLayout);
		fullImageLayout.setOnTouchListener(new ListView.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				int action = event.getAction();
				v.getParent().requestDisallowInterceptTouchEvent(true);
				// Handle ListView touch events.
				v.onTouchEvent(event);
				return true;
			}
		});
		// curDate.setText(": " + dateFormat.format(date));
		fullPreview = (TouchImageView) fullImageLayout.findViewById(R.id.fullImage);

		fullPreview.setMaxZoom(4);
		fullPreview.setVisibility(View.INVISIBLE);

	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in prop details resume");
		new HideSysUi(getWindow(), getApplicationContext());
	}

	public void displayListView(List<Property> result)
	{
		new HideSysUi(getWindow(), getApplicationContext());
		Log.d("ggg");
		new DownloadTask().execute(result);
		for (Property property : result)
		{
			for (final PhotoType photo : property.getPhotos())
			{
				final String photoPath = photo.getPath();
				// Util.getPhotos(photoPath, "Thumb");

			}
		}
	}

	public class CheckFileTask extends AsyncTask<File, Void, Boolean>
	{
		String photo;

		@Override
		protected Boolean doInBackground(File... urls)
		{
			Log.d("ChecFileTask background");
			Log.d("ChecFileTask:" + urls);
			Log.d("ChecFileTask:" + urls[0]);
			Boolean ret = false;
			if (urls[0].exists())
				ret = true;
			return ret;
		}

		@Override
		protected void onPostExecute(Boolean res)
		{
			Log.d("ChecFileTask onPostExecute" + res);
			if (res)
			{
				try
				{
					File propertiesDetails1 = new File(xmlFolderPath + "propertiesDetails.xml");
					if (propertiesDetails1.exists())
						resultSdCard = Util.loadPropertiesXmlFromSdCard(getApplicationContext(), xmlFolderPath, "propertiesDetails");
					displayListView(resultSdCard);
				}
				catch (XmlPullParserException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else
			{
				File propertiesDetails1 = new File(xmlFolderPath + "propertiesDetails.xml");
				new CheckFileTask().execute(propertiesDetails1);
			}
		}
	}

	public class DownloadTask extends AsyncTask<List<Property>, Void, Boolean>
	{
		String photo;
		List<Property> lp;

		@Override
		protected Boolean doInBackground(List<Property>... urls)
		{
			Log.d("ggg background");
			Log.d("urls:" + urls);
			Log.d("urls:" + urls[0]);
			Boolean ret = false;
			lp = urls[0];
			for (Property property : urls[0])
			{
				for (final PhotoType photoP : property.getPhotos())
				{
					final String photoPath = photoP.getPath().toString();
					photo = photoPath;
					Log.d("urls[0]:" + urls[0]);
					// Log.d("urls[1]:" + urls[1]);
					Log.d("file verify:" + CommonUtilities.xmlFolderPath + "Images" + File.separator + "Thumb" + File.separator + photo);
					File photoExistent = new File(CommonUtilities.xmlFolderPath + "Images" + File.separator + "Thumb" + File.separator + photo);
					if (!(photoExistent.exists()))
					{
						Log.d("details save photo not exists:"+photo);
						ret = Util.savePhotos(photo, "Thumb", uniqId);
						if (!ret)
						{
							Log.d("details save photo ret false:"+photo);
							ret = Util.savePhotos(photo, "Thumb", uniqId);
						}
					}
				}
			}
			return ret;
		}

		@Override
		protected void onPostExecute(Boolean res)
		{
			Log.d("on post execute");
			Log.d("show rez:" + res);
			// if (!res)
			// {
			// Log.d("rez false for photo:  "+photo);
			// //Util.savePhotos(photo, "Thumb", uniqId);
			// //new DownloadTask().execute(lp);
			// }

			// else
			{
				int id = 0, prevId, taskId = 0;
				int photoId = 0;
				int marginTopPhoto;
				detailsLayout.removeViewInLayout(loading);
				loading.setVisibility(View.GONE);
				Log.d("prop det ggg result size:" + resultSdCard.size());
				ContextThemeWrapper newContext = new ContextThemeWrapper(getApplicationContext(), R.style.btnStyle1);
				if (resultSdCard.size() == 0)
				{
					Log.d("prop det ggg if");
				}
				else
				{
					Log.d("prop det ggg else");
				}
				if (resultSdCard.size() == 0)
				{
					Log.d("postexe in zero");
					TextView message = new TextView(getApplicationContext());
					RelativeLayout.LayoutParams propAddressLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
					// RelativeLayout.LayoutParams propAddressLayoutParams = new
					// RelativeLayout.LayoutParams(200,300, weight);
					propAddressLayoutParams.leftMargin = 20;
					propAddressLayoutParams.topMargin = 60;
					propAddressLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
					propAddressLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					propAddressLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
					propAddressLayoutParams.addRule(RelativeLayout.CENTER_VERTICAL);
					message.setLines(2);
					message.setId(1000);
					// message.setLayoutParams(new
					// TableLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
					// LayoutParams.WRAP_CONTENT, 0f));
					message.setTextSize(27);
					// message.setTextColor(Color.WHITE);
					// String
					// txt="<p><font style='background-color: #800000;'>dfgdgd&nbsp;<font style='color: #ff6600;'>sdfsdfsdfsfsffds</font></font></p> ";
					// String
					// txt="<p>test note <font color='#993300'>bla bla</font> dfgdfh&nbsp;<b>dsfsfd&nbsp;<i>sdfsfsfdf</i></b></p> ";
					// String
					// txt="<p>test <font color='#ff9900'>note</font> bla bla dfgdfh</p> ";
					String txt = "test <b>note</b> bla bla <font color='#ff6600'>dfgdfh</font><br><font color='#3366ff'><i><b>dfgfdgdfg</b> </i></font><i>dfdsff</i><br> ";
					// message.setText(Html.fromHtml("<i><small><font color=\"#c5c5c5\">"
					// + "Competitor ID: " + "</font></small></i>" +
					// "<font style=\"color:'#47a842'\">sadsad</font>"));
					// message.setText(Html.fromHtml(txt));
					// Spannable WordtoSpan = new SpannableString(txt);
					//
					// WordtoSpan.setSpan(new BackgroundColorSpan(Color.BLUE),
					// 0, 4,
					// Spannable.SPAN_INCLUSIVE_INCLUSIVE);
					// message.setText(Html.fromHtml("<font color='#145A14'>text</font>"));
					message.setText("Sorry, this page has no data at the moment!");
					detailsLayout.addView(message, propAddressLayoutParams);
					Button okButton = new Button(getApplicationContext());
					RelativeLayout.LayoutParams propOkButtonLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
					propOkButtonLayoutParams.topMargin = 300;
					propOkButtonLayoutParams.height = 200;
					propOkButtonLayoutParams.addRule(RelativeLayout.BELOW, message.getId());
					propOkButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
					propOkButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					propOkButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
					okButton.setId(2000);
					okButton.setTextSize(25);
					okButton.setText("Ok");

					okButton.setTextAppearance(getApplicationContext(), R.style.btnStyle1);
					okButton.setBackgroundResource(R.drawable.custom_button);
					okButton.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View arg0)
						{
							Log.d("click ok button:");

							Log.d("click pe layout details");
							NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
							mNotificationManager.cancelAll();
							RequestParams paramsRegister = new RequestParams();
							paramsRegister.put("activity", "property_details_ok");
							paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
							paramsRegister.put("empId", empId);
							AsyncHttpClient client = new AsyncHttpClient();
							client.post(op, paramsRegister, new AsyncHttpResponseHandler()
							{
								@Override
								public void onSuccess(String response)
								{
									Log.d("AsyncHttpClient ack_acknowledge ok op success!!!!:" + response);
								}

								@Override
								public void onFailure(Throwable e, String response)
								{
									Log.d("AsyncHttpClient ack_acknowledge ok onFailure e !!!!:" + e);
									Log.d("AsyncHttpClient ack_acknowledge ok onFailure response!!!!:" + response);
								}
							});
							// Intent i = new Intent(getApplicationContext(),
							// Availability.class);
							// startActivity(i);
							moveTaskToBack(true);
						}
					});
					detailsLayout.addView(okButton, propOkButtonLayoutParams);
				}
				else
				{
					Log.d("postexe  in else dupa 0");
					Log.d("postexe:" + resultSdCard);
					Log.d("postexe:" + resultSdCard.size());
					Log.d("postexe id:" + id);
					for (Property property : resultSdCard)
					{
						Log.d("postexe for:" + id);
						TextView propAddress = new TextView(getApplicationContext());
						TextView propTime = new TextView(getApplicationContext());
						TextView propNotes = new TextView(PropertyDetails.this);
						TextView notesTitle = new TextView(getApplicationContext());
						TextView tasks = new TextView(getApplicationContext());
						id++;
						RelativeLayout.LayoutParams propAddressLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						propAddressLayoutParams.leftMargin = 20;
						propAddressLayoutParams.topMargin = 20;
						propAddressLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						if (id != 1)
						{
							Log.d("below: " + id);
							propAddressLayoutParams.addRule(RelativeLayout.BELOW, id - 2);
						}
						propAddress.setId(id);
						propAddress.setTextSize(27);
						propAddress.setTextColor(Color.GREEN);
						propAddress.setText("Address:" + property.getAddres());
						detailsLayout.addView(propAddress, propAddressLayoutParams);
						id++;
						RelativeLayout.LayoutParams propTimeLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						propTimeLayoutParams.topMargin = 20;
						propTimeLayoutParams.addRule(RelativeLayout.ALIGN_LEFT, propAddress.getId());
						propTimeLayoutParams.addRule(RelativeLayout.BELOW, propAddress.getId());
						propTime.setId(id);
						propTime.setTextSize(27);
						propTime.setTextColor(Color.GREEN);
						propTime.setText("Time : " + property.getTime());
						detailsLayout.addView(propTime, propTimeLayoutParams);
						id++;
						RelativeLayout.LayoutParams tasksParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						tasksParams.topMargin = 20;
						tasksParams.addRule(RelativeLayout.ALIGN_LEFT, propTime.getId());
						tasksParams.addRule(RelativeLayout.BELOW, propTime.getId());
						tasks.setId(id);
						tasks.setTextSize(27);
						tasks.setTextColor(Color.GREEN);
						tasks.setText("Tasks ");
						detailsLayout.addView(tasks, tasksParams);
						id++;
						taskId = id;

						for (Tasks taskProperty : property.getTasks())
						{
							Log.d("task details:"+property.getAddres()+": "+taskProperty.getName());
							TextView propTasks = new TextView(getApplicationContext());
							prevId = tasks.getId();
							int marginTop = 20;
							if (taskId != id)
							{
								prevId = taskId - 1;
								marginTop = 0;
							}

							RelativeLayout.LayoutParams propTasksLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
							propTasksLayoutParams.leftMargin = 65;
							propTasksLayoutParams.topMargin = marginTop;
							// propTasksLayoutParams.addRule(RelativeLayout.RIGHT_OF,
							// tasks.getId());
							propTasksLayoutParams.addRule(RelativeLayout.BELOW, prevId);
							propTasks.setId(taskId);
							propTasks.setTextSize(20);
							propTasks.setTextColor(Color.WHITE);
							propTasks.setText(taskProperty.getName());
							detailsLayout.addView(propTasks, propTasksLayoutParams);
							taskId++;
						}
						id = taskId;
						RelativeLayout.LayoutParams notes = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						notes.topMargin = 20;
						notes.addRule(RelativeLayout.ALIGN_LEFT, propTime.getId());
						notes.addRule(RelativeLayout.BELOW, taskId - 1);
						notesTitle.setId(id++);
						notesTitle.setTextSize(27);
						notesTitle.setTextColor(Color.GREEN);
						notesTitle.setText("Notes");
						detailsLayout.addView(notesTitle, notes);
						RelativeLayout.LayoutParams notesParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						notesParams.topMargin = 20;
						notesParams.leftMargin = 65;
						// notesParams.addRule(RelativeLayout.RIGHT_OF,
						// notesTitle.getId());
						notesParams.addRule(RelativeLayout.BELOW, id - 1);
						// notesParams.addRule(RelativeLayout.ALIGN_LEFT,
						// propTime.getId());
						propNotes.setId(id++);
						propNotes.setTextSize(20);
						propNotes.setTextColor(Color.GREEN);
						propNotes.setText(Html.fromHtml(property.getNotes()));
						// propNotes.setText(Html.fromHtml(property.getNotes()));
						// propNotes.setText(Html.fromHtml("<a href='http://pillar.pilon.co.uk:82/propertyFiles/1878_16297_propPack_28.07.2014arrow_right.png'>Link</a>"));
						// propNotes.setMovementMethod(LinkMovementMethod.getInstance());
						// propNotes.setText(
						// Html.fromHtml("<u>hdjhadasd</u><a href=\"http://www.google.com\">Google</a>"));
						// propNotes.setMovementMethod(LinkMovementMethod.getInstance());
						// propNotes.setText(Html.fromHtml("<b>text3:</b>  Text with a <br> "
						// +
						// "<a href=\"http://pillar.pilon.co.uk:82/propertyFiles/916_14614_gasCert_07.03.2013CP12-07032013-512018.pdf\">link</a> "
						// + "created in the Java source code using HTML."));
						propNotes.setMovementMethod(LinkMovementMethod.getInstance());
						propNotes.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(View arg0)
							{
								Log.d("arg0:" + arg0);
							}
						});
						detailsLayout.addView(propNotes, notesParams);
						photoId = id;
						int row = 1;
						int indexPhoto = 1;

						for (final PhotoType photo : property.getPhotos())
						{
							final String photoPath = photo.getPath();
							prevId = propNotes.getId();
							marginTopPhoto = 20;
							int marginRightPhoto = 0;
							File poza = new File(xmlFolderPath + "Images/Thumb/" + photoId);
//							if (!poza.exists())
//							{
								// Util.getPhotos(photo.getPath(), "thumb",
								// xmlFolderPath + "Images/Thumb/" + photoId);
								//Util.savePhotos(photo.getPath(), "thumb", "-1");

								Log.d("photo poz path:" + " prevId:" + prevId + " " + " photoId:" + photoId + " " + photo.getPath());

								// try
								// {
								// Thread.sleep(1);
								// }
								// catch (InterruptedException e)
								// {
								// Log.d("exception sleeping:" + e);
								// // TODO Auto-generated catch block
								// e.printStackTrace();
								// }

								Log.d("init activity get fullsize");
								// Util.getPhotos(photoPath, "FullSize");

								ImageView photos = new ImageView(getApplicationContext());
								RelativeLayout.LayoutParams propImgParams = new RelativeLayout.LayoutParams(300, 300);
								Log.d("photo id inainte de add:" + photoId);
								photos.setId(photoId);
								photos.setTag(photoPath);
								photos.setOnClickListener(new OnClickListener()
								{
									@Override
									public void onClick(View arg0)
									{
										viewImage(arg0, photoPath, uniqId);
									}
								});
								if (photoId != id)
								{
									prevId = photoId - 1;
									marginTopPhoto = 5;
									Log.d("photo poz row:" + row);
									if (indexPhoto % 2 == 0)
									{
										propImgParams.addRule(RelativeLayout.RIGHT_OF, prevId);
										propImgParams.addRule(RelativeLayout.BELOW, prevId - 1);
										propImgParams.addRule(RelativeLayout.ALIGN_TOP, prevId);
										if (row > 1)
											propImgParams.addRule(RelativeLayout.ALIGN_LEFT, prevId - 1);
										marginTopPhoto = 0;
									}
									else
									{
										propImgParams.addRule(RelativeLayout.BELOW, prevId - 1);
										marginTopPhoto = 5;
										marginRightPhoto = 10;
									}
								}
								else
								{
									propImgParams.addRule(RelativeLayout.BELOW, prevId);
								}
								propImgParams.topMargin = marginTopPhoto;
								propImgParams.rightMargin = marginRightPhoto;

								BitmapFactory.Options o = new BitmapFactory.Options();
								Bitmap b = BitmapFactory.decodeFile(xmlFolderPath + "Images/Thumb/" + photoPath, o);
								photos.setImageBitmap(b);

								detailsLayout.addView(photos, propImgParams);
								photoId++;

								if (indexPhoto % 2 == 0)
								{
									row++;
								}
								indexPhoto++;
							}
//						}
						id = photoId;
						Log.d("id after photos:" + id);
						Log.d("taskId after:" + taskId);
						Log.d("id pt buton:" + id);
						Button button = new Button(newContext);
						RelativeLayout.LayoutParams propButtonLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
						propButtonLayoutParams.topMargin = 20;
						propButtonLayoutParams.height = 200;
						Log.d("task id after all:" + taskId);
						propButtonLayoutParams.addRule(RelativeLayout.BELOW, id - 1);
						propButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
						propButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
						propButtonLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
						button.setId(id + 50);
						button.setTextSize(25);
						button.setText("Acknowledge");
						button.setTextAppearance(getApplicationContext(), R.style.btnStyle1);
						button.setBackgroundResource(R.drawable.custom_button);

						button.setOnClickListener(new OnClickListener()
						{
							@Override
							public void onClick(View arg0)
							{
								Log.d("click:");

								Log.d("click pe layout details");
								NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
								mNotificationManager.cancelAll();

								RequestParams paramsRegister = new RequestParams();
								paramsRegister.put("activity", "ack_acknowledge");
								paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
								paramsRegister.put("empId", empId);
								AsyncHttpClient client = new AsyncHttpClient();
								client.post(op, paramsRegister, new AsyncHttpResponseHandler()
								{
									@Override
									public void onSuccess(String response)
									{
										Log.d("AsyncHttpClient ack_acknowledge op success!!!!:" + response);
									}

									@Override
									public void onFailure(Throwable e, String response)
									{
										Log.d("AsyncHttpClient ack_acknowledge  onFailure e !!!!:" + e);
										Log.d("AsyncHttpClient ack_acknowledge  onFailure response!!!!:" + response);
									}
								});
								// Intent i = new
								// Intent(getApplicationContext(),
								// Availability.class);
								// startActivity(i);
								moveTaskToBack(true);
							}
						});
						detailsLayout.addView(button, propButtonLayoutParams);
					}
				}
			}
		}
	}

	@Override
	public void onBackPressed()
	{
		// Log.d("CDA", "onBackPressed Called");
		// Intent setIntent = new Intent(Intent.ACTION_MAIN);
		// setIntent.addCategory(Intent.CATEGORY_HOME);
		// setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// startActivity(setIntent);
	}

	public void viewImage(View v, String path, String uniqId)
	{
		ImageView img = (ImageView) v;
		Log.d("img getId:" + img.getId());
		Log.d("img getTag:" + img.getTag(img.getId()));
		Log.d("img path:" + path);
		// File parametersFile = new File(xmlFolderPath + "Images/FullSize/" +
		// path);
		Intent i = new Intent(getApplicationContext(), ViewImage.class);
		Log.d("image pas " + path);
		i.putExtra("image", path);
		i.putExtra("uniqId", uniqId);
		startActivity(i);
	}

	public void killPopup(View v)
	{
		Log.d("killPopup");
		// ImageView img = (ImageView) v;
		// Log.d("img:"+img.getDrawable());
		// fullPreview.setImageDrawable(img.getDrawable());
		fullPreview.setVisibility(View.INVISIBLE);
		// close.setVisibility(View.INVISIBLE);
		// fullPreview.setIfre
	}

	public void GoToAvailability(View v)
	{
		Intent i = new Intent(getApplicationContext(), Availability.class);
		startActivity(i);
	}
}
