package com.workers.types;

import java.util.ArrayList;

public class Property
{
	public Property(String id, String name, String addres, String time, ArrayList<Tasks> tasks, ArrayList<PhotoType> photos, String notes)
	{
		super();
		this.id = id;
		this.name = name;
		this.addres = addres;
		this.time = time;
		this.tasks = tasks;
		this.photos = photos;
		this.notes = notes;
	}

	String id = null;
	String name = null;
	String addres = null;
	String time = null;
	ArrayList<Tasks> tasks = null;
	ArrayList<PhotoType> photos = null;
	String notes = null;

	public String getTime()
	{
		return time;
	}

	public void setTime(String time)
	{
		this.time = time;
	}

	public ArrayList<PhotoType> getPhotos()
	{
		return photos;
	}

	public void setPhotos(ArrayList<PhotoType> photos)
	{
		this.photos = photos;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public ArrayList<Tasks> getTasks()
	{
		return tasks;
	}

	public void setTasks(ArrayList<Tasks> tasks)
	{
		this.tasks = tasks;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getAddres()
	{
		return addres;
	}

	public void setAddres(String addres)
	{
		this.addres = addres;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

}
