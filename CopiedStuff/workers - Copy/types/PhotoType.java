package com.workers.types;

public class PhotoType
{

	String id = null;
	String path = null;
	String empId = null;
	String propId = null;
	String takenFrom = null;
	String takenFor = null;

	public PhotoType(String id, String path, String empId, String propId, String takenFrom, String takenFor)
	{
		super();
		this.id = id;
		this.path = path;
		this.empId = empId;
		this.propId = propId;
		this.takenFrom = takenFrom;
		this.takenFor = takenFor;
	}

	public String getTakenFor()
	{
		return takenFor;
	}

	public void setTakenFor(String takenFor)
	{
		this.takenFor = takenFor;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public String getEmpId()
	{
		return empId;
	}

	public void setEmpId(String empId)
	{
		this.empId = empId;
	}

	public String getPropId()
	{
		return propId;
	}

	public void setPropId(String propId)
	{
		this.propId = propId;
	}

	public String getTakenFrom()
	{
		return takenFrom;
	}

	public void setTakenFrom(String takenFrom)
	{
		this.takenFrom = takenFrom;
	}
}
