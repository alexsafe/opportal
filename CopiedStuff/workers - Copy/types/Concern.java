package com.workers.types;

public class Concern
{

	String code = null;
	String name = null;
	String marked = null;
	boolean selected = false;

	public Concern(String code, String name, boolean selected, String marked)
	{
		super();
		this.code = code;
		this.name = name;
		this.selected = selected;
		this.marked = marked;
	}

	public Concern(String code, String name, String marked)
	{
		super();
		this.code = code;
		this.name = name;
		this.marked = marked;
	}

	public String getMarked()
	{
		return marked;
	}

	public void setMarked(String marked)
	{
		this.marked = marked;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public boolean isSelected()
	{
		return selected;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

}
