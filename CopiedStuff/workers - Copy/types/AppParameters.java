package com.workers.types;

public class AppParameters
{
	public AppParameters(String empId, String propId, String regId, String empName)
	{
		super();
		this.empId = empId;
		this.propId = propId;
		this.regId = regId;
		this.empName = empName;
	}

	public AppParameters(String empId, String propId, String regId)
	{
		super();
		this.empId = empId;
		this.propId = propId;
		this.regId = regId;
	}

	String empId = null;
	String propId = null;
	String regId=null;
	String empName=null;
	public String getEmpName()
	{
		return empName;
	}

	public void setEmpName(String empName)
	{
		this.empName = empName;
	}

	public String getRegId()
	{
		return regId;
	}

	public void setRegId(String regId)
	{
		this.regId = regId;
	}

	public String getEmpId()
	{
		return empId;
	}

	public void setEmpId(String empId)
	{
		this.empId = empId;
	}

	public String getPropId()
	{
		return propId;
	}

	public void setPropId(String propId)
	{
		this.propId = propId;
	}

}
