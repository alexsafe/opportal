package com.workers;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import com.workers.util.HideSysUi;
import com.workers.util.Log;

public class Goodbye extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.goobye);
		new HideSysUi(getWindow(), getApplicationContext());
		Handler handler = new Handler();
		if (!getIntent().hasExtra("end_program"))
		{
			Log.d("nu e primit end");
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
			Editor editor1 = sharedPreferences.edit();
			editor1.putString("pauseString", "enterProperty");
			editor1.commit();
		}
		else
		{
			Log.d("e primit end");
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
			Editor editor1 = sharedPreferences.edit();
			editor1.putString("pauseString", "goodbye");
			editor1.commit();
		}
		handler.postDelayed(new Runnable()
		{
			public void run()
			{
				moveTaskToBack(true);
				Log.d("cod dupa move back init activ");
				// Intent i = new Intent(getApplicationContext(),
				// EnterProperty.class);
				// startActivity(i);
			}
		}, 5000);

		// try
		// {
		// Thread.sleep(5000);
		// }
		// catch (InterruptedException e)
		// {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}

//	@Override
//	protected void onPause()
//	{
//		super.onPause();
//		Log.d("on pause enter prop");
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//		Editor editor1 = sharedPreferences.edit();
//		editor1.putString("pauseString", "enterProperty");
//		editor1.commit();
//	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow(), getApplicationContext());
	}
}
