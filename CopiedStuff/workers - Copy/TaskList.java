package com.workers;

import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.propId;

import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.ListActivity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.types.AppParameters;
import com.workers.types.Tasks;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class TaskList extends ListActivity
{

	/** Called when the activity is first created. */
	ListView listView;
	TaskAdapter dataAdapter = null;
	String employee = "0";

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		Log.d("in task layout");
		super.onCreate(savedInstanceState);
		ListView lv = getListView();
		lv.setOnItemClickListener(new OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapter, View v, int position, long arg3)
			{
				Log.d("click pe list viee");
			}
		});
		setContentView(R.layout.task_list);
		new HideSysUi(getWindow(), getApplicationContext());
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "tasklist");
		editor1.commit();
		RelativeLayout taskLayout = (RelativeLayout) findViewById(R.id.tasksLayout);
		taskLayout.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				Log.d("click pe taskLayout");
				new HideSysUi(getWindow(), getApplicationContext());
				NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
				mNotificationManager.cancelAll();
				return true;
			}
		});
		try
		{
			if (Util.checkParametersExists(getApplicationContext()))
			{
				AppParameters appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				employee = appParameters.getEmpId();
				propId = appParameters.getPropId();
				List<Tasks> resultSdCard = Util.loadTaskXmlFromSdCard(xmlFolderPath, "task_" + propId);
				if (resultSdCard != null)
				{
					String title = (String) getTitle();
					String name = appParameters.getEmpName();
					final Spannable spanYou = new SpannableString(title + " " + name);
					spanYou.setSpan(new ForegroundColorSpan(Color.GREEN), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new RelativeSizeSpan(0.7f), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new StyleSpan(Typeface.ITALIC), title.length(), spanYou.length(), 0);
					setTitle(spanYou);
					displayListView(resultSdCard);
				}
			}
		}
		catch (XmlPullParserException e)
		{
			Log.d("XmlPullParserException:" + e);
			e.printStackTrace();
		}
		catch (IOException e)
		{
			Log.d("IOException:" + e);
			e.printStackTrace();
		}
		RequestParams paramsRegister = new RequestParams();
		paramsRegister.put("activity", "enter_taskList");
		paramsRegister.put("empId", employee);
		paramsRegister.put("uniqId", getIntent().getStringExtra("uniqId"));
		paramsRegister.put("propId", propId);

		AsyncHttpClient client = new AsyncHttpClient();
		client.post(op, paramsRegister, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("AsyncHttpClient enter_taskList op success!!!!:" + response);
				// processResponse(response);
			}

			@Override
			public void onFailure(Throwable e, String response)
			{
				Log.d("AsyncHttpClient enter_taskList op onFailure e !!!!:" + e);
				Log.d("AsyncHttpClient enter_taskList op onFailure response!!!!:" + response);
			}
		});

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{

		Log.d("list item click");
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow(), getApplicationContext());
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d("on pause tasklist");
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "tasklist");
		editor1.commit();
	}

	private void displayListView(List<Tasks> list)
	{
		Log.d("list:" + list);
		ArrayList<Tasks> listItems = (ArrayList<Tasks>) list;
		Log.d("listItems:" + listItems);
		for (Tasks tasku : listItems)
		{
			Log.d("tasku id:" + tasku.getCode());
			Log.d("tasku name:" + tasku.getName());
			Log.d("tasku quantity:" + tasku.getQuantity());
			Log.d("tasku selected:" + tasku.getSelected());
		}

		dataAdapter = new TaskAdapter(this, R.layout.list_items, listItems);
		setListAdapter(dataAdapter);
		TextView next = (TextView) findViewById(R.id.nextText);
		TextView back = (TextView) findViewById(R.id.prevText);

		back.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe backkkk");
				Intent i = new Intent(getApplicationContext(), QualityWG.class);
				startActivity(i);
			}
		});
		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe nextttt");
				ArrayList<Tasks> TasksList = dataAdapter.TasksList;
				Boolean checkedAll = true;
				for (int i = 0; i < TasksList.size(); i++)
				{
					Tasks Tasks = TasksList.get(i);

					if (Tasks.getSelected().equals("false"))
					{
						checkedAll = false;
					}
				}
				Log.d("ch all:" + checkedAll);
				// if (checkedAll)
				// {
				// RequestParams params = new RequestParams();
				// params.put("checked", "all");
				// AsyncHttpClient client = new AsyncHttpClient();
				// client.post(saveTask, params, new AsyncHttpResponseHandler()
				// {
				// @Override
				// public void onSuccess(String response)
				// {
				// Log.d("success  !!!!");
				// }
				// });
				// }
				Intent i = new Intent(getApplicationContext(), ResidentNotes.class);
				startActivity(i);
			}
		});

	}

	@Override
	public void onBackPressed()
	{
		// moveTaskToBack(true);

	}

	public void clik(View v)
	{
		Log.d("click pe clik :" + v);

	}

	public void goToQualityWb(View v)
	{
		Log.d("click pe prev ");
		Intent i = new Intent(getApplicationContext(), QualityWG.class);
		startActivity(i);
	}

	public void goToResidentSatisfaction(View v)
	{
		Log.d("click pe next goToResidentSatisfaction ");
		Intent i = new Intent(getApplicationContext(), ResidentNotes.class);
		startActivity(i);
	}
}
