package com.workers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import com.workers.util.Log;

public class AutoUpdater extends AsyncTask<String, Void, Void>
{
	private Context context;

	public void setContext(Context contextf)
	{
		context = contextf;
	}

	@Override
	protected Void doInBackground(String... arg0)
	{
		try
		{
			URL url = new URL(arg0[0]);
			HttpURLConnection c = (HttpURLConnection) url.openConnection();
			c.setRequestMethod("GET");
			c.setDoOutput(true);
			c.connect();

			//String PATH = "/mnt/sdcard/Download/";
			String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Download" + File.separator;
			File file = new File(PATH);
			file.mkdirs();
			File outputFile = new File(file, "OpPortal.apk");
			if (outputFile.exists())
			{
				outputFile.delete();
			}
			FileOutputStream fos = new FileOutputStream(outputFile);

			InputStream is = c.getInputStream();

			byte[] buffer = new byte[1024];
			int len1 = 0;
			while ((len1 = is.read(buffer)) != -1)
			{
				fos.write(buffer, 0, len1);
			}
			fos.close();
			is.close();

			Intent intent = new Intent(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.fromFile(new File(PATH + file)), "application/vnd.android.package-archive");
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag
															// android returned
															// a intent error!
//			intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + "app.apk")), "application/vnd.android.package-archive");
//			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			  startActivity(intent);			
			context.startActivity(intent);

		}
		catch (Exception e)
		{
			Log.d("Update error! " + e.getMessage());
		}
		return null;
	}
}