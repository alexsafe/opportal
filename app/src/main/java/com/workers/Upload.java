package com.workers;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;

import com.workers.types.Concern;
import com.workers.util.Compress;
import com.workers.util.Log;
import com.workers.util.Util;

import static com.workers.util.CommonUtilities.sdCardPath;
import static com.workers.util.CommonUtilities.xmlFolderPath;

public class Upload extends Service
{

	private final IBinder mBinder = new LocalBinder();

	public class LocalBinder extends Binder
	{
		Upload getService()
		{
			Log.d("Service getService");
			return Upload.this;
		}
	}

	public static void zip(String[] files, String zipFile) throws IOException
	{
		BufferedInputStream origin = null;
		final int BUFFER_SIZE = 80000;
		ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile)));
		try
		{
			byte data[] = new byte[BUFFER_SIZE];

			for (int i = 0; i < files.length; i++)
			{
				FileInputStream fi = new FileInputStream(files[i]);
				origin = new BufferedInputStream(fi, BUFFER_SIZE);
				try
				{
					ZipEntry entry = new ZipEntry(files[i].substring(files[i].lastIndexOf("/") + 1));
					out.putNextEntry(entry);
					int count;
					while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1)
					{
						out.write(data, 0, count);
					}
				}
				finally
				{
					origin.close();
				}
			}
		}
		finally
		{
			out.close();
		}
	}

	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.d("Service onStartCommand upload:" + START_STICKY);
		if (intent != null)
		{
			// Log.d("get uniqId:" + intent.getStringExtra("uniqId"));
			String uniqId = "default";
			if (intent.getExtras() != null)
			{
				if (intent.hasExtra("uniqId"))
				{
					uniqId = intent.getStringExtra("uniqId");
				}
			}
			List<File> files = Util.getXmlToUPloadFiles(new File(xmlFolderPath.toString()), intent.getStringExtra("uniqId"));
			for (File fis : files)
			{
				Log.d("onStartCommand:" + fis.getAbsolutePath());
				Util.postFile(fis.getAbsolutePath());
			}
		}
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		Log.d("Service onBind:" + mBinder);
		return mBinder;
	}

	public class DownloadXmlTask extends AsyncTask<String, Void, List<Concern>>
	{

		@Override
		protected void onPostExecute(List<Concern> result)
		{
			Log.d("Service result:" + result);
			Util.saveListToXml(result, "hs");
		}

		@Override
		protected List<Concern> doInBackground(String... urls)
		{
			try
			{
				Log.d("Service in doInBackground");
				return Util.loadXmlFromNetwork(urls[0]);
			}
			catch (IOException e)
			{
				Log.d("Service 1:error" + e);
				// return "error" + e;
			}
			catch (XmlPullParserException e)
			{
				Log.d("Service 1:error" + e);
				// return "error" + e;
			}
			return null;
		}
	}
}
