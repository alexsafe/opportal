package com.workers.fcm;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.workers.util.Log;
import com.workers.util.ServerUtilities;


public class MyInstanceIDListenerService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {

        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.d("Firbase id login Refreshed token: " + refreshedToken);


            //To displaying token on logcat
            Log.d("TOKEN: " + refreshedToken);

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            preferences.edit().putString("FIREBASE_TOKEN", refreshedToken).apply();

            String imei = getPhoneInfo(getApplicationContext());
            ServerUtilities.registerDevice(getApplicationContext(), refreshedToken, imei);
        } catch (Exception e) {
            Log.e("onTokenRefresh exception:" + e);
            e.printStackTrace();
        }

    }

    private String  getPhoneInfo(Context context) {
        TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return "0";
        }
        String imei = mngr.getDeviceId();
        String number = mngr.getNetworkOperatorName();
        String line1Number = mngr.getLine1Number();
        String simSerialNumber = mngr.getSimSerialNumber();
        String subscriberId = mngr.getSubscriberId();
        com.workers.util.Log.d("mngr imei:" + imei);
        com.workers.util.Log.d("mngr.getLine1Number():" + mngr.getLine1Number());
        com.workers.util.Log.d("mngr.getSimSerialNumber():" + mngr.getSimSerialNumber());
        com.workers.util.Log.d("mngr.getSubscriberId():" + mngr.getSubscriberId());

        return imei;
    }


    class RegisterTask extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            // Register on our server // On server creates a new
            // user
            Log.d("registerind device");
            ServerUtilities.registerDevice(getApplicationContext(), params[0], "imei");
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

        }

    }

    ;
}