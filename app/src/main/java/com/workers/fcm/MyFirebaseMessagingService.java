package com.workers.fcm;


import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.R;
import com.workers.Upload;
import com.workers.activities.Availability;
import com.workers.activities.Download;
import com.workers.activities.EndProgram;
import com.workers.activities.EnterProperty;
import com.workers.activities.HS;
import com.workers.activities.InitActivity;
import com.workers.activities.MainActivity;
import com.workers.activities.PropertyDetails;
import com.workers.activities.TaskList;
import com.workers.activities.TestActivity;
import com.workers.types.AppParameters;
import com.workers.util.CommonUtilities;
import com.workers.util.Log;
import com.workers.util.Util;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.xmlFolderPath;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static String ADMIN_CHANNEL_ID = "32097";
    private static String TAG = "MyFirebaseMessagingService";
    private NotificationManager notificationManager;
    private String uniqId = "";
    private String empId = "0";

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(final Context context, String message) {
        android.util.Log.d(TAG, "generateNotification: " + message);
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        final AudioManager audiomanager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        android.util.Log.d(TAG, "ringer mode:" + audiomanager.getRingerMode());
        // if (audiomanager.getRingerMode()!=2)
        // {

        // }
        String title = context.getString(R.string.app_name);

        Intent notificationIntent = new Intent();// , MainActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
//        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context).setSmallIcon(R.drawable.ic_launcher).setContentTitle(title).setContentText(message);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(resultPendingIntent);
        // builder.setOngoing(true);
        builder.setAutoCancel(true);

        // Vibration
        builder.setVibrate(new long[]
                {
                        1000, 1000, 600, 1000, 600, 1000, 600, 1000, 600, 1000, 400, 1000, 500, 700, 300, 1000, 300, 1000, 300, 1000, 300, 2000, 300
                });
        builder.setVibrate(new long[]
                {
                        1000, 1000, 600, 1000, 600, 2000
                });
        // builder.setVibrate(2000);

        // LED
        // builder.setLights(Color.RED, 200, 200);

        // Vibrator v = (Vibrator)
        // context.getSystemService(Context.VIBRATOR_SERVICE);
        // // Vibrate for 500 milliseconds
        // v.vibrate(500);

        builder.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.bell_message));
        Notification note = builder.build();
        // note.defaults |= Notification.DEFAULT_VIBRATE;
        // note.defaults |= Notification.DEFAULT_SOUND;

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        // if ((!(message.toLowerCase().equalsIgnoreCase("upload"))) &&
        // (!(message.toLowerCase().equalsIgnoreCase("download")))
        // && (!(message.toLowerCase().equalsIgnoreCase("hs"))) &&
        // (!(message.toLowerCase().equalsIgnoreCase("uninstall")))
        // && (!(message.toLowerCase().equalsIgnoreCase("uninstall_delete"))) &&
        // (!(message.toLowerCase().equalsIgnoreCase("check_upload_date")))
        // && (!(message.toLowerCase().equalsIgnoreCase("init")))
        // )
        android.util.Log.d(TAG, "message e:" + message);
        if (!message.equals(null) || !message.equals("")) {
            if (message.toLowerCase().equals("availability") || message.toLowerCase().equals("property") || message.toLowerCase().equals("enter") || message.toLowerCase().equals("tasks")) {
                android.util.Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_MUSIC):" + audiomanager.getStreamVolume(AudioManager.STREAM_MUSIC));
                android.util.Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM):" + audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM));
                android.util.Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_ALARM):" + audiomanager.getStreamVolume(AudioManager.STREAM_ALARM));
                android.util.Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION):" + audiomanager.getStreamVolume(AudioManager.STREAM_VOICE_CALL));

                audiomanager.setRingerMode(2);
                audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 3, 0);

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                String mode = sharedPreferences.getString("initRingerMode", "mode");
                String volume = sharedPreferences.getString("initRingerVolume", "volume");
                android.util.Log.d(TAG, "mode:" + mode);
                android.util.Log.d(TAG, "volume:" + volume);
                if (!mode.equals("mode")) {
                    audiomanager.setRingerMode(Integer.parseInt(mode));
                    audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, Integer.parseInt(volume), 0);
                }
                Handler handler = new Handler();
                android.util.Log.d(TAG, "init after set getRingerMode():" + audiomanager.getRingerMode());
                android.util.Log.d(TAG, "audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);:" + audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION));
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.util.Log.d(TAG, "delayed 2");
                        audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 1, 0);
                    }

                }, 2000);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.util.Log.d(TAG, "delayed 2");
                        // audiomanager.setRingerMode(2);
                        audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 2, 0);
                    }

                }, 4000);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.util.Log.d(TAG, "delayed 3");// audiomanager.setRingerMode(2);
                        audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 3, 0);
                    }

                }, 9000);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.util.Log.d(TAG, "delayed 4");// audiomanager.setRingerMode(2);
                        audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 4, 0);
                    }

                }, 14000);
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        android.util.Log.d(TAG, "delayed sfarsit");
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                        String mode = sharedPreferences.getString("initRingerMode", "mode");
                        String volume = sharedPreferences.getString("initRingerVolume", "volume");
                        android.util.Log.d(TAG, "mode:" + mode);
                        android.util.Log.d(TAG, "volume:" + volume);
                        if (!mode.equals("mode")) {
                            audiomanager.setRingerMode(Integer.parseInt(mode));
                            audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, Integer.parseInt(volume), 0);
                        }
                        // audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,
                        // initVolume, 0);
                    }

                }, 17000);

                // Log.d(TAG,"audiomanager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)::"+audiomanager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
                // Play default notification sound
                // notification.defaults |= Notification.DEFAULT_SOUND;

                // notification.sound = Uri.parse("android.resource://" +
                // context.getPackageName() + "your_sound_file_name.mp3");

                // Vibrate if vibrate is enabled
                // notification.defaults |= Notification.DEFAULT_VIBRATE;

                notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.bell_message);
                // notificationManager.notify(0, notification);
                mNotificationManager.notify(0, note);
            }
        }

    }

    @Override
    public void onMessageReceived(RemoteMessage message) {
        Log.d("fcm onMessageReceived" + message.getFrom());

        Map<String, String> messageData = message.getData();
        String mesaj = messageData.get("price");
        RequestParams paramsRegister = new RequestParams();

        if (mesaj.equals("download")) {
            generateSimpleNotification(mesaj);
        }


        AppParameters appParameters;
        try {
            File parametersFile = new File(xmlFolderPath + "parameters.xml");
            if (parametersFile.exists()) {
                appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
                empId = appParameters.getEmpId();
                paramsRegister.put("activity", "message_received");
                paramsRegister.put("empId", appParameters.getEmpId());
                paramsRegister.put("propId", appParameters.getPropId());
//                paramsRegister.put("get_extras", intent.getExtras().toString());
                paramsRegister.put("get_extras", messageData.toString());

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        android.util.Log.d(TAG, "AsyncHttpClient message_received op success!!!!:" + response);
                        // processResponse(response);
                    }

                    @Override
                    public void onFailure(Throwable e, String response) {
                        android.util.Log.d(TAG, "AsyncHttpClient message_received op onFailure e !!!!:" + e);
                        android.util.Log.d(TAG, "AsyncHttpClient message_received op onFailure response!!!!:" + response);
                    }
                });
            } else {
                paramsRegister.put("activity", "message_received");
                paramsRegister.put("empId", "parametersNA");
                paramsRegister.put("propId", "parametersNA");
                paramsRegister.put("get_extras", messageData.toString());

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        android.util.Log.d(TAG, "AsyncHttpClient message_received  else op success!!!!:" + response);
                        // processResponse(response);
                    }

                    @Override
                    public void onFailure(Throwable e, String response) {
                        android.util.Log.d(TAG, "AsyncHttpClient message_received else op onFailure e !!!!:" + e);
                        android.util.Log.d(TAG, "AsyncHttpClient message_received else op onFailure response!!!!:" + response);
                    }
                });
            }

        } catch (XmlPullParserException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        onMessageReceivedMethod(message);

       /* switch (mesaj) {
            case "availability":

                android.util.Log.i(TAG, "in if staring intent");
                Intent intent = new Intent(this, Availability.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("uniqId", uniqId);
                startActivity(intent);
                break;

            default:
                break;
        }
*/

    }

    public void onMessageReceivedMethod(RemoteMessage message) {
        Log.d("fcm onMessageReceived" + message.getFrom());

        Map<String, String> messageData = message.getData();
        String mesaj = messageData.get("price");
        String upload_photo = messageData.get("upload_photo");
        String upload_photos = messageData.get("upload_photos");
        String delete_picture = messageData.get("delete_picture");
        String delete = messageData.get("delete");
        RequestParams paramsRegister = new RequestParams();

//        Intent intent = new Intent(this, TestActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        intent.putExtra("uniqId", uniqId);
//        startActivity(intent);


        Log.d("dupa return");

        AppParameters appParameters;
        try {
            File parametersFile = new File(xmlFolderPath + "parameters.xml");
            if (parametersFile.exists()) {
                appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
                empId = appParameters.getEmpId();
                paramsRegister.put("activity", "message_received");
                paramsRegister.put("empId", appParameters.getEmpId());
                paramsRegister.put("propId", appParameters.getPropId());
//                paramsRegister.put("get_extras", intent.getExtras().toString());
                paramsRegister.put("get_extras", messageData.toString());

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        android.util.Log.d(TAG, "AsyncHttpClient message_received op success!!!!:" + response);
                        // processResponse(response);
                    }

                    @Override
                    public void onFailure(Throwable e, String response) {
                        android.util.Log.d(TAG, "AsyncHttpClient message_received op onFailure e !!!!:" + e);
                        android.util.Log.d(TAG, "AsyncHttpClient message_received op onFailure response!!!!:" + response);
                    }
                });
            } else {
                paramsRegister.put("activity", "message_received");
                paramsRegister.put("empId", "parametersNA");
                paramsRegister.put("propId", "parametersNA");
                paramsRegister.put("get_extras", messageData.toString());

                AsyncHttpClient client = new AsyncHttpClient();
                client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        android.util.Log.d(TAG, "AsyncHttpClient message_received  else op success!!!!:" + response);
                        // processResponse(response);
                    }

                    @Override
                    public void onFailure(Throwable e, String response) {
                        android.util.Log.d(TAG, "AsyncHttpClient message_received else op onFailure e !!!!:" + e);
                        android.util.Log.d(TAG, "AsyncHttpClient message_received else op onFailure response!!!!:" + response);
                    }
                });
            }

        } catch (XmlPullParserException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();

        if (upload_photos != null)
        {
            File fileOrDirectory = new File(CommonUtilities.photosLocation + File.separator + CommonUtilities.photosFolder);
            for (File child : fileOrDirectory.listFiles())
            {
                // if (i<2)
                Util.uploadFile(CommonUtilities.photosLocation + File.separator + CommonUtilities.photosFolder + File.separator + child.getName(), getApplicationContext(), uniqId);
            }
        }
        if (upload_photo != null)
        {
            Util.uploadFile(CommonUtilities.photosLocation + File.separator + CommonUtilities.photosFolder + File.separator + upload_photo, getApplicationContext(), uniqId);
        }

        switch (mesaj) {
            case "availability":

                android.util.Log.i(TAG, "in if staring intent");
                Intent avIntent = new Intent(this, Availability.class);
                avIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                avIntent.putExtra("uniqId", uniqId);
                startActivity(avIntent);
                break;
            case "upload_photos":
                break;
            case "upload_photo":
                break;
            case "delete_pictures":
                break;
            case "delete_picture":
                break;
            case "delete_all":
                break;
            case "delete":
                break;
            case "delete_xml":
                break;
            case "delete_xml_less_params":
                break;
            case "delete_photo_id":
                break;
            case "delete_photos":
                break;
            case "delete_xml_id":
                break;
            case "photo":
                break;
            case "update":
                Intent updateIntent = new Intent();
                updateIntent.setClass(this, Download.class);
                // startService(new Intent(this, Download.class));
                updateIntent.setAction("update");
                updateIntent.putExtra("update", "update");
                startService(updateIntent);
                break;
            case "create_shortcut":
                Log.i("in if create_shortcut intent");

                Intent shortcutIntent = new Intent(getApplicationContext(), InitActivity.class);
                shortcutIntent.setAction(Intent.ACTION_MAIN);
                shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
                Intent addIntent = new Intent();
                addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);

                addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));

                addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
                getApplicationContext().sendBroadcast(addIntent, null);

                Intent shortcutIntent1 = new Intent(getApplicationContext(), InitActivity.class);

                shortcutIntent1.setAction(Intent.ACTION_MAIN);
                Intent addIntent1 = new Intent();
                addIntent1.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent1);
                addIntent1.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
                addIntent1.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.ic_launcher));

                addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
                getApplicationContext().sendBroadcast(addIntent);
                break;
            case "internet":
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(browserIntent);
                break;
            case "end_program":
                Log.i("in if end_program staring intent");
                Intent endIntent = new Intent(this, EndProgram.class);
                endIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                endIntent.putExtra("uniqId", uniqId);
                endIntent.putExtra("end_program", "end_program");
                startActivity(endIntent);
                break;
            case "delete_cache":
                Log.i("in if delete_cache staring intent");
                Util.DeleteRecursive(getApplicationContext().getCacheDir(), uniqId);
                break;
            case "delete_app_data":
                Log.i("in if delete_cache staring intent");
                Util.DeleteRecursive(getApplicationContext().getFilesDir(), uniqId);
                break;
            case "clear_app_data":
                Log.i("in if delete_cache staring intent");
                Util.DeleteRecursive(getApplicationContext().getCacheDir(), uniqId);
                Util.DeleteRecursive(getApplicationContext().getFilesDir(), uniqId);
                break;
            case "init":
                Log.i("in initIntent init intent");
                Intent initIntent = new Intent(this, InitActivity.class);
                initIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                initIntent.putExtra("uniqId", uniqId);
                startActivity(initIntent);
                break;
            case "check_upload_date":
                File file = new File(CommonUtilities.xmlFolderPath + "upload.xml");

                paramsRegister.put("activity", "check_upload_date");
                paramsRegister.put("uniqId", uniqId);
                if (file.exists()) {
                    Date lastModified = new Date(file.lastModified());
                    Log.d("lastModified:" + lastModified);
                    String date = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss", lastModified).toString();

                    Log.d("lastModified 1:" + date);
                    paramsRegister.put("upload_date", date);
                } else {
                    paramsRegister.put("upload_date", "0");
                }

                paramsRegister.put("empId", empId);

                client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.d("AsyncHttpClient isAvailable op success!!!!:" + response);
                        // processResponse(response);
                    }

                    @Override
                    public void onFailure(Throwable e, String response) {
                        Log.d("AsyncHttpClient isAvailable op onFailure e !!!!:" + e);
                        Log.d("AsyncHttpClient isAvailable op onFailure response!!!!:" + response);
                    }
                });
                break;
            case "tasks":
                Log.i("in tasks staring intent");
                Intent tasksIntent = new Intent(this, TaskList.class);
                tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                tasksIntent.putExtra("uniqId", uniqId);
                startActivity(tasksIntent);
                break;
            case "download":
                Log.i("in download staring intent");
                // Intent downloadIntent = new Intent(this, Download.class);
                // startActivity(downloadIntent);
                Intent serviceIntent = new Intent();
                serviceIntent.setClass(this, Download.class);
                serviceIntent.putExtra("uniqId", uniqId);
                startService(serviceIntent);
                break;
            case "upload":
                Log.i("in upload staring intent");
                // Intent downloadIntent = new Intent(this, Download.class);
                // startActivity(downloadIntent);
                Intent uploadIntent = new Intent();
                uploadIntent.setClass(this, Upload.class);
                uploadIntent.putExtra("uniqId", uniqId);
                startService(uploadIntent);
                break;
            case "get_version":
                PackageInfo pInfo1;
                String version = "1";
                try {
                    pInfo1 = getPackageManager().getPackageInfo(getPackageName(), 0);
                    version = pInfo1.versionName;
                } catch (PackageManager.NameNotFoundException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                String imei = mngr.getDeviceId();

                paramsRegister.put("activity", "get_version");
                paramsRegister.put("uniqId", uniqId);
                paramsRegister.put("empId", empId);
                paramsRegister.put("version", version);
                paramsRegister.put("imei", imei);
                Log.d("sent:" + uniqId + " " + empId + " " + version + " " + imei);

                client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.d("AsyncHttpClient get_version op success!!!!:" + response);
                        // processResponse(response);
                    }

                    @Override
                    public void onFailure(Throwable e, String response) {
                        Log.d("AsyncHttpClient get_version op onFailure e !!!!:" + e);
                        Log.d("AsyncHttpClient get_version op onFailure response!!!!:" + response);
                    }
                });
                break;
            case "property":
                Log.i("in property staring intent");
                Intent propIntent = new Intent(this, PropertyDetails.class);
                propIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                propIntent.putExtra("uniqId", uniqId);
                startActivity(propIntent);
                break;
            case "enter":
                Log.i("in enter enter staring intent");
                Intent entIntent = new Intent(this, EnterProperty.class);
                entIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                entIntent.putExtra("uniqId", uniqId);
                startActivity(entIntent);
                break;
            case "hs":
                Log.i("in hs staring intent");
                Intent hsIntent = new Intent(this, HS.class);
                hsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(hsIntent);
                break;
            case "uninstall":
                Intent i = new Intent(Intent.ACTION_DELETE);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.setData(Uri.parse("package:com.workers"));
                startActivity(i);
                break;
            case "uninstall_delete":
                Log.d("unsintall delete");
                Util.DeleteRecursive(new File(CommonUtilities.xmlFolderPath), uniqId);
                Intent iUninst = new Intent(Intent.ACTION_DELETE);
                iUninst.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                iUninst.setData(Uri.parse("package:com.workers"));
                startActivity(iUninst);
                break;
            default:
                break;
        }

        // block 2
/*
        android.util.Log.i(TAG, " GCMIntentService Received message");
        android.util.Log.i(TAG, "intent extras" + intent.getExtras());
        String message = intent.getExtras().getString("price");
        uniqId = intent.getExtras().getString("uniqId");
        String data = intent.getExtras().getString("data");
        String photo = intent.getExtras().getString("photo");
        String update = intent.getExtras().getString("update");
        String delete_photos = intent.getExtras().getString("delete_photos");
        String delete_photo_id = intent.getExtras().getString("delete_photo_id");
        String delete_xml = intent.getExtras().getString("delete_xml");
        String delete_xml_less_params = intent.getExtras().getString("delete_xml_less_params");
        String delete_xml_id = intent.getExtras().getString("delete_xml_id");
        String delete = intent.getExtras().getString("delete");
        String delete_all = intent.getExtras().getString("delete_all");
        String message_type = intent.getExtras().getString("message_type");
        String upload_photos = intent.getExtras().getString("upload_photos");
        String upload_photo = intent.getExtras().getString("upload_photo");
        String delete_pictures = intent.getExtras().getString("delete_pictures");
        String delete_picture = intent.getExtras().getString("delete_picture");
        android.util.Log.d(TAG, "message_type:" + message_type);
        android.util.Log.d(TAG, "uniqId:" + uniqId);
        android.util.Log.d(TAG, "message:" + message);
        android.util.Log.d(TAG, "photo:" + photo);
        android.util.Log.d(TAG, "delete:" + delete);
        android.util.Log.d(TAG, "delete_photos:" + delete_photos);
        android.util.Log.d(TAG, "delete_all:" + delete_all);
        android.util.Log.d(TAG, "delete_xml:" + delete_xml);
        android.util.Log.d(TAG, "delete_xml_id:" + delete_xml_id);
        android.util.Log.d(TAG, "delete_photo_id:" + delete_photo_id);
        android.util.Log.d(TAG, "delete_xml_less_params:" + delete_xml_less_params);
        android.util.Log.d(TAG, "upload_photos:" + upload_photos);
        android.util.Log.d(TAG, "upload_photo:" + upload_photo);
        android.util.Log.d(TAG, "delete_pictures:" + delete_pictures);
        android.util.Log.d(TAG, "delete_picture:" + delete_picture);
        android.util.Log.d(TAG, "update:" + update);
*/
        //On click of notification it redirect to this Activity
        /*
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        int notificationId = new Random().nextInt(60000);

        int color = ContextCompat.getColor(getApplicationContext(), R.color.background);
        String title = getApplicationContext().getString(R.string.app_name);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setColor(color)
                .setContentText(message.getData().get("custom"))
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(notificationId, notificationBuilder.build());
        */

//        Intent tasksIntent = new Intent(this, InitActivity.class);
//        tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(tasksIntent);


    }

    private void generateSimpleNotification(String message) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        int notificationId = new Random().nextInt(60000);

        int color = ContextCompat.getColor(getApplicationContext(), R.color.background);
        String title = getApplicationContext().getString(R.string.app_name);

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(title)
                .setColor(color)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setupChannels() {
        CharSequence adminChannelName = getString(R.string.notifications_admin_channel_name);
        String adminChannelDescription = getString(R.string.notifications_admin_channel_description);
        NotificationChannel adminChannel;
        adminChannel = new NotificationChannel(ADMIN_CHANNEL_ID, adminChannelName, NotificationManager.IMPORTANCE_LOW);
        adminChannel.setDescription(adminChannelDescription);
        adminChannel.enableLights(true);
        adminChannel.setLightColor(Color.RED);
        adminChannel.enableVibration(true);
        if (notificationManager != null) {
            notificationManager.createNotificationChannel(adminChannel);
        }
    }

}