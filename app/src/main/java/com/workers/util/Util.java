package com.workers.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Xml;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.activities.EndProgram;
import com.workers.adapters.ListToggleAdapter;
import com.workers.activities.MainActivity;
import com.workers.activities.TaskList;
import com.workers.activities.Quality;
import com.workers.activities.QualityWG;
import com.workers.types.AppParameters;
import com.workers.types.Concern;
import com.workers.types.Notes;
import com.workers.types.PhotoType;
import com.workers.types.Property;
import com.workers.types.Tasks;
import com.workers.types.UploadType;

import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.workers.util.CommonUtilities.currDWritten;
import static com.workers.util.CommonUtilities.imgPillServer;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.sdCardPath;
import static com.workers.util.CommonUtilities.uploadFilesServer;
import static com.workers.util.CommonUtilities.xmlFolderPath;

public class Util
{
	public static Boolean photosGot = false;

	public static Boolean getPhotosGot()
	{
		return photosGot;
	}

	public static void setPhotosGot(Boolean photosGot)
	{
		Util.photosGot = photosGot;
	}

	public static Boolean stringToBool(String string)
	{
		if (string.equals("true"))
			return true;
		return false;
	}

	public static String boolToString(Boolean boolVar)
	{
		if (boolVar)
			return "true";
		return "false";
	}

	public static void goNext(Context context, String currentClass)
	{
		if (currentClass.toLowerCase().equalsIgnoreCase("HS"))
		{
			Log.d("launching from HS");
			Intent i = new Intent(context, Quality.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		}
		if (currentClass.toLowerCase().equalsIgnoreCase("quality"))
		{
			Log.d("launching from  quality");
			Intent i = new Intent(context, QualityWG.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);

		}
		if (currentClass.toLowerCase().equalsIgnoreCase("qualitywg"))
		{

			// Util.initTasksXml();
			Intent i = new Intent(context, TaskList.class);
			i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(i);
		}
	}

	public static boolean checkParametersExists(Context context)
	{
		File parametersFile = new File(xmlFolderPath + "parameters.xml");
		if (parametersFile.exists())
		{

			AppParameters appParameters;
			try
			{
				appParameters = loadParametersFromSdCard(xmlFolderPath, "parameters");
				if (appParameters.getEmpId().equals("1"))
				{
					Intent tasksIntent = new Intent(context, MainActivity.class);
					tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					// tasksIntent.putExtra("moveback", "moveback");
					// tasksIntent.putExtra("end_program", "end_program");
					context.startActivity(tasksIntent);
					return false;
				}

			}
			catch (XmlPullParserException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;
		}
		else
		{
			Intent tasksIntent = new Intent(context, MainActivity.class);
			tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			// tasksIntent.putExtra("moveback", "moveback");
			// tasksIntent.putExtra("end_program", "end_program");
			context.startActivity(tasksIntent);
			return false;
		}

	}

	public static void getXmls(final Context context, String propId, final String uniqId)
	{
		try
		{
			File parametersFile = new File(xmlFolderPath + "parameters.xml");
			if (parametersFile.exists())
			{
				AppParameters appParameters = loadParametersFromSdCard(xmlFolderPath, "parameters");
				final String empId = appParameters.getEmpId();
				Log.d("in get xmls empid:" + empId);
				Log.d("in get xmls propId:" + propId);
				RequestParams paramsProperty = new RequestParams();
				if (propId.equals("0"))
				{
					Log.d("in if principal");

					RequestParams paramsCurrProperty = new RequestParams();
					paramsCurrProperty.put("activity", "enterProperty");
					paramsCurrProperty.put("empId", empId);
					// DeleteRecursive(new
					// File(xmlFolderPath+"propertiesCurrDetails.xml"));
					AsyncHttpClient client1 = new AsyncHttpClient();
					client1.setTimeout(20000);
					client1.setMaxRetriesAndTimeout(7, 30000);
					client1.setMaxConnections(20);
					client1.post(op, paramsCurrProperty, new AsyncHttpResponseHandler()
					{
						@Override
						public void onSuccess(String response)
						{
							Log.d("AsyncHttpClient PropertyCurrDetails suc op success!!!!:" + response);
							Log.d("xmlFolderPath propertiesCurrDetails:" + xmlFolderPath + "propertiesCurrDetails.xml");
							File details = new File(xmlFolderPath + "propertiesCurrDetails.xml");
							if (details.exists())
							{
								Log.d("delete curr details before write:");
								details.delete();
								// DeleteRecursive(details);
							}
							Log.d("currDWritten utils:" + currDWritten);
							Util.writeFileOnSDCard(response, context, xmlFolderPath, "propertiesCurrDetails.xml", uniqId);

							Log.d("currDWritten utils:" + currDWritten);
							List<Property> resultSdCard;
							RequestParams paramsRegister = new RequestParams();
							paramsRegister.put("activity", "ack_propertiesCurrDetails");
							paramsRegister.put("uniqId", uniqId);
							paramsRegister.put("empId", empId);
							// paramsRegister.put("updated", "yes");
							AsyncHttpClient client = new AsyncHttpClient();
							client.post(op, paramsRegister, new AsyncHttpResponseHandler()
							{
								@Override
								public void onSuccess(String response)
								{
									Log.d("AsyncHttpClient ack_propertiesCurrDetails op success!!!!:" + response);
								}

								@Override
								public void onFailure(Throwable e, String response)
								{
									Log.d("AsyncHttpClient ack_propertiesCurrDetails op onFailure e !!!!:" + e);
									Log.d("AsyncHttpClient ack_propertiesCurrDetails op onFailure response!!!!:" + response);
								}
							});
							try
							{
								if (new File(xmlFolderPath + "propertiesCurrDetails.xml").exists())
								{
									resultSdCard = Util.loadPropertiesXmlFromSdCard(context, xmlFolderPath, "propertiesCurrDetails");

									Log.d("resultSdCard curr main resultSdCard:" + resultSdCard.size());
									Log.d("resultSdCard curr empId:" + empId);
									if (resultSdCard != null)
										if (resultSdCard.size() == 0)
										{
											// AlertDialog.Builder
											// alertDialogBuilder =
											// new
											// AlertDialog.Builder(context);
											// // set title
											// alertDialogBuilder.setTitle("No properties");
											// // set dialog message
											// alertDialogBuilder.setMessage(" ").setCancelable(false).setPositiveButton("OK",
											// new
											// DialogInterface.OnClickListener()
											// {
											// public void
											// onClick(DialogInterface
											// dialog,
											// int id)
											// {
											// Log.d("exit!!!!");
											// return;
											// // context.
											// }
											// });
											// // create alert dialog
											// AlertDialog alertDialog =
											// alertDialogBuilder.create();
											// // show it
											// alertDialog.show();
											return;
										}
									// Log.d("resultSdCard curr main propId:"+resultSdCard.get(0).getId());
									for (final Property property : resultSdCard)
									{
										getXmlsAllProp(context, empId, property.getId(), uniqId);
										try
										{
											Log.d("gett sleeping");
											Thread.sleep(500);
										}
										catch (InterruptedException e1)
										{
											// TODO Auto-generated catch block
											e1.printStackTrace();
										}
									}
								}
							}
							catch (XmlPullParserException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							catch (IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}

						@Override
						public void onFailure(Throwable e, String response)
						{
							Log.d("AsyncHttpClient propertiesCurrDetails op onFailure e !!!!:" + e);
							Log.d("AsyncHttpClient propertiesCurrDetails op onFailure response!!!!:" + response);
						}
					});
					paramsProperty.put("activity", "property");
					paramsProperty.put("empId", empId);
					// AppParameters appParameters =
					// Util.loadParametersFromSdCard(xmlFolderPath,
					// "parameters");

					AsyncHttpClient client = new AsyncHttpClient();
					client.setTimeout(20000);
					client.setMaxRetriesAndTimeout(7, 30000);
					client.setMaxConnections(20);
					client.post(op, paramsProperty, new AsyncHttpResponseHandler()
					{
						@Override
						public void onSuccess(String response)
						{
							Log.d("AsyncHttpClient PropertyDetails op success!!!!:" + response);
							File details = new File(xmlFolderPath + "propertiesDetails.xml");
							Util.writeFileOnSDCard(response, context, xmlFolderPath, "propertiesDetails.xml", uniqId);
							RequestParams paramsRegister = new RequestParams();
							paramsRegister.put("activity", "ack_propertiesDetails");
							paramsRegister.put("empId", empId);
							paramsRegister.put("uniqId", uniqId);
							// paramsRegister.put("updated", "yes");
							AsyncHttpClient client = new AsyncHttpClient();
							client.post(op, paramsRegister, new AsyncHttpResponseHandler()
							{
								@Override
								public void onSuccess(String response)
								{
									Log.d("AsyncHttpClient ack_propertiesDetails op success!!!!:" + response);
								}

								@Override
								public void onFailure(Throwable e, String response)
								{
									Log.d("AsyncHttpClient ack_propertiesDetails op onFailure e !!!!:" + e);
									Log.d("AsyncHttpClient ack_propertiesDetails op onFailure response!!!!:" + response);
								}
							});
							Log.d("xmlFolderPath propertiesDetails:" + xmlFolderPath + "propertiesDetails.xml");

							// processResponse(response);

						}

						@Override
						public void onFailure(Throwable e, String response)
						{
							Log.d("AsyncHttpClient PropertyDetails op onFailure e !!!!:" + e);
							Log.d("AsyncHttpClient PropertyDetails op onFailure response!!!!:" + response);
						}
					});
				}
				else
				{

				}
			}
			else
			{
				Intent tasksIntent = new Intent(context, MainActivity.class);
				tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				// tasksIntent.putExtra("moveback", "moveback");
				// tasksIntent.putExtra("end_program", "end_program");
				context.startActivity(tasksIntent);

			}

		}
		catch (XmlPullParserException e2)
		{
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (IOException e2)
		{
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}

	public static void get(final String activity, final String empId, final String propId, final Context context, final String uniqId)
	{
		Log.d("in gettt de " + activity);
		Log.d("in gettt de empId" + empId);
		Log.d("in gettt de propId" + propId);
		RequestParams paramsTasks = new RequestParams();
		final String activityP = activity.toLowerCase();
		paramsTasks.put("activity", activity);

		paramsTasks.put("empId", empId);
		paramsTasks.put("propId", propId);

		// HttpClient client = new DefaultHttpClient();
		// HttpPost post = new HttpPost(op);
		// try
		// {
		// List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
		// nameValuePairs.add(new BasicNameValuePair("activity", activity));
		// nameValuePairs.add(new BasicNameValuePair("empId", empId));
		// nameValuePairs.add(new BasicNameValuePair("propId", propId));
		// post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		//
		// HttpResponse response = client.execute(post);
		// BufferedReader rd = new BufferedReader(new
		// InputStreamReader(response.getEntity().getContent()));
		// String line = "";
		// while ((line = rd.readLine()) != null)
		// {
		// System.out.println(line);
		// Log.d("response:"+line);
		// }
		//
		// }
		// catch (IOException e)
		// {
		// e.printStackTrace();
		// }

		// Log.d("response:" + activity + " " + activity + "_" + propId +
		// ".xml: " + text);
		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(20000);
		client.setMaxRetriesAndTimeout(7, 30000);
		client.setMaxConnections(20);
		Log.d("client.getMaxConnections():" + client.getMaxConnections());
		client.post(op, paramsTasks, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{

				Log.d("AsyncHttpClient " + activity + " " + activity + "_" + propId + ".xml" + " op success!!!!:" + response);
				Util.writeFileOnSDCard(response, context, xmlFolderPath, activityP + "_" + propId + ".xml", uniqId);

				RequestParams paramsRegister = new RequestParams();
				paramsRegister.put("activity", "ack_lists_xml");
				paramsRegister.put("type", activity);
				paramsRegister.put("uniqId", uniqId);
				paramsRegister.put("empId", empId);
				paramsRegister.put("propIdId", propId);
				// paramsRegister.put("updated", "yes");
				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("AsyncHttpClient ack_lists_xml " + activityP + "_" + propId + ".xml" + " op success!!!!:" + response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("AsyncHttpClient ack_lists_xml " + activityP + "_" + propId + ".xml" + " op onFailure e !!!!:" + e);
						Log.d("AsyncHttpClient ack_lists_xml " + activityP + "_" + propId + ".xml" + " op onFailure response!!!!:" + response);
					}
				});

				// processResponse(response);

			}

			@Override
			public void onFailure(Throwable e, String response)
			{
				// get(activity, empId, propId, context);
				RequestParams paramsRegister = new RequestParams();
				paramsRegister.put("activity", "ack_lists_xml_failed");
				paramsRegister.put("type", activity);
				paramsRegister.put("uniqId", uniqId);
				paramsRegister.put("empId", empId);
				paramsRegister.put("propIdId", propId);
				// paramsRegister.put("updated", "yes");
				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("AsyncHttpClient ack_lists_xml_failed " + activityP + "_" + propId + ".xml" + " op success!!!!:" + response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("AsyncHttpClient ack_lists_xml_failed " + activityP + "_" + propId + ".xml" + " op onFailure e !!!!:" + e);
						Log.d("AsyncHttpClient ack_lists_xml_failed " + activityP + "_" + propId + ".xml" + " op onFailure response!!!!:" + response);
					}
				});
				Log.d("AsyncHttpClient " + activity + " op onFailure e !!!!:" + e);
				Log.d("AsyncHttpClient " + activity + " op onFailure response!!!!:" + response);
			}
		});

	}

	public static void getXmlsAllProp(final Context context, final String empId, final String propId, final String uniqId)
	{
		Log.d("get all xmls");
		get("hs", empId, propId, context, uniqId);
		get("quality", empId, propId, context, uniqId);
		get("qualityWG", empId, propId, context, uniqId);
		get("task", empId, propId, context, uniqId);
		/*
		 * Handler handler = new Handler(); handler.postDelayed(new Runnable() {
		 * 
		 * @Override public void run() { Log.d("get quality"); get("quality",
		 * empId, propId, context, uniqId); }
		 * 
		 * }, 500); handler.postDelayed(new Runnable() {
		 * 
		 * @Override public void run() { Log.d("get qualityWG");
		 * get("qualityWG", empId, propId, context, uniqId); }
		 * 
		 * }, 1000); handler.postDelayed(new Runnable() {
		 * 
		 * @Override public void run() { Log.d("get task"); get("task", empId,
		 * propId, context, uniqId); }
		 * 
		 * }, 1500);
		 */
	}

	public static String convertStreamToString(InputStream is) throws Exception
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null)
		{
			sb.append(line).append("\n");
		}
		reader.close();
		return sb.toString();
	}

	public static String getStringFromFile(String filePath) throws Exception
	{
		File fl = new File(filePath);
		FileInputStream fin = new FileInputStream(fl);
		String ret = convertStreamToString(fin);
		Log.d("ret:" + ret);
		// Make sure you close all streams.
		fin.close();
		return ret;
	}

	public static List<File> getListFiles(File parentDir)
	{
		ArrayList<File> inFiles = new ArrayList<File>();
		File[] files = parentDir.listFiles();
		for (File file : files)
		{
			if (file.isDirectory())
			{
				inFiles.addAll(getListFiles(file));
			}
			else
			{
				if (file.getName().endsWith(".xml"))
				{
					inFiles.add(file);
				}
			}
		}
		return inFiles;
	}

	public static List<File> getXmlToUPloadFiles(File parentDir, String uniqId)
	{
		ArrayList<File> inFiles = new ArrayList<File>();
		File[] files = parentDir.listFiles();
		for (File file : files)
		{
			Log.d("getXmlToUPloadFiles:" + file.getName());
			if (file.getName().equalsIgnoreCase("upload.xml"))
			{
				try
				{
					List<UploadType> resultSdCard = loadUploadXmlFromSdCard(xmlFolderPath, "upload");
					Util.saveUploadListToXml(resultSdCard, "cls", uniqId);
				}
				catch (XmlPullParserException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (file.getName().equalsIgnoreCase("notes.xml"))
			{
				try
				{

					List<Notes> resultSdCard = loadNotesXmlFromSdCard(xmlFolderPath, "notes");
					Util.saveNotesToXml(resultSdCard, uniqId);
				}
				catch (XmlPullParserException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (file.getName().equalsIgnoreCase("photos.xml"))
			{
				try
				{
					List<PhotoType> resultSdCard = loadPhotosXmlFromSdCard(xmlFolderPath, "photos");
					Util.savePhotosListToXml(resultSdCard, uniqId);
				}
				catch (XmlPullParserException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (file.getName().equalsIgnoreCase("upload.xml") || file.getName().equalsIgnoreCase("notes.xml") || file.getName().equalsIgnoreCase("photos.xml"))
			{
				Log.d("getXmlToUPloadFiles vizate:" + file.getName());
				inFiles.add(file);
			}
			// if (file.getName().endsWith(".xml"))
			// {
			// if (file.getName().equalsIgnoreCase("upload") ||
			// file.getName().equalsIgnoreCase("notes") ||
			// file.getName().equalsIgnoreCase("photos"))
			// {
			// Log.d("getXmlToUPloadFiles vizate:"+file.getName());
			// inFiles.add(file);
			// }
			// }
			// inFiles.addAll(getListFiles(file));
			// else
			// {
			// if (file.getName().endsWith(".xml"))
			// {
			// inFiles.add(file);
			// }
			// }
		}
		Log.d("getXmlToUPloadFiles size:" + inFiles.size());
		return inFiles;
	}

	public static void createXmlFolder(String path)
	{
		File myXmlFolder = new File(path);
		if (!myXmlFolder.exists())
		{
			if (myXmlFolder.mkdirs())
			{
				Log.d("made folder:" + path);
			}
			else
			{
				Log.d("not made folder:" + path);
			}
		}
	}

	public static void writeFileOnSDCard(String strWrite, Context context, String folderPath, final String fileName, String uniqId)
	{

		try
		{
			if (isSdReadable()) // isSdReadable()e method is define at bottom of
			// the post
			{
				// String fullPath =
				// Environment.getExternalStorageDirectory().getAbsolutePath();
				// if (file_iterations==0)
				if (fileName.equals("propertiesCurrDetails.xml"))
				{
					Log.d("currDWritten in sfarsit writeFileOnSDCard:" + CommonUtilities.currDWritten);
					CommonUtilities.currDWritten = "0";
				}
				File myFile = new File(folderPath + File.separator + "/" + fileName);
				if (myFile.exists())
				{
					Log.d("exista " + fileName + " si va fi sters");
					if (myFile.delete())
					{
						Log.d(fileName + " a fost sters");
					}
					else
					{
						Log.d(fileName + " nu a fost sters");
					}
				}
				FileOutputStream fOut = new FileOutputStream(myFile);
				OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
				myOutWriter.append(strWrite);
				myOutWriter.close();
				fOut.close();
				if (fileName.equals("propertiesCurrDetails.xml"))
				{

					CommonUtilities.currDWritten = "1";
					Log.d("currDWritten in sfarsit writeFileOnSDCard:" + CommonUtilities.currDWritten);
				}

				/*
				 * RequestParams paramsRegister = new RequestParams();
				 * paramsRegister.put("activity", "ack_file_written");
				 * paramsRegister.put("uniqId", uniqId);
				 * paramsRegister.put("file", fileName);
				 * 
				 * AsyncHttpClient client = new AsyncHttpClient();
				 * client.post(op, paramsRegister, new
				 * AsyncHttpResponseHandler() {
				 * 
				 * @Override public void onSuccess(String response) {
				 * Log.d("AsyncHttpClient ack_file_written " + fileName +
				 * " op success!!!!:" + response); // processResponse(response);
				 * }
				 * 
				 * @Override public void onFailure(Throwable e, String response)
				 * { Log.d("AsyncHttpClient ack_file_written " + fileName +
				 * " op onFailure e !!!!:" + e);
				 * Log.d("AsyncHttpClient ack_file_written " + fileName +
				 * " op onFailure response!!!!:" + response); } });
				 */
			}
		}
		catch (Exception e)
		{
			Log.d("Exception writeFileOnSDCard:" + e);
		}
	}

	public static void writeFileOnSDCardIteration(String strWrite, Context context, String folderPath, String fileName, int iteration)
	{
		try
		{
			if (isSdReadable()) // isSdReadable()e method is define at bottom of
								// the post
			{
				// String fullPath =
				// Environment.getExternalStorageDirectory().getAbsolutePath();
				File myFile = null;
				if (iteration == 1)
				{
					myFile = new File(folderPath + File.separator + "/" + fileName);
				}

				FileOutputStream fOut = new FileOutputStream(myFile);
				OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
				myOutWriter.append(strWrite);
				myOutWriter.close();
				fOut.close();
			}
		}
		catch (Exception e)
		{
			Log.d("Exception writeFileOnSDCard:" + e);
		}
	}

	public static String readFileFromSDCard(String fileName, Context context)
	{
		String stringToReturn = "";
		try
		{
			if (isSdReadable()) // isSdReadable()e method is define at bottom of
								// the post
			{
				String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "/" + fileName;

				InputStream inputStream = context.openFileInput(fullPath);

				if (inputStream != null)
				{
					InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
					BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
					String receiveString = "";
					StringBuilder stringBuilder = new StringBuilder();

					while ((receiveString = bufferedReader.readLine()) != null)
					{
						stringBuilder.append(receiveString);
					}
					inputStream.close();
					stringToReturn = stringBuilder.toString();
				}
			}
		}
		catch (FileNotFoundException e)
		{
			Log.d("File not found: " + e.toString());
		}
		catch (IOException e)
		{
			Log.d("Can not read file: " + e.toString());
		}

		return stringToReturn;
	}

	public static boolean isSdReadable()
	{

		boolean mExternalStorageAvailable = false;
		try
		{
			String state = Environment.getExternalStorageState();

			if (Environment.MEDIA_MOUNTED.equals(state))
			{
				// We can read and write the media
				mExternalStorageAvailable = true;
				Log.d("isSdReadable External storage card is readable.");
			}
			else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state))
			{
				// We can only read the media
				Log.d("isSdReadable External storage card is readable.");
				mExternalStorageAvailable = true;
			}
			else
			{
				// Something else is wrong. It may be one of many other
				// states, but all we need to know is we can neither read nor
				// write
				mExternalStorageAvailable = false;
			}
		}
		catch (Exception ex)
		{

		}
		return mExternalStorageAvailable;
	}

	public static void postFile(final String path)
	{
		Log.d("in postFile");
		// InputStream myInputStream = "blah";
		RequestParams params = new RequestParams();
		// params.put("secret_passwords", myInputStream, "passwords.txt");
		File myFile = new File(path);
		try
		{
			params.put("uploaded_file", myFile);
		}
		catch (FileNotFoundException e)
		{
			params.put("uploaded_file", "FILENOTFOUND");
			Log.d("exceptie ba la fisier:" + e);
		}
		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(20000);
		client.setMaxRetriesAndTimeout(7, 30000);
		client.setMaxConnections(20);
		// client.post("http://pill.pilon.co.uk/Example/xmpphp/gcm_server_php/scripts/upload_photo.php",
		// params, new AsyncHttpResponseHandler()
		// la ce mesaj
		// uniqId scriu in fisier SI SA TRIMIT MESAJ CATRE SERVER
		//
		client.post(uploadFilesServer, params, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("success with!!!!:" + path + "  " + response);
			}

			public void onFailure(Throwable e, String response)
			{
				Log.d("postFile op onFailure e !!!!:" + path + "  " + e);
				Log.d("postFile op onFailure response!!!!:" + path + "  " + response);
			}
		});
	}

	public static void saveStringToFile(String data, String fileName, Context context)
	{
		Log.d("saveStringToFile");
		Log.d("fileName:" + fileName);
		DataOutputStream out;
		try
		{
			out = new DataOutputStream(context.openFileOutput(fileName, Context.MODE_PRIVATE));
			out.writeUTF(data);
			out.close();
		}
		catch (FileNotFoundException e1)
		{
			Log.d("Exception File write failed: " + e1.toString());
			e1.printStackTrace();
		}
		catch (IOException e)
		{
			Log.d("IOException: " + e.toString());
			e.printStackTrace();
		}

		// try
		// {
		//
		// OutputStreamWriter outputStreamWriter = new
		// OutputStreamWriter(context.openFileOutput(fileName,
		// Context.MODE_PRIVATE));
		// outputStreamWriter.write(data);
		// outputStreamWriter.close();
		// }
		// catch (IOException e)
		// {
		// Log.d("Exception File write failed: " + e.toString());
		// }

	}

	public static void saveTaskListToXml(List<Tasks> list, String className, String propId)
	{
		Log.d("saveTaskListToXml");
		Log.d("Environment.getExternalStorageDirectory():" + Environment.getExternalStorageDirectory());
		Log.d("sdCardPath:" + sdCardPath);
		for (Tasks task : list)
		{
			Log.d("task:" + task.getName() + "quantity:" + task.getQuantity() + "marked:" + task.getSelected() + "id:" + task.getCode() + "ew:" + task.getEw());

		}
		className = className.toLowerCase();
		File newxmlfile = new File(xmlFolderPath + className + "_" + propId + ".xml");
		// File fileToUpload = new
		// File(Environment.getExternalStorageDirectory() + "/upload.xml");
		try
		{
			newxmlfile.createNewFile();
			// fileToUpload.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
			// fileos1 = new FileOutputStream(fileToUpload);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");
			Log.d("tasklist size:" + list.size());
			for (Tasks task : list)
			{
				Log.d("taskul de salvat:" + task.getName() + "marked:" + task.getSelected() + "quantity:" + task.getQuantity());
				Log.d("id:" + task.getCode());

				serializer.startTag(null, "task");
				serializer.attribute(null, "id", task.getCode());
				serializer.attribute(null, "q", task.getQuantity());
				serializer.attribute(null, "ew", task.getEw());
				if (task.getSelected() == "")
				{
					Log.d("hs.getSelected() ==''");
					serializer.attribute(null, "marked", "0");
				}
				else if (task.getSelected().equals(""))
				{
					Log.d("hs.getSelected() equals=''");
					serializer.attribute(null, "marked", "0");
				}
				else
				{
					Log.d("hs.getMarked(): " + task.getSelected());
					serializer.attribute(null, "marked", task.getSelected());
				}
				serializer.text(task.getName());
				serializer.endTag(null, "task");

			}
			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file has been created on SD card");
		}
		catch (Exception e)
		{
			Log.e("Exception error occurred while creating xml file:" + e);
		}
	}

	public static void createParametersXml(String empId, String propId, String regId, String empName)
	{
		Log.d("create parameters: empId:" + empId + " propId:" + propId + " regId:" + regId + " empName:" + empName);
		Log.d("Environment.getExternalStorageDirectory():" + Environment.getExternalStorageDirectory());
		File newxmlfile = new File(xmlFolderPath + "parameters" + ".xml");
		// File newxmlfileCurr = new File(xmlFolderPath +
		// "propertiesCurrDetails" + ".xml");
		try
		{
			newxmlfile.createNewFile();
			// newxmlfileCurr.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream2
		FileOutputStream fileos = null;
		// FileOutputStream fileos1 = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
			// fileos1 = new FileOutputStream(newxmlfileCurr);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");

			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");
			serializer.startTag(null, "item");
			serializer.attribute(null, "empId", empId);
			serializer.attribute(null, "propId", propId);
			serializer.attribute(null, "regId", regId);
			serializer.attribute(null, "empName", empName);
			serializer.endTag(null, "item");
			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file parameters has been created on SD card");

		}
		catch (Exception e)
		{
			Log.e("Exception error occurred while creating parameters xml file:" + e);
		}
		/*
		 * XmlSerializer serializer1 = Xml.newSerializer(); try { // we set the
		 * FileOutputStream as output for the serializer, using // UTF-8
		 * encoding serializer1.setOutput(fileos1, "UTF-8"); // Write <?xml
		 * declaration with encoding (if encoding not null) and // standalone
		 * flag (if standalone not null) serializer1.startDocument(null,
		 * Boolean.valueOf(true)); // set indentation option
		 * serializer1.setFeature
		 * ("http://xmlpull.org/v1/doc/features.html#indent-output", true); //
		 * start a tag called "root" serializer1.startTag(null, "xml");
		 * serializer1.startTag(null, "item"); serializer1.attribute(null,
		 * "empId", empId); serializer1.attribute(null, "propId", propId);
		 * serializer1.endTag(null, "item"); serializer1.endTag(null, "xml");
		 * serializer1.endDocument(); // write xml data into the
		 * FileOutputStream serializer1.flush(); // finally we close the file
		 * stream fileos1.close();
		 * 
		 * Log.d("file propertiesCurrDetails has been created on SD card"); }
		 * catch (Exception e) { Log.e(
		 * "Exception error occurred while creating propertiesCurrDetails xml file:"
		 * + e); }
		 */
	}

	public static void createUploadXml()
	{
		Log.d("create UploadXml");
		Log.d("Environment.getExternalStorageDirectory():" + Environment.getExternalStorageDirectory());
		File newxmlfile = new File(xmlFolderPath + "upload" + ".xml");
		if (!newxmlfile.exists())
		{
			try
			{
				newxmlfile.createNewFile();
			}
			catch (IOException e)
			{
				Log.e("IOException exception in createNewFile() method:" + e);
			}
			// we have to bind the new file with a FileOutputStream
			FileOutputStream fileos = null;
			try
			{
				fileos = new FileOutputStream(newxmlfile);
			}
			catch (FileNotFoundException e)
			{
				Log.e("FileNotFoundException can't create FileOutputStream:" + e);
			}
			// we create a XmlSerializer in order to write xml data
			XmlSerializer serializer = Xml.newSerializer();
			try
			{
				// we set the FileOutputStream as output for the serializer,
				// using
				// UTF-8 encoding
				serializer.setOutput(fileos, "UTF-8");
				// Write <?xml declaration with encoding (if encoding not null)
				// and
				// standalone flag (if standalone not null)
				serializer.startDocument(null, Boolean.valueOf(true));
				// set indentation option
				serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
				// start a tag called "root"
				serializer.startTag(null, "xml");

				serializer.endTag(null, "xml");
				serializer.endDocument();
				// write xml data into the FileOutputStream
				serializer.flush();
				// finally we close the file stream
				fileos.close();

				Log.d("file upload. xml has been created on SD card");
			}
			catch (Exception e)
			{
				Log.e("Exception error occurred while creating xml file:" + e);
			}
		}
	}

	public static void createNotesXml()
	{
		Log.d("UploadXml");
		Log.d("Environment.getExternalStorageDirectory():" + Environment.getExternalStorageDirectory());
		File newxmlfile = new File(xmlFolderPath + "notes" + ".xml");
		try
		{
			newxmlfile.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");

			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file notes. xml has been created on SD card");
		}
		catch (Exception e)
		{
			Log.e("Exception error occurred while creating notes xml file:" + e);
		}
	}

	public static void createPhotosXml()
	{
		Log.d("`createPhotosXml");
		Log.d("Environment.getExternalStorageDirectory():" + Environment.getExternalStorageDirectory());
		File newxmlfile = new File(xmlFolderPath + "photos" + ".xml");
		try
		{
			newxmlfile.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");

			// serializer.startTag(null, "item");
			// serializer.attribute(null, "id", "id");
			// serializer.attribute(null, "tag", "photos");
			// serializer.attribute(null, "path", "path");
			// serializer.attribute(null, "empId", "empId");
			// serializer.attribute(null, "propId", "propId");
			// serializer.attribute(null, "takenFrom", "takenFrom");
			// serializer.attribute(null, "takenFor", "takenFor");
			// serializer.endTag(null, "item");

			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file has been created on SD card");
		}
		catch (Exception e)
		{
			Log.e("Exception error occurred while creating xml file:" + e);
		}

	}

	public static void saveListToXml(List<Concern> list, String className)
	{
		Log.d("saveListToXml");
		Log.d("Environment.getExternalStorageDirectory():" + Environment.getExternalStorageDirectory());
		Log.d("sdCardPath:" + sdCardPath);
		className = className.toLowerCase();
		File newxmlfile = new File(xmlFolderPath + className + "_" + propId + ".xml");
		// File fileToUpload = new
		// File(Environment.getExternalStorageDirectory() + "/upload.xml");
		try
		{
			newxmlfile.createNewFile();
			// fileToUpload.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		// FileOutputStream fileos1 = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
			// fileos1 = new FileOutputStream(fileToUpload);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");
			for (Concern hs : list)
			{
				Log.d("hs:" + hs.getName() + "marked:" + hs.getMarked());
				Log.d("id:" + hs.getCode());

				serializer.startTag(null, className);
				serializer.attribute(null, "id", hs.getCode());
				if (hs.getMarked() == "")
				{
					Log.d("hs.getMarked() ==''");
					serializer.attribute(null, "marked", "0");
				}
				else if (hs.getMarked().equals(""))
				{
					Log.d("hs.getMarked() equals=''");
					serializer.attribute(null, "marked", "0");
				}
				else
				{
					Log.d("hs.getMarked(): " + hs.getMarked());
					serializer.attribute(null, "marked", hs.getMarked());
				}
				serializer.text(hs.getName());
				serializer.endTag(null, className);

			}
			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file has been created on SD card");
		}
		catch (Exception e)
		{
			Log.e("Exception error occurred while creating xml file:" + e);
		}
	}

	public static List<Concern> loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException
	{
		Log.d("inloadXmlFromNetwork");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		List<Concern> entries = null;
		String title = null;
		String url = null;
		String summary = null;
		Calendar rightNow = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

		StringBuilder htmlString = new StringBuilder();
		try
		{
			stream = downloadUrl(urlString);
			entries = xmlParser.parse(stream, "hs");
			Log.d("entries:" + entries);

		}
		finally
		{
			if (stream != null)
			{
				Log.d(":stream: close" + stream);
				stream.close();
				Log.d(":stream: closed" + stream);
			}
		}
		Log.d("in return gen");
		for (Concern entry : entries)
		{
			Log.d("id:" + entry.getCode());
			Log.d("name:" + entry.getName());
			Log.d("marked:" + entry.getMarked());
			Log.d("chestie:" + entry);
		}
		Log.d("entries:" + entries);
		Log.d("entries size:" + entries.size());
		return entries;
	}

	public static List<Tasks> loadTaskXmlFromSdCard(String folderPath, String xml) throws XmlPullParserException, IOException
	{
		Log.d("loadTaskXmlFromSdCard");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		List<Tasks> entries = null;
		File fileXml = new File(folderPath + xml + ".xml");
		if (fileXml.exists())
		{
			String title = null;
			String url = null;
			String summary = null;
			Calendar rightNow = Calendar.getInstance();
			DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

			StringBuilder htmlString = new StringBuilder();

			stream = new FileInputStream(new File(folderPath + xml + ".xml"));
			try
			{
				// stream = downloadUrl(urlString);
				entries = xmlParser.parseTasks(stream, xml);
				Log.d("entries:" + entries);

			}
			finally
			{
				if (stream != null)
				{
					Log.d(":stream: close" + stream);
					stream.close();
					Log.d(":stream: closed" + stream);
				}
			}
			Log.d("in return gen");
			for (Tasks entry : entries)
			{
				Log.d("id:" + entry.getCode());
				Log.d("name:" + entry.getName());
				Log.d("quantity:" + entry.getQuantity());
				Log.d("selected:" + entry.getSelected());
				Log.d("chestie:" + entry);
			}
		}
		return entries;
	}

	public static ArrayList<UploadType> loadUploadXmlFromSdCard(String folderPath, String xml) throws XmlPullParserException, IOException
	{
		Log.d("loadUploadXmlFromSdCard");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		ArrayList<UploadType> entries = null;
		File fileXml = new File(folderPath + xml + ".xml");
		if (fileXml.exists())
		{
			String title = null;
			String url = null;
			String summary = null;
			Calendar rightNow = Calendar.getInstance();
			DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

			StringBuilder htmlString = new StringBuilder();

			stream = new FileInputStream(new File(folderPath + "upload" + ".xml"));
			try
			{
				// stream = downloadUrl(urlString);
				entries = (ArrayList<UploadType>) xmlParser.parseUploadXml(stream);
				Log.d("entries:" + entries);

			}
			finally
			{
				if (stream != null)
				{
					Log.d(":stream: close" + stream);
					stream.close();
					Log.d(":stream: closed" + stream);
				}
			}
			Log.d("in return gen");
			for (UploadType entry : entries)
			{
				Log.d("id:" + entry.getId());
				// Log.d("name:" + entry.getName());
				Log.d("marked:" + entry.getMarked());
				Log.d("selected:" + entry.getMarked());
				Log.d("quantity:" + entry.getQuantity());
				Log.d("chestie:" + entry);
			}
		}
		return entries;
	}

	public static List<Notes> loadNotesXmlFromSdCard(String folderPath, String xml) throws XmlPullParserException, IOException
	{
		Log.d("loadNotesXmlFromSdCard");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		List<Notes> entries = null;
		File fileXml = new File(folderPath + xml + ".xml");
		if (fileXml.exists())
		{
			String title = null;
			String url = null;
			String summary = null;
			Calendar rightNow = Calendar.getInstance();
			DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

			StringBuilder htmlString = new StringBuilder();

			stream = new FileInputStream(new File(folderPath + "notes" + ".xml"));
			try
			{
				// stream = downloadUrl(urlString);
				entries = xmlParser.parseNotesXml(stream);
				Log.d("entries:" + entries);

			}
			finally
			{
				if (stream != null)
				{
					Log.d(":stream: close" + stream);
					stream.close();
					Log.d(":stream: closed" + stream);
				}
			}
			Log.d("in return gen");
			for (Notes entry : entries)
			{
				Log.d("getEmpId:" + entry.getEmpId());
				Log.d("getPropId:" + entry.getPropId());
				Log.d("getNote:" + entry.getNote());
				Log.d("chestie:" + entry);
			}
		}
		return entries;
	}

	public static List<PhotoType> loadPhotosXmlFromSdCard(String folderPath, String xml) throws XmlPullParserException, IOException
	{
		Log.d("loadPhotosXmlFromSdCard");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		List<PhotoType> entries = null;
		File fileXml = new File(folderPath + xml + ".xml");
		if (fileXml.exists())
		{
			stream = new FileInputStream(new File(folderPath + xml + ".xml"));
			try
			{
				// stream = downloadUrl(urlString);
				entries = xmlParser.parsePhotosXml(stream);
				Log.d("entries:" + entries);

			}
			finally
			{
				if (stream != null)
				{
					Log.d(":stream: close" + stream);
					stream.close();
					Log.d(":stream: closed" + stream);
				}
			}
			Log.d("in return gen");
			for (PhotoType entry : entries)
			{
				Log.d("id:" + entry.getId());
				Log.d("getPath:" + entry.getPath());
				Log.d("getTakenFor:" + entry.getTakenFor());
				Log.d("getTakenFrom:" + entry.getTakenFrom());
				Log.d("getEmpId:" + entry.getEmpId());
				Log.d("chestie:" + entry);
			}

		}
		return entries;
	}

	public static List<Concern> loadXmlFromSdCard(String folderPath, String xml) throws XmlPullParserException, IOException
	{
		Log.d("loadXmlFromSdCard");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		List<Concern> entries = null;
		File fileXml = new File(folderPath + xml + ".xml");
		if (fileXml.exists())
		{
			String title = null;
			String url = null;
			String summary = null;
			Calendar rightNow = Calendar.getInstance();
			DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

			StringBuilder htmlString = new StringBuilder();

			stream = new FileInputStream(new File(folderPath + xml + ".xml"));
			try
			{
				// stream = downloadUrl(urlString);
				entries = xmlParser.parse(stream, xml);
				Log.d("entries:" + entries);

			}
			finally
			{
				if (stream != null)
				{
					Log.d(":stream: close" + stream);
					stream.close();
					Log.d(":stream: closed" + stream);
				}
			}
			Log.d("in return gen");
			for (Concern entry : entries)
			{
				Log.d("id:" + entry.getCode());
				Log.d("name:" + entry.getName());
				Log.d("marked:" + entry.getMarked());
				Log.d("chestie:" + entry);
			}
		}
		return entries;
	}

	public static AppParameters loadParametersFromSdCard(String folderPath, String xml) throws XmlPullParserException, IOException
	{
		Log.d("loadParametersFromSdCard");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		AppParameters entries = null;
		File fileXml = new File(folderPath + xml + ".xml");
		if (fileXml.exists())
		{
			String title = null;
			String url = null;
			String summary = null;
			Calendar rightNow = Calendar.getInstance();
			DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

			StringBuilder htmlString = new StringBuilder();

			try
			{
				// stream = downloadUrl(urlString);
				stream = new FileInputStream(new File(folderPath + xml + ".xml"));
				entries = xmlParser.parseParameters(stream);
				Log.d("entries:" + entries);

			}
			catch (Exception e)
			{
				Log.d("exception loading file:" + e);
			}
			finally
			{
				if (stream != null)
				{
					Log.d(":stream: close" + stream);
					stream.close();
					Log.d(":stream: closed" + stream);
				}
			}
		}
		// Log.d("AppParameters getPropId" + entries.getEmpId());
		// Log.d("AppParameters getPropId" + entries.getPropId());
		return entries;
	}

	public static class DownloadUpdate extends AsyncTask<String, Void, Boolean>
	{
		Context context;
		String file;
		String uniqId;

		private DownloadUpdate(Context context, String file, String uniqId)
		{
			this.context = context.getApplicationContext();
			this.file = file;
			this.uniqId = uniqId;
		}

		@Override
		protected Boolean doInBackground(String... urls)
		{
			Log.d("urls:" + urls);
			Log.d("urls[0]:" + urls[0]);
			Log.d("las index of /" + urls[0].lastIndexOf("/"));
			Log.d("las index of : " + urls[0].substring(urls[0].lastIndexOf("/") + 1));
			// String nameFile=urls[0].lastIndexOf("/");
			String file = urls[0].substring(urls[0].lastIndexOf("/") + 1);
			this.file = file;
			Boolean ret = Util.saveUpdateFile(urls[0]);
			return ret;
		}

		@Override
		protected void onPostExecute(Boolean result)
		{
			Log.d("show rez update:" + result);
			Log.d("file e :" + file);
			String folder = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Download" + File.separator;
			Log.d("folder:" + folder);
			if (result)
			{
				Log.d("result e treu a intrat:");

				Intent intent1 = new Intent(Intent.ACTION_VIEW);
				String fileUpdate = "OpPortal.apk";

				// try
				// {
				// Runtime.getRuntime().exec(new String[]
				// {
				// "su", "-c",
				// "pm install -r /storage/emulated/0/Download/OpPortal.apk"
				// });
				// }
				// catch (IOException e)
				// {
				// Log.d("no root update error:"+e.toString());
				// System.out.println(e.toString());
				// System.out.println("no root");
				// }
				intent1.setDataAndType(Uri.fromFile(new File(folder + fileUpdate)), "application/vnd.android.package-archive");
				intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent1);

				String regId = "1";
				RequestParams paramsRegister = new RequestParams();
				try
				{
					if (checkParametersExists(context))
					{
						AppParameters appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
						regId = appParameters.getRegId();
					}
				}
				catch (XmlPullParserException e1)
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				catch (IOException e1)
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				PackageInfo pInfo = null;
				String version = "0";
				try
				{
					pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
					version = pInfo.versionName;
					Log.d("version in try:" + version);
				}
				catch (NameNotFoundException e1)
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				String imei = mngr.getDeviceId();
				paramsRegister.put("activity", "ack_update");
				paramsRegister.put("updated", "yes");
				paramsRegister.put("uniqId", uniqId);
				paramsRegister.put("regId", regId);
				paramsRegister.put("imei", imei);
				paramsRegister.put("version", version);

				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("AsyncHttpClient ack_update op success!!!!:" + response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("AsyncHttpClient ack_update op onFailure e !!!!:" + e);
						Log.d("AsyncHttpClient ack_update op onFailure response!!!!:" + response);
					}
				});
			}
		}
	}

	public static void doUpdate(String update, Context context, String uniqId)
	{
		Log.d("get update:" + update);
		Log.d("get update:" + uniqId);

		new Util.DownloadUpdate(context, update, uniqId).execute(update);
	}

	public static void getPhotos(String img, String type, String uniqId)
	{
		Log.d("get photos:" + type);
		Log.d("get uniqId:" + uniqId);
		new DownloadTask().execute(img, type, uniqId);
	}

	public static Boolean saveUpdateFile(String update)
	{
		URL url;
		Log.d("saveUpdateFile " + update);
		String subfolder = "";
		Boolean ret = false;

		Log.d("update:" + "http://pill.pilon.co.uk/updates/" + update);
		try
		{
			// url = new URL("http://pill.pilon.co.uk/updates/" + update);
			url = new URL(update);

			InputStream input = url.openStream();
			final int aReasonableSize = 80000;
			try
			{
				// The sdcard directory e.g. '/sdcard' can be used directly, or
				// more safely abstracted with getExternalStorageDirectory()
				String folder = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Download" + File.separator;
				Log.d("saved in:" + folder);
				OutputStream output = new FileOutputStream(new File(folder, "OpPortal.apk"));
				try
				{
					byte[] buffer = new byte[aReasonableSize];
					int bytesRead = 0;
					while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0)
					{
						output.write(buffer, 0, bytesRead);
					}
					ret = true;
				}
				catch (Exception e)
				{
					Log.d("Exception1:" + e);
				}
				finally
				{
					output.close();
				}
			}
			catch (IOException e)
			{
				Log.d("Exception2 saveUpdateFile:" + e);
			}
			finally
			{
				input.close();
			}
			input.close();
		}
		catch (MalformedURLException e)
		{
			Log.d("MalformedURLException:" + e);
			e.printStackTrace();
		}
		catch (IOException e)
		{
			Log.d("IOException:" + e);
			e.printStackTrace();
		}
		Log.d("saved:" + ret);
		return ret;
	}

	public static Boolean savePhotos(String img, String type, String uniqId)
	{
		URL url;
		Log.d("savePhotos " + img);
		Log.d("savePhotos " + type);
		Log.d("savePhotos " + uniqId);
		String subfolder = "";
		Boolean ret = false;
		if (type.toLowerCase().equalsIgnoreCase("thumb"))
			subfolder = "thumb/";
		Log.d("IMAGE:" + "http://pill.pilon.co.uk/img/SMPimg/" + subfolder + img);
		try
		{
			url = new URL(imgPillServer + subfolder + img);

			InputStream input = url.openStream();
			final int aReasonableSize = 80000;
			try
			{
				Log.d("url:" + url);
				File imagine = new File(xmlFolderPath + "Images/" + type + "/" + img);
				if (!imagine.exists())
				{
					Log.d("imagine not exists");
					OutputStream output = new FileOutputStream(new File(xmlFolderPath + "Images/" + type + "/", img));
					try
					{
						byte[] buffer = new byte[aReasonableSize];
						int bytesRead = 0;
						while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0)
						{
							output.write(buffer, 0, bytesRead);
						}
						ret = true;
					}
					catch (Exception e)
					{
						Log.d("Exception1:" + e);
					}
					finally
					{
						output.close();
					}
				}
			}
			catch (IOException e)
			{
				Log.d("Exception2 savePhotos:" + e);
			}
			finally
			{
				input.close();
			}
			input.close();
		}
		catch (MalformedURLException e)
		{
			Log.d("MalformedURLException:" + e);
			e.printStackTrace();
		}
		catch (IOException e)
		{
			Log.d("IOException:" + e);
			e.printStackTrace();
		}
		Log.d("saved:" + ret);
		/*
		 * RequestParams paramsRegister = new RequestParams();
		 * paramsRegister.put("activity", "ack_photos_saved");
		 * paramsRegister.put("uniqId", uniqId); paramsRegister.put("photo",
		 * img); AsyncHttpClient client = new AsyncHttpClient(); client.post(op,
		 * paramsRegister, new AsyncHttpResponseHandler() {
		 * 
		 * @Override public void onSuccess(String response) {
		 * Log.d("AsyncHttpClient ack_photos_saved op success!!!!:" + response);
		 * }
		 * 
		 * @Override public void onFailure(Throwable e, String response) {
		 * Log.d("AsyncHttpClient ack_photos_saved op onFailure e !!!!:" + e);
		 * Log.d("AsyncHttpClient ack_photos_saved op onFailure response!!!!:" +
		 * response); } });
		 */
		return ret;
	}

	public static List<Property> loadPropertiesXmlFromSdCard(Context context, String folderPath, String xml) throws XmlPullParserException, IOException
	{
		Log.d("loadPropertiesXmlFromSdCard");
		File xmlFile = new File(folderPath + xml + ".xml");
		List<Property> entries = null;
		if (xmlFile.exists())
		{
			InputStream stream = null;
			XmlParser xmlParser = new XmlParser();

			String title = null;
			String url = null;
			String summary = null;
			Calendar rightNow = Calendar.getInstance();
			DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");
			StringBuilder htmlString = new StringBuilder();
			try
			{
				Log.d("parse:" + folderPath + xml + ".xml");
				stream = new FileInputStream(new File(folderPath + xml + ".xml"));
				Log.d("stream:" + stream);
				Log.d("stream:" + stream.toString().length());
				// if (xml == "propertiesCurrDetails")
				// {
				// entries = xmlParser.parseCurrProperty(stream, xml);
				// }
				// else
				// {
				entries = xmlParser.parseProperty(stream);
				// }
				Log.d("entries:" + entries);
			}
			catch (Exception e)
			{
				Log.d("Exception la stream:" + e);
				Intent tasksIntent = new Intent(context, EndProgram.class);
				// tasksIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				tasksIntent.putExtra("moveback", "moveback");
				tasksIntent.putExtra("end_program", "end_program");
				context.startActivity(tasksIntent);
			}
			finally
			{
				if (stream != null)
				{
					stream.close();
					Log.d(":stream: closed" + stream);
				}
			}
			Log.d("in return gen");
			for (Property entry : entries)
			{
				Log.d("Property entry id:" + entry.getId());
				Log.d("Property entry name:" + entry.getName());
				Log.d("Property entry time:" + entry.getTime());
				Log.d("Property entry getTasks:" + entry.getTasks());
				for (Tasks taskProperty : entry.getTasks())
				{
					Log.d("Property entry getTasks name:" + taskProperty.getName());
				}
				Log.d("Property entry chestie:" + entry);
			}
		}
		return entries;
	}

	private List<Tasks> loadXmlFromNetworkTasks(String urlString) throws XmlPullParserException, IOException
	{
		Log.d("loadXmlFromNetworkTasks");
		InputStream stream = null;
		XmlParser xmlParser = new XmlParser();
		List<Tasks> entries = null;

		String title = null;
		String url = null;
		String summary = null;
		Calendar rightNow = Calendar.getInstance();
		DateFormat formatter = new SimpleDateFormat("MMM dd h:mmaa");

		StringBuilder htmlString = new StringBuilder();
		try
		{
			stream = downloadUrl(urlString);
			entries = xmlParser.parseTasks(stream, "tasks");
			Log.d("entries:" + entries);

		}
		finally
		{
			if (stream != null)
			{
				Log.d(":stream: close" + stream);
				stream.close();
				Log.d(":stream: closed" + stream);
			}
		}
		Log.d("in return gen");
		for (Tasks entry : entries)
		{
			Log.d("id:" + entry.getCode());
			Log.d("name:" + entry.getName());
			Log.d("sel:" + entry.getSelected());
			Log.d("quantity:" + entry.getQuantity());
			Log.d("chestie:" + entry);
		}
		return entries;
	}

	// Given a string representation of a URL, sets up a connection and gets
	// an input stream.
	private static InputStream downloadUrl(String urlString) throws IOException
	{
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setReadTimeout(10000 /* milliseconds */);
		conn.setConnectTimeout(15000 /* milliseconds */);
		conn.setRequestMethod("GET");
		conn.setDoInput(true);
		conn.setUseCaches(false);
		conn.addRequestProperty("Cache-Control", "no-cache");

		// Starts the query
		conn.connect();
		InputStream stream = conn.getInputStream();
		return stream;
	}

	public class DownloadXmlTask extends AsyncTask<String, Void, List<Concern>>
	{

		@Override
		protected void onPostExecute(List<Concern> result)
		{
			// setContentView(R.layout.list_hs);
			// Displays the HTML string in the UI via a WebView
			ListToggleAdapter dataAdapter = null;
			// dataAdapter = new ListToggleAdapter(this, R.layout.list_hs,
			// listItems);
			// setListAdapter(dataAdapter);
			// myWebView.loadData(result, "text/html", null);
		}

		@Override
		protected List<Concern> doInBackground(String... urls)
		{
			try
			{
				Log.d(" in doInBackground");
				return loadXmlFromNetwork(urls[0]);
			}
			catch (IOException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			catch (XmlPullParserException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			return null;
		}
	}

	public class DownloadXmlTasks extends AsyncTask<String, Void, List<Tasks>>
	{

		@Override
		protected List<Tasks> doInBackground(String... urls)
		{
			try
			{
				Log.d(" in doInBackground");
				return loadXmlFromNetworkTasks(urls[0]);
			}
			catch (IOException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			catch (XmlPullParserException e)
			{
				Log.d("1:error" + e);
				// return "error" + e;
			}
			return null;
		}
	}

	public static void savePhotoToXml(String imgPath, String empId, String propId, String takenFrom, String takenFor)
	{
		Log.d("savePhotoToXml");

		Log.d("imgPath:" + imgPath + "empId" + empId + "propId" + propId + "takenFrom:" + takenFrom + "takenFor:" + takenFor);
		try
		{
			if (!(new File(xmlFolderPath + File.separator + "photos.xml").exists()))
				Util.createPhotosXml();
			List<PhotoType> resultSdCard = loadPhotosXmlFromSdCard(xmlFolderPath, "photos");
			// if (className.equals("task"))
			PhotoType uploadAppend = new PhotoType("id", imgPath, empId, propId, takenFrom, takenFor);

			resultSdCard.add(uploadAppend);
			Util.savePhotosListToXml(resultSdCard, "0");
		}
		catch (XmlPullParserException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	public static void saveToUploadXml(String empId, String propId, String className, String id, String name, String marked, String quantity, String reasonForAbsence, String photo, String ew)
	{
		Log.d("saveToUploadXml");
		Log.d("ew:" + ew);
		// createUploadXml("xml");
		try
		{
			File upload = new File(xmlFolderPath + File.separator + "upload.xml");
			if (!upload.exists())
			{
				Log.d("task stuff create upload xml:");
				Util.createUploadXml();
			}
			else
			{
				Log.d("upload len:" + upload.length());
				if (upload.length() == 0)
				{
					Log.d("task stuff create upload xml ca exista sie gol:");
					Util.createUploadXml();
				}
			}

			List<UploadType> resultSdCard = loadUploadXmlFromSdCard(xmlFolderPath, "upload");
			Log.d("resultSdCard:" + resultSdCard);
			// if (className.equals("task"))
			Log.d("task stuff poza in savetoupload:" + photo);
			Log.d("task id matter id trimis:" + id);
			int location = 0;

			// for (UploadType uploadEntry : resultSdCard)
			// {
			//
			// String idUpload = uploadEntry.getId();
			// Log.d("task id matter id in upload:" + idUpload);
			// if (idUpload.equals(id))
			// {
			// UploadType oldUpload = resultSdCard.get(location);
			// Log.d("task id matter mai exista");
			// resultSdCard.remove(oldUpload);
			// }
			// location++;
			// }
			UploadType uploadAppend = new UploadType(id, marked, className, quantity, empId, propId, reasonForAbsence, photo, ew);
			resultSdCard.add(uploadAppend);
			for (int j = 0; j < resultSdCard.size() - 1; j++)
			{
				UploadType currElem = resultSdCard.get(j);
				Log.d("task stuff currElem.getId():" + currElem.getId());
				Log.d("task stuff lastVal.getId():" + uploadAppend.getId());
				Log.d("task stuff currElem.getTag():" + currElem.getTag());
				Log.d("task stuff lastVal.getTag():" + uploadAppend.getTag());
				Log.d("task stuff ---------------------------------------");
				if (currElem.getId().equals(uploadAppend.getId()) && currElem.getTag().equals(uploadAppend.getTag()) && currElem.getPropId().equals(uploadAppend.getPropId()))
				{
					Log.d("task stuff egal val:" + currElem.getId());
					Log.d("task stuff egal val:" + currElem.getId());
					resultSdCard.remove(currElem);
				}
				// lastVal=resultSdCard.get(j);
			}
			// resultSdCard.add(uploadAppend);
			Iterator<UploadType> i = resultSdCard.iterator();

			// for (Iterator<UploadType> iter = resultSdCard.iterator();
			// iter.hasNext();)
			// {
			// UploadType value = iter.next();
			// Log.d("task stuff val:" + value);
			// Log.d("task stuff val:" + value.getId());
			// Log.d("task stuff id:" + id);
			// Log.d("task stuff getClass:" + value.getTag());
			// Log.d("task stuff className:" + className);
			// Log.d("task stuff resultSdCard.size():" + resultSdCard.size());
			// if (resultSdCard.size() > 1)
			// {
			// if (value.getId().equals(id) && value.getTag().equals(className))
			// {
			// Log.d("task stuff egal val:" + value.getId());
			// Log.d("task stuff egal id:" + id);
			// Log.d("task stuff egal getClass:" + value.getClass());
			// Log.d("task stuff egal className:" + className);
			// //resultSdCard.remove(value);
			// iter.remove();
			// }
			// }
			// }

			// // add elements to al, including duplicates
			// LinkedHashSet<UploadType> lhs = new LinkedHashSet<UploadType>();
			// lhs.addAll(resultSdCard);
			// resultSdCard.clear();
			// resultSdCard.addAll(lhs);
			// UploadType lastVal=new UploadType("0", "0", "0", "0", "0", "0",
			// "0", "0", "0");
			// for (int j=0;j<resultSdCard.size()-1;j++)
			// {
			// UploadType currElem=resultSdCard.get(j);
			// Log.d("task stuff currElem.getId():"+currElem.getId());
			// Log.d("task stuff lastVal.getId():"+lastVal.getId());
			// Log.d("task stuff currElem.getTag():"+currElem.getTag());
			// Log.d("task stuff lastVal.getTag():"+lastVal.getTag());
			// Log.d("task stuff ---------------------------------------");
			// if (currElem.getId().equals(lastVal.getId()) &&
			// currElem.getTag().equals(lastVal.getTag()))
			// {
			// Log.d("task stuff egal val:" + currElem.getId());
			// Log.d("task stuff egal val:" + currElem.getId());
			// resultSdCard.remove(j);
			// }
			// lastVal=resultSdCard.get(j);
			// }
			// while (i.hasNext())
			// {
			// UploadType value = i.next();
			// Log.d("task stuff val:" + value);
			// Log.d("task stuff val:" + value.getId());
			// Log.d("task stuff id:" + id);
			// Log.d("task stuff getClass:" + value.getTag());
			// Log.d("task stuff className:" + className);
			// Log.d("task stuff resultSdCard.size():" + resultSdCard.size());
			// if (resultSdCard.size() > 1)
			// {
			// if (value.getId().equals(id) && value.getTag().equals(className))
			// {
			// Log.d("task stuff egal val:" + value.getId());
			// Log.d("task stuff egal id:" + id);
			// Log.d("task stuff egal getClass:" + value.getTag());
			// Log.d("task stuff egal className:" + className);
			// i.remove();
			// }
			// }
			// }
			Util.saveUploadListToXml(resultSdCard, className, "0");
			File file = new File(CommonUtilities.xmlFolderPath + "upload.xml");
			Date lastModified = new Date(file.lastModified());
			String date = android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss", lastModified).toString();
			// SimpleDateFormat sdf = new
			// SimpleDateFormat("yyyy-MM-dd HH:mm:ss", lastModified);
			Log.d("lastModified 1:" + date);
			Log.d("lastModified:" + lastModified);
		}
		catch (XmlPullParserException e1)
		{
			// TODO Auto-generated catch block
			Log.d("saveToUploadXml XmlPullParserException " + e1);
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			Log.d("saveToUploadXml IOException " + e1);
			e1.printStackTrace();
		}
	}

	public static void saveToNotesXml(String empId, String propId, String note)
	{
		Log.d("saveToUploadXml");
		// createUploadXml("xml");
		try
		{
			if (!(new File(xmlFolderPath + File.separator + "notes.xml")).exists())
				// Util.createUploadXml();
				Util.createNotesXml();

			List<Notes> resultSdCard = loadNotesXmlFromSdCard(xmlFolderPath, "notes");
			Log.d("resultSdCard:" + resultSdCard);
			// if (className.equals("task"))
			Notes notesAppend = new Notes(empId, propId, note);
			resultSdCard.add(notesAppend);
			Util.saveNotesToXml(resultSdCard, "0");
		}
		catch (XmlPullParserException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	private static void savePhotosListToXml(List<PhotoType> list, String uniqId)
	{
		Log.d("savePhotosListToXml");
		for (PhotoType task : list)
		{
			Log.d("savePhoto:" + "path:" + task.getPath() + "empId:" + task.getEmpId() + "propId:" + task.getPropId() + "takenFrom:" + task.getTakenFrom() + "takenFor:" + task.getTakenFor());

		}

		File newxmlfile = new File(xmlFolderPath + "photos" + ".xml");
		try
		{
			newxmlfile.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");
			Log.d("tasklist size:" + list.size());
			for (PhotoType task : list)
			{
				Log.d("savePhoto:" + "path" + task.getPath() + "empId" + task.getEmpId() + "propId" + task.getPropId() + "takenFrom" + task.getTakenFrom() + "takenFor" + task.getTakenFor());

				serializer.startTag(null, "item");
				serializer.attribute(null, "id", "id");
				serializer.attribute(null, "tag", "photos");
				serializer.attribute(null, "path", task.getPath());
				serializer.attribute(null, "empId", task.getEmpId());
				serializer.attribute(null, "propId", task.getPropId());
				serializer.attribute(null, "takenFrom", task.getTakenFrom());
				serializer.attribute(null, "takenFor", task.getTakenFor());
				serializer.endTag(null, "item");

			}
			if (uniqId != "0")
			{
				serializer.startTag(null, "uniqId");
				serializer.attribute(null, "id", uniqId);
				serializer.endTag(null, "uniqId");
			}
			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file has been created on SD card");
		}
		catch (Exception e)
		{
			Log.e("Exception error occurred while creating xml file:" + e);
		}

	}

	private static void saveUploadListToXml(List<UploadType> list, String className, String uniqId)
	{
		Log.d("saveUploadListToXml");
		for (UploadType task : list)
		{
			Log.d("task:" + task.getName() + "quantity:" + task.getQuantity() + "marked:" + task.getMarked() + "id:" + task.getId() + "empID:" + task.getEmpId());

		}
		// className = className.toLowerCase();
		File newxmlfile = new File(xmlFolderPath + "upload" + ".xml");
		Log.d("upload xml: " + newxmlfile);
		Log.d("upload xml: " + newxmlfile.exists());
		try
		{
			newxmlfile.createNewFile();
		}
		catch (IOException e)
		{
			Log.d("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
		}
		catch (FileNotFoundException e)
		{
			Log.d("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");
			Log.d("tasklist size:" + list.size());
			for (UploadType task : list)
			{
				Log.d("taskul de salvat in save upload:" + task.getName() + "id:" + task.getId() + "marked:" + task.getMarked() + "tag:" + task.getTag() + " empId:" + task.getEmpId() + " propId:" + task.getPropId() + "quantity:" + task.getQuantity() + "reasonForAbsence "
						+ task.getReasonForAbsence() + "photo:" + task.getPhoto() + "ew:" + task.getEw());
				Log.d("id:" + task.getId());
				Log.d("ew:" + task.getEw());
				serializer.startTag(null, "item");
				if (task.getId() != null)
					serializer.attribute(null, "id", task.getId());
				else
					serializer.attribute(null, "id", "-1");
				if (task.getMarked() != null)
					serializer.attribute(null, "marked", task.getMarked());
				else
					serializer.attribute(null, "marked", "-1");
				if (task.getTag() != null)
					serializer.attribute(null, "tag", task.getTag());
				else
					serializer.attribute(null, "tag", "-1");
				if (task.getEmpId() != null)
					serializer.attribute(null, "empId", task.getEmpId());
				else
					serializer.attribute(null, "empId", "-1");
				if (task.getPropId() != null)
					serializer.attribute(null, "propId", task.getPropId());
				else
					serializer.attribute(null, "propId", "-1");
				if (task.getQuantity() != null)
					serializer.attribute(null, "quantity", task.getQuantity());
				else
					serializer.attribute(null, "quantity", "-1");
				if (task.getReasonForAbsence() != null)
					serializer.attribute(null, "reasonForAbsence", task.getReasonForAbsence());
				else
					serializer.attribute(null, "reasonForAbsence", "-1");
				if (task.getPhoto() != null)
					serializer.attribute(null, "photo", task.getPhoto());
				else
					serializer.attribute(null, "photo", "-1");
				if (task.getEw() != null)
					serializer.attribute(null, "ew", task.getEw());
				else
					serializer.attribute(null, "ew", "-1");
				serializer.endTag(null, "item");
			}
			if (uniqId != "0")
			{
				serializer.startTag(null, "uniqId");
				serializer.attribute(null, "id", uniqId);
				serializer.endTag(null, "uniqId");
			}
			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file upload xml has been created on SD card");
		}
		catch (Exception e)
		{
			Log.d("Exception error occurred while creating upload xml file:" + e);
		}

	}

	private static void saveNotesToXml(List<Notes> list, String uniqId)
	{
		Log.d("saveNotesToXml");
		for (Notes task : list)
		{
			Log.d("empID:" + task.getEmpId() + " propID:" + task.getPropId() + " notes:" + task.getNote());

		}
		File newxmlfile = new File(xmlFolderPath + "notes" + ".xml");
		try
		{
			newxmlfile.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");
			Log.d("tasklist size:" + list.size());
			for (Notes task : list)
			{
				Log.d("notes de salvat empID:" + task.getEmpId() + " propID:" + task.getPropId() + " notes:" + task.getNote());

				serializer.startTag(null, "item");
				serializer.attribute(null, "empId", task.getEmpId());
				serializer.attribute(null, "propId", task.getPropId());
				serializer.attribute(null, "note", task.getNote());
				serializer.endTag(null, "item");

			}
			if (uniqId != "0")
			{
				serializer.startTag(null, "uniqId");
				serializer.attribute(null, "id", uniqId);
				serializer.endTag(null, "uniqId");
			}
			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file has been created on SD card");
		}
		catch (Exception e)
		{
			Log.e("Exception error occurred while creating notes xml file:" + e);
		}

	}

	public static void saveNotesXml(String note, String empId, String propId)
	{
		Log.d("saveNotesXml");

		// className = className.toLowerCase();
		File newxmlfile = new File(xmlFolderPath + "notes" + ".xml");
		try
		{
			newxmlfile.createNewFile();
		}
		catch (IOException e)
		{
			Log.e("IOException exception in createNewFile() method:" + e);
		}
		// we have to bind the new file with a FileOutputStream
		FileOutputStream fileos = null;
		try
		{
			fileos = new FileOutputStream(newxmlfile);
		}
		catch (FileNotFoundException e)
		{
			Log.e("FileNotFoundException can't create FileOutputStream:" + e);
		}
		// we create a XmlSerializer in order to write xml data
		XmlSerializer serializer = Xml.newSerializer();
		try
		{
			// we set the FileOutputStream as output for the serializer, using
			// UTF-8 encoding
			serializer.setOutput(fileos, "UTF-8");
			// Write <?xml declaration with encoding (if encoding not null) and
			// standalone flag (if standalone not null)
			serializer.startDocument(null, Boolean.valueOf(true));
			// set indentation option
			serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
			// start a tag called "root"
			serializer.startTag(null, "xml");
			serializer.startTag(null, "item");
			serializer.attribute(null, "empId", empId);
			serializer.attribute(null, "propId", propId);
			serializer.attribute(null, "note", note);
			serializer.endTag(null, "item");
			// if (uniqId!="0")
			// {
			// serializer.startTag(null, "uniqId");
			// serializer.attribute(null, "id", uniqId);
			// serializer.endTag(null, "uniqId");
			// }
			serializer.endTag(null, "xml");
			serializer.endDocument();
			// write xml data into the FileOutputStream
			serializer.flush();
			// finally we close the file stream
			fileos.close();

			Log.d("file has been created on SD card");
		}
		catch (Exception e)
		{
			Log.e("Exception error occurred while creating xml file:" + e);
		}
	}

	public static void uploadFile(String sourceFileUri, Context context, String uniqId)
	{
		Log.d("in upload file din util");
		String fileName = sourceFileUri;
		// idpropr.empId.
		HttpURLConnection conn = null;
		DataOutputStream dos = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		int bytesRead, bytesAvailable, bufferSize;
		byte[] buffer;
		int maxBufferSize = 1 * 1024 * 1024;
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inSampleSize = 2;
		Log.d("1" + new Date());
		Bitmap b = BitmapFactory.decodeFile(sourceFileUri, o);
		if (b != null)
		{
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			int w = b.getWidth();
			int h = b.getHeight();

			b.compress(Bitmap.CompressFormat.JPEG, 100, stream);

			InputStream is = new ByteArrayInputStream(stream.toByteArray());
			Log.d("is:" + is);
			File sourceFile = new File(sourceFileUri);
			Log.d("" + new Date());
			AppParameters appParameters;
			try
			{
				InputStream fileInputStream = null;
				if (checkParametersExists(context))
				{
					appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
					URL url;
					fileInputStream = is;// new FileInputStream(sourceFile);

					Log.d("fileInputStream:" + fileInputStream);
					Log.d("6" + new Date());
					url = new URL(CommonUtilities.uploadFilesServer);
					// if (serverResponseCode != 200)
					// {
					// open a URL connection to the Servlet

					// Open a HTTP connection to the URL
					conn = (HttpURLConnection) url.openConnection();
					conn.setDoInput(true); // Allow Inputs
					conn.setDoOutput(true); // Allow Outputs
					conn.setUseCaches(false); // Don't use a Cached Copy
					conn.setRequestMethod("POST");
					conn.setRequestProperty("Connection", "Keep-Alive");
					conn.setRequestProperty("ENCTYPE", "multipart/form-data");
					conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
					conn.setRequestProperty("uploaded_file", fileName);
					dos = new DataOutputStream(conn.getOutputStream());
					dos.writeBytes(twoHyphens + boundary + lineEnd);
					dos.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\"" + fileName + "\"" + lineEnd);
					dos.writeBytes(lineEnd);
					// create a buffer of maximum size
					bytesAvailable = fileInputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					buffer = new byte[bufferSize];
					// read file and write it into form...
					bytesRead = fileInputStream.read(buffer, 0, bufferSize);
					while (bytesRead > 0)
					{
						dos.write(buffer, 0, bufferSize);
						bytesAvailable = fileInputStream.available();
						bufferSize = Math.min(bytesAvailable, maxBufferSize);
						bytesRead = fileInputStream.read(buffer, 0, bufferSize);
					}
					// send multipart form data necesssary after file data...
					dos.writeBytes(lineEnd);
					dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

					// Responses from the server (code and message)
					int serverResponseCode = conn.getResponseCode();
					String serverResponseMessage = conn.getResponseMessage();

					Log.d("HTTP Response is : " + serverResponseMessage + ": " + serverResponseCode);

					if (serverResponseCode == 200)
					{
						RequestParams paramsRegister = new RequestParams();
						paramsRegister.put("activity", "uploaded_file");
						TelephonyManager mngr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
						String imei = mngr.getDeviceId();
						paramsRegister.put("imei", imei);
						paramsRegister.put("gcm_regId", appParameters.getRegId());
						paramsRegister.put("empId", appParameters.getEmpId());
						paramsRegister.put("file", sourceFileUri);
						paramsRegister.put("uniqId", uniqId);
						AsyncHttpClient client = new AsyncHttpClient();
						client.post(op, paramsRegister, new AsyncHttpResponseHandler()
						{
							@Override
							public void onSuccess(String response)
							{
								Log.d("AsyncHttpClient password success!!!!:" + response);
								// processResponse(response);
							}

							@Override
							public void onFailure(Throwable e, String response)
							{
								Log.d("AsyncHttpClient password onFailure e !!!!:" + e);
								Log.d("AsyncHttpClient password onFailure response!!!!:" + response);
							}
						});
					}

					fileInputStream.close();
					dos.flush();
					dos.close();
				}
			}
			catch (MalformedURLException ex)
			{

				// dialog.dismiss();
				ex.printStackTrace();

				Log.d("MalformedURLException Exception : check script url.");

				Log.d("Upload file to server error: " + ex);
			}
			catch (Exception e)
			{

				Log.d("Got Exception : see logcat: ");

				// return 2;
				Log.d("Upload file to server Exception : " + e);
			}
			finally
			{
				if (conn != null)
				{
					Log.d("finaly: ");
					conn.disconnect();
				}

			}
		}
		// dialog.dismiss();
		// return serverResponseCode;

	}

	public static void DeleteRecursive(File fileOrDirectory, String uniqId)
	{
		Log.d("in deleterecursive");
		if (fileOrDirectory.exists())
		{
			if (fileOrDirectory.isDirectory())
				for (File child : fileOrDirectory.listFiles())
					DeleteRecursive(child, uniqId);
			if (fileOrDirectory.delete())
			{
				Log.d("fileOrDirectory:" + fileOrDirectory.getName());
				RequestParams paramsRegister = new RequestParams();
				paramsRegister.put("activity", "ack_delete");
				paramsRegister.put("fileOrDirectory", fileOrDirectory.getName());
				paramsRegister.put("uniqId", uniqId);

				AsyncHttpClient client = new AsyncHttpClient();
				client.post(op, paramsRegister, new AsyncHttpResponseHandler()
				{
					@Override
					public void onSuccess(String response)
					{
						Log.d("AsyncHttpClient ack_delete op success!!!!:" + response);
						// processResponse(response);
					}

					@Override
					public void onFailure(Throwable e, String response)
					{
						Log.d("AsyncHttpClient ack_delete op onFailure e !!!!:" + e);
						Log.d("AsyncHttpClient ack_delete op onFailure response!!!!:" + response);
					}
				});
			}
		}
	}

	public static void DeleteXmls(File fileOrDirectory, String uniqId)
	{
		Log.d("in DeleteXmls");
		if (fileOrDirectory.isDirectory())
		{
			for (File child : fileOrDirectory.listFiles())
			{
				Log.d("child:" + child.getName());
				Log.d("child:" + child.isFile());
				if (child.isFile())
				{
					Log.d("child e fisier :" + child.getName());
					if (child.delete())
					{
						RequestParams paramsRegister = new RequestParams();
						paramsRegister.put("activity", "ack_delete_xmls");
						paramsRegister.put("fileOrDirectory", fileOrDirectory.getName());
						paramsRegister.put("uniqId", uniqId);

						AsyncHttpClient client = new AsyncHttpClient();
						client.post(op, paramsRegister, new AsyncHttpResponseHandler()
						{
							@Override
							public void onSuccess(String response)
							{
								Log.d("AsyncHttpClient ack_delete_xmls op success!!!!:" + response);
								// processResponse(response);
							}

							@Override
							public void onFailure(Throwable e, String response)
							{
								Log.d("AsyncHttpClient ack_delete_xmls op onFailure e !!!!:" + e);
								Log.d("AsyncHttpClient ack_delete_xmls op onFailure response!!!!:" + response);
							}
						});
					}
				}

			}
		}
	}

	public static void DeleteXmlsLessParams(File fileOrDirectory, String uniqId)
	{
		Log.d("in DeleteXmlslessparams");
		if (fileOrDirectory.isDirectory())
		{
			for (File child : fileOrDirectory.listFiles())
			{
				Log.d("child:" + child.getName());
				Log.d("child:" + child.isFile());
				if (child.isFile() && !child.getName().equals("parameters.xml"))
				{
					Log.d("child e fisier :" + child.getName());
					if (child.delete())
					{
						RequestParams paramsRegister = new RequestParams();
						paramsRegister.put("activity", "ack_delete_xmls_less_params");
						paramsRegister.put("fileOrDirectory", fileOrDirectory.getName());
						paramsRegister.put("uniqId", uniqId);

						AsyncHttpClient client = new AsyncHttpClient();
						client.post(op, paramsRegister, new AsyncHttpResponseHandler()
						{
							@Override
							public void onSuccess(String response)
							{
								Log.d("AsyncHttpClient ack_delete_xmls_less_params op success!!!!:" + response);
								// processResponse(response);
							}

							@Override
							public void onFailure(Throwable e, String response)
							{
								Log.d("AsyncHttpClient ack_delete_xmls_less_params op onFailure e !!!!:" + e);
								Log.d("AsyncHttpClient ack_delete_xmls_less_params op onFailure response!!!!:" + response);
							}
						});
					}
				}

			}
		}
	}

	public static void DeleteXmlsId(File fileOrDirectory, String id, String uniqId)
	{
		Log.d("in DeleteXmls");
		if (fileOrDirectory.isDirectory())
		{
			for (File child : fileOrDirectory.listFiles())
			{
				Log.d("child:" + child.getName());
				Log.d("child:" + child.isFile());
				if (child.isFile())
				{
					Log.d("child e fisier :" + child.getName());
					if (child.getName().contains(id))
					{
						Log.d("child:" + child.getName());
						if (child.delete())
						{
							RequestParams paramsRegister = new RequestParams();
							paramsRegister.put("activity", "ack_delete_xmls_id");
							paramsRegister.put("fileOrDirectory", fileOrDirectory.getName());
							paramsRegister.put("uniqId", uniqId);

							AsyncHttpClient client = new AsyncHttpClient();
							client.post(op, paramsRegister, new AsyncHttpResponseHandler()
							{
								@Override
								public void onSuccess(String response)
								{
									Log.d("AsyncHttpClient ack_delete_xmls_id op success!!!!:" + response);
									// processResponse(response);
								}

								@Override
								public void onFailure(Throwable e, String response)
								{
									Log.d("AsyncHttpClient ack_delete_xmls_id op onFailure e !!!!:" + e);
									Log.d("AsyncHttpClient ack_delete_xmls_id op onFailure response!!!!:" + response);
								}
							});
						}
					}
				}

			}
		}
	}

	public static void DeletePhotoId(String id, String uniqId)
	{
		Log.d("in DeletePhotoId");
		File thumbs = new File(CommonUtilities.xmlFolderPath + "Images" + File.separator + "Thumb" + File.separator);
		File full = new File(CommonUtilities.xmlFolderPath + "Images" + File.separator + "FullSize" + File.separator);

		if (thumbs.isDirectory())
		{
			for (File child : thumbs.listFiles())
			{
				Log.d("child:" + child.getName());
				Log.d("child:" + child.isFile());
				if (child.isFile())
				{
					Log.d("child e fisier :" + child.getName());
					if (child.getName().contains(id))
					{
						Log.d("child contine:" + child.getName());
						if (child.delete())
						{
							RequestParams paramsRegister = new RequestParams();
							paramsRegister.put("activity", "ack_delete_photo_id");
							paramsRegister.put("thumbs", "thumbs");
							paramsRegister.put("uniqId", uniqId);

							AsyncHttpClient client = new AsyncHttpClient();
							client.post(op, paramsRegister, new AsyncHttpResponseHandler()
							{
								@Override
								public void onSuccess(String response)
								{
									Log.d("AsyncHttpClient ack_delete_photo_id  thumbs op success!!!!:" + response);
									// processResponse(response);
								}

								@Override
								public void onFailure(Throwable e, String response)
								{
									Log.d("AsyncHttpClient ack_delete_photo_id thumbs op onFailure e !!!!:" + e);
									Log.d("AsyncHttpClient ack_delete_photo_id op onFailure response!!!!:" + response);
								}
							});
						}
					}
				}

			}
		}
		if (full.isDirectory())
		{
			for (File child : full.listFiles())
			{
				Log.d("child:" + child.getName());
				Log.d("child:" + child.isFile());
				if (child.isFile())
				{
					Log.d("child e fisier :" + child.getName());
					if (child.getName().contains(id))
					{
						Log.d("child contine:" + child.getName());
						if (child.delete())
						{
							RequestParams paramsRegister = new RequestParams();
							paramsRegister.put("activity", "ack_delete_photo_id");
							paramsRegister.put("full", "full");
							paramsRegister.put("uniqId", uniqId);

							AsyncHttpClient client = new AsyncHttpClient();
							client.post(op, paramsRegister, new AsyncHttpResponseHandler()
							{
								@Override
								public void onSuccess(String response)
								{
									Log.d("AsyncHttpClient ack_delete_photo_id full op success!!!!:" + response);
									// processResponse(response);
								}

								@Override
								public void onFailure(Throwable e, String response)
								{
									Log.d("AsyncHttpClient ack_delete_photo_id full op onFailure e !!!!:" + e);
									Log.d("AsyncHttpClient ack_delete_photo_id op onFailure response!!!!:" + response);
								}
							});
						}
					}
				}

			}
		}
	}

	public static boolean checkFiles(String uniqId, Context context)
	{
		Log.d("in checkFiles");
		File xmlF = new File(xmlFolderPath);
		String uniqIdSent = "0";
		if (!uniqId.equals(null))
			uniqIdSent = uniqId;
		// Boolean returnValue=true;
		if (xmlF.exists())
		{
			for (File child : xmlF.listFiles())
			{
				if (child.isFile())
				{
					if (!child.getName().equals("parameters.xml"))
					{
						Date lastModified = new Date(new File(xmlFolderPath + File.separator + child.getName()).lastModified());
						Date now = new Date();
						SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd");
						ft.format(lastModified);
						if (ft.format(now).compareTo(ft.format(lastModified)) != 0)
						{
							Log.d("fisier vechi:" + child.getName());
							Log.d("uniqId:" + uniqId);
							// returnValue=false;
							return false;
							// Util.DeleteRecursive(new File(xmlFolderPath +
							// File.separator + child.getName()), uniqIdSent);

						}
					}
				}
			}
		}
		return true;
		// Util.getXmls(context, "0", uniqIdSent);

	}

	public static void checkAndReplaceFiles(String uniqId, Context context)
	{
		Log.d("in checkAndReplaceFiles");
		File xmlF = new File(xmlFolderPath);
		String uniqIdSent = "0";
		if (!uniqId.equals(null))
			uniqIdSent = uniqId;
		if (xmlF.exists())
		{
			for (File child : xmlF.listFiles())
			{
				if (child.isFile())
				{
					if (!child.getName().equals("parameters.xml"))
					{
						Date lastModified = new Date(new File(xmlFolderPath + File.separator + child.getName()).lastModified());
						Date now = new Date();
						SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd");
						ft.format(lastModified);
						if (ft.format(now).compareTo(ft.format(lastModified)) != 0)
						{
							Log.d("fisier vechi:" + child.getName());
							Log.d("uniqId:" + uniqId);
							Util.DeleteRecursive(new File(xmlFolderPath + File.separator + child.getName()), uniqIdSent);

						}
					}
				}
			}
		}
		ConnectionDetector cd = new ConnectionDetector(context);
		if (cd.isConnectingToInternet())
			Util.getXmls(context, "0", uniqIdSent);

	}
}
