package com.workers.util;

import java.io.InputStream;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Movie;
import android.util.AttributeSet;
import android.view.View;

import com.workers.R;

public class GifView extends View
{

	private Movie mMovie;
	private long movieStart;

	public GifView(Context context)
	{
		super(context);
		initializeView();
	}

	public GifView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initializeView();
	}

	public GifView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initializeView();
	}

	private int gifId;

	public void setGIFResource(int resId)
	{
		this.gifId = resId;
		initializeView();
	}

	public int getGIFResource()
	{
		return this.gifId;
	}

	// private void initializeView() {
	//
	// InputStream is = getContext().getResources().openRawResource(gifId);
	// movie = Movie.decodeStream(is);
	//
	// }
	// }
	//
	private void initializeView()
	{
		if (gifId != 0)
		{
			InputStream is = getContext().getResources().openRawResource(R.drawable.ajax_loader);
			mMovie = Movie.decodeStream(is);
			movieStart = 0;
			this.invalidate();
		}
	}

	protected void onDraw(Canvas canvas)
	{
		canvas.drawColor(Color.TRANSPARENT);
		super.onDraw(canvas);
		long now = android.os.SystemClock.uptimeMillis();

		if (movieStart == 0)
		{
			movieStart = (int) now;
		}
		if (mMovie != null)
		{
			int relTime = (int) ((now - movieStart) % mMovie.duration());
			mMovie.setTime(relTime);
			mMovie.draw(canvas, getWidth() - mMovie.width(), getHeight() - mMovie.height());
			this.invalidate();
		}
	}
}