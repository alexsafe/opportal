/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.workers.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

import com.workers.types.AppParameters;
import com.workers.types.Concern;
import com.workers.types.Notes;
import com.workers.types.PhotoType;
import com.workers.types.Property;
import com.workers.types.Response;
import com.workers.types.Tasks;
import com.workers.types.UploadType;

/**
 * This class parses XML feeds from stackoverflow.com. Given an InputStream
 * representation of a feed, it returns a List of entries, where each list
 * element represents a single entry (post) in the XML feed.
 */
public class XmlParser
{
	private static final String ns = null;

	// We don't use namespaces

	public Response parseResponse(InputStream in) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readRegister(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}
	}

	public AppParameters parseParameters(InputStream in) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readParameters(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}
	}

	public List<UploadType> parseUploadXml(InputStream in) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			
			parser.nextTag();
			return readUploadXml(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}
	}
	public List<Notes> parseNotesXml(InputStream in) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readNotesXml(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}
	}

	public List<PhotoType> parsePhotosXml(InputStream in) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readPhotosXml(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}
	}

	private List<Notes> readNotesXml(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<Notes> ConcernList = new ArrayList<Notes>();
		int eventType = parser.getEventType();
		Log.d("in readLists:" + parser.toString());
		
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<Notes>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);
				
				if (name.equals("item"))
				{
					String empIdUpload = parser.getAttributeValue(null, "empId");
					String propIdUpload = parser.getAttributeValue(null, "propId");
					String noteUpload = parser.getAttributeValue(null, "note");
					
					Notes currentConcern = new Notes( empIdUpload, propIdUpload, noteUpload);
					ConcernList.add(currentConcern);
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return ConcernList;
		
	}
	private List<UploadType> readUploadXml(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<UploadType> ConcernList = new ArrayList<UploadType>();
		int eventType = parser.getEventType();
		Log.d("in readLists:" + parser.toString());

		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<UploadType>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);

				if (name.equals("item"))
				{
					String idUpload = parser.getAttributeValue(null, "id");
					//String nameUpload = parser.getAttributeValue(null, "name");
					String markedUpload = parser.getAttributeValue(null, "marked");
					String tagUpload = parser.getAttributeValue(null, "tag");
					String quantityUpload = parser.getAttributeValue(null, "quantity");
					String empIdUpload = parser.getAttributeValue(null, "empId");
					String propIdUpload = parser.getAttributeValue(null, "propId");
					String reasonUpload = parser.getAttributeValue(null, "reasonForAbsence");
					String photoUpload = parser.getAttributeValue(null, "photo");
					String ewUpload = parser.getAttributeValue(null, "ew");

					Log.d("id:" + idUpload);
					//Log.d("id:" + nameUpload);
					Log.d("id:" + markedUpload);
					Log.d("id:" + tagUpload);
					Log.d("id:" + quantityUpload);
					Log.d("id:" + photoUpload);

					UploadType currentConcern = new UploadType(idUpload,  markedUpload, tagUpload, quantityUpload, empIdUpload, propIdUpload, reasonUpload,photoUpload,ewUpload);
					ConcernList.add(currentConcern);
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return ConcernList;

	}

	private List<PhotoType> readPhotosXml(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<PhotoType> ConcernList = new ArrayList<PhotoType>();
		int eventType = parser.getEventType();
		Log.d("in readLists:" + parser.toString());

		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<PhotoType>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);

				if (name.equals("item"))
				{
					Log.d("next");

					String idPhotoType = parser.getAttributeValue(null, "id");
					String pathPhotoType = parser.getAttributeValue(null, "path");
					String takenFromPhotoType = parser.getAttributeValue(null, "takenFrom");
					String tagPhotoType = parser.getAttributeValue(null, "tag");
					String takenForPhotoType = parser.getAttributeValue(null, "takenFor");
					String empIdPhotoType = parser.getAttributeValue(null, "empId");
					String propIdPhotoType = parser.getAttributeValue(null, "propId");

					Log.d("id:" + idPhotoType);
					Log.d("id:" + pathPhotoType);
					Log.d("id:" + takenFromPhotoType);
					Log.d("id:" + tagPhotoType);
					Log.d("id:" + propIdPhotoType);

					PhotoType currentConcern = new PhotoType(idPhotoType, pathPhotoType, empIdPhotoType, propIdPhotoType, takenFromPhotoType, takenForPhotoType);
					ConcernList.add(currentConcern);
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return ConcernList;

	}

	public List<Property> parseProperty(InputStream in) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readPropertiesDetail(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}
		
	}
	public List<Property> parseCurrProperty(InputStream in, String xml) throws XmlPullParserException, IOException
	{
		try
		{
			Log.d("parseCurrProperty:"+xml);
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			//parser.nextTag();
			return readCurrPropertiesDetail(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}

	}

	public List<Concern> parse(InputStream in, String parseObject) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readLists(parser, parseObject);
		}
		finally
		{
			in.close();
		}

	}

	public List<Tasks> parseTasks(InputStream in, String parseObject) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readTasks(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}

	}

	public List<Tasks> parseUp(InputStream in, String parseObject) throws XmlPullParserException, IOException
	{
		try
		{
			XmlPullParser parser = Xml.newPullParser();
			parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
			parser.setInput(in, null);
			parser.nextTag();
			return readTasks(parser);
			// return readFeed(parser);
		}
		finally
		{
			in.close();
		}

	}

	private List<Property> readPropertiesDetail(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<Property> PropertyList = new ArrayList<Property>();
		ArrayList<Tasks> TaskList = new ArrayList<Tasks>();
		ArrayList<PhotoType> photos = new ArrayList<PhotoType>();
		Property currentProperty = null;
		int eventType = parser.getEventType();
		int ends;
		Log.d("in readPropertiesDetail:" + parser.toString());
		Log.d("in eventType:" + eventType);
		String PropertyId = null, PropertyAddress = null, PropertyTime = null, PropertyNotes = null;
		// Concern currentConcern = null;
		int i=0;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			i++;
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				PropertyList = new ArrayList<Property>();
				TaskList = new ArrayList<Tasks>();
				photos = new ArrayList<PhotoType>();
				Log.d("START_DOCUMENT");
				break;
			case XmlPullParser.START_TAG:

				name = parser.getName();
				Log.d("name START_TAG:" + name);
				
				if (name.equals("property"))
				{
					Log.d("next property");
					
					PropertyId = parser.getAttributeValue(null, "id");
					PropertyAddress = parser.getAttributeValue(null, "address");
					PropertyTime = parser.getAttributeValue(null, "time");
					
					Log.d("id:" + PropertyId);
					Log.d("address:" + PropertyAddress);
					Log.d("time:" + PropertyTime);
					
				}
				if (name.equals("task"))
				{
					Log.d("next task");
					
					String taskId = parser.getAttributeValue(null, "id");
					String taskName = parser.nextText();
					
					Log.d("taskId:" + taskId);
					Log.d("taskName:" + taskName);
					
//					parser.nextTag();
					Tasks currentPropTask = new Tasks(taskId, taskName);
					TaskList.add(currentPropTask);
				}
				if (name.equals("notes"))
				{
					Log.d("next notes");
					PropertyNotes = parser.nextText();
					Log.d("PropertyNotes:" + PropertyNotes);
					
				}
				if (name.equals("photo"))
				{
					Log.d("next photo");
					String path = parser.nextText();
					PhotoType currentPhoto = new PhotoType("", path, "", "", "", "");
					photos.add(currentPhoto);
				}
				currentProperty = new Property(PropertyId, "name", PropertyAddress, PropertyTime, TaskList, photos, PropertyNotes);
				Log.d("current property time:" + PropertyTime);
				
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				ends=0;
				if (name.equalsIgnoreCase("property") && currentProperty != null)
				{
					ends++;
					Log.d("if  END_TAG:");
					PropertyList.add(currentProperty);
					TaskList = new ArrayList<Tasks>();
					photos = new ArrayList<PhotoType>();
//					return PropertyList;
				}
				
			}
//			PropertyList.add(currentProperty);
			eventType = parser.next();
			
		}
		Log.d("iterations:"+i);
//		for (Property entry : PropertyList)
//		{
//			Log.d("PropertyList entry id:" + entry.getId());
//			Log.d("PropertyList entry name:" + entry.getName());
//			Log.d("PropertyList entry time:" + entry.getTime());
//		}
//		PropertyList.add(currentProperty);
		return PropertyList;
	}
	private List<Property> readCurrPropertiesDetail(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<Property> PropertyList = new ArrayList<Property>();
		ArrayList<Tasks> TaskList = new ArrayList<Tasks>();
		ArrayList<PhotoType> photos = new ArrayList<PhotoType>();
		Property currentProperty = null;
		int eventType = parser.getEventType();
		int ends;
		Log.d("in readCurrPropertiesDetail:" + parser.toString());
		Log.d("in eventType:" + eventType);
		String PropertyId = null, PropertyAddress = null, PropertyTime = null, PropertyNotes = null;
		// Concern currentConcern = null;
		int i=0;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			i++;
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				PropertyList = new ArrayList<Property>();
				TaskList = new ArrayList<Tasks>();
				photos = new ArrayList<PhotoType>();
				Log.d(" readCurrPropertiesDetail START_DOCUMENT");
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("readCurrPropertiesDetail name START_TAG:" + name);

				if (name.equals("property"))
				{
					Log.d("next property");
					
					PropertyId = parser.getAttributeValue(null, "id");
					PropertyAddress = parser.getAttributeValue(null, "address");
					
					Log.d("curr id:" + PropertyId);
					Log.d("curr address:" + PropertyAddress);
					
					
				}
				if (name.equals("item"))
				{
					Log.d("next item");

					PropertyId = parser.getAttributeValue(null, "id");
					PropertyAddress = parser.getAttributeValue(null, "address");

					Log.d("curr id:" + PropertyId);
					Log.d("curr address:" + PropertyAddress);


				}
				//currentProperty = new Property(PropertyId, "name", PropertyAddress, PropertyTime, TaskList, photos, PropertyNotes);
				//Log.d("current property time:" + PropertyTime);

				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
//				ends=0;
//				if (name.equalsIgnoreCase("property") && currentProperty != null)
//				{
//					ends++;
//					 Log.d("if  END_TAG:");
//					PropertyList.add(currentProperty);
////					return PropertyList;
//				}
				
			}
//			PropertyList.add(currentProperty);
			eventType = parser.next();

		}
		Log.d("iterations:"+i);
//		for (Property entry : PropertyList)
//		{
//			Log.d("PropertyList entry id:" + entry.getId());
//			Log.d("PropertyList entry name:" + entry.getName());
//			Log.d("PropertyList entry time:" + entry.getTime());
//		}
//		PropertyList.add(currentProperty);
		return PropertyList;
	}

	private AppParameters readParameters(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		AppParameters currentResponse = null;
		int eventType = parser.getEventType();
		Log.d("in readParameters:" + parser.toString());
		Log.d("in eventType:" + eventType);
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				// ConcernList = new ArrayList<Concern>();
				Log.d("START_DOCUMENT");
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);
				Log.d("parser.getText() desupra:" + parser.getText());
				if (name.equals("item"))
				{
					Log.d("next");
					String password;
					String empId = parser.getAttributeValue(null, "empId");
					String propId = parser.getAttributeValue(null, "propId");
					String regId = parser.getAttributeValue(null, "regId");
					String empName = parser.getAttributeValue(null, "empName");
					currentResponse = new AppParameters(empId, propId,regId,empName);
				}

				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return currentResponse;
	}

	private Response readRegister(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		Response currentResponse = null;
		int eventType = parser.getEventType();
		Log.d("in readResponse:" + parser.toString());
		Log.d("in eventType:" + eventType);
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				// ConcernList = new ArrayList<Concern>();
				Log.d("START_DOCUMENT");
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);
				Log.d("parser.getText() desupra:" + parser.getText());
				if (name.equals("register"))
				{
					Log.d("next");
					String password;
					String empName="0";
					String type = parser.getAttributeValue(null, "type");
					if (type.equalsIgnoreCase("info"))
					{
						password = parser.nextText();
					}
					else
					{
						password = parser.getAttributeValue(null, "value");
						empName=parser.getAttributeValue(null, "empName");
						Log.d("in else: "+parser.getAttributeValue(null, "value")+parser.getAttributeValue(null, "empName"));
					}
					Log.d("type:" + type);
					Log.d("password:" + password);
					Log.d("empName:" + empName);
					currentResponse = new Response(name, type, password,empName);
				}

				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return currentResponse;
	}

	private List<Concern> readLists(XmlPullParser parser, String tag) throws XmlPullParserException, IOException
	{
		ArrayList<Concern> ConcernList = new ArrayList<Concern>();
		int eventType = parser.getEventType();
		Log.d("in readLists:" + parser.toString());
		tag=tag.substring(0, tag.indexOf("_"));
		Log.d("tag:" + tag);
		
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<Concern>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);

				if (name.equals(tag))
				{
					Log.d("next");

					String idConcern = parser.getAttributeValue(null, "id");
					String markedConcern = parser.getAttributeValue(null, "marked");
					String nameConcern = parser.nextText();

					Log.d("id:" + idConcern);
					Log.d("name:" + nameConcern);
					Log.d("marked:" + markedConcern);
					// currentConcern.setId(idConcern);
					// currentConcern.setName(nameConcern);

					Concern currentConcern = new Concern(idConcern, nameConcern, markedConcern);
					ConcernList.add(currentConcern);
					// parser.nextTag();
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return ConcernList;
	}

	private List<Tasks> readTasks(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<Tasks> ConcernList = new ArrayList<Tasks>();
		int eventType = parser.getEventType();
		Log.d("in readHs:" + parser.toString());
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<Tasks>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);
				if (name.equals("task"))
				{
					Log.d("next");
					// parser.nextTag();
					String idConcern = parser.getAttributeValue(null, "id");

					String quantityConcern = parser.getAttributeValue(null, "q");
					String selectedConcern = parser.getAttributeValue(null, "marked");
					String ewConcern = parser.getAttributeValue(null, "ew");
					String nameConcern = parser.nextText();
					Log.d("id:" + idConcern);
					Log.d("name:" + nameConcern);
					Log.d("quantityConcern:" + quantityConcern);
					Log.d("marked:" + selectedConcern);
					Log.d("ew:" + ewConcern);
					// currentConcern.setId(idConcern);
					// currentConcern.setName(nameConcern);
					Tasks currentConcern = new Tasks(idConcern, nameConcern, selectedConcern, quantityConcern,ewConcern);
					ConcernList.add(currentConcern);
				}

				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
			}
			eventType = parser.next();
		}
		return ConcernList;
	}

	private List<Concern> readProperties(XmlPullParser parser) throws XmlPullParserException, IOException
	{
		ArrayList<Concern> ConcernList = new ArrayList<Concern>();
		int eventType = parser.getEventType();
		// Concern currentConcern = null;
		while (eventType != XmlPullParser.END_DOCUMENT)
		{
			String name = null;
			switch (eventType)
			{
			case XmlPullParser.START_DOCUMENT:
				ConcernList = new ArrayList<Concern>();
				break;
			case XmlPullParser.START_TAG:
				name = parser.getName();
				Log.d("name START_TAG:" + name);
				if (name == "rows")
				{
					// currentConcern = new Concern();
				}
				// else if (currentConcern != null)
				{

					if (name.equals("row"))
					{
						Log.d("next");
						parser.nextTag();
						String idConcern = parser.getAttributeValue(null, "id");
						String nameConcern = parser.getAttributeValue(null, "name");
						String markedConcern = parser.getAttributeValue(null, "marked");
						Log.d("id:" + idConcern);
						Log.d("name:" + nameConcern);
						// currentConcern.setId(idConcern);
						// currentConcern.setName(nameConcern);
						Concern currentConcern = new Concern(idConcern, nameConcern, markedConcern);
						ConcernList.add(currentConcern);
					}
				}
				break;
			case XmlPullParser.END_TAG:
				name = parser.getName();
				Log.d("name END_TAG:" + name);
				// if (name.equalsIgnoreCase("rows") && currentConcern != null)
				{
					// Log.d("if  END_TAG:");
					// ConcernList.add(currentConcern);
				}
			}
			eventType = parser.next();
		}
		return ConcernList;
	}

}
