package com.workers.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.media.AudioManager;
import android.os.Handler;
import android.view.View;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.Window;

import com.workers.activities.InitActivity;

public class HideSysUi
{
	public Window window;
	final Handler mHideHandler = new Handler();
	Context context;

	public HideSysUi(final Window window)
	{
		hideSystemUi(window);
		final Runnable mHideRunnable = new Runnable()
		{
			@Override
			public void run()
			{
				hideSystemUi(window);
			}
		};
		window.getDecorView().setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener()
		{
			@Override
			public void onSystemUiVisibilityChange(int visibility)
			{
				if (visibility == 0)
				{
					mHideHandler.postDelayed(mHideRunnable, 2000);
				}
			}
		});

	}

	public HideSysUi(final Window window, final Context context)
	{
		
		hideSystemUi(window, context);
		Log.d("in hide sys");
		new InitActivity();
		Util.DeleteRecursive(context.getCacheDir(), "default");
		Util.DeleteRecursive(context.getFilesDir(), "default");
		final Runnable mHideRunnable = new Runnable()
		{
			@Override
			public void run()
			{
				hideSystemUi(window, context);
			}
		};
		window.getDecorView().setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener()
		{
			@Override
			public void onSystemUiVisibilityChange(int visibility)
			{
				if (visibility == 0)
				{
					mHideHandler.postDelayed(mHideRunnable, 2000);
				}
			}
		});
		final AudioManager audiomanager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		// Log.d("init STREAM_NOTIFICATION:"+audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION));
		// Log.d("init getRingerMode():"+audiomanager.getRingerMode());
		final int initMode = audiomanager.getRingerMode();
		// final int
		// initVolume=audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);
		final int initVolume = audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM);
		Log.d("init getRingerMode():" + audiomanager.getRingerMode());
/*		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("initRingerMode", audiomanager.getRingerMode() + "");
		editor1.putString("initRingerVolume", audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM) + "");
		editor1.commit();

		audiomanager.setRingerMode(2);
		audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 1, 0);
		Handler handler = new Handler();
		Log.d("init after set getRingerMode():" + audiomanager.getRingerMode());
		Log.d("audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION);:" + audiomanager.getStreamVolume(AudioManager.STREAM_NOTIFICATION));
		handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				Log.d("delayed 2");
				audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 1, 0);
			}

		}, 2000);
		handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				Log.d("delayed 2");
				// audiomanager.setRingerMode(2);
				audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 2, 0);
			}

		}, 4000);
		handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				Log.d("delayed 3");// audiomanager.setRingerMode(2);
				audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 3, 0);
			}

		}, 9000);
		handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				Log.d("delayed 4");// audiomanager.setRingerMode(2);
				audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 4, 0);
			}

		}, 14000);
		// handler.postDelayed(new Runnable()
		// {
		// @Override
		// public void run()
		// {
		// Log.d("delayed 5");//audiomanager.setRingerMode(2);
		// audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 5, 0);
		// }
		//
		// }, 13000);
		// handler.postDelayed(new Runnable()
		// {
		// @Override
		// public void run()
		// {
		// Log.d("delayed 6");//audiomanager.setRingerMode(2);
		// audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 6, 0);
		// }
		//
		// }, 14000);
		// handler.postDelayed(new Runnable()
		// {
		// @Override
		// public void run()
		// {
		// Log.d("delayed 7");
		// audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 7, 0);
		// }
		//
		// }, 15000);
		handler.postDelayed(new Runnable()
		{
			@Override
			public void run()
			{
				Log.d("delayed sfarsit");
				// if (initMode==2)
				// {
				// Log.d("init mode e 2");
				// audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,
				// initVolume, 0);
				// }
				// else
				// Log.d("init mode e 0");
				SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
				String mode = sharedPreferences.getString("initRingerMode", "mode");
				String volume = sharedPreferences.getString("initRingerVolume", "volume");
				Log.d("mode:"+mode);
				Log.d("volume:"+volume);
				if (!mode.equals("mode"))
				{
					audiomanager.setRingerMode(Integer.parseInt(mode));
					audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION,Integer.parseInt(volume), 0);
				}
				//audiomanager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, initVolume, 0);
			}

		}, 17000);
 
		 */
		PackageInfo pInfo;
		try
		{
			pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			String version = pInfo.versionName;
			Log.d("version in try:" + version);
		}
		catch (NameNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void hideSystemUi(Window window2, Context context2)
	{
		Log.d("hide 2");
//		AudioManager audiomanager = (AudioManager) context2.getSystemService(Context.AUDIO_SERVICE);
//		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context2);
//		Editor editor1 = sharedPreferences.edit();
//		editor1.putString("initRingerMode", audiomanager.getRingerMode() + "");
//		editor1.putString("initRingerVolume", audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM) + "");
//		Log.d("audiomanager.getRingerMode():"+audiomanager.getRingerMode());
//		Log.d("audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM):"+audiomanager.getStreamVolume(AudioManager.STREAM_SYSTEM));
//		editor1.commit();
		window2.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
	
		
	}

	public void hideSystemUi(Window window)
	{
		window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
	}
}
