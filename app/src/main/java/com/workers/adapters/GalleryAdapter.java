package com.workers;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;

import com.workers.util.Log;
import static com.workers.util.CommonUtilities.PICTURES_DIR;
public class GalleryAdapter extends BaseAdapter
{
	private Context mContext;
	private static final String SDCARD = Environment.getExternalStorageDirectory().getPath();
	private static final String DIRECTORY = SDCARD + "/Pictures/Spike/";
	private static final String DATA_DIRECTORY = "/Pictures/Spike/";
	private static final String DATA_FILE = SDCARD + "/Pictures/Spike/imagelist.dat";
	private static final String PHOTO_ALBUM = "Spike";
	// Number of columns of Grid View
	public static final int NUM_OF_COLUMNS = 3;

	// Gridview image padding
	public static final int GRID_PADDING = 8; // in dp

	// supported file formats
	public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg", "png");
	
    //array to store bitmaps to display
    private Bitmap[] imageBitmaps;
    //placeholder bitmap for empty spaces in gallery
    Bitmap placeholder;
    private Integer[] mImageIds;

	public ArrayList<String> getFilePaths()
	{
		ArrayList<String> filePaths = new ArrayList<String>();

		File directory = new File(android.os.Environment.getExternalStorageDirectory() + File.separator + PICTURES_DIR);
		Log.d("directory:" + directory);
		// check for directory
		if (directory.isDirectory())
		{
			// getting list of file paths
			File[] listFiles = directory.listFiles();

			// Check for count
			if (listFiles.length > 0)
			{

				// loop through all files
				for (int i = 0; i < listFiles.length; i++)
				{

					// get file path
					String filePath = listFiles[i].getAbsolutePath();

					// check for supported file extension
					if (IsSupportedFile(filePath))
					{
						// Add image path to array list
						filePaths.add(filePath);
					}
				}
			}
			else
			{
				// image directory is empty
				Toast.makeText(mContext, PHOTO_ALBUM + " is empty. Please load some images in it !", Toast.LENGTH_LONG).show();
			}

		}
		else
		{
			AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
			alert.setTitle("Error!");
			alert.setMessage(PHOTO_ALBUM + " directory path is not valid! Please set the image directory name AppConstant.java class");
			alert.setPositiveButton("OK", null);
			alert.show();
		}
		return filePaths;
	}

	private boolean IsSupportedFile(String filePath)
	{
		String ext = filePath.substring((filePath.lastIndexOf(".") + 1), filePath.length());

		if (FILE_EXTN.contains(ext.toLowerCase(Locale.getDefault())))
			return true;
		else
			return false;

	}

	

	public GalleryAdapter(Context context)
	{
		mContext = context;
		
	}
	public GalleryAdapter(Context context,Integer[] mImageIds)
	{
		mContext = context;
		this.mImageIds = mImageIds;
	}

	public int getCount()
	{
		return mImageIds.length;
	}

	public Object getItem(int position)
	{
		return position;
	}

	public long getItemId(int position)
	{
		return position;
	}

	// Override this method according to your need
	public View getView(int index, View view, ViewGroup viewGroup)
	{
		Log.d("getView");
		getFilePaths();
		ImageView i = new ImageView(mContext);

		i.setImageResource(mImageIds[index]);
		i.setLayoutParams(new Gallery.LayoutParams(200, 200));

		i.setScaleType(ImageView.ScaleType.FIT_XY);

		return i;
	}
}