package com.workers.types;

public class Response
{

	public Response(String activity, String type, String value, String name)
	{
		super();
		this.activity = activity;
		this.type = type;
		this.value = value;
		this.name = name;
	}

	String activity = null;
	String type = null;
	String value = null;
	String name = null;

	public void setName(String name)
	{
		this.name = name;
	}

	public String getActivity()
	{
		return activity;
	}

	public void setActivity(String activity)
	{
		this.activity = activity;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public String getName()
	{
		// TODO Auto-generated method stub
		return name;
	}
}
