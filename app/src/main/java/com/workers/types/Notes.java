package com.workers.types;

public class Notes
{
	public Notes(String empId, String propId, String note)
	{
		super();
		this.empId = empId;
		this.propId = propId;
		this.note = note;
	}

	private String empId;
	private String propId;
	private String note;

	public String getEmpId()
	{
		return empId;
	}

	public void setEmpId(String empId)
	{
		this.empId = empId;
	}

	public String getPropId()
	{
		return propId;
	}

	public void setPropId(String propId)
	{
		this.propId = propId;
	}

	public String getNote()
	{
		return note;
	}

	public void setNote(String note)
	{
		this.note = note;
	}
}
