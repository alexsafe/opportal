package com.workers.types;

import android.os.Parcel;
import android.os.Parcelable;

public class Tasks implements Parcelable
{

	String code = null;
	String name = null;
	String quantity = null;
	String selected = null;
	String ew = null;

	public String getEw()
	{
		return ew;
	}

	public void setEw(String ew)
	{
		this.ew = ew;
	}

	public Tasks(String code, String name, String selected, String quantity, String ew)
	{
		super();
		this.code = code;
		this.name = name;
		this.selected = selected;
		this.quantity = quantity;
		this.ew = ew;
	}

	public Tasks(String code, String name)
	{
		super();
		this.code = code;
		this.name = name;
	}

	public String getQuantity()
	{
		return quantity;
	}

	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}

	public String getCode()
	{
		return code;
	}

	public void setCode(String code)
	{
		this.code = code;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String isSelected()
	{
		return selected;
	}

	public void setSelected(String selected)
	{
		this.selected = selected;
	}

	// Parcelling part
	public Tasks(Parcel in)
	{
		String[] data = new String[5];

		in.readStringArray(data);
		this.code = data[0];
		this.name = data[1];
		this.selected = data[2];
		this.quantity = data[3];
		this.ew = data[4];
	}

	public String getSelected()
	{
		return selected;
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeStringArray(new String[]
		{
				this.code, this.name, this.selected, this.quantity, this.ew
		});
	}

	public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
	{
		public Tasks createFromParcel(Parcel in)
		{
			return new Tasks(in);
		}

		public Tasks[] newArray(int size)
		{
			return new Tasks[size];
		}
	};

}
