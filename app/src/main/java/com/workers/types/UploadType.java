package com.workers.types;

import com.workers.util.Log;

public class UploadType
{
	private String empId;
	private String propId;
	private String photo;
	private String ew;
//	private String uniqId;

//	public String getUniqId()
//	{
//		return uniqId;
//	}
//
//	public void setUniqId(String uniqId)
//	{
//		this.uniqId = uniqId;
//	}

	public String getEw()
	{
		return ew;
	}

	public void setEw(String ew)
	{
		this.ew = ew;
	}

	public String getEmpId()
	{
		return empId;
	}

	public String getPhoto()
	{
		return photo;
	}

	public void setPhoto(String photo)
	{
		this.photo = photo;
	}

	public void setEmpId(String empId)
	{
		this.empId = empId;
	}

	public String getPropId()
	{
		return propId;
	}

	public void setPropId(String propId)
	{
		this.propId = propId;
	}

	public UploadType(String id, String marked, String tag, String quantity, String empId, String propId, String reasonForAbsence, String photo, String ew)//, String uniqId)
	{
		super();
		this.id = id;
		this.photo = photo;
		this.empId = empId;
		this.propId = propId;
		this.marked = marked;
		this.tag = tag;
		this.quantity = quantity;
		this.reasonForAbsence = reasonForAbsence;
		this.ew = ew;
		Log.d("upload type: "+photo+" "+empId+" "+marked+" "+tag);
//		this.uniqId = uniqId;
	}

	public String getReasonForAbsence()
	{
		return reasonForAbsence;
	}

	public void setReasonForAbsence(String reasonForAbsence)
	{
		this.reasonForAbsence = reasonForAbsence;
	}

	String id = null;
	String name = null;
	String marked = null;
	String tag = null;
	String quantity = null;
	String reasonForAbsence = null;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getMarked()
	{
		return marked;
	}

	public void setMarked(String marked)
	{
		this.marked = marked;
	}

	public String getTag()
	{
		return tag;
	}

	public void setTag(String tag)
	{
		this.tag = tag;
	}

	public String getQuantity()
	{
		return quantity;
	}

	public void setQuantity(String quantity)
	{
		this.quantity = quantity;
	}

}
