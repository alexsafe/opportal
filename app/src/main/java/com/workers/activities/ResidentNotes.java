package com.workers.activities;

import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.activities.CameraActivity;
import com.workers.R;
import com.workers.types.AppParameters;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class ResidentNotes extends Activity
{
	Button button;
	EditText edit;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resident_notes);
		new HideSysUi(getWindow());
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "notes");
		editor1.commit();
		Log.d("Enter ResidentNotes");
		button = (Button) findViewById(R.id.btnResident);
		edit = (EditText) findViewById(R.id.editTennant);
		AppParameters appParameters;
		try
		{
			if (Util.checkParametersExists(getApplicationContext()))
			{
				appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				String title = (String) getTitle();
				String name = appParameters.getEmpName();
				final Spannable spanYou = new SpannableString(title + " " + name);
				spanYou.setSpan(new ForegroundColorSpan(Color.GREEN), title.length(), spanYou.length(), 0);
				spanYou.setSpan(new RelativeSizeSpan(0.7f), title.length(), spanYou.length(), 0);
				spanYou.setSpan(new StyleSpan(Typeface.ITALIC), title.length(), spanYou.length(), 0);
				setTitle(spanYou);
			}
		}
		catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (edit != null)
		{
			edit.setHorizontallyScrolling(false);
			edit.setLines(3);
		}
		button.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				Log.d("ResidentNotes edit.getText().toString()" + edit.getText().toString());
				// Util.saveNotesXml(edit.getText().toString(), empId, propId);
				Util.saveToNotesXml(empId, propId, edit.getText().toString());
				Toast.makeText(getApplicationContext(), "Sent", Toast.LENGTH_LONG).show();
				AppParameters appParameters;
				try
				{
					if (Util.checkParametersExists(getApplicationContext()))
					{
						appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");

						String employee = appParameters.getEmpId();
						RequestParams paramsRegister = new RequestParams();
						paramsRegister.put("activity", "sentNote");
						paramsRegister.put("empId", employee);
						paramsRegister.put("message", edit.getText().toString());

						AsyncHttpClient client = new AsyncHttpClient();
						client.post(op, paramsRegister, new AsyncHttpResponseHandler()
						{
							@Override
							public void onSuccess(String response)
							{
								Log.d("AsyncHttpClient sentNote op success!!!!:" + response);
								// processResponse(response);
							}

							@Override
							public void onFailure(Throwable e, String response)
							{
								Log.d("AsyncHttpClient sentNote op onFailure e !!!!:" + e);
								Log.d("AsyncHttpClient sentNote op onFailure response!!!!:" + response);
							}
						});

					}
				}
				catch (XmlPullParserException e1)
				{
					Log.d("XmlPullParserException:" + e1);
					e1.printStackTrace();
				}
				catch (IOException e1)
				{
					Log.d("IOException:" + e1);
					e1.printStackTrace();
				}
				Intent i = new Intent(getApplicationContext(), CameraActivity.class);
				i.putExtra("cameraSource", "residentNotes");
				i.putExtra("takenFrom", "residentNotes");
				i.putExtra("tag", "residentNotes");
				i.putExtra("name", "residentNotes");
				i.putExtra("clsName", "residentNotes");
				i.putExtra("takenFor", "0");
				startActivity(i);

				// moveTaskToBack(true);
				// Log.d("cod dupa move back");
				// Intent i = new Intent(getApplicationContext(),
				// MainActivity.class);
				// startActivity(i);
				// finish();
			}
		});
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d("on pause notes");
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "notes");
		editor1.commit();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow());
	}
}
