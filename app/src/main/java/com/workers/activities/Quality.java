package com.workers.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import static com.workers.util.CommonUtilities.hs_xml_get_got;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.xmlFolderPath;
import static com.workers.util.Util.loadXmlFromSdCard;

import android.app.ListActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.workers.adapters.ListToggleAdapter;
import com.workers.activities.QualityWG;
import com.workers.R;
import com.workers.types.AppParameters;
import com.workers.types.Concern;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class Quality extends ListActivity
{
	ListView listView;
	ListToggleAdapter dataAdapter = null;
	ToggleButton tgbutton;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quality);
		new HideSysUi(getWindow());
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "quality");
		editor1.commit();
		try
		{
			if (Util.checkParametersExists(getApplicationContext()))
			{
				AppParameters appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
				propId = appParameters.getPropId();
				List<Concern> resultSdCard = loadXmlFromSdCard(xmlFolderPath, "quality_" + propId);
				if (resultSdCard != null)
				{
					String title = (String) getTitle();
					String name = appParameters.getEmpName();
					final Spannable spanYou = new SpannableString(title + " " + name);
					spanYou.setSpan(new ForegroundColorSpan(Color.GREEN), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new RelativeSizeSpan(0.7f), title.length(), spanYou.length(), 0);
					spanYou.setSpan(new StyleSpan(Typeface.ITALIC), title.length(), spanYou.length(), 0);
					setTitle(spanYou);
					// List<Concern> resultSdCard =
					// loadXmlFromSdCard(sdCardPath,"hs");
					displayListView(resultSdCard);
				}
			}
		}
		catch (XmlPullParserException e)
		{
			Log.d("XmlPullParserException:" + e);
			e.printStackTrace();
		}
		catch (IOException e)
		{
			Log.d("IOException:" + e);
			e.printStackTrace();
		}
		// displayListView();
	}

	private void displayListView(List<Concern> list)
	{
		Log.d("list:" + list);
		ArrayList<Concern> listItems = (ArrayList<Concern>) list;
		Log.d("listItems:" + listItems);

		dataAdapter = new ListToggleAdapter(this, R.layout.list_hs, listItems);

		// ListView listView = (ListView) findViewById(R.id.list_hs);
		TextView next = (TextView) findViewById(R.id.nextText);
		setListAdapter(dataAdapter);
		TextView back = (TextView) findViewById(R.id.prevText);
		back.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe back");
				Intent i = new Intent(getApplicationContext(), HS.class);
				startActivity(i);
			}
		});
		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click:");
				Boolean allAnswered = checkAnswered();
				// allAnswered=true;
				if (allAnswered)
				{
					Intent i = new Intent(getApplicationContext(), QualityWG.class);
					startActivity(i);
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public void onBackPressed()
	{
		// moveTaskToBack(true);

	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d("on pause quality");
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "quality");
		editor1.commit();
	}

	private void displayListView()
	{
		// Array list of countries
		ArrayList<Concern> TasksList = new ArrayList<Concern>();
		Concern Tasks = new Concern("0", "Labouring(strip out) works quality", true, "");
		TasksList.add(Tasks);
		Tasks = new Concern("1", "Electrical works quality", false, "");
		TasksList.add(Tasks);

		// create an ArrayAdaptar from the String Array
		dataAdapter = new ListToggleAdapter(this, R.layout.list_quality, TasksList);
		// ListView listView = (ListView) findViewById(R.id.list_quality);
		// listView.setAdapter(dataAdapter);
		TextView next = (TextView) findViewById(R.id.nextText);

		setListAdapter(dataAdapter);

		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{

				Log.d("click:");
				Boolean allAnswered = checkAnswered();
				// allAnswered=true;
				if (allAnswered)
				{
					Intent i = new Intent(getApplicationContext(), QualityWG.class);
					startActivity(i);
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
				}
			}
		});

	}

	public Boolean checkAnswered()
	{
		ListView lv = getListView();
		Log.d("lv:" + lv);
		int size = 0;
		if (getListAdapter() != null)
			size = getListAdapter().getCount();
		Log.d("click pe next");
		Log.d("size:" + size);
		Boolean allAnswered = true;
		for (int i = 0; i < size; i++)
		{
			Concern task = (Concern) lv.getItemAtPosition(i);
			Log.d("name:" + task.getName());
			Log.d("marked:" + task.getMarked());
			Log.d("item at " + i + " : " + lv.getItemAtPosition(i).getClass().getName());
			if (task.getMarked().equals("0"))
			{
				Log.d("egal ");
				allAnswered = false;
				// break;
				return false;
			}
		}
		Log.d("allAnswered:" + allAnswered);
		return allAnswered;
	}

	// public Boolean checkAnswered()
	// {
	// ListView lv = getListView();
	// Log.d("lv:" + lv);
	// int size = getListAdapter().getCount();
	// Log.d("click pe next");
	// Log.d("size:" + size);
	// Boolean allAnswered = true;
	// for (int i = 0; i < size; i++)
	// {
	// Concern task = (Concern) lv.getItemAtPosition(i);
	// Log.d("name:" + task.getName());
	// Log.d("marked:" + task.getMarked());
	// Log.d("item at " + i + " : " +
	// lv.getItemAtPosition(i).getClass().getName());
	// if (task.getMarked() == "0")
	// {
	// allAnswered = false;
	// break;
	// }
	// }
	// Log.d("allAnswered:" + allAnswered);
	// return allAnswered;
	// }

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow());
	}

	public void goToHs(View v)
	{
		Log.d("click pe prev ");
		Intent i = new Intent(getApplicationContext(), HS.class);
		i.putExtra("from", "quality");
		startActivity(i);
	}

	public void goToQualityWb(View v)
	{
		Log.d("click pe next ");
		Boolean allAnswered = checkAnswered();
		if (allAnswered)
		{
			Intent i = new Intent(getApplicationContext(), QualityWG.class);
			startActivity(i);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
		}
	}
}
