package com.workers.activities;

import static com.workers.util.CommonUtilities.op;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.R;
import com.workers.util.HideSysUi;
import com.workers.util.Log;

public class EndProgram extends Activity {
	Button okButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.end_program);
		PackageInfo pInfo1;
		String version1 = "1";
		try {
			pInfo1 = getPackageManager().getPackageInfo(getPackageName(), 0);
			version1 = pInfo1.versionName;
		} catch (NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
		String imei = mngr.getDeviceId();
		Button okButton=(Button) findViewById(R.id.okButton);
		new HideSysUi(getWindow(), getApplicationContext());
		RequestParams paramsRegister1 = new RequestParams();
		paramsRegister1.put("activity", "enter_end_program");
		paramsRegister1.put("uniqId", getIntent().getStringExtra("uniqId"));
		paramsRegister1.put("imei", imei);
		paramsRegister1.put("version", version1);
		okButton.setVisibility(View.INVISIBLE);
		// paramsRegister.put("updated", "yes");
		AsyncHttpClient client1 = new AsyncHttpClient();
		client1.post(op, paramsRegister1, new AsyncHttpResponseHandler()
		{
			@Override
			public void onSuccess(String response)
			{
				Log.d("AsyncHttpClient enter_end_program op success!!!!:" + response);
			}

			@Override
			public void onFailure(Throwable e, String response)
			{
				Log.d("AsyncHttpClient enter_end_program op onFailure e !!!!:" + e);
				Log.d("AsyncHttpClient enter_end_program op onFailure response!!!!:" + response);
			}
		});
		Handler handler = new Handler();
		Log.d("e primit end");
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "end_program");
		editor1.commit();
		Log.d("moveback:"+getIntent().getStringExtra("moveback"));
		String moveback="move";
		if (getIntent().getStringExtra("moveback") != null)
			moveback="moveback";
		if (moveback.equals("move"))
		{
			okButton.setVisibility(View.VISIBLE);
//			handler.postDelayed(new Runnable()
//			{
//				public void run()
//				{
//					moveTaskToBack(true);
//					Log.d("cod dupa move back init activ");
//					// Intent i = new Intent(getApplicationContext(),
//					// EnterProperty.class);
//					// startActivity(i);
//				}
//			}, 5000);
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow(), getApplicationContext());
	}
	
	protected void action(View v)
	{
		moveTaskToBack(true);
	}
	
	@Override
	protected void onPause()
	{
		super.onPause();
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Editor editor1 = sharedPreferences.edit();
		editor1.putString("pauseString", "end_program");
		editor1.commit();
	}
}
