package com.workers.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.R;
import com.workers.WakeLocker;
import com.workers.types.AppParameters;
import com.workers.types.PhotoType;
import com.workers.types.Property;
import com.workers.types.Response;
import com.workers.util.AlertDialogManager;
import com.workers.util.CommonUtilities;
import com.workers.util.ConnectionDetector;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;
import com.workers.util.XmlParser;

import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.workers.util.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.workers.util.CommonUtilities.EXTRA_MESSAGE;
import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.xmlFolderPath;

//import com.google.android.gcm.GCMRegistrar;

public class InitActivity extends Activity {
    public static final String PREFS_NAME = "MyPrefsFile";
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 2;
    private static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 3;
    private static final int MY_PERMISSIONS_REQUEST_GET_ACCOUNTS = 4;
    public static SharedPreferences settings;
    static SharedPreferences.Editor editor;
    private static String[] PERMISSIONS_CONTACT = {Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.GET_ACCOUNTS};
    /**
     * Receiving push messages
     */
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("receiver");
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

            /**
             * Take appropriate action on this message depending upon your app
             * requirement For now i am just displaying it on the screen
             * */

            // Showing received message
            Log.d("New Message: " + newMessage);
            // Toast.makeText(getApplicationContext(), "New Message: " +
            // newMessage, Toast.LENGTH_LONG).show();

            // Releasing wake lock
            WakeLocker.release();
        }
    };
    String imei, number, line1Number, simSerialNumber, subscriberId, uniqId;
    String regId, lastScreen = "init";
    ConnectionDetector cd;
    AsyncTask<Void, Void, Void> mRegisterTask;
    AlertDialogManager alert = new AlertDialogManager();
    Context context;
    AppParameters appParameters;
    SharedPreferences appPreferences;
    boolean isAppInstalled = false;


    private boolean checkAndRequestPermissions() {
        int permissionCamera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int permissionReadPhoneState = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int permissionStorage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionAccounts = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (permissionReadPhoneState != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (permissionStorage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionAccounts != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.GET_ACCOUNTS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.d("Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_PHONE_STATE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.GET_ACCOUNTS, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.GET_ACCOUNTS) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("camera phone storage acounts  permission granted");

                        doAppLogic();

                        // process the normal flow
                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.GET_ACCOUNTS)
                                ) {
                            showDialogOK("camera phone storage acounts  Permission required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            Toast.makeText(this, "Go to settings and enable permissions", Toast.LENGTH_LONG)
                                    .show();
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }

    /*
        @Override
        public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
            Log.d("onRequestPermissionsResult requestCode:" + requestCode);
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_CAMERA: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted, do your work....
                    } else {
                        // permission denied
                        // Disable the functionality that depends on this permission.
                    }

                }

                case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted, do your work....
                    } else {
                        // permission denied
                        // Disable the functionality that depends on this permission.
                    }

                }

                case MY_PERMISSIONS_REQUEST_READ_PHONE_STATE: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted, do your work....
                    } else {
                        // permission denied
                        // Disable the functionality that depends on this permission.
                    }
                    return;
                }

                case MY_PERMISSIONS_REQUEST_GET_ACCOUNTS: {
                    // If request is cancelled, the result arrays are empty.
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // permission was granted, do your work....
                    } else {
                        // permission denied
                        // Disable the functionality that depends on this permission.
                    }
                    return;
                }


                // other 'case' statements for other permssions
            }
        }
    */

    private void createXMLs() {
        Util.createXmlFolder(xmlFolderPath);
        Util.createXmlFolder(xmlFolderPath + "Images/");
        Util.createXmlFolder(xmlFolderPath + "Images/Thumb");
        Util.createXmlFolder(xmlFolderPath + "Images/FullSize");
        Util.createUploadXml();
    }

    private void doAppLogic() {
        Log.d("constructor init activ");
        createXMLs();

        appPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        isAppInstalled = appPreferences.getBoolean("isAppInstalled", false);
        if (isAppInstalled == false) {
            // removeShortcut();
            // addShortcut();
        }
        SharedPreferences.Editor editor = appPreferences.edit();
        editor.putBoolean("isAppInstalled", true);
        editor.commit();
        String PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Download" + File.separator;
        String fileUpdate = "OpPortal.apk";
        Log.d("path:" + PATH + fileUpdate);
        Process process;

        //install the app from downloads if found there
        /*
        try {
            process = Runtime.getRuntime().exec("su");

            DataOutputStream os = new DataOutputStream(process.getOutputStream());
            os.writeBytes("pm install /storage/emulated/0/Download/OpPortal.apk\n");
            os.flush();
            os.writeBytes("exit\n");
            os.flush();

            int waitFor = process.waitFor();
        } catch (IOException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
            Log.d("pm IOException:" + e2);
        }
        // Log.d("app cache dir:" + getCacheDir());
        // Log.d("app data dir:" + getFilesDir());
        catch (InterruptedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            Log.d("pm InterruptedException:" + e1);
        }
*/
        // Util.DeleteRecursive(getCacheDir(), uniqId);
        // if (getCacheDir().exists())
        // {
        // for (File f : getCacheDir().listFiles())
        // {
        // // perform here your operation
        // Log.d("cache file:"+f.getName());
        // }
        // }
        // if (getFilesDir().exists())
        // {
        // for (File f : getCacheDir().listFiles())
        // {
        // // perform here your operation
        // Log.d("app file:"+f.getName());
        // }
        // }
        setContentView(R.layout.init_activity);
        context = getApplicationContext();


        cd = new ConnectionDetector(getApplicationContext());
        // String myString = savedInstanceState.getString("myString");
        // Log.d("my string:"+myString);
        uniqId = "0";
        if (getIntent().getStringExtra("uniqId") != null)
            uniqId = getIntent().getStringExtra("uniqId");
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String pauseString = sharedPreferences.getString("pauseString", "pauseString");
        // String empId = sharedPreferences.getString("empId", "-1");
        Log.d("prefs 1:" + sharedPreferences);
        // Log.d("empId 1:" + empId);
        Log.d("restoredText pauseString:" + pauseString);
        if (pauseString != null) {
            Log.d("pauseString:" + pauseString);
            lastScreen = pauseString;
        }

        PackageInfo pInfo1;
        String version1 = "1";
        try {
            pInfo1 = getPackageManager().getPackageInfo(getPackageName(), 0);
            version1 = pInfo1.versionName;
        } catch (NameNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        Log.d("initactiv");
        Log.d("lastScreen" + lastScreen);
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imei = mngr.getDeviceId();
        RequestParams paramsRegister1 = new RequestParams();
        paramsRegister1.put("activity", "enter_init");
        paramsRegister1.put("uniqId", uniqId);
        paramsRegister1.put("imei", imei);
        paramsRegister1.put("version", version1);
        // paramsRegister.put("updated", "yes");
        AsyncHttpClient client1 = new AsyncHttpClient();
        client1.post(op, paramsRegister1, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                Log.d("AsyncHttpClient enter_init op success!!!!:" + response);
            }

            @Override
            public void onFailure(Throwable e, String response) {
                Log.d("AsyncHttpClient enter_init op onFailure e !!!!:" + e);
                Log.d("AsyncHttpClient enter_init op onFailure response!!!!:" + response);
            }
        });
        if (!cd.isConnectingToInternet()) {
            if (!Util.checkFiles(uniqId, context)) {
                Log.d("a gasit ceva false!!");
                Util.checkAndReplaceFiles(uniqId, context);
                if (!new File(xmlFolderPath + File.separator + "propertiesCurrDetails.xml").exists()) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                    // set title
                    alertDialogBuilder.setTitle("Internet Connection Error");
                    // set dialog message
                    alertDialogBuilder.setMessage("Please connect to working Internet connection").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, close
                            // current activity
                            InitActivity.this.finish();
                        }
                    });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                    return;
                }
            } else {
                gotoLastScreen();
            }
        } else {
            Log.d("are net deci:");
            new HideSysUi(getWindow());

            Util.createXmlFolder(xmlFolderPath);
            Util.createXmlFolder(xmlFolderPath + "Images/");
            Util.createXmlFolder(xmlFolderPath + "Images/Thumb");
            Util.createXmlFolder(xmlFolderPath + "Images/FullSize");
            Util.createUploadXml();
            File parametersFile = new File(xmlFolderPath + "parameters.xml");
//            register();
            getPhoneInfo();
            try {
                if (!(parametersFile.exists()))// ||
                // !(parameters1File.exists()))
                {
                    Util.createParametersXml("1", "1", "1", "1");
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                } else {
                    Log.d("pe else in init");
                    Date lastModified = new Date(new File(xmlFolderPath + File.separator + "parameters.xml").lastModified());
                    Date now = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat("yyyy.MM.dd");
                    ft.format(lastModified);
                    Log.d("lastmodi:" + lastModified);
                    Log.d("lastmodi:" + ft.format(lastModified));
                    Log.d("now:" + now);
                    Log.d("now:" + ft.format(now));

                    appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
                    if (ft.format(now).compareTo(ft.format(lastModified)) != 0) {
                        Log.d("in if inittt");
                        if (appParameters.getRegId().length() < 3)
                            register();
                        regId = CommonUtilities.regId;
                        Log.d("CommonUtilities.regId:" + CommonUtilities.regId);
                        Log.d("appParameters.getRegId():" + appParameters.getRegId());
                        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
                        Editor editor1 = sharedPrefs.edit();
                        editor1.putString("empId", appParameters.getEmpId());
                        editor1.commit();
                        PackageInfo pInfo;
                        String version = "1";
                        try {
                            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                            version = pInfo.versionName;
                        } catch (NameNotFoundException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                        // /--------get register xml----------//
                        RequestParams paramsRegister = new RequestParams();
                        paramsRegister.put("activity", "register");
                        paramsRegister.put("imei", imei);
                        paramsRegister.put("sim", simSerialNumber);
                        paramsRegister.put("version", version);

                        regId = appParameters.getRegId();
                        Log.d("gcm reg:" + regId);
                        paramsRegister.put("gcm_regId", regId);
                        paramsRegister.put("number", 0);
                        paramsRegister.put("empId", appParameters.getEmpId());

                        Log.d("editor reg put:" + regId);
                        AsyncHttpClient client = new AsyncHttpClient();
                        client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(String response) {
                                Log.d("AsyncHttpClient register op success!!!!:" + response);
                                processResponse(response);
                            }

                            @Override
                            public void onFailure(Throwable e, String response) {
                                if (cd.isConnectingToInternet()) {
                                    PackageInfo pInfo;
                                    String version = "1";
                                    try {
                                        pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                                        version = pInfo.versionName;
                                    } catch (NameNotFoundException e1) {
                                        // TODO Auto-generated catch block
                                        e1.printStackTrace();
                                    }
                                    RequestParams paramsRegister = new RequestParams();
                                    paramsRegister.put("activity", "register");
                                    paramsRegister.put("imei", imei);
                                    paramsRegister.put("sim", simSerialNumber);
                                    paramsRegister.put("version", version);
                                    regId = appParameters.getRegId();
                                    Log.d("gcm reg:" + regId);
                                    paramsRegister.put("gcm_regId", regId);
                                    paramsRegister.put("number", 0);
                                    paramsRegister.put("empId", appParameters.getEmpId());

                                    Log.d("editor reg put:" + regId);
                                    AsyncHttpClient client = new AsyncHttpClient();
                                    client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                                        @Override
                                        public void onSuccess(String response) {
                                            Log.d("AsyncHttpClient retry register op success!!!!:" + response);
                                            processResponse(response);
                                        }

                                        @Override
                                        public void onFailure(Throwable e, String response) {
                                            Log.d("AsyncHttpClient retry register op onFailure e !!!!:" + e);
                                            Log.d("AsyncHttpClient retry register op onFailure response!!!!:" + response);
                                        }
                                    });
                                }
                                Log.d("AsyncHttpClient register op onFailure e !!!!:" + e);
                                Log.d("AsyncHttpClient register op onFailure response!!!!:" + response);
                            }
                        });
                    } else {
                        Log.d("else else");
                        Util.getXmls(context, "0", uniqId);
                        gotoLastScreen();
                    }
                }
            } catch (XmlPullParserException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("init activ");
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkAndRequestPermissions()) {
                doAppLogic();
                // carry on the normal flow, as the case of  permissions  granted.
            }

        }

    }

    private void addShortcut() {
        // Adding shortcut for MainActivity
        // on Home screen
        Log.d("adding shortcut");
        Intent shortcutIntent = new Intent(getApplicationContext(), InitActivity.class);

        shortcutIntent.setAction(Intent.ACTION_MAIN);
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.ic_launcher));

        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
    }

    private void removeShortcut() {

        // Deleting shortcut for MainActivity
        // on Home screen
        Intent shortcutIntent = new Intent(getApplicationContext(), InitActivity.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);

        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, getString(R.string.app_name));

        addIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent, null);
    }

    private void processResponse(String response) {
        XmlParser xmlParser = new XmlParser();
        Response responseRegister = null;

        InputStream in = new ByteArrayInputStream(response.getBytes());
        try {
            Log.d("testing");
            responseRegister = xmlParser.parseResponse(in);
            Log.d("processResponse responseRegister.getActivity():" + responseRegister.getActivity());
            Log.d("processResponse responseRegister.getType():" + responseRegister.getType());
            Log.d("processResponse responseRegister.getValue():" + responseRegister.getValue());
            Log.d("processResponse responseRegister.getname():" + responseRegister.getName());
            Util.createUploadXml();
            Util.createPhotosXml();

            if (responseRegister.getValue().toLowerCase().equals("ok")) {
                empId = appParameters.getEmpId();
                propId = appParameters.getPropId();
                String empName = appParameters.getEmpName();
                File parametersFile = new File(xmlFolderPath + "parameters.xml");
                // if (!(parametersFile.exists()))// ||
                // !(parameters1File.exists()))
                {
                    Util.createParametersXml(empId, propId, regId, empName);
                }
                // propId = "16202";
                // Util.createParametersXml(empId, propId);
                String[] params =
                        {
                                "props", "hs"
                        };

                Util.getXmls(context, "0", uniqId);
                // resultSdCard.add(resultSdCard.get(0));
                int file_iterations = 1;
                List<Property> resultSdCard = null;
                if (new File(xmlFolderPath + File.separator + "propertiesCurrDetails.xml").exists()) {
                    resultSdCard = Util.loadPropertiesXmlFromSdCard(getApplicationContext(), xmlFolderPath, "propertiesCurrDetails");
                } else {
                    Log.d("nu exista deci il luam");
                    RequestParams paramsCurrProperty = new RequestParams();
                    paramsCurrProperty.put("activity", "enterProperty");
                    paramsCurrProperty.put("empId", empId);
                    // DeleteRecursive(new
                    // File(xmlFolderPath+"propertiesCurrDetails.xml"));
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.post(op, paramsCurrProperty, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String response) {
                            Log.d("AsyncHttpClient PropertyCurrDetails op success!!!!:" + response);
                            Log.d("xmlFolderPath propertiesCurrDetails:" + xmlFolderPath + "propertiesCurrDetails.xml");
                            File details = new File(xmlFolderPath + "propertiesCurrDetails.xml");
                            if (details.exists()) {
                                Log.d("delete curr details before write:");
                                details.delete();
                                // DeleteRecursive(details);
                            }
                            Util.writeFileOnSDCard(response, context, xmlFolderPath, "propertiesCurrDetails.xml", uniqId);
                            List<Property> resultSdCard;
                            RequestParams paramsRegister = new RequestParams();
                            paramsRegister.put("activity", "ack_propertiesCurrDetails");
                            paramsRegister.put("uniqId", uniqId);
                            paramsRegister.put("empId", empId);
                            // paramsRegister.put("updated", "yes");
                            AsyncHttpClient client = new AsyncHttpClient();
                            client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                                @Override
                                public void onSuccess(String response) {
                                    Log.d("AsyncHttpClient ack_propertiesCurrDetails op success!!!!:" + response);
                                }

                                @Override
                                public void onFailure(Throwable e, String response) {
                                    Log.d("AsyncHttpClient ack_propertiesCurrDetails op onFailure e !!!!:" + e);
                                    Log.d("AsyncHttpClient ack_propertiesCurrDetails op onFailure response!!!!:" + response);
                                }
                            });
                        }

                        @Override
                        public void onFailure(Throwable e, String response) {
                            Log.d("AsyncHttpClient propertiesCurrDetails op onFailure e !!!!:" + e);
                            Log.d("AsyncHttpClient propertiesCurrDetails op onFailure response!!!!:" + response);
                        }
                    });

                    Util.getXmls(context, "0", uniqId);
                }
                // Log.d("resultSdCard curr resultSdCard:"+resultSdCard.size());
                // Log.d("resultSdCard curr propId:"+resultSdCard.get(0).getId());
                if (resultSdCard != null)
                    for (final Property property : resultSdCard) {
                        for (final PhotoType photo : property.getPhotos()) {
                            final String photoPath = photo.getPath();
                            Log.d("init activity get photos");
                            Util.getPhotos(photoPath, "Thumb", "0");
                        }
                    }
                gotoLastScreen();
            } else {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        } catch (XmlPullParserException e) {
            Log.d("processResponse XmlPullParserException:" + e);
            e.printStackTrace();
        } catch (IOException e) {
            Log.d("processResponse IOException:" + e);
            e.printStackTrace();
        }

    }

    private void gotoLastScreen() {
        if (!cd.isConnectingToInternet() && !new File(xmlFolderPath + "propertiesCurrDetails.xml").exists()) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            // set title
            alertDialogBuilder.setTitle("Internet Connection Error");
            // set dialog message
            alertDialogBuilder.setMessage("Please connect to working Internet connection").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // if this button is clicked, close
                    // current activity
                    InitActivity.this.finish();
                }
            });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();
            return;
        }
        if (!lastScreen.equals("init")) {
            Log.d("lastScreen not init");
            if (lastScreen.equals("enterProperty")) {
                Log.d("lastScreen not init ci enterProperty");
                Intent i = new Intent(getApplicationContext(), EnterProperty.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else if (lastScreen.equals("availability")) {
                Log.d("lastScreen not init ci availa");
                Intent i = new Intent(getApplicationContext(), Availability.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else if (lastScreen.equals("hs")) {
                Log.d("lastScreen not init ci hs");
                Intent i = new Intent(getApplicationContext(), HS.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else if (lastScreen.equals("quality")) {
                Log.d("lastScreen not init ci quality");
                Intent i = new Intent(getApplicationContext(), Quality.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else if (lastScreen.equals("qualitywg")) {
                Log.d("lastScreen not init ci qualitywg");
                Intent i = new Intent(getApplicationContext(), QualityWG.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else if (lastScreen.equals("tasklist")) {
                Log.d("lastScreen not init ci tasklist");
                Intent i = new Intent(getApplicationContext(), TaskList.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else if (lastScreen.equals("notes")) {
                Log.d("lastScreen not init ci notes");
                Intent i = new Intent(getApplicationContext(), Goodbye.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else if (lastScreen.equals("end_program")) {
                Log.d("lastScreen not init ci end_program");
                Intent i = new Intent(getApplicationContext(), EndProgram.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else if (lastScreen.equals("propertyDetails")) {
                Log.d("lastScreen not init ci propertyDetails");
                Intent i = new Intent(getApplicationContext(), PropertyDetails.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            } else {
                Intent i = new Intent(getApplicationContext(), PropertyDetails.class);
                i.putExtra("uniqId", uniqId);
                startActivity(i);
            }
        }

    }

    private void getPhoneInfo() {
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imei = mngr.getDeviceId();
        number = mngr.getNetworkOperatorName();
        line1Number = mngr.getLine1Number();
        simSerialNumber = mngr.getSimSerialNumber();
        subscriberId = mngr.getSubscriberId();

        Log.d("mngr.imei():" + mngr.getDeviceId());
        Log.d("mngr.getLine1Number():" + mngr.getLine1Number());
        Log.d("mngr.getSimSerialNumber():" + mngr.getSimSerialNumber());
        Log.d("mngr.getSubscriberId():" + mngr.getSubscriberId());
    }

    private void register() {
        try {
            Log.d("REGISTER din init activ");
            // Make sure the device has the proper dependencies.
//			GCMRegistrar.checkDevice(this);
        } catch (Exception e) {
            Log.d("exceptie baaa:" + e);
        }
//		GCMRegistrar.checkManifest(this);
        // GCMRegistrar.unregister(this);
        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));
        regId = "";//GCMRegistrar.getRegistrationId(this);
        regId = FirebaseInstanceId.getInstance().getToken();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Log.d("token instante:" + regId);
        Log.d("token prefs:" + preferences.getString("FIREBASE_TOKEN", "none"));
        CommonUtilities.regId = regId;
        // regId:APA91bELOwjQ1lhcXpBJz-bgsKMtiHNkiJeXI98qZd-KS1GpfF8V7uLeYsVe3BkYlDY5lgeUDtGCH_HGvKXAxWK2gel5Pij5sH2z5lbCWZxaTkSHe-xL5-qHmCeAcKW8ztrH-NkHSi04tLXMVik_RRCkL9oSgG_mpA
        Log.d("regId din init:" + regId);
        // Check if regid already presents

        if (regId.equals("")) {
            Log.d("register din init e gol");
            // Registration is not present, register now with GCM
//			GCMRegistrar.register(this, SENDER_ID);
            regId = "";//GCMRegistrar.getRegistrationId(this);
            CommonUtilities.regId = regId;
            Log.d("register:" + regId);
        } else {
            Log.d("else");
            final Context context = this;
            // ServerUtilities.registerDevice(context, regId);
//			if (GCMRegistrar.isRegisteredOnServer(this))
//			{
//				// Skips registration.
//				// GCMRegistrar.unregister(this);
//				Log.d("Already registered with GCM");
//				// Toast.makeText(getApplicationContext(),
//				// "Already registered with GCM", Toast.LENGTH_LONG).show();
//			}
//			else
            {
                Log.d("GCMRegistrar not trying again...");
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                String empId1 = "";
                if (new File(xmlFolderPath + "parameters.xml").exists()) {
                    try {
                        if (Util.checkParametersExists(context)) {
                            appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
                            empId1 = appParameters.getEmpId();
                        }
                    } catch (XmlPullParserException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
                RequestParams paramsRegister = new RequestParams();
                paramsRegister.put("activity", "register");
                paramsRegister.put("imei", imei);
                paramsRegister.put("sim", simSerialNumber);
                paramsRegister.put("gcm_regId", regId);
                paramsRegister.put("number", 0);
                paramsRegister.put("empId", empId1);
                Log.d("gcm reg:" + regId);

                Log.d("editor reg put:" + regId);
                AsyncHttpClient client = new AsyncHttpClient();
                client.post(op, paramsRegister, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        Log.d("AsyncHttpClient register op success!!!!:" + response);
//						GCMRegistrar.setRegisteredOnServer(context, true);
                        processResponse(response);

                    }

                    @Override
                    public void onFailure(Throwable e, String response) {
                        Log.d("AsyncHttpClient register op onFailure e !!!!:" + e);
                        Log.d("AsyncHttpClient register op onFailure response!!!!:" + response);
                    }
                });
                // mRegisterTask = new AsyncTask<Void, Void, Void>()
                // {
                //
                // @Override
                // protected Void doInBackground(Void... params)
                // {
                // // Register on our server // On server creates a new
                // // user
                // Log.d("registerind device");
                // ServerUtilities.registerDevice(context, regId);
                // return null;
                // }
                //
                // @Override
                // protected void onPostExecute(Void result)
                // {
                // mRegisterTask = null;
                // }
                //
                // };
                // mRegisterTask.execute(null, null, null);

            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            unregisterReceiver(mHandleMessageReceiver);
//			GCMRegistrar.onDestroy(this);
        } catch (Exception e) {
            Log.d("UnRegister Receiver Error" + e);
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
