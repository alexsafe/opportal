package com.workers.activities;

import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.propId;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.IOException;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.activities.CameraActivity;
import com.workers.R;
import com.workers.adapters.TaskAdapter;
import com.workers.types.AppParameters;
import com.workers.types.Tasks;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class TaskDetail extends Activity
{

	/** Called when the activity is first created. */
	TaskAdapter dataAdapter = null;
	public String from = "";
	ArrayList<Tasks> TaskList;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Log.d("tab3");
		Intent intent = getIntent();
		new HideSysUi(getWindow());
		AppParameters appParameters;
		try
		{
			appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
			String title = (String) getTitle();
			String name1 = appParameters.getEmpName();
			final Spannable spanYou = new SpannableString(title + " " + name1);
			spanYou.setSpan(new ForegroundColorSpan(Color.GREEN), title.length(), spanYou.length(), 0);
			spanYou.setSpan(new RelativeSizeSpan(0.7f), title.length(), spanYou.length(), 0);
			spanYou.setSpan(new StyleSpan(Typeface.ITALIC), title.length(), spanYou.length(), 0);
			setTitle(spanYou);
		}
		catch (XmlPullParserException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		from = intent.getStringExtra("from");
		if (from != null)
		{
			if (!(from.equals("tasklist")))
			{
				Log.d("nu-i din tasklist");
				finish();
			}
		}
		Log.d("getApplicationContext().getClass():" + getApplicationContext().getClass().getSimpleName());
		String id = (String) getIntent().getExtras().get("id");
		Log.i("idul task" + id);
		Bundle data = getIntent().getExtras();
		TaskList = (ArrayList<Tasks>) getIntent().getSerializableExtra("TaskList");
		Log.d("TaskList in detail:" + TaskList);
		Log.d("data get:" + data);
		Log.d("from:" + intent.getStringExtra("from"));

		Log.d("position:!!!!" + intent.getStringExtra("position"));
		final Tasks tasks = (Tasks) data.getParcelable("Task");
		final String position = intent.getStringExtra("position");
		Log.d("tasks:" + tasks.getName());

		setContentView(R.layout.task_detail);
		final TextView name = (TextView) findViewById(R.id.name);
		final TextView selected = (TextView) findViewById(R.id.selected);
		final EditText quantity = (EditText) findViewById(R.id.quantity);
		Button saveValue = (Button) findViewById(R.id.saveValue);
		name.setText(tasks.getName());

		if (!(tasks.getQuantity().equals("")))
		{
			if (tasks.getQuantity().equals("0") || tasks.getQuantity().equals("1"))
				quantity.setText("");
			else
				quantity.setText(tasks.getQuantity());
		}
		else
		{
			quantity.setVisibility(View.INVISIBLE);
		}
		TextView next = (TextView) findViewById(R.id.nextText);
		TextView back = (TextView) findViewById(R.id.prevText);

		saveValue.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("save value");
				RequestParams params = new RequestParams();
				AppParameters appParameters;
				try
				{
					if (Util.checkParametersExists(getApplicationContext()))
					{
						appParameters = Util.loadParametersFromSdCard(xmlFolderPath, "parameters");
						empId = appParameters.getEmpId();
						propId = appParameters.getPropId();
					}
				}
				catch (XmlPullParserException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Log.d("tasks:" + tasks);
				Log.d("tasks q:" + tasks.getQuantity());
				Log.d("tasks s:" + tasks.getSelected());
				Log.d("q:" + quantity.getText());
				Log.d("q:" + quantity.getText().toString());
				Log.d("s:" + selected.getText());
				Log.d("position:" + position);
				String qXml = quantity.getText().toString();
				if (quantity.getText().toString().equals(""))
				{
					qXml = "1";
				}
				params.put("index", position);
				params.put("quantity", qXml);
				params.put("checked", selected.getText());

				Tasks Tasks = TaskList.get(Integer.parseInt(position));
				Tasks.setSelected("1");
				Tasks.setQuantity(qXml);
				TaskList.set(Integer.parseInt(position), Tasks);
				Util.saveTaskListToXml(TaskList, "task", propId);
				Util.saveToUploadXml(empId, propId, "tasks", Tasks.getCode(), Tasks.getName(), Tasks.getSelected(), qXml, "", "-1", Tasks.getEw());
				String className = getApplicationContext().getClass().getSimpleName();
				String itemName = tasks.getCode();
				Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
				intent.putExtra("photoPath", className + "_" + itemName);
				intent.putExtra("classInception", "TaskDetail");
				intent.putExtra("takenFrom", "TaskDetail");
				intent.putExtra("code", itemName);
				intent.putExtra("name", tasks.getName());
				intent.putExtra("marked", tasks.getSelected());
				intent.putExtra("clsName", "tasks");
				intent.putExtra("quantity", qXml);
				intent.putExtra("takenFor", itemName);
				intent.putExtra("ew",  Tasks.getEw());
				startActivity(intent);
			}
		});

		back.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe back");
				Intent i = new Intent(getApplicationContext(), TaskList.class);
				startActivity(i);
			}
		});

	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d("in resume");
		new HideSysUi(getWindow());
		Intent intent = getIntent();
		from = intent.getStringExtra("from");
		Log.d("from in resume:" + from);
		if (!(from.equals("tasklist")))
		{
			Log.d("nu-i din tasklist");
			finish();
		}
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK)
		{
			Bundle extras = data.getExtras();
			Bitmap bmp = (Bitmap) extras.get("data");
		}
	}

	public void saveValue(View button)
	{

	}

	// @Override
	// public void onBackPressed()
	// {
	// // moveTaskToBack(true);
	// Intent i = new Intent(getApplicationContext(), MainActivity.class);
	// startActivity(i);
	// }

	public void goToTasksList(View v)
	{
		Log.d("click pe prev ");
		Intent i = new Intent(getApplicationContext(), TaskList.class);
		startActivity(i);
	}

	// public void goToResidentSatisfaction(View v)
	// {
	// Log.d("click pe next goToResidentSatisfaction ");
	// Intent i = new Intent(getApplicationContext(),
	// ResidentSatisfaction.class);
	// startActivity(i);
	// }
}
