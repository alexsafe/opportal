package com.workers.activities;

import java.util.ArrayList;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.workers.activities.HS;
import com.workers.adapters.ListToggleAdapter;
import com.workers.activities.Quality;
import com.workers.R;

import com.workers.activities.TaskList;
import com.workers.types.Concern;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class QualityWb extends ListActivity
{
	ListView listView;
	ListToggleAdapter dataAdapter = null;
	ToggleButton tgbutton;

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quality_wb);
		new HideSysUi(getWindow());
		displayListView();
	}

	private void displayListView()
	{
		// Array list of countries
		ArrayList<Concern> TasksList = new ArrayList<Concern>();
		Concern Tasks = new Concern("0", "Washing machine condition", true, "");
		TasksList.add(Tasks);
		Tasks = new Concern("1", "Fridge/Freezer condition", false, "");
		TasksList.add(Tasks);
		Tasks = new Concern("2", "Cooker condition", false, "");
		TasksList.add(Tasks);
		Tasks = new Concern("3", "Oven condition", false, "");
		TasksList.add(Tasks);
		Tasks = new Concern("4", "Dishwasher condition", false, "");
		TasksList.add(Tasks);

		// create an ArrayAdaptar from the String Array
		dataAdapter = new ListToggleAdapter(this, R.layout.list_hs, TasksList);
		// ListView listView = (ListView) findViewById(R.id.list_quality);
		// listView.setAdapter(dataAdapter);
		TextView next = (TextView) findViewById(R.id.nextText);
		TextView back = (TextView) findViewById(R.id.prevText);
		setListAdapter(dataAdapter);
		back.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Log.d("click pe next");
				Intent i = new Intent(getApplicationContext(), Quality.class);
				startActivity(i);
			}
		});
		next.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{

				Log.d("click:");
				Boolean allAnswered = checkAnswered();
				allAnswered=true;
				if (allAnswered)
				{
//					Util.initTasksXml();
					try
					{
						Thread.sleep(1000);
					}
					catch (InterruptedException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
						Log.d("error on sleep:"+e);
					}
					Intent i = new Intent(getApplicationContext(), TaskList.class);
					startActivity(i);
				}
				else
				{
					Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	public Boolean checkAnswered()
	{
		ListView lv = getListView();
		Log.d("lv:" + lv);
		int size = getListAdapter().getCount();
		Log.d("click pe next");
		Log.d("size:" + size);
		Boolean allAnswered = true;
		for (int i = 0; i < size; i++)
		{
			Concern task = (Concern) lv.getItemAtPosition(i);
			Log.d("name:" + task.getName());
			Log.d("marked:" + task.getMarked());
			Log.d("item at " + i + " : " + lv.getItemAtPosition(i).getClass().getName());
			if (task.getMarked() == "")
			{
				allAnswered = false;
				break;
			}
		}
		Log.d("allAnswered:" + allAnswered);
		return allAnswered;
	}

	public void goToQuality(View v)
	{
		Log.d("click pe prev ");
		Intent i = new Intent(getApplicationContext(), HS.class);
		startActivity(i);
	}

	public void goToTasks(View v)
	{
		Boolean allAnswered = checkAnswered();
		if (allAnswered)
		{
//			Util.initTasksXml();
			Intent i = new Intent(getApplicationContext(), TaskList.class);
			startActivity(i);
		}
		else
		{
			Toast.makeText(getApplicationContext(), "Mark all items first", Toast.LENGTH_LONG).show();
		}
	}
}
