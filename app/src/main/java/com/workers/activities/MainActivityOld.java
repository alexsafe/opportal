package com.workers.activities;

import static com.workers.util.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.workers.util.CommonUtilities.EXTRA_MESSAGE;
import static com.workers.util.CommonUtilities.SENDER_ID;
import static com.workers.util.CommonUtilities.empId;
import static com.workers.util.CommonUtilities.op;
import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.workers.R;
import com.workers.types.Concern;
import com.workers.types.Response;
import com.workers.util.AlertDialogManager;
import com.workers.util.CommonUtilities;
import com.workers.util.ConnectionDetector;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;
import com.workers.util.XmlParser;

public class MainActivityOld extends Activity
{
/*
    Button buttonLogIn;
    EditText enterPwd;
    ConnectionDetector cd;
    public static String name = "name";
    public static String email = "email";
    AsyncTask<Void, Void, Void> mRegisterTask;
    AlertDialogManager alert = new AlertDialogManager();
    Context context;
    String imei, number, line1Number, simSerialNumber, subscriberId;
    String regId;
    public static final String PREFS_NAME = "MyPrefsFile";
    public static SharedPreferences settings;
    RelativeLayout mainLayout;

    List<Concern> hsXMl;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("IN MAIN!!!");
        context = getApplicationContext();
        cd = new ConnectionDetector(getApplicationContext());
        if (!cd.isConnectingToInternet())
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            // set title
            alertDialogBuilder.setTitle("Internet Connection Error");
            // set dialog message
            alertDialogBuilder.setMessage("Please connect to working Internet connection").setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener()
            {
                public void onClick(DialogInterface dialog, int id)
                {
                    // if this button is clicked, close
                    // current activity
                    MainActivityOld.this.finish();
                }
            });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();
            return;
            // Internet Connection is not present
            // alert.showAlertDialog(MainActivity.this,
            // "Internet Connection Error",
            // "Please connect to working Internet connection", false);
            // stop executing code by return
            // return;
        }
        Log.d("context.getFilesDir():" + context.getFilesDir());
        new HideSysUi(getWindow());
        register();

        Log.d("getFilesDir():" + getFilesDir());
        // Util.writeFileOnSDCard("strWrite", getApplicationContext(),
        // "fileName1.txt");

        getPhoneInfo();
        Log.d("simSerialNumber in processResponse:" + simSerialNumber);
//		Util.DeleteRecursive(new File(CommonUtilities.xmlFolderPath));
        Log.d("Enter pwd");
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        buttonLogIn = (Button) findViewById(R.id.butLogin);
        enterPwd = (EditText) findViewById(R.id.enterPwd);
        buttonLogIn.setOnClickListener(new OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                Log.d("pwd:" + enterPwd.getText().toString());
                Log.d("regId inainte de if:"+regId);
                if (regId.equals(""))
                {
                    Log.d("regId e gol");
                    regId=CommonUtilities.regId;
                    Log.d("regId dupa attr:"+regId);
                }
//				if (!(CommonUtilities.regId.equals("")) && appParameters.getRegId().equals(""))
//					regId=CommonUtilities.regId;
//				if (CommonUtilities.regId.equals("") && !(appParameters.getRegId().equals("")))
//					regId=appParameters.getRegId();
                PackageInfo pInfo;
                String version="1";
                try
                {
                    pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                    version=pInfo.versionName;
                }
                catch (NameNotFoundException e1)
                {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                RequestParams paramsRegister = new RequestParams();
                paramsRegister.put("activity", "register");
                paramsRegister.put("version", version);
                paramsRegister.put("imei", imei);
                paramsRegister.put("sim", simSerialNumber);
                paramsRegister.put("gcm_regId", regId);
                paramsRegister.put("number", 0);
                paramsRegister.put("passwd", enterPwd.getText().toString());
                AsyncHttpClient client = new AsyncHttpClient();
                client.post(op, paramsRegister, new AsyncHttpResponseHandler()
                {
                    @Override
                    public void onSuccess(String response)
                    {
                        Log.d("AsyncHttpClient password success!!!!:" + response);
                        processResponse(response);
                    }

                    @Override
                    public void onFailure(Throwable e, String response)
                    {
                        Log.d("AsyncHttpClient password onFailure e !!!!:" + e);
                        Log.d("AsyncHttpClient password onFailure response!!!!:" + response);
                    }
                });
                // Intent i = new Intent(getApplicationContext(),
                // Language.class);
                // startActivity(i);
            }
        });
        mainLayout.setOnClickListener(new OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {
                Log.d("click pe layout details");
                NotificationManager mNotificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.cancelAll();
            }
        });
    }

    private void processResponse(String respose)
    {
        Log.d("processResponse main");

        XmlParser xmlParser = new XmlParser();
        Response responseRegister = null;
        Log.d("processResponse response in parser:" + respose);
        InputStream in = new ByteArrayInputStream(respose.getBytes());
        try
        {
            responseRegister = xmlParser.parseResponse(in);
            Log.d("processResponse responseRegister.getActivity():" + responseRegister.getActivity());
            Log.d("processResponse responseRegister.getType():" + responseRegister.getType());
            Log.d("processResponse responseRegister.getValue():" + responseRegister.getValue());
            Log.d("processResponse responseRegister.getName():" + responseRegister.getName());
            if (responseRegister.getType().toString().toLowerCase().equalsIgnoreCase("id"))
            {
                File dir = new File(xmlFolderPath);
                if(!dir.exists() && !dir.isDirectory()) {
                    dir.mkdir();
                }
                empId = responseRegister.getValue();
                SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
                Editor editor1 = sharedPrefs.edit();
                editor1.putString("empId", empId);
                editor1.commit();
                String empName=responseRegister.getName();
                // propId = "16202";
                //File parametersFile = new File(xmlFolderPath + "parameters.xml");
                Log.d("empId main:" + empId);
                // if (!(parametersFile.exists()))// ||
                // !(parameters1File.exists()))
                // {
                Log.d("global reg id:"+CommonUtilities.regId);
                Util.createParametersXml(empId, "1", CommonUtilities.regId,empName);
                // }
                // else
                // {
                // Log.d(" else file exists");
                // }
                Util.createUploadXml();
                Util.createPhotosXml();
                Util.getXmls(context, "0", "0");
//				List<Property> resultSdCard = Util.loadPropertiesXmlFromSdCard(xmlFolderPath, "propertiesDetails");
//				Log.d("result:"+resultSdCard);
//				List<Property> resultSdCard = Util.loadPropertiesXmlFromSdCard(xmlFolderPath, "propertiesDetails");
//				for (final Property property : resultSdCard)
//				{
//					for (final PhotoType photo : property.getPhotos())
//					{
//						final String photoPath = photo.getPath();
//						Util.getPhotos(photoPath, "Thumb");
//						// Util.getPhotos(photoPath, "FullSize");
//					}
//				}
//				for (final Property property : resultSdCard)
//				{
//					for (final PhotoType photo : property.getPhotos())
//					{
//						final String photoPath = photo.getPath();
//						// Util.getPhotos(photoPath, "Thumb");
//						// Util.getPhotos(photoPath, "FullSize");
//					}
//				}
                // /-----------get properties details xml--------------//

                Intent i = new Intent(getApplicationContext(), Language.class);
                startActivity(i);
            }
            else
            {
                Log.d("response not ok");
                // Internet Connection is not present
                alert.showAlertDialog(this, "Password Error", responseRegister.getValue().toString(), false);
                // stop executing code by return
                // moveTaskToBack(true);
                return;
            }
        }
        catch (XmlPullParserException e)
        {
            Log.d("processResponse XmlPullParserException:" + e);
            e.printStackTrace();
        }
        catch (IOException e)
        {
            Log.d("processResponse IOException:" + e);
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.d("in resume");
        new HideSysUi(getWindow());
    }

    private void getPhoneInfo()
    {
        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        imei = mngr.getDeviceId();
        number = mngr.getNetworkOperatorName();
        line1Number = mngr.getLine1Number();
        simSerialNumber = mngr.getSimSerialNumber();
        subscriberId = mngr.getSubscriberId();
        Log.d("mngr imei:" + imei);
        Log.d("mngr.getLine1Number():" + mngr.getLine1Number());
        Log.d("mngr.getSimSerialNumber():" + mngr.getSimSerialNumber());
        Log.d("mngr.getSubscriberId():" + mngr.getSubscriberId());
    }

    private void register()
    {
        try
        {
            // Make sure the device has the proper dependencies.
            GCMRegistrar.checkDevice(this);
        }
        catch (Exception e)
        {
            Log.d("exceptie baaa:" + e);
        }
        GCMRegistrar.checkManifest(this);
        // GCMRegistrar.unregister(this);
        registerReceiver(mHandleMessageReceiver, new IntentFilter(DISPLAY_MESSAGE_ACTION));
        regId = GCMRegistrar.getRegistrationId(this);
        CommonUtilities.regId=regId;
        // regId:APA91bELOwjQ1lhcXpBJz-bgsKMtiHNkiJeXI98qZd-KS1GpfF8V7uLeYsVe3BkYlDY5lgeUDtGCH_HGvKXAxWK2gel5Pij5sH2z5lbCWZxaTkSHe-xL5-qHmCeAcKW8ztrH-NkHSi04tLXMVik_RRCkL9oSgG_mpA
        Log.d("regId:" + regId);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("regId", regId);

        // Commit the edits!
        editor.commit();
        // Check if regid already presents
        if (regId.equals(""))
        {
            Log.d("not registered");
            // Registration is not present, register now with GCM
            GCMRegistrar.register(this, SENDER_ID);
            regId = GCMRegistrar.getRegistrationId(this);
            Log.d("after registered:"+regId);
            Log.d("after registered 2:"+CommonUtilities.regId);
        }
        else
        {
            Log.d("else regid nu e gol ");
            final Context context = this;
            // ServerUtilities.registerDevice(context, regId);
            if (GCMRegistrar.isRegisteredOnServer(this))
            {
                // Skips registration.
                // GCMRegistrar.unregister(this);
                Log.d("Already registered with GCM");
                // Toast.makeText(getApplicationContext(),
                // "Already registered with GCM", Toast.LENGTH_LONG).show();
            }
            else
            {
                Log.d("GCMRegistrar not trying again...");
                // Try to register again, but not in the UI thread.
                // It's also necessary to cancel the thread onDestroy(),
                // hence the use of AsyncTask instead of a raw thread.
                Log.d("regId inainte de if:"+regId);
                if (regId.equals(""))
                {
                    Log.d("regId e gol");
                    regId=CommonUtilities.regId;
                    Log.d("regId dupa attr:"+regId);
                }
                RequestParams paramsRegister = new RequestParams();
                paramsRegister.put("activity", "register");
                paramsRegister.put("imei", imei);
                paramsRegister.put("sim", simSerialNumber);
                paramsRegister.put("gcm_regId", regId);
                paramsRegister.put("number", 0);
                paramsRegister.put("passwd", enterPwd.getText().toString());
                AsyncHttpClient client = new AsyncHttpClient();
                client.post(op, paramsRegister, new AsyncHttpResponseHandler()
                {
                    @Override
                    public void onSuccess(String response)
                    {
                        Log.d("AsyncHttpClient password success!!!!:" + response);
                        processResponse(response);
                        GCMRegistrar.setRegisteredOnServer(context, true);
                    }

                    @Override
                    public void onFailure(Throwable e, String response)
                    {
                        Log.d("AsyncHttpClient password onFailure e !!!!:" + e);
                        Log.d("AsyncHttpClient password onFailure response!!!!:" + response);
                    }
                });
//				mRegisterTask = new AsyncTask<Void, Void, Void>()
//				{
//
//					@Override
//					protected Void doInBackground(Void... params)
//					{
//						// Register on our server // On server creates a new
//						// user
//						Log.d("registerind device");
//						ServerUtilities.registerDevice(context, regId);
//						return null;
//					}
//
//					@Override
//					protected void onPostExecute(Void result)
//					{
//						mRegisterTask = null;
//					}
//
//				};
//				mRegisterTask.execute(null, null, null);

            }
        }
    }

    public class DownloadXmlTask extends AsyncTask<String, Void, List<Concern>>
    {
        @Override
        protected void onPostExecute(List<Concern> result)
        {
            Log.d("main post exec");
            Log.d("result:" + result);
            Util.saveListToXml(result, "hs");
        }

        @Override
        protected List<Concern> doInBackground(String... urls)
        {
            try
            {
                Log.d(" in doInBackground");
                return Util.loadXmlFromNetwork(urls[0]);
            }
            catch (IOException e)
            {
                Log.d("IOException:" + e);
                // return "error" + e;
            }
            catch (XmlPullParserException e)
            {
                Log.d("XmlPullParserException:" + e);
                // return "error" + e;
            }
            return null;
        }
    }


    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Log.d("receiver");
            String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            WakeLocker.acquire(getApplicationContext());

             * Take appropriate action on this message depending upon your app
             * requirement For now i am just displaying it on the screen


            // Showing received message
            Log.d("New Message: " + newMessage);
            //Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();

            // Releasing wake lock
            WakeLocker.release();
        }
    };

    @Override
    protected void onDestroy()
    {
        if (mRegisterTask != null)
        {
            mRegisterTask.cancel(true);
        }
        try
        {
            unregisterReceiver(mHandleMessageReceiver);
            GCMRegistrar.onDestroy(this);
        }
        catch (Exception e)
        {
            Log.d("UnRegister Receiver Error" + e);
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    */

}
