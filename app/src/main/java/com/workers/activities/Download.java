package com.workers.activities;

import static com.workers.util.CommonUtilities.xmlFolderPath;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;

import com.workers.types.Concern;
import com.workers.util.Log;
import com.workers.util.Util;

public class Download extends Service
{

	private final IBinder mBinder = new LocalBinder();
	Context context;

	public class LocalBinder extends Binder
	{
		Download getService()
		{
			Log.d("Service getService");
			return Download.this;
		}
	}

	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Log.d("Service onStartCommand doanload:" + START_STICKY);
		Log.d("Service onStartCommand intent:" + intent);
//		Log.d("Service onStartCommand intent.getAction():" + intent.getAction());
		File xmlsFolder = new File(xmlFolderPath);
		// Util.DeleteRecursive(xmlsFolder);
		// Util.createXmlFolder(xmlFolderPath);
		// Util.createXmlFolder(xmlFolderPath + "/Images/");
		// Util.createXmlFolder(xmlFolderPath + "/Images/Thumb");
		// Util.createXmlFolder(xmlFolderPath + "/Images/FullSize");
		// Util.createUploadXml();
		// File parametersFile = new File(xmlFolderPath + "parameters.xml");
		// if (!(parametersFile.exists()))// || !(parameters1File.exists()))
		// {
		// Util.createParametersXml("1", "1");
		// }
		//
		if (intent != null)
		{
			Log.d("intent action:"+intent.getAction());
			if (intent.getAction() != null)
			{
				Log.d("get action:" + intent.getAction());
				Log.d("get photo:" + intent.getStringExtra("photo"));
				Log.d("get update:" + intent.getStringExtra("update"));
				Log.d("get uniqId:" + intent.getStringExtra("uniqId"));
				if (intent.getAction() == "dld_photo")
				{
					if (intent.getStringExtra("photo") != null)
					{
						Util.getPhotos(intent.getStringExtra("photo"), "FullSize",intent.getStringExtra("uniqId"));
					}
				}
				if (intent.getAction() == "update")
				{
					Log.d("in update in if");
					//if (intent.getStringExtra("update") != null)
//					{
						Log.d("in update service");
						context = getApplicationContext();
						Util.doUpdate(intent.getStringExtra("update"), context,intent.getStringExtra("uniqId"));
//					}
				}
			}
			else
			{
				Log.d("get xmls from dld");
				context = getApplicationContext();
				Util.getXmls(context, "0", intent.getStringExtra("uniqId"));
			}
		}

		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		Log.d("Service onBind:" + mBinder);
		return mBinder;
	}

	public class DownloadXmlTask extends AsyncTask<String, Void, List<Concern>>
	{

		@Override
		protected void onPostExecute(List<Concern> result)
		{
			Log.d("Service result:" + result);
			Util.saveListToXml(result, "hs");
		}

		@Override
		protected List<Concern> doInBackground(String... urls)
		{
			try
			{
				Log.d("Service in doInBackground");
				return Util.loadXmlFromNetwork(urls[0]);
			}
			catch (IOException e)
			{
				Log.d("Service 1:error" + e);
				// return "error" + e;
			}
			catch (XmlPullParserException e)
			{
				Log.d("Service 1:error" + e);
				// return "error" + e;
			}
			return null;
		}
	}
}
