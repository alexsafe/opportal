package com.workers.activities;

import java.io.IOException;
import java.util.List;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.workers.R;
import com.workers.types.PhotoType;
import com.workers.types.Property;
import com.workers.util.CommonUtilities;
import com.workers.util.HideSysUi;
import com.workers.util.Log;
import com.workers.util.Util;

public class Language extends Activity
{

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.lang_select);
		new HideSysUi(getWindow());

	}

	public void GoToDetails(View v)
	{
//		Intent i = new Intent(getApplicationContext(), PropertyDetails.class);
//		startActivity(i);
		 moveTaskToBack(true);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		new HideSysUi(getWindow());
//		List<Property> resultSdCard;
//		try
//		{
//			resultSdCard = Util.loadPropertiesXmlFromSdCard(CommonUtilities.xmlFolderPath, "propertiesDetails");
//
//			Log.d("result:" + resultSdCard);
//			for (final Property property : resultSdCard)
//			{
//				for (final PhotoType photo : property.getPhotos())
//				{
//					final String photoPath = photo.getPath();
//					Util.getPhotos(photoPath, "Thumb");
//					Util.getPhotos(photoPath, "FullSize");
//				}
//			}
//		}
//		catch (XmlPullParserException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		catch (IOException e)
//		{
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}
